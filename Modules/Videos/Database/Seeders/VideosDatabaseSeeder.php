<?php

namespace Modules\Videos\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use Modules\Videos\Models\VideosSections;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class VideosDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $sections = [
            [
                'id'         => '1',
                'slug'      =>  'doctor_speak',
                'name'      =>  'Doctor Speak',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => '2',
                'slug'      =>  'patient_stories',
                'name'      =>  'Patient Stories',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
        ];

        $permissions = [
            [
                'id'         => '226',
                'title'      => 'videos_sections_access',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '227',
                'title'      => 'videos_sections_create',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '228',
                'title'      => 'videos_sections_edit',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '229',
                'title'      => 'videos_sections_show',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '230',
                'title'      => 'videos_sections_delete',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],

            [
                'id'         => '231',
                'title'      => 'videos_access',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '232',
                'title'      => 'videos_create',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '233',
                'title'      => 'videos_edit',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '234',
                'title'      => 'videos_show',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '235',
                'title'      => 'videos_delete',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
        ];

        Permission::insert($permissions);
        VideosSections::insert($sections);
    }
}
