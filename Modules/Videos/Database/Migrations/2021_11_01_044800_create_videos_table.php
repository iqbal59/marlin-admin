<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_sections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->string('banner_id')->nullable();
            $table->boolean('status')->default('1');
            $table->boolean('is_editable')->default(true);
            $table->timestamps();
        });
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('link')->nullable();
            $table->string('type')->nullable();
            $table->string('file')->nullable();
            $table->string('image')->nullable();
            $table->integer('videos_sections_id');
            $table->integer('videoable_id')->nullable();
            $table->string('videoable_type')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_sections');
        Schema::dropIfExists('videos');
    }
}
