<?php
use Modules\Videos\Http\Controllers\VideosController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('videos')->group(function() {
    Route::get('/', 'VideosController@index');
});


Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::middleware('auth')->prefix('videossections')->name('videossections.')->group(function () {
        Route::get('/videoSectionList', [VideosController::class, 'videoSectionList'])->name('videoSectionList');
        Route::get('addVideoSections', [VideosController::class, 'addVideoSections'])->name('addVideoSections');
        Route::post('saveVideoSections', [VideosController::class, 'saveVideoSections'])->name('saveVideoSections');
        Route::get('editVideoSections', [VideosController::class, 'editVideoSections'])->name('editVideoSections');
        Route::post('updateVideoSections', [VideosController::class, 'updateVideoSections'])->name('updateVideoSections');
        Route::get('viewVideoSections', [VideosController::class, 'viewVideoSections'])->name('viewVideoSections');
        Route::get('deleteVideoSections', [VideosController::class, 'deleteVideoSections'])->name('deleteVideoSections');   
            
    });  
    Route::middleware('auth')->prefix('videos')->name('videos.')->group(function () {
    
        Route::get('/videoList', [VideosController::class, 'videoList'])->name('videoList');
        Route::get('/addVideos', [VideosController::class, 'addVideos'])->name('addVideos');
        Route::post('saveVideos', [VideosController::class, 'saveVideos'])->name('saveVideos');
        Route::get('editVideos', [VideosController::class, 'editVideos'])->name('editVideos');
        Route::post('updateVideos', [VideosController::class, 'updateVideos'])->name('updateVideos');
        Route::get('viewVideos', [VideosController::class, 'viewVideos'])->name('viewVideos');
        Route::get('deleteVideos', [VideosController::class, 'deleteVideos'])->name('deleteVideos');
        Route::get('remove_video', [VideosController::class, 'removeVideo'])->name('remove_video');
        Route::get('remove_file', [VideosController::class, 'removeFile'])->name('remove_file');
        Route::get('remove_doc', [VideosController::class, 'removeDoc'])->name('remove_doc');
        Route::get('remove_author_image', [VideosController::class, 'removeAuthorImage'])->name('remove_author_image');

    });  
});
