<?php

namespace Modules\Videos\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
 

class Videos extends Model
{
    use HasFactory;
    protected $table = 'videos';

    protected $fillable = [
        'id',
        'title',
        'type',
        'link',
        'file',
        'image',
        'videos_sections_id',
        'status',
        'author',
        'author_image',
        'description',
        'video_description'
    ];

    /**
     * Get all of the owning commentable models.
     */
    public function videoable()
    {
        return $this->morphTo();
    }
    public function videosections()
    {
        return $this->belongsTo('Modules\Videos\Models\VideosSections',  'videos_sections_id', 'id');
    }


}