<?php

namespace Modules\Videos\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

 

class VideosSections extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'video_sections';

    protected $fillable = ['id','name','slug','banner_id','status','is_editable'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function videos()
    {
        return $this->hasMany('Modules\Videos\Models\Videos');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->videos->count()
        ) {
            return false;
        }
        return true;
    }

   
}