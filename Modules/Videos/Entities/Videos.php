<?php

namespace Modules\Videos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Videos extends Model
{
    use HasFactory;

    protected $fillable =  ['id','title','type','link','file','image','videos_sections_id','status','author','author_image','description','video_description'];
    
    protected static function newFactory()
    {
        return \Modules\Videos\Database\factories\VideosFactory::new();
    }
}
