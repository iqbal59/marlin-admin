<?php

namespace Modules\Videos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;


class VideoSections extends Model
{
    use HasFactory;
    use Sluggable;


    protected $fillable =  ['id','name','slug','banner_id','status','is_editable'];
    
    protected static function newFactory()
    {
        return \Modules\Videos\Database\factories\VideoSectionsFactory::new();
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
