<?php

namespace Modules\Videos\Http\Requests\VideosSections;

use Illuminate\Foundation\Http\FormRequest;

class VideosSectionsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_sections_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'banner_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'banner_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'banner_id' => 'Banner',
        ];
    }
}
