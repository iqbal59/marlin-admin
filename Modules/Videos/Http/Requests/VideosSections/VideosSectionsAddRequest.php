<?php

namespace Modules\Videos\Http\Requests\VideosSections;

use Illuminate\Foundation\Http\FormRequest;

class VideosSectionsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_sections_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
