<?php

namespace Modules\Videos\Http\Requests\VideosSections;

use Illuminate\Foundation\Http\FormRequest;

class VideosSectionsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_sections_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
