<?php

namespace Modules\Videos\Http\Requests\VideosSections;

use Illuminate\Foundation\Http\FormRequest;

class VideosSectionsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_sections_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
