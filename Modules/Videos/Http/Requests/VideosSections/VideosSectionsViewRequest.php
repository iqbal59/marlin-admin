<?php

namespace Modules\Videos\Http\Requests\VideosSections;

use Illuminate\Foundation\Http\FormRequest;

class VideosSectionsViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_sections_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
