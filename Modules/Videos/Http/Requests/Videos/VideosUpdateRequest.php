<?php

namespace Modules\Videos\Http\Requests\Videos;

use Illuminate\Foundation\Http\FormRequest;

class VideosUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'section_id' => 'required',
        ];    
    }

    public function messages()
    {      
        return [
            'title.required' => ':attribute is required',
            'section_id' => ':attribute is required',
        ];
        
    }

    public function attributes()
    {
 
        return [
            'title' => 'Title',
            'section_id' => 'Section',
        ];
         
    }
}
