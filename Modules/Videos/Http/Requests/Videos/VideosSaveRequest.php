<?php

namespace Modules\Videos\Http\Requests\Videos;

use Illuminate\Foundation\Http\FormRequest;

class VideosSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_create') ? true : false;
    }

    public function rules()
    { 
        if($this->type =="vimeo" || $this->type=="youtube"){
            return [
                'title' => 'required',
                'section_id' => 'required',
                'link' => 'required',
            ];
        }elseif($this->type =="file"){
            return [
                'title' => 'required',
                'section_id' => 'required',
                // 'file' => 'required',
            ];
        }
        else{
            return [
                'title' => 'required',
                'section_id' => 'required',
            ];
        }

        
    }

    public function messages()
    {
        if($this->type =="vimeo" || $this->type=="youtube"){
            return [
                'title' =>':attribute is required',
                'section_id' => ':attribute is required',
                'link' => ':attribute is required',
            ];
        }elseif($this->type =="file"){
            return [
                'title' => ':attribute is required',
                'section_id' => ':attribute is required',
                // 'file' => ':attribute is required',
            ];
        }else{
            return [
                'title.required' => ':attribute is required',
                'section_id.required' => ':attribute is required',
            ];
        }
        
    }

    public function attributes()
    {
        if($this->type =="vimeo" || $this->type=="youtube"){
            return [
                'title' =>'Title',
                'section_id' => 'Section',
                'link' => 'Link',
            ];
        }elseif($this->type =="file"){
            return [
                'title' => 'Title',
                'section_id' => 'Section',
                // 'file' => 'File',
            ];
        }else{
            return [
                'title' => 'Title',
                'section_id' => 'Section',
            ];
        }
       
    }
}
