<?php

namespace Modules\Videos\Http\Requests\Videos;

use Illuminate\Foundation\Http\FormRequest;

class VideosViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
