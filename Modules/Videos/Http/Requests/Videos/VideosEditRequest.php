<?php

namespace Modules\Videos\Http\Requests\Videos;

use Illuminate\Foundation\Http\FormRequest;

class VideosEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
