<?php

namespace Modules\Videos\Http\Requests\Videos;

use Illuminate\Foundation\Http\FormRequest;

class VideosAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('videos_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
