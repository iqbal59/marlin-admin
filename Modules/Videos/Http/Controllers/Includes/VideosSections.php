<?php

namespace Modules\Videos\Http\Controllers\Includes;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsAddRequest;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsSaveRequest;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsEditRequest;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsUpdateRequest;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsViewRequest;
use Modules\Videos\Http\Requests\VideosSections\VideosSectionsDeleteRequest;
use Modules\Videos\Repositories\Includes\VideosSections\VideosSectionsRepositoryInterface as VideosSectionsRepository;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface as BannerRepository;
use Illuminate\Http\Request;
trait VideosSections
{
    public function videoSectionList(VideosSectionsRepository $videosectionRepo)
    {   
        $videos = $videosectionRepo->getForDatatable();   
        return view('videos::VideosSections.listVideosSections', compact('videos'));   
    }

    public function addVideoSections(VideosSectionsAddRequest $request ,BannerRepository $bannerRepo)
    {  
        $banner = $bannerRepo->all();
        return view('videos::VideosSections.addVideosSections', compact('banner'));   
    }

    public function saveVideoSections(VideosSectionsSaveRequest $request, VideosSectionsRepository $videosectionRepo)
    {     
        $inputData = [
            'name'  => $request->name,
            'banner_id'  => $request->banner_id,
        ];
        $videosection = $videosectionRepo->save($inputData);  
        connectify('success', 'Success!', 'Videos Sections added successfully');               
        return redirect()
            ->route('admin.videossections.videoSectionList');
   
    } 

    public function viewVideoSections(VideosSectionsViewRequest $request,VideosSectionsRepository $videosectionRepo,BannerRepository $bannerRepo)
    {  
        $videos = $videosectionRepo->get($request->id);  
        $banner = $bannerRepo->get($videos->banner_id);
        return view('videos::VideosSections.viewVideoSections', compact('videos','banner'));
    }

    public function editVideoSections(VideosSectionsEditRequest $request,VideosSectionsRepository $videosectionRepo,BannerRepository $bannerRepo)
    {   
        $videos = $videosectionRepo->get($request->id);    
        $banner = $bannerRepo->all();
        return view('videos::VideosSections.editVideoSections', compact('videos','banner'));
    }

    public function updateVideoSections(VideosSectionsUpdateRequest $request, VideosSectionsRepository $videosectionRepo)
    {   
        $inputData = [
            'id' => $request->id,      
            'name' => $request->name,            
            'banner_id' => $request->banner_id,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
 

        $videosectionRepo->update($inputData);
        connectify('success', 'Success!', 'Videos Sections updated successfully');               
        return redirect()
        ->route('admin.videossections.videoSectionList');
    }

    public function deleteVideoSections(VideosSectionsRepository $videosectionRepo, VideosSectionsDeleteRequest $request)
    { 
        if ($videosectionRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Videos Sections deleted successfully');               
            return redirect()
            ->route('admin.videossections.videoSectionList');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}