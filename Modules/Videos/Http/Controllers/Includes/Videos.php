<?php

namespace Modules\Videos\Http\Controllers\Includes;
use Modules\Videos\Http\Requests\Videos\VideosAddRequest;
use Modules\Videos\Http\Requests\Videos\VideosSaveRequest;
use Modules\Videos\Http\Requests\Videos\VideosEditRequest;
use Modules\Videos\Http\Requests\Videos\VideosUpdateRequest;
use Modules\Videos\Http\Requests\Videos\VideosViewRequest;
use Modules\Videos\Http\Requests\Videos\VideosDeleteRequest;
use Modules\Videos\Repositories\Includes\Videos\VideosRepositoryInterface as VideosRepository;
use Modules\Videos\Repositories\Includes\VideosSections\VideosSectionsRepositoryInterface as VideosSectionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cohensive\Embed\Facades\Embed;
use League\CommonMark\Inline\Parser\EscapableParser;

trait Videos
{
   
    public function videoList(VideosRepository $videosRepo)
    { 
        $videos = $videosRepo->getForDatatable();   
        return view('videos::Videos.listVideos', compact('videos'));   
    } 

    public function addVideos(VideosAddRequest $request, VideosRepository $videosRepo, VideosSectionsRepository $videosectionRepo)
    {   
        $videoSection = $videosectionRepo->all(); 
        return view('videos::Videos.addVideos', compact('videoSection'));      
    }

    public function saveVideos(VideosSaveRequest $request, VideosRepository $videosRepo)
    {   
        if($request->type == "vimeo" || $request->type =="youtube"){
            if(preg_match('/https:\/\/(www\.)*vimeo\.com\/.*/',$request->link)){
                $request->link = $request->link;
            } 
            elseif(preg_match('/https:\/\/(www\.)*youtube\.com\/.*/',$request->link)){
                $request->link = $request->link;
            } 
            else{
                connectify('failed', 'Faild!', 'Enter valid url');                 
                return redirect()
                ->route('admin.videos.addVideos');
            }
        }
        $imageUrl = $request->hasFile('image')?Storage::disk('public')->putFile('video_image/',$request->file('image')):"";
        $fileUrl = $request->hasFile('file')?Storage::disk('public')->putFile('video_files/',$request->file('file')):""; 
        $authorImage = $request->hasFile('author_image')?Storage::disk('public')->putFile('author_image/',$request->file('author_image')):""; 

         $inputData = [
            'title'  => $request->title,
            'videos_sections_id'  => $request->section_id,
            'link'  => $request->link,
            'type'  => $request->type,
            'file'  => $fileUrl,
            'image'  => $imageUrl,
            'author'  => $request->author,
            'description' => $request->description,
            'author_image'  => $authorImage,
            'video_description' =>$request->video_description
        ];
        $videos = $videosRepo->save($inputData); 
        connectify('success', 'Success!', 'Videos added successfully');                 
            return redirect()
            ->route('admin.videos.videoList');
    }     

    public function viewVideos(VideosViewRequest $request,VideosRepository $videosRepo, VideosSectionsRepository $videosectionRepo)
    {  
        $videos = $videosRepo->get($request->id);  
        $section = $videosectionRepo->get($videos->videos_sections_id); 
        return view('videos::Videos.viewVideos', compact('videos','section'));
    }

    public function editVideos(VideosEditRequest $request,VideosRepository $videosRepo, VideosSectionsRepository $videosectionRepo)
    {   
        $videos = $videosRepo->get($request->id); 
        $section = $videosectionRepo->all();
        return view('videos::Videos.editVideos', compact('videos','section'));
    }

    public function updateVideos(VideosUpdateRequest $request, VideosRepository $videosRepo)
    {  
        $inputData = [
            'id'  => $request->id,
            'title'  => $request->title,
            'videos_sections_id'  => $request->section_id,
            'author' =>$request->author,
            'description' => $request->description,
            'video_description' =>$request->video_description
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
 
        if ($request->link) { 
            $inputData['link'] = $request->link;
        }

        if ($request->hasFile('image')) { 
            $this->_removeFile($videosRepo, $request->id);                 
            $imageUrl = Storage::disk('public')->putFile('video_image/',$request->file('image'));
            $inputData['image'] = $imageUrl;
        } 

        if ($request->hasFile('file')) { 
            $this->_removeFile($videosRepo, $request->id);                 
            $fileUrl = Storage::disk('public')->putFile('video_files/',$request->file('file'));
            $inputData['file'] = $fileUrl;
        } 

        if ($request->hasFile('author_image')) { 
            $this->_removeAuthorImage($videosRepo, $request->id);                 
            $fileUrl = Storage::disk('public')->putFile('author_image/',$request->file('author_image'));
            $inputData['author_image'] = $fileUrl;
        } 
        
        $videosRepo->update($inputData);
        connectify('success', 'Success!', 'Videos Updated successfully');               
        return redirect()
        ->route('admin.videos.videoList')
            ->with('success', 'Updated successfuly');
        
       
    }

     

    public function removeFile(VideosRepository $videoRepo,Request $request)
    {  
        if ($this->_removeFile($videoRepo, $request->id)) {
            $videoRepo->resetFile($request->id);
        }
        return redirect()
            ->route('admin.videos.editVideos', ['id' => $request->id])
            ->with('success', 'File deleted successfuly');
    }

    public function removeVideo(VideosRepository $videoRepo,Request $request)
    {    
        $videoRepo->resetVideo($request->id);
        connectify('success', 'Success!', 'File deleted successfully');               
        return redirect()
            ->route('admin.videos.editVideos', ['id' => $request->id]);
    }

    private function _removeFile($videoRepo, $id)
    { 
        $image = $videoRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }

    public function removeDoc(VideosRepository $videoRepo,Request $request)
    {    
        if ($this->_removeDoc($videoRepo, $request->id)) {
            $videoRepo->resetDoc($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');               
        return redirect()
            ->route('admin.videos.editVideos', ['id' => $request->id]);
    }

    private function _removeDoc($videoRepo, $id)
    { 
        $image = $videoRepo->get($id);
        if (Storage::disk('public')->delete($image->file)) {
            return true;
        }
        return false;
    }

    public function deleteVideos(VideosRepository $videoRepo, VideosDeleteRequest $request)
    { 
        if ($videoRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Videos deleted successfully');               
            return redirect()
            ->route('admin.videos.videoList');
        }
            return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function removeAuthorImage(VideosRepository $videoRepo,Request $request)
    {    
        if ($this->_removeAuthorImage($videoRepo, $request->id)) {
            $videoRepo->removeAuthorImage($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');               
        return redirect()
            ->route('admin.videos.editVideos', ['id' => $request->id]);
    }

    private function _removeAuthorImage($videoRepo, $id)
    { 
        $image = $videoRepo->get($id); 
        if (Storage::disk('public')->delete($image->author_image)) {
            return true;
        }
        return false;
    }
    
}