<?php

namespace Modules\Videos\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Videos\Http\Controllers\Includes\Videos;
use Modules\Videos\Http\Controllers\Includes\VideosSections;

class VideosController extends Controller
{
    use Videos;
    use VideosSections;
}

