<?php

namespace Modules\Videos\Repositories\Includes\Videos;

interface VideosRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($id);

    public function resetFile(string $id);

    public function resetVideo(string $id);

    public function update(array $input);

    public function resetDoc(string $id);

    public function delete(string $id);

    public function getPatientVoice();

    public function doctorPatientVoice($patientvoiceId);

    public function removeAuthorImage(string $id);


    
}