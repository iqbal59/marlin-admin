<?php

namespace Modules\Videos\Repositories\Includes\Videos;
use Modules\Videos\Models\Videos;
use Illuminate\Database\Eloquent\Builder;

class VideosRepository implements VideosRepositoryInterface
{
    public function getForDatatable()
    { 
        return Videos::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Videos::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($videos =  Videos::create($input)) {
            return $videos;
        }
        return false;
    }

    public function get($id)
    { 
        return Videos::where('id',$id)->first();
    }

    public function resetFile(string $id)
    {
        $videos = Videos::find($id);
        $videos->image = '';
        return $videos->save();
    }

    public function resetDoc(string $id)
    {
        $videos = Videos::find($id);
        $videos->file = '';
        return $videos->save();
    }

    public function resetVideo(string $id)
    {
        $videos = Videos::find($id);
        $videos->link = '';
        return $videos->save();
    }

    public function update(array $input)
    { 
        $videos = Videos::find($input['id']); 
        unset($input['id']);
        if ($videos->update($input)) {
            return $videos;
        }
        return false;
    }

    public function delete(string $id)
    {
        $videos = Videos::find($id);
        return $videos->delete();
    }

    public function getPatientVoice()
    {
        $videos = Videos::where('videos_sections_id',2)->get();
        return $videos;
    }

    public function doctorPatientVoice($patientvoiceId)
    {
        $videos = Videos::whereIn('id',$patientvoiceId)->get();
        return $videos;
    }

    public function removeAuthorImage(string $id)
    {
        $videos = Videos::find($id);
        $videos->author_image = '';
        return $videos->save();
    }

}