<?php

namespace Modules\Videos\Repositories\Includes\VideosSections;

interface VideosSectionsRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($id);

    public function update(array $input);

    public function delete(string $id);
    
}