<?php

namespace Modules\Videos\Repositories\Includes\VideosSections;
use Modules\Videos\Models\VideosSections;
use Illuminate\Database\Eloquent\Builder;

class VideosSectionsRepository implements VideosSectionsRepositoryInterface
{
    public function getForDatatable()
    { 
        return VideosSections::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return VideosSections::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($videos =  VideosSections::create($input)) {
            return $videos;
        }
        return false;
    }

    public function get($id)
    { 
        return VideosSections::where('id',$id)->first();
    }

    public function update(array $input)
    { 
        $videos = VideosSections::find($input['id']); 
        unset($input['id']);
        if ($videos->update($input)) {
            return $videos;
        }
        return false;
    }

    public function delete(string $id)
    {
        $videos = VideosSections::find($id);
        return $videos->delete();
    }

}