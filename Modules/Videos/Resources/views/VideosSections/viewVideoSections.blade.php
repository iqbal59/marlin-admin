@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
       View Video Section
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $videos->name }}
                        </td>
                    </tr>
                    @if($banner != null)    
                    <tr>
                        <th>
                            Banner
                        </th>
                        <td> 
                            {{ $banner->title }}
                        </td>
                    </tr>
                    @endif
                     
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection