@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Add Video Section
    </div>

    <div class="card-body">
        <form action="{{ route("admin.videossections.saveVideoSections") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name"> Banner<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <select id="banner_id" name="banner_id" class="js-states form-control">
                                <option value="">{{ trans('panel.select') }}</option>
                                @foreach($banner as $data) 
                                <option value="{{$data->id}}" name="banner_id-dropdown" id="banner_id-dropdown" onclick="myFunction()" >
                                {{$data->title}}
                                </option>
                                @endforeach
                            </select>    
                            @if($errors->has('banner_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('banner_id') }}
                            </em>
                            @endif           
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>     
                </div>
            </div>
        
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#banner_id").select2({
                placeholder: "Select Banner",
                allowClear: true
            });
           
        });
</script>

@endsection
