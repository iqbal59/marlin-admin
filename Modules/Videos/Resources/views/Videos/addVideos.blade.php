@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<html>
<body>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="card">
    <div class="card-header">
        Add Video  
    </div>

    <div class="card-body">
        <form action="{{ route("admin.videos.saveVideos") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('section_id') ? 'has-error' : '' }}">
                        <label for="section_id">Choose Section <i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <select id="section_id" name="section_id" class="js-states form-control">
                                <option value="">{{ trans('panel.select') }}</option>
                                @foreach($videoSection as $data) 
                                <option value="{{$data->id}}" name="section_id-dropdown" id="section_id-dropdown" onclick="myFunction()" >
                                {{$data->name}}
                                </option>
                                @endforeach
                            </select> 
                            @if($errors->has('section_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('section_id') }}
                            </em>
                        @endif              
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>     
                </div>
            </div>
            <div class="row">
           
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Type</label>
                            <select id="type" name="type" class="js-states form-control" onchange="change_type()">
                               <option value="">Choose:</option>
                                <option value="vimeo">Vimeo</option>
                                <option value="youtube">Youtube</option>
                                <option value="file">File</option>
                            </select>               
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>     
                 
                </div>
                <div class="col-md-6" id="toshow">
                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">Link<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                </div>

                <div class="col-md-6" id="toshow2">
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        <label for="file"> File<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <input type="file" name="file" id="input-file-now-banner" class="dropify validate" data-default-file="" accept="application/pdf,application/doc,application/docx" /> 
                            @if($errors->has('file'))
                            <em class="invalid-feedback">
                                {{ $errors->first('file') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                    </div>   
                </div>
            </div>

            <div class="row" >

                <div class="col-md-6" id="toshow1">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image">Thumb Line Images<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                    </div>         
                </div>
                <div class="col-md-6">
               
                </div>

               
            </div>  
            
            <div class="row">
                <div class="col-md-12"> 
                <div class="form-group {{ $errors->has('video_description') ? 'has-error' : '' }}">
                <label for="video_description"> Video Description</label>
                <textarea class="ckeditor form-control" id="video_description" name="video_description" required>
                {!! old('video_description', isset($department) ? $department->video_description : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
            </div> 
                </div>
            </div>
            
            <div class="row" >
                <div class="col-md-6" >
                    <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                        <label for="author">Author</label>
                        <input type="text" id="author" name="author" class="form-control" value="{{old('author')}}" >
                        @if($errors->has('author'))
                            <em class="invalid-feedback">
                                {{ $errors->first('author') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>   
                </div>
                
                <div class="col-md-6" >
                    <div class="form-group {{ $errors->has('author_image') ? 'has-error' : '' }}">
                        <label for="author_image">Author Image </label><br>
                            <input type="file" name="author_image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('author_image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('author_image') }}
                            </em>
                            @endif
                    </div>   
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }}</label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($department) ? $department->description : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
                </div>
            </div>
           
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#section_id").select2({
                placeholder: "Select Video Section",
                allowClear: true
            });
            $("#type").select2({
                placeholder: "Choose Type",
                allowClear: true
            });
           
           
        });

         
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script> 
    $("#toshow").hide(); 
    $("#toshow1").hide();  
    $("#toshow2").hide();  

    function change_type(){
        var type =$("#type").val();
            if(type == 'vimeo' || type =="youtube"){
                $("#toshow").show();
                $("#toshow1").show();
                $("#toshow2").hide();

            }else{
                $("#toshow").hide();
                $("#toshow1").hide();
                $("#toshow2").show();
            }    
    }   
</script>
@endsection
