@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th img, .table.video-table td img {
    width: 80px;
    height: 80px;
    border-radius: 10px;
}
</style>
<div class="card">
    <div class="card-header">
       View Video 
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $videos->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Video Section
                        </th>
                        <td>
                            {{ $section->name }}
                        </td>
                    </tr>
                    @if($videos->link != null)
                    <tr>
                        <th>
                            Video
                        </th>
                        <td> 
                        {!! Embed::make($videos->link)->parseUrl()->getIframe() !!}
                       
                        </td>
                    </tr>
                    @endif
                    @if($videos->type =="youtube" || $videos->type =="vimeo")
                    <tr>
                        <th>
                            Image
                        </th>
                        <td> 
                        <img src="{{asset('storage/'.$videos->image)}}" alt="" width="1000px" height="500px">
                       
                        </td>
                    </tr>
                    @else 
                    <tr>
                        <th>
                           File
                        </th>
                        <td>                                                                                           
                            <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$videos->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>                       
                        </td>
                    </tr>
                    @endif 
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection