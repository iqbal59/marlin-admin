@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<html>
<body>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} Videos
    </div>

    <style>
        input[type="number"] {
        -webkit-appearance: textfield;
        -moz-appearance: textfield;
        appearance: textfield;
        }
        
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
          -webkit-appearance: none;
        }
        
        .number-input {
          border: 2px solid #ddd;
          display: inline-flex;
        }
        
        .number-input,
        .number-input * {
          box-sizing: border-box;
        }
        
        .number-input button {
          outline:none;
          -webkit-appearance: none;
          background-color: transparent;
          border: none;
          align-items: center;
          justify-content: center;
          width: 3rem;
          height: 2rem;
          cursor: pointer;
          margin: 0;
          position: relative;
        }
        
        .number-input button:before,
        .number-input button:after {
          display: inline-block;
          position: absolute;
          content: '';
          width: 1rem;
          height: 2px;
          background-color: #212121;
          transform: translate(-50%, -50%);
        }
        .number-input button.plus:after {
          transform: translate(-50%, -50%) rotate(90deg);
        }
        
        .number-input input[type=number] {
          font-family: sans-serif;
          max-width: 3rem;
          padding: .5rem;
          border: solid #ddd;
          border-width: 0 2px;
          font-size: 1rem;
          height: 2rem;
          font-weight: bold;
          text-align: center;
        }
        i.mdi.mdi-minus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        i.mdi.mdi-plus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        span.on {
            width: 95px;
            float: left;
        }
        .custom-switch {
            padding-left: 0;
        }
        .custom-switch .custom-control-label::before{
            width: 2rem;
        }
        .custom-switch .custom-control-label::after {
            width: calc(1.3rem - 4px);
            }
        .badge-contr span.off {
            width: 70px;
            float: left;
        }
        .badge-contr span.on {
            float: none;
        }
        </style>
    

    <div class="card-body">
        <form action="{{ route("admin.videos.updateVideos") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$videos->id}}">

                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($videos) ? $videos->title : '') }}" >
                            @if($errors->has('title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                    </div>  

                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Choose Section<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <select id="section_id" name="section_id" class="js-states form-control">
                                @foreach ($section as $key => $value)
                                <option value="{{$value->id}}" {{(old('section_id', $videos->videos_sections_id) == $value->id ? 'selected' : '')}} > {{$value->name}} </option>
                                @endforeach
                            </select>
                        
                            @if($errors->has('section_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('section_id') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                    </div>  

                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label for="type">Type </label>
                        <input type="text" id="type" name="type" class="form-control" value="{{$videos->type}}" readonly >      
                    </div>  

                @if($videos->type != "file")     
                 <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">Link</label>
                       @if( ($videos->link != null) && ($videos->link != ""))
                        {!! Embed::make($videos->link)->parseUrl()->getIframe() !!}
                            <a href="{{route('admin.videos.remove_video',['id'=>$videos->id])}}" class="confirm-delete text-danger">
                            <i class="menu-icon mdi mdi-close pink-text"></i>
                            </a>                           
                            @if($errors->has('link'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('link') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        @else
                        <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                        @endif    
                    </div> 
                @endif    
            @if($videos->type == "file")
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label for="file">File </label>                
                        @if(($videos->type == "file") && (Storage::disk('public')->exists($videos->file))) 
                        <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$videos->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>    
                            <a href="{{route('admin.videos.remove_doc',['id'=>$videos->id])}}" class="confirm-delete text-danger">
                                    <i class="menu-icon mdi mdi-close pink-text"></i>
                            </a>
                            
                         @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                        <input type="file" name="file" id="input-file-now-banner" class="dropify validate" data-default-file="" accept="application/pdf,application/doc,application/docx" /> 
                            @if($errors->has('file'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('file') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                        @endif
                </div>
                @endif
                @if($videos->type != "file")     
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label for="image">Thumb Line Images </label>                
                        @if( ($videos->image != "")&&(Storage::disk('public')->exists($videos->image))) 
                            <img class="preview" src="{{asset('storage/'.$videos->image)}}" alt="" width="150px">
                                <a href="{{route('admin.videos.remove_file',['id'=>$videos->id])}}" class="confirm-delete text-danger">
                                <i class="menu-icon mdi mdi-close pink-text"></i>
                                </a>
                         @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                        @endif
                </div>
                @endif

                <div class="form-group {{ $errors->has('video_description') ? 'has-error' : '' }}">
                    <label for="video_description"> Video Description</label>
                    <textarea class="ckeditor form-control" id="video_description" name="video_description" required>
                    {!! old('video_description', isset($videos->video_description) ? $videos->video_description : '') !!}
                    </textarea>      

                    @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    @if($errors->has('department'))
                        <em class="invalid-feedback">
                            {{ $errors->first('department') }}
                        </em>
                    @endif
                </div> 

                <div class="row" >
                    <div class="col-md-6" >
                    <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                            <label for="author">Author<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <input type="text" id="author" name="author" class="form-control" value="{{ old('author', isset($videos) ? $videos->author : '') }}" >
                                @if($errors->has('author'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('author') }}
                                    </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                        </div>  
                    </div> 
                    <div class="col-md-6" > 
                        <div class="form-group {{ $errors->has('author_image') ? 'has-error' : '' }}">
                            <label for="image">Author Image </label>   
                                @if( ($videos->author_image != "")&&(Storage::disk('public')->exists($videos->author_image))) 
                                    <img class="preview" src="{{asset('storage/'.$videos->author_image)}}" alt="" width="150px">
                                        <a href="{{route('admin.videos.remove_author_image',['id'=>$videos->id])}}" class="confirm-delete text-danger">
                                        <i class="menu-icon mdi mdi-close pink-text"></i>
                                        </a>
                                @else
                                <div class="form-group {{ $errors->has('author_image') ? 'has-error' : '' }}">                
                                <input type="file" name="author_image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                                    @if($errors->has('author_image'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('author_image') }}
                                        </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.name_helper') }}
                                    </p>
                                </div>  
                                @endif
                        </div>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description"> {{ trans('panel.description') }}</label>
                    <textarea class="ckeditor form-control" id="description" name="description" required>
                    {!! old('description', isset($videos->description) ? $videos->description : '') !!}
                    </textarea>      

                    @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    @if($errors->has('department'))
                        <em class="invalid-feedback">
                            {{ $errors->first('department') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 

                <div class="col-md-6 badge-contr">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" {{ old('status', $videos->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                            <span class="off">No</span>
                            <label class="custom-control-label" for="status"></label>
                            <span class="on">Yes</span>
                        </div>
                    </div> 
                </div> 
            
            <br>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
            $("#banner_id").select2({
                placeholder: "Select Banner",
                allowClear: true
            });
            $("#section_id").select2({
                placeholder: "Select Section",
                allowClear: true
            });
         
            
        });
</script>
 
@endsection