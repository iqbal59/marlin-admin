<?php

namespace Modules\Treatment\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TreatmentCategory extends Model
{
    use HasFactory;
    protected $table = 'treatment_category';

    protected $fillable = [
        'hospital_id','category_name','status','added_by','department_id','image','logo'
    ];

    public function treatment()
    {
        return $this->hasMany('Modules\Treatment\Models\Treatment', 'treatment_category_id', 'id');
        
    }
   
}