<?php

namespace Modules\Treatment\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Enquiry\Models\Enquiry;

class Treatment extends Model
{
    use HasFactory;
    use Sluggable;
    protected $table = 'treatments';

    protected $fillable = [
        'slug','diseases_id','treatment_category_id','treatment_plants_id','name','title','description','image','logo','status','options','added_by'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function department()
    {
        return $this->belongsTo('Modules\Department\Models\Department',  'department_id', 'id');
    }

    // public function treatmentPlans()
    // {
    //     return $this->belongsTo('Modules\TreatmentPlants\Models\TreatmentPlants',  'treatment_plants_id', 'id');
    // }
    
    public function enquiries()
    { 
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

    public function category()
    {
        return $this->belongsTo('Modules\Treatment\Models\TreatmentCategory', 'treatment_category_id', 'id');
    }
    

   
}