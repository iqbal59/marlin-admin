<?php

namespace Modules\Treatment\Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TreatmentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '80',
                'title'      => 'treatment_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '81',
                'title'      => 'treatment_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '82',
                'title'      =>  'treatment_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '83',
                'title'      => 'treatment_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '84',
                'title'      => 'treatment_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],


            // 
            [
                'id'         => '316',
                'title'      => 'treatment_category_create',
                'created_at' => '2022-02-14 12:14:15',
                'updated_at' => '2022-02-14 12:14:15',
            ],
            [
                'id'         => '317',
                'title'      => 'treatment_category_edit',
                'created_at' => '2022-02-14 12:14:15',
                'updated_at' => '2022-02-14 12:14:15',
            ],
            [
                'id'         => '318',
                'title'      =>  'treatment_category_show',
                'created_at' => '2022-02-14 12:14:15',
                'updated_at' => '2022-02-14 12:14:15',
            ],
            [
                'id'         => '319',
                'title'      => 'treatment_category_delete',
                'created_at' => '2022-02-14 12:14:15',
                'updated_at' => '2022-02-14 12:14:15',
            ],
            [
                'id'         => '320',
                'title'      => 'treatment_category_access',
                'created_at' => '2022-02-14 12:14:15',
                'updated_at' => '2022-02-14 12:14:15',
            ],

        ];

        Permission::insert($permissions);
    }
}
