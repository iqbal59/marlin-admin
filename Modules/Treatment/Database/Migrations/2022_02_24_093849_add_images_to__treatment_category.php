<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToTreatmentCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('treatment_category', function (Blueprint $table) {
            $table->string('image')->after('added_by')->nullable();
            $table->string('logo')->after('image')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('treatment_category', function (Blueprint $table) {
            $table->string('image')->after('added_by')->nullable();
            $table->string('logo')->after('image')->nullable();
        });
    }
}
