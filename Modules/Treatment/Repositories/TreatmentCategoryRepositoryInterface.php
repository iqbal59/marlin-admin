<?php

namespace Modules\Treatment\Repositories;

interface TreatmentCategoryRepositoryInterface
{
    public function getForDatatable();

    public function save(array $input);

    public function get($id);

    public function update(array $input);

    public function delete(string $id);

    public function all();

    public function getsearchBasedTreatmentCategory($searchText);

    public function aaaa($hospital);

    public function resetFile(string $id);

    public function resetLogo(string $id);

}