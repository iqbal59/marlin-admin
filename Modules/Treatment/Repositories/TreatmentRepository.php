<?php

namespace Modules\Treatment\Repositories;

use Modules\Treatment\Models\Treatment;
use Illuminate\Database\Eloquent\Builder;
use Modules\Treatment\Entities\Treatment as EntitiesTreatment;

class TreatmentRepository implements TreatmentRepositoryInterface
{
    public function getForDatatable()
    {
        return Treatment::orderBy('id', 'DESC')->get()->toArray();
    }

    public function all()
    {
        return Treatment::orderBy('id', 'DESC')->get()->toArray();
    }

    public function allTreatments()
    {
        return Treatment::where('status', 1)->get();
    }

    public function save(array $input)
    {
        if ($treatment =  Treatment::create($input)) {
            return $treatment;
        }
        return false;
    }

    public function get($id)
    {
        return Treatment::with('category')->where('id', $id)->first();
    }

    public function getBySlug($slug)
    {
        return Treatment::with('category')->where('slug', $slug)->first();
    }

    public function getActiveTreatment($id)
    {
        return Treatment::where('status', 1)->where('id', $id)->first();
    }

    public function resetFile(string $id)
    {
        $treatment = Treatment::find($id);
        $treatment->image = '';
        return $treatment->save();
    }

    public function resetLogo(string $id)
    {
        $treatment = Treatment::find($id);
        $treatment->logo = '';
        return $treatment->save();
    }

    public function update(array $input)
    {
        $treatment = Treatment::find($input['id']);
        unset($input['id']);
        if ($treatment->update($input)) {
            return $treatment;
        }
        return false;
    }

    public function delete(string $id)
    {
        $treatment = Treatment::find($id);
        return $treatment->delete();
    }

    public function getTreatmentCount()
    {
        $treatmentCount = Treatment::count();
        return $treatmentCount;
    }

    public function getDepartmentBasedTreatments($deptId, $searchText)
    {
        if ($searchText != null) {
            $treatment = Treatment::where('status', 1)->where('department_id', $deptId)->where('diseases_id', 'like', '%'.$searchText.'%')->get();
            return $treatment;
        } else {
            $treatment = Treatment::where('status', 1)->where('department_id', $deptId)->get();
            return $treatment;
        }
    }

    public function getsearchBasedTreatments($searchText)
    {
        $treatment = Treatment::where('status', 1)->where('diseases_id', 'like', '%'.$searchText.'%')->get();
        return $treatment;
    }

    public function getTreatmentDetails($deptId)
    {
        $treatments = Treatment::where('department_id', $deptId)->get();
        return $treatments;
    }

    public function getTreatments($treatments)
    {
        $treatments = Treatment::whereIn('id', $treatments)->get();
        return $treatments;
    }

    public function getDepartmentBasedTreatment($deptId)
    {
        $treatments = Treatment::whereIn('department_id', $deptId)->get();
        return $treatments;
    }

    public function searchBasedTreatments($searchText)
    {
        return Treatment::where('status', 1)->where('name', 'like', '%'.$searchText.'%')->pluck('id')->toArray();
    }

    public function getTreatmentBasedDepartment($treatmentId)
    {
        return Treatment::where('status', 1)->whereIn('id', $treatmentId)->pluck('department_id')->toArray();
    }

    public function searchTreatments($searchText)
    {
        return  Treatment::where('status', 1)->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('name', 'like', '%'.$searchText.'%');
            }
        })->get();
    }

    public function slugBasedTreatments($slug)
    {
        return Treatment::where('status', 1)->whereIn('diseases_id', $slug)->get();
    }

    public function categoryBasedTreatment($categoryId)
    {
        return Treatment::where('status', 1)->where('treatment_category_id', $categoryId)->pluck('name');
    }

    public function getallTreatmentBasedOnCategory($categoryId)
    {
        return Treatment::where('status', 1)->where('treatment_category_id', $categoryId)->select('id', 'name')->get();
    }
}