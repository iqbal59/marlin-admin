<?php

namespace Modules\Treatment\Repositories;

interface TreatmentRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function save(array $input);

    public function get($id);

    public function getBySlug($slug);

    public function getActiveTreatment($id);

    public function resetFile(string $id);

    public function update(array $input);

    public function delete(string $id);

    public function getTreatmentCount();

    public function allTreatments();

    public function getDepartmentBasedTreatments($deptId, $searchText);

    public function getsearchBasedTreatments($searchText);

    public function getTreatmentDetails($deptId);

    public function getTreatments($treatments);

    public function getDepartmentBasedTreatment($deptId);

    public function resetLogo(string $id);

    public function searchBasedTreatments($searchText);

    public function getTreatmentBasedDepartment($treatmentId);

    public function searchTreatments($searchText);

    public function slugBasedTreatments($slug);

    public function categoryBasedTreatment($categoryId);

    public function getallTreatmentBasedOnCategory($categoryId);
}