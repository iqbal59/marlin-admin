<?php


namespace Modules\Treatment\Repositories;
use Modules\Treatment\Models\TreatmentCategory;
use Illuminate\Database\Eloquent\Builder;
use Modules\Treatment\Entities\Treatment as EntitiesTreatment;

class TreatmentCategoryRepository implements TreatmentCategoryRepositoryInterface
{
    public function getForDatatable()
    { 
        return TreatmentCategory::orderBy('id', 'DESC')->get()->toArray();
    }

    public function save(array $input)
    { 
        if ($treatment =  TreatmentCategory::create($input)) {
            return $treatment;
        }
        return false;
    }

    public function get($id)
    { 
        return TreatmentCategory::where('id',$id)->first();
    }

    public function update(array $input)
    { 
        $treatment = TreatmentCategory::find($input['id']); 
        unset($input['id']);
        if ($treatment->update($input)) {
            return $treatment;
        }
        return false;
    }

    public function delete(string $id)
    {
        $treatment = TreatmentCategory::find($id);
        return $treatment->delete();
    }

    public function all()
    { 
        return TreatmentCategory::orderBy('id', 'DESC')->get();
    }

    public function getsearchBasedTreatmentCategory($searchText)
    {
        $treatment = TreatmentCategory::where('status',1)->where('category_name','like','%'.$searchText.'%')->get();
        return $treatment;
    }

    public function aaaa($hospital)
    {
        // $doctor = TreatmentCategory::whereRaw('json_contains(hospital_id, \'["' . $hospital . '"]\')')->get();
        // dd($doctor);

        $array = $hospital;
        $array = array_values(array_map('strval',$array)); 
        $hospital = TreatmentCategory::where('status',1)->where(function ($query) use ($array) {
        foreach ($array as $id) {
            $query->orWhereJsonContains('hospital_id', $id);
        }
        })->get(); 
        
        return $hospital;
    }

    public function resetFile(string $id)
    {
        $treatment = TreatmentCategory::find($id);
        $treatment->image = '';
        return $treatment->save();
    }

    public function resetLogo(string $id)
    {
        $treatment = TreatmentCategory::find($id);
        $treatment->logo = '';
        return $treatment->save();
    }


}