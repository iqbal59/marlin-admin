@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    .treatment-edit-cn span.select2-selection.select2-selection--multiple:after {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 5px;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
    content: "";
}
    </style>

<div class="card treatment-edit-cn">
    <div class="card-header">
        {{ trans('global.edit') }} Treatment Category 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentcategory.updateCategory") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$treatmentCategory->id}}">
            <div class="row">                
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('category_name') ? 'has-error' : '' }}">
                        <label for="category_name"> Category Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="category_name" name="category_name" class="form-control" value="{{ old('category_name', isset($treatmentCategory) ? $treatmentCategory->category_name : '') }}" >
                        @if($errors->has('category_name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('category_name') }}
                            </em>
                        @endif
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('hospital_ids') ? 'has-error' : '' }}">
                        <label for="hospital_id">Hospital <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                        <select class="js-example-basic-multiple" name="hospital_ids[]" multiple="multiple" id="hospital-select">
                        @foreach ($hospitals as $key => $value) 
                            <option value="{{$value->id}}" @if(in_array($value->id, $selectedHospitals)) selected @endif>{{$value->title}}</option>
                        @endforeach
                        </select>
                        @if($errors->has('hospital_ids'))
                        <em class="invalid-feedback">{{ $errors->first('hospital_ids') }}</em>
                        @endif  
                    </div> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                        <label for="name"> Department <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        @if($selectedDepartments == null)
                        <select id="department_id" name="department_id[]" class="js-example-basic-multiple" multiple="multiple" >
                            @foreach($hospitalDepartments as $data)
                            <option value="{{$data->id}}">{{$data->name}}</option>
                            @endforeach 
                        </select>
                        @else   
                        <select id="department_id" name="department_id[]" class="js-example-basic-multiple" multiple="multiple" >
                            @foreach($hospitalDepartments as $data) 
                            <option value="{{$data->id}}" @if(in_array($data->id, $selectedDepartments)) selected @endif>{{$data->name}}</option>
                            @endforeach 
                        </select>
                        @endif
                        
                        @if($errors->has('department_id'))
                        <em class="invalid-feedback">{{ $errors->first('department_id') }}</em>
                        @endif  
                    </div> 
                </div> 
                <div class="col-md-6">
                    <label for="image"> {{ trans('panel.image') }} </label>                
                    @if(Storage::disk('public')->exists($treatmentCategory->image)) 
                    <img class="preview" src="{{asset('storage/'.$treatmentCategory->image)}}" alt="" width="150px">
                        <a href="{{route('admin.treatmentcategory.remove_file_image',['id'=>$treatmentCategory->id])}}" class="confirm-delete text-danger">
                        <i class="menu-icon mdi mdi-close"></i>
                        </a>
                    @else
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                    <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                    @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
                    @endif
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="logo">Logo </label>                
                    @if(Storage::disk('public')->exists($treatmentCategory->logo)) 
                    <img class="preview" src="{{asset('storage/'.$treatmentCategory->logo)}}" alt="" width="150px">
                        <a href="{{route('admin.treatmentcategory.remove_logo_file',['id'=>$treatmentCategory->id])}}" class="confirm-delete text-danger">
                        <i class="menu-icon mdi mdi-close"></i>
                        </a>
                    @else
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">                
                    <input type="file" name="logo" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                    @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
                    @endif
                </div>  
                <div class="col-md-6">
                 <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $treatmentCategory->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>  
            </div> 
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
        $("#hospital_id").select2({
                placeholder: "Select Hospital",
                allowClear: true
            });
            $('#hospital-select').on('change', function () {   
                // alert($('#hospital-select').val())     
                var hospital_id = $('#hospital-select').val(); 
                $("#state-dropdown").html('');
                $.ajax({
                    url:"{{route('admin.doctor.search')}}",
                    type: "POST",
                    data: {
                        hospital_id: hospital_id,
                    _token: '{{csrf_token()}}' 
                    },
                dataType : 'json',
                success: function(result){
                    $('#department_id').empty();
                    $('#state-dropdown').html('<option value="">Select State</option>'); 
                    $.each(result.dept,function(key,value){ 
                        $('select[name="department_id[]"]').append('<option value="'+value.id+'">'+value.name+'</option');
                    });
                }
            }); 
        });
        });
</script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
     </script>
@endsection