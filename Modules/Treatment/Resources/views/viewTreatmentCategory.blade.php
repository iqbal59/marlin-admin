@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
       View Treatment Category
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <td>{{ $treatment->id }}</td>
                    </tr>
                    <tr>
                    <tr>
                        <th>Category Name</th>
                        <td>{{ $treatment->category_name }}</td>
                    </tr>
                    <tr>
                        <th>Hospital</th>
                        <td>{{$hospital->title}} </td>
                    </tr> 
                    @if($addedBy!= "")
                    <tr>
                        <th>Added By</th>
                        <td>{{ $addedBy->first_name }}</td>
                    </tr> 
                    @endif
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection