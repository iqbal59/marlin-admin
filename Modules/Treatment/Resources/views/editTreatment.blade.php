@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
}

.number-input {
    border: 2px solid #ddd;
    display: inline-flex;
}

.number-input,
.number-input * {
    box-sizing: border-box;
}

.number-input button {
    outline: none;
    -webkit-appearance: none;
    background-color: transparent;
    border: none;
    align-items: center;
    justify-content: center;
    width: 3rem;
    height: 2rem;
    cursor: pointer;
    margin: 0;
    position: relative;
}

.number-input button:before,
.number-input button:after {
    display: inline-block;
    position: absolute;
    content: '';
    width: 1rem;
    height: 2px;
    background-color: #212121;
    transform: translate(-50%, -50%);
}

.number-input button.plus:after {
    transform: translate(-50%, -50%) rotate(90deg);
}

.number-input input[type=number] {
    font-family: sans-serif;
    max-width: 3rem;
    padding: .5rem;
    border: solid #ddd;
    border-width: 0 2px;
    font-size: 1rem;
    height: 2rem;
    font-weight: bold;
    text-align: center;
}

i.mdi.mdi-minus {
    width: 2rem;
    text-align: center;
    line-height: 30px;
}

i.mdi.mdi-plus {
    width: 2rem;
    text-align: center;
    line-height: 30px;
}

span.on {
    width: 95px;
    float: left;
}

.custom-switch {
    padding-left: 0;
}

.custom-switch .custom-control-label::before {
    width: 2rem;
}

.custom-switch .custom-control-label::after {
    width: calc(1.3rem - 4px);
}

.badge-contr span.off {
    width: 70px;
    float: left;
}

.badge-contr span.on {
    float: none;
}

.treatment-edit-cn span.select2-selection.select2-selection--multiple:after {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 5px;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
    content: "";
}
</style>

<div class="card treatment-edit-cn">
    <div class="card-header">
        {{ trans('global.edit') }} Treatment
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatment.update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$treatment->id}}">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}<i
                        class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="name" class="form-control"
                    value="{{ old('name', isset($treatment) ? $treatment->name : '') }}" required>
                @if($errors->has('name'))
                <em class="invalid-feedback">
                    {{ $errors->first('name') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('treatment_plants_id') ? 'has-error' : '' }}">
                        <label for="treatment_plants_id"> Treatment Plans <i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <select class="js-states form-control" name="treatment_plants_id" id="treatment_plants_id">
                            @foreach($treatmentPlans as $data)
                            @if($data->id == $treatment->treatment_plants_id)
                            <option value="{{$data->id}}" name="treatment_plants_id" selected=""> {{$data->title}}
                            </option>
                            @else
                            <option value="{{$data->id}}" name="treatment_plants_id"> {{$data->title}} </option>
                            @endif
                            @endforeach
                        </select>
                        @if($errors->has('treatment_plants_id'))
                        <em class="invalid-feedback">{{ $errors->first('treatment_plants_id') }}</em>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('treatment_category_id') ? 'has-error' : '' }}">
                        <label for="treatment_category_id"> Treatment Catgory <i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <select id="treatment_category_id" name="treatment_category_id" class="js-states form-control">
                            @foreach ($treatmentCategory as $key => $value)
                            <option value="{{$value->id}}" {{(old('treatment_category_id', $treatment->
                                treatment_category_id) == $value->id ? 'selected' : '')}}> {{$value->category_name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>




            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('panel.title') }} </label>
                <input type="text" id="title" name="title" class="form-control"
                    value="{{ old('title', isset($treatment) ? $treatment->title : '') }}">
                @if($errors->has('title'))
                <em class="invalid-feedback">
                    {{ $errors->first('title') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description">
                {!! old('description', isset($treatment) ? $treatment->description : '') !!}
                </textarea>
                @if($errors->has('description'))
                <em class="invalid-feedback">
                    {{ $errors->first('description') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="image"> {{ trans('panel.image') }} </label>
                    @if(Storage::disk('public')->exists($treatment->image))
                    <img class="preview" src="{{asset('storage/'.$treatment->image)}}" alt="" width="150px">
                    <a href="{{route('admin.treatment.remove_file',['id'=>$treatment->id])}}"
                        class="confirm-delete text-danger">
                        <i class="menu-icon mdi mdi-close"></i>
                    </a>
                    @else
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="name">Tags</label>
                        <select id="diseases_id" name="diseases_id" class="js-states form-control">
                            @foreach ($tags as $key => $data)
                            <option value="{{$data['slug']}}" {{(old('diseases_id', $treatment->diseases_id) ==
                                $data['slug'] ? 'selected' : '')}}> {{$data['title']}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('options') ? 'has-error' : '' }}">
                        <label for="name">Options</label>
                        @if($selecteValue == null)
                        <select id="options" name="options[]" class="js-example-tokenizer" multiple="multiple">
                        </select>
                        @else
                        <select id="options" name="options[]" class="js-example-tokenizer" multiple="multiple">
                            @foreach($selecteValue as $data)
                            <option value="{{$data}}" selected>{{$data}}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $treatment->status == 1) ? "checked" : "" }}
                            class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div>
            </div>

            <br>

            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
$(document).ready(function() {
    $("#rating").select2({
        placeholder: "Select Rating",
        allowClear: true
    });
    $("#treatment_category_id").select2({
        placeholder: "Select Category",
        allowClear: true
    });
    $("#treatment_plants_id").select2({
        placeholder: "Treatment Plans",
        allowClear: true
    });
    $("#diseases_id").select2({
        placeholder: "Select Tag",
        allowClear: true
    });
});
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.ckeditor').ckeditor();
});
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<script>
$(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: [',', ' ']
})
</script>
@endsection