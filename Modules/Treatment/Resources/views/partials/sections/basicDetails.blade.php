<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="mb-2">
    <table class="table table-bordered table-striped video-table">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $treatment->id }}</td>
            </tr>
            <tr>
                <th>{{ trans('cruds.user.fields.name') }}</th>
                <td>{{ $treatment->name }}</td>
            </tr>
            <tr> 
                <th>Treatment Category</th>
                <td>{{ $treatment->category->category_name }}</td>
            </tr>
            @if($treatment->title !="")
            <tr>
                <th>Title</th>
                <td>{{ $treatment->title }}</td>
            </tr>
            @endif
            @if($treatment->description !="")
            <tr>
                <th>Description</th>
                <td>{!! $treatment->description !!} </td>
            </tr> 
            @endif
            @if($treatment->image !='')
                    <tr>
                        <th>
                       {{ trans('panel.image') }} 
                        </th>
                        <td>
                        <img src="{{asset('storage/'.$treatment->image)}}" alt="" width="500px">
                        </td>
                    </tr>
            @endif
            @if($treatment->logo !='')
                    <tr>
                        <th>
                       Logo
                        </th>
                        <td>
                        <img src="{{asset('storage/'.$treatment->logo)}}" alt="" width="500px">
                        </td>
                    </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif
        </tbody>
    </table>
</div>