<?php

use Illuminate\Http\Request;
use Modules\Treatment\Http\Controllers\Api\TreatmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/treatment', function (Request $request) {
    return $request->user();
});

Route::prefix('treatment')->group(function () {
    Route::get('/list', [TreatmentController::class, 'list']);
    Route::post('/view', [TreatmentController::class, 'view']);
    Route::post('/bestHospitalForTreatment', [TreatmentController::class, 'bestHospitalForTreatment']);
    Route::post('/bestDoctorForTreatment', [TreatmentController::class, 'bestDoctorForTreatment']);
    Route::post('/searchTreatment', [TreatmentController::class, 'searchTreatment']);
    Route::post('/treatmentEnquiry', [TreatmentController::class, 'treatmentEnquiry']);
    Route::post('/treatmentPlans', [TreatmentController::class, 'treatmentPlans']);

    Route::post('/treatmentDiseases', [TreatmentController::class, 'treatmentDiseases']);
    Route::post('/treatmentDiseasesDescription', [TreatmentController::class, 'treatmentDiseasesDescription']);


    Route::get('/allTreatmentFaq', [TreatmentController::class, 'allTreatmentFaq']);
    Route::post('/TreatmentFaq', [TreatmentController::class, 'TreatmentFaq']);
    Route::post('/treatmentAutocompleteSearch', [TreatmentController::class, 'treatmentAutocompleteSearch']);   

});