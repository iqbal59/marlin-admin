<?php

use Modules\Treatment\Http\Controllers\TreatmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'treatment', 'as' => 'treatment.'], function () {         
        Route::get('/listTreatment', [TreatmentController::class, 'listTreatment'])->name('listTreatment');
        Route::get('add', [TreatmentController::class, 'add'])->name('add');
        Route::post('save', [TreatmentController::class, 'save'])->name('save');
        Route::get('edit', [TreatmentController::class, 'edit'])->name('edit');
        Route::post('update', [TreatmentController::class, 'update'])->name('update');
        Route::get('view', [TreatmentController::class, 'view'])->name('view');
        Route::get('delete', [TreatmentController::class, 'delete'])->name('delete');
        Route::get('remove_file', [TreatmentController::class, 'removeFile'])->name('remove_file');
        Route::post('ckeditor_image_upload', [TreatmentController::class, 'ckeditor_image_upload'])->name('ckeditor_image_upload');
        Route::get('remove_logofile', [TreatmentController::class, 'removeLogo'])->name('remove_logofile');
      
    });
    Route::group(['prefix' => 'treatmentcategory', 'as' => 'treatmentcategory.'], function () { 
        Route::get('/listCategory', [TreatmentController::class, 'listCategory'])->name('listCategory');    
        Route::get('addCategory', [TreatmentController::class, 'addCategory'])->name('addCategory');
        Route::post('saveCategory', [TreatmentController::class, 'saveCategory'])->name('saveCategory');
        Route::get('categoryedit', [TreatmentController::class, 'categoryedit'])->name('categoryedit');
        Route::post('updateCategory', [TreatmentController::class, 'updateCategory'])->name('updateCategory');
        Route::get('viewCategory', [TreatmentController::class, 'viewCategory'])->name('viewCategory');
        Route::get('deleteCategory', [TreatmentController::class, 'deleteCategory'])->name('deleteCategory');
         Route::get('remove_file_image', [TreatmentController::class, 'removeFileImage'])->name('remove_file_image');
         Route::get('remove_logo_file', [TreatmentController::class, 'removeLogoFile'])->name('remove_logo_file');

         
    });
});

