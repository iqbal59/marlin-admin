<?php

namespace Modules\Treatment\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Treatment\Http\Requests\Api\DoctorViewRequest;
use Modules\Treatment\Http\Requests\Api\HospitalViewRequest;
use Modules\Treatment\Http\Requests\Api\TreatmentFaqRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentViewRequest;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Exception;
use Modules\TreatmentPlants\Repositories\TreatmentPlantsRepositoryInterface as TreatmentPlantsRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Symfony\Component\HttpFoundation\Request;
use Modules\Treatment\Models\Treatment;
use Modules\Enquiry\Models\Enquiry;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
use Illuminate\Support\Facades\Storage;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface as BlogsRepository;
use Modules\Treatment\Repositories\TreatmentCategoryRepositoryInterface as TreatmentCategoryRepository;

class TreatmentController extends Controller
{
    public function list(TreatmentRepository $treatmentRepo)
    {
        try {
            $treatments = $treatmentRepo->allTreatments();
            $departments = $treatments->map(function ($treatment, $key) {
                $treatment->department_id = json_decode($treatment->department_id);
                return $treatment;
            });
            $data = compact("treatments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function view(TreatmentViewRequest $request, TreatmentRepository $treatmentRepo, DepartmentRepository $deptRepo, TreatmentPlantsRepository $treatmentplansRepo)
    {
        try {
            $treatmentId = $request->treatment_id;
            if (!$treatmentId) {
                $response = ['status' => false, 'message' => 'The selected treatment id is invalid'];
                return response()->json($response, 200);
            }

            $treatmentPlans =  $treatmentplansRepo->get($treatmentId);
            $data = compact("treatmentPlans");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function bestHospitalForTreatment(HospitalViewRequest $request, HospitalRepository $hospitalRepo, TreatmentRepository $treatmentRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo, CityRepository $cityRepo)
    {
        try {
            $treatmentId = $request->treatment_id;
            $treatmentDetails = $treatmentRepo->get($treatmentId);
            if ($treatmentDetails->diseases_id != null) {
                $diseasesDetails = $diseaseRepo->getDiseases($treatmentDetails->diseases_id);
                if ($diseasesDetails == null) {
                    $diseasesDetails = $problemRepo->getProblems($treatmentDetails->diseases_id);
                    $bestHospital = $hospitalRepo->bestHospitalsForProblems($diseasesDetails->id);
                    $hospital = $bestHospital->map(function ($hospitals, $key) use ($cityRepo) {
                        $hospitals->countryId = $cityRepo->getCountry($hospitals->city_id);
                        return $hospitals;
                    });
                } else {
                    $bestHospital = $hospitalRepo->bestHospitals($diseasesDetails->id);
                    $hospital = $bestHospital->map(function ($hospitals, $key) use ($cityRepo) {
                        $hospitals->countryId = $cityRepo->getCountry($hospitals->city_id);
                        return $hospitals;
                    });
                }
            } else {
                $response = ['status' => false, 'message' => 'No hospital available for this diseases'];
                return response()->json($response, 200);
            }
            $data = compact("bestHospital");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function bestDoctorForTreatment(DoctorViewRequest $request, DoctorRepository $doctorRepo, TreatmentRepository $treatmentRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo)
    {
        try {
            $treatmentId = $request->treatment_id;
            $treatmentDetails = $treatmentRepo->get($treatmentId);
            if ($treatmentDetails->diseases_id != null) {
                $diseasesDetails = $diseaseRepo->getDiseases($treatmentDetails->diseases_id);
                if ($diseasesDetails == null) {
                    $diseasesDetails = $problemRepo->getProblems($treatmentDetails->diseases_id);
                    $bestDoctor = $doctorRepo->bestDoctorForProblems($diseasesDetails->id);
                } else {
                    $bestDoctor = $doctorRepo->bestDoctors($diseasesDetails->id);
                }
            } else {
                $response = ['status' => false, 'message' => 'No Doctor available for this diseases'];
                return response()->json($response, 200);
            }
            $data = compact("bestDoctor");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function searchTreatment(Request $request, TreatmentRepository $treatmentRepo, TreatmentCategoryRepository $treatmentCategoryRepo)
    {
        try {
            if ($request->tab_id == 3) {
                $searchText = $request->search_text;
                if ($searchText != null) {
                    $treatmentsCategory = $treatmentCategoryRepo->getsearchBasedTreatmentCategory($searchText);
                    $details = $treatmentsCategory->map(function ($category, $key) use ($treatmentRepo) {
                        $treatments = $treatmentRepo->getallTreatmentBasedOnCategory($category->id);
                        $category->treatments = $treatments;
                        return $category;
                    });
                } else {
                    $treatmentsCategory = $treatmentCategoryRepo->all();
                    $details = $treatmentsCategory->map(function ($category, $key) use ($treatmentRepo) {
                        $treatments = $treatmentRepo->getallTreatmentBasedOnCategory($category->id);
                        $category->treatments = $treatments;
                        return $category;
                    });
                }
                $data = compact("treatmentsCategory");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function treatmentEnquiry(EnquiryAddRequest $request, Enquiry $enquiry)
    {
        try {
            $fileUrl=$request->hasFile('file') ? Storage::disk('public')->putFile('treatment_api_file/', $request->file('file')) : "";
            $treatment = Treatment::find($request->treatment_id);
            if ($treatment != null) {
                $enquiry = new Enquiry();
                $enquiry->enquiryable_id  = $request->treatment_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $enquiry->file  = $fileUrl;
                $enquiry->country_code  = $request->country_code;
                $enquiry->diseases_id  = $request->diseases_id;
                $treatment->enquiries()->save($enquiry);

                $response = ['status' => true,'message' => 'Treatment Enquiry Added Successfully'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function treatmentPlans(TreatmentViewRequest $request, TreatmentPlantsRepository $treatmentPlansRepo, TreatmentRepository $treatmentRepo)
    {
        try {
            $treatmentID = $request->treatment_id;
            if (is_numeric($treatmentID)) {
                $treatmentDetails = $treatmentRepo->get($treatmentID);
            } else {
                $treatmentDetails = $treatmentRepo->getBySlug($treatmentID);
            }
            $tratmentPlans = $treatmentPlansRepo->get($treatmentDetails->treatment_plants_id);
            if ($tratmentPlans != null) {
                $data = compact("tratmentPlans");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'data' => 'No data Found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function treatmentDiseases(TreatmentViewRequest $request, TreatmentRepository $treatmentRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo)
    {
        try {
            $treatmentID = $request->treatment_id;
            $diseasesSlug = $treatmentRepo->get($treatmentID);
            if ($diseasesSlug != null) {
                $diseases = $diseaseRepo->getDiseases($diseasesSlug->diseases_id);
                $treatments = $treatmentRepo->getActiveTreatment($treatmentID);
                if ($diseases == null) {
                    $diseases = $problemRepo->getProblems($diseasesSlug->diseases_id);
                    $treatments = $treatmentRepo->getActiveTreatment($treatmentID);
                }
                if ($diseases != null) {
                    $diseases->image = $treatments->image;
                    if ($diseases->description == null) {
                        $diseases->description='';
                    }
                } else {
                    $diseases=[];
                }
                $data = compact("diseases");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'data' => 'No data Found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function treatmentDiseasesDescription(TreatmentViewRequest $request, TreatmentRepository $treatmentRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo)
    {
        try {
            $treatmentID = $request->treatment_id;
            $treatments = $treatmentRepo->get($treatmentID);
            $data = compact("treatments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }

        // try {
        //     $treatmentID = $request->treatment_id;
        //     $diseasesSlug = $treatmentRepo->getActiveTreatment($treatmentID);
        //     $diseases = $diseaseRepo->getDiseases($diseasesSlug->diseases_id);
        //     if($diseases == null){
        //         $diseases = $problemRepo->getProblems($diseasesSlug->diseases_id);
        //     }
        //     $description = $diseases->description;
        //     $data = compact("description");
        //     $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
        //     return response()->json($response, 200);
        // } catch (Exception $e) {
        //     $response = ['status' => false, 'message' => $e->getMessage()];
        //     return response()->json($response, 200);
        // }
    }

    public function allTreatmentFaq(FaqRepository $faqRepo)
    {
        try {
            $allTreatmentFaq = $faqRepo->getAllFaq($askable_type="Treatment");
            $data = compact("allTreatmentFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function TreatmentFaq(TreatmentFaqRequest $request, FaqRepository $faqRepo)
    {
        try {
            $treatmentId = $request->treatment_id;
            $treatmentFaq = $faqRepo->getFaq($treatmentId, $askable_type="Treatment");
            $data = compact("treatmentFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function treatmentAutocompleteSearch(Request $request, TreatmentRepository $treatmentRepo, DepartmentRepository $deptRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepo, BlogsRepository $blogRepo)
    {
        try {
            $searchText = $request->search_text;
            $treatments = $treatmentRepo->searchTreatments($searchText);
            if (count($treatments) == 0) {/**department based */
                $departmentID = $deptRepo->searchBasedDepartment($searchText);
                $treatments = $treatmentRepo->getDepartmentBasedTreatment($departmentID);
            }
            if (count($treatments) == 0) {/**diseases based */
                $diseasesId = $diseasesRepo->searchBasedDiseases($searchText);
                $diseases = $diseasesRepo->diseases($diseasesId);
                if ($diseases != null) {
                    $treatments = $treatmentRepo->slugBasedTreatments($diseases);
                }
            }
            if (count($treatments) == 0) { /**problems based */
                $problemsId = $problemsRepo->searchBasedProblems($searchText);
                $problems = $problemsRepo->problems($problemsId);
                if ($problems != null) {
                    $treatments = $treatmentRepo->slugBasedTreatments($problems);
                }
            }
            $treatmentsDetails = $treatments->map(function ($treatment, $key) {
                if ($treatment->options != null) {
                    $treatment->options = json_decode($treatment->options);
                } else {
                    $treatment->options =[];
                }
                $treatment->department_id = json_decode($treatment->department_id);
                return $treatment;
            });
            $recentBlogs = $blogRepo->getRecentBlogs();
            $response = ['status' => true, 'treatments' => $treatments,'recentBlogs' => $recentBlogs, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}