<?php

namespace Modules\Treatment\Http\Controllers\Includes;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Treatment\Http\Requests\Treatment\TreatmentAddRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentSaveRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentEditRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentUpdateRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentViewRequest;
use Modules\Treatment\Http\Requests\Treatment\TreatmentDeleteRequest;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\TreatmentPlants\Repositories\TreatmentPlantsRepositoryInterface as TreatmentPlantsRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Enquiry\Repositories\EnquiryRepositoryInterface as EnquiryRepository;
use Illuminate\Support\Facades\Storage;
use Modules\Department\Repositories\DepartmentRepositoryInterface;
use Intervention\Image\ImageManagerStatic as Image;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
use Modules\Treatment\Repositories\TreatmentCategoryRepositoryInterface as TreatmentCategoryRepository;

trait Treatment
{
    public function listTreatment(TreatmentRepository $treatmentRepo, DepartmentRepository $deptRepo)
    {
        $treatments = $treatmentRepo->getForDatatable();
        $department = $deptRepo->all();
        return view('treatment::listTreatment', compact('treatments', 'department'));
    }

    public function add(TreatmentAddRequest $request, TreatmentCategoryRepository $treatmentRepo)
    {
        $category = $treatmentRepo->all();
        return view('treatment::addTreatment', compact('category'));
    }

    public function save(TreatmentSaveRequest $request, TreatmentRepository $treatmentRepo)
    {
        $inputData = [
            'treatment_category_id'  =>$request->category_id,
            'treatment_plants_id'=>$request->treatment_plants_id,
            'name'  => $request->name,
            'title'  => $request->title,
            'description'  => $request->description,
            'added_by'     => auth()->user()->id,
        ];

        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(380, 270);
            $image_resize->save(storage_path('app/public/treatmentimage/' .$filename));
            $inputData['image'] = 'treatmentimage/' .$filename;
        }
        if ($request->hasFile('logo')) {
            $image1       = $request->file('logo');
            $filename1    = $image1->getClientOriginalName();
            $image_resize1 = Image::make($image1->getRealPath());
            $image_resize1->resize(70, 70);
            $image_resize1->save(storage_path('app/public/treatmentlogo/' .$filename1));
            $inputData['logo'] = 'treatmentlogo/' .$filename1;
        }

        $treatment = $treatmentRepo->save($inputData);
        connectify('success', 'Success!', 'Treatment added successfully');
        return redirect()
            ->route('admin.treatment.listTreatment');
    }

    public function view(EnquiryRepository $enquiryRepo, TreatmentViewRequest $request, TreatmentRepository $treatmentRepo, DepartmentRepository $deptRepo, FaqRepository $faqRepo, UserRepository $userRepo)
    {
        $treatment = $treatmentRepo->get($request->id);
        $dept = $deptRepo->get(json_decode($treatment->department_id));
        $faqs = $faqRepo->getFaq($request->id, $type="Treatment");
        $enquiry = $enquiryRepo->getEnquiry($request->id, $type="Modules\Treatment\Models\Treatment");
        $addedBy = $userRepo->get($treatment->added_by);
        return view('treatment::viewTreatment', compact('addedBy', 'enquiry', 'treatment', 'dept', 'faqs'));
    }

    public function edit(TreatmentEditRequest $request, TreatmentRepository $treatmentRepo, TreatmentCategoryRepository $treatmentcategoryRepo, TreatmentPlantsRepository $treatmentPlanstsRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepo)
    {
        $treatment = $treatmentRepo->get($request->id);
        $treatmentCategory = $treatmentcategoryRepo->all();
        $treatmentPlans = $treatmentPlanstsRepo->all();

        $diseases = $diseasesRepo->getAll()->toArray();
        $problems = $problemsRepo->all()->toArray();
        $tags = array_merge($diseases, $problems);
        $selectedDepartment = json_decode($treatment->department_id);

        $selecteValue = json_decode($treatment->options);
        return view('treatment::editTreatment', compact('selecteValue', 'selectedDepartment', 'tags', 'treatmentPlans', 'treatment', 'treatmentCategory'));
    }

    public function update(TreatmentUpdateRequest $request, TreatmentRepository $treatmentRepo)
    {
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'title'  => $request->title,
            'description'  => $request->description,
            'treatment_category_id'=>$request->treatment_category_id,
            'treatment_plants_id'=>$request->treatment_plants_id,
            'diseases_id' => $request->diseases_id,
            'options'=>$request->options,
        ];

        if (isset($request->status)) {
            $inputData['status']=($request->status == "on") ? 1 : 0;
        } else {
            $inputData['status']='0';
        }

        if ($request->hasFile('image')) {
            $this->_removeFile($treatmentRepo, $request->id);
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(380, 270);
            $image_resize->save(storage_path('app/public/treatmentimage/' .$filename));
            $inputData['image'] = 'treatmentimage/' .$filename;
        }

        if ($request->hasFile('logo')) {
            $this->_removeLogo($treatmentRepo, $request->id);
            $image       = $request->file('logo');
            $filename1    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(70, 70);
            $image_resize->save(storage_path('app/public/treatmentlogo/' .$filename1));
            $inputData['logo'] ='treatmentlogo/' .$filename1;
        }

        $treatment = $treatmentRepo->update($inputData);
        connectify('success', 'Success!', 'Treatment updated successfully');
        return redirect()
            ->route('admin.treatment.listTreatment');
    }

    public function removeFile(TreatmentRepository $treatmentRepo, Request $request)
    {
        if ($this->_removeFile($treatmentRepo, $request->id)) {
            $treatmentRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');
        return redirect()
            ->route('admin.treatment.edit', ['id' => $request->id]);
    }

    private function _removeFile($treatmentRepo, $id)
    {
        $logo = $treatmentRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }

    public function removeLogo(TreatmentRepository $treatmentRepo, Request $request)
    {
        if ($this->_removeLogo($treatmentRepo, $request->id)) {
            $treatmentRepo->resetLogo($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');
        return redirect()
            ->route('admin.treatment.edit', ['id' => $request->id]);
    }

    private function _removeLogo($treatmentRepo, $id)
    {
        $logo = $treatmentRepo->get($id);
        if (Storage::disk('public')->delete($logo->logo)) {
            return true;
        }
        return false;
    }

    public function delete(TreatmentRepository $treatmentRepo, TreatmentDeleteRequest $request)
    {
        if ($treatmentRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Treatment deleted successfully');
            return redirect()
            ->route('admin.treatment.listTreatment');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function ckeditor_image_upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image successfully uploaded';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}