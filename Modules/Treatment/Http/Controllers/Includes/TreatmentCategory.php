<?php

namespace Modules\Treatment\Http\Controllers\Includes;

use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategoryAddRequest;
use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategorySaveRequest;
use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategoryEditRequest;
use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategoryUpdateRequest;
use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategoryViewRequest;
use Modules\Treatment\Http\Requests\TreatmentCategory\TreatmentCategoryDeleteRequest;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Treatment\Repositories\TreatmentCategoryRepositoryInterface as TreatmentCategoryRepository;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

trait TreatmentCategory
{
    public function listCategory(TreatmentCategoryRepository $treatmentRepo)
    {  
        $treatments = $treatmentRepo->getForDatatable(); 
        return view('treatment::listTreatmentCategory', compact('treatments'));                         
    }

    public function addCategory(TreatmentCategoryAddRequest $request , TreatmentCategoryRepository $treatmentRepo, HospitalRepository $hospitalRepo)
    { 
        $hospitals = $hospitalRepo->all();    
        return view('treatment::addTreatmentCategory', compact('hospitals'));               
    }

    public function saveCategory(TreatmentCategorySaveRequest $request, TreatmentCategoryRepository $treatmentRepo)
    {   
        $inputData = [
            'department_id'=>json_encode($request->department_id),
            'hospital_id' => json_encode($request->hospital_ids),
            'category_name'=>$request->name,
            'added_by'     => auth()->user()->id,    
        ];
        if($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(380, 270); 
            $image_resize->save(storage_path('app/public/treatmentimage/' .$filename)); 
            $inputData['image'] = 'treatmentimage/' .$filename;           
        }
        if($request->hasFile('logo')) {
            $image1       = $request->file('logo');
            $filename1    = $image1->getClientOriginalName();
            $image_resize1 = Image::make($image1->getRealPath());              
            $image_resize1->resize(70, 70); 
            $image_resize1->save(storage_path('app/public/treatmentlogo/' .$filename1));
            $inputData['logo'] = 'treatmentlogo/' .$filename1;                    
        }
        $treatment = $treatmentRepo->save($inputData);    
        connectify('success', 'Success!', 'Treatment Category added successfully');    
        return redirect()
            ->route('admin.treatmentcategory.listCategory');
    } 

    public function categoryedit(TreatmentCategoryEditRequest $request,DepartmentRepository $deptRepo, TreatmentCategoryRepository $treatmentRepo,HospitalRepository $hospitalRepo)
    {   
        $treatmentCategory =$treatmentRepo->get($request->id); 
        $hospitals = $hospitalRepo->all();

        $selectedHospitals = json_decode($treatmentCategory->hospital_id);
        $selectedDepartments = json_decode($treatmentCategory->department_id);
        $hospitalDepartments = $deptRepo->deptData($selectedHospitals);
        return view('treatment::editTreatmentCategory', compact('hospitalDepartments','treatmentCategory','hospitals','selectedHospitals','selectedDepartments'));
    }
    public function updateCategory(TreatmentCategoryUpdateRequest $request, TreatmentCategoryRepository $treatmentRepo)
   
    {  
        $inputData = [
            'id'  => $request->id,
            'category_name'  => $request->category_name,
            'department_id' => json_encode($request->department_id),
            'hospital_id'=>json_encode($request->hospital_ids),
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }

        if ($request->hasFile('image')) { 
            $this->_removeFile($treatmentRepo, $request->id);                 
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(380, 270); 
            $image_resize->save(storage_path('app/public/treatmentimage/' .$filename));         
            $inputData['image'] = 'treatmentimage/' .$filename;     
        }   
        
        if ($request->hasFile('logo')) { 
            $this->_removeLogo($treatmentRepo, $request->id);                 
            $image       = $request->file('logo');
            $filename1    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(70, 70);  
            $image_resize->save(storage_path('app/public/treatmentlogo/' .$filename1)); 
            $inputData['logo'] ='treatmentlogo/' .$filename1;   
        }   
        
        $treatment = $treatmentRepo->update($inputData);
        connectify('success', 'Success!', 'Treatment Category updated successfully');    
        return redirect()
            ->route('admin.treatmentcategory.listCategory');
    } 

    public function viewCategory(TreatmentCategoryViewRequest $request,TreatmentCategoryRepository $treatmentRepo,HospitalRepository $hospitalRepo,UserRepository $userRepo)
    {
        $treatment = $treatmentRepo->get($request->id); 
        $hospital = $hospitalRepo->get($treatment->hospital_id);
        $addedBy = $userRepo->get($treatment->added_by);   
        return view('treatment::viewTreatmentCategory', compact('treatment','hospital','addedBy'));
    }

    public function deleteCategory(TreatmentCategoryRepository $treatmentRepo, TreatmentCategoryDeleteRequest $request)
    { 
        if ($treatmentRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Treatment deleted successfully');    
            return redirect()
            ->route('admin.treatmentcategory.listCategory');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function removeFileImage(TreatmentCategoryRepository $treatmentCategoryRepo,Request $request)
    {  
        if ($this->_removeFileImage($treatmentCategoryRepo, $request->id)) {
            $treatmentCategoryRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');    
        return redirect()
            ->route('admin.treatmentcategory.categoryedit', ['id' => $request->id]);
    }

    private function _removeFileImage($treatmentCategoryRepo, $id)
    { 
        $logo = $treatmentCategoryRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }

    public function removeLogoFile(TreatmentCategoryRepository $treatmentCategoryRepo,Request $request)
    {  
        if ($this->_removeLogoFile($treatmentCategoryRepo, $request->id)) {
            $treatmentCategoryRepo->resetLogo($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');    
        return redirect()
            ->route('admin.treatmentcategory.categoryedit', ['id' => $request->id]);
    }

    private function _removeLogoFile($treatmentCategoryRepo, $id)
    { 
        $logo = $treatmentCategoryRepo->get($id);
        if (Storage::disk('public')->delete($logo->logo)) {
            return true;
        }
        return false;
    }
    

}