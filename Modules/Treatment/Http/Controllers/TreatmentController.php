<?php

namespace Modules\Treatment\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Treatment\Http\Controllers\Includes\Treatment;
use Modules\Treatment\Http\Controllers\Includes\TreatmentCategory;
class TreatmentController extends Controller
{
    use Treatment;
    use TreatmentCategory;
}
