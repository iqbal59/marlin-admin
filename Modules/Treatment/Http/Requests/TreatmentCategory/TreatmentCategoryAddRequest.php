<?php

namespace Modules\Treatment\Http\Requests\TreatmentCategory;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCategoryAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_category_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}