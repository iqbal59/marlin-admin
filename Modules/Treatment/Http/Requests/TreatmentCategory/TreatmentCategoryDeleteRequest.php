<?php

namespace Modules\Treatment\Http\Requests\TreatmentCategory;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCategoryDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_category_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
