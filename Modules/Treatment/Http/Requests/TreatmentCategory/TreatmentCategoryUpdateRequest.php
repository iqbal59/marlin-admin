<?php

namespace Modules\Treatment\Http\Requests\TreatmentCategory;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCategoryUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'category_name' => 'required|max:50|min:2',
            'hospital_ids' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'category_name.required' => ':attribute is required',
            'hospital_ids.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'category_name' => 'Category Name',
            'hospital_ids' => 'Hospital',
            'hospital_ids.*' => 'Hospital',
        ];
    }
}
