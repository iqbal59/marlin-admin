<?php

namespace Modules\Treatment\Http\Requests\TreatmentCategory;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCategoryEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
