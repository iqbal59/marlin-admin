<?php

namespace Modules\Treatment\Http\Requests\TreatmentCategory;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentCategorySaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_category_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            'hospital_ids' => 'required',
            // 'department_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'hospital_ids.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'hospital_ids' => 'Hospital',
            'hospital_ids.*' => 'Hospital',
        ];
    }
}
