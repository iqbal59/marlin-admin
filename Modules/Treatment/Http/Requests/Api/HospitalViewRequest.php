<?php

namespace Modules\Treatment\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class HospitalViewRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'treatment_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'treatment_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'treatment_id' => 'Treatment Id',
        ];
    }
}
