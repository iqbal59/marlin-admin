<?php

namespace Modules\Treatment\Http\Requests\Treatment;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
