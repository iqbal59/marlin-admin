<?php

namespace Modules\Treatment\Http\Requests\Treatment;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
