<?php

namespace Modules\Treatment\Http\Requests\Treatment;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            'treatment_category_id' => 'required',
            'treatment_plants_id' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'treatment_category_id.required' => ':attribute is required',
            'treatment_plants_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'treatment_category_id' => 'Treatment Category',
            'treatment_plants_id' => 'Treatment Plans',
        ];
    }
}
