<?php

namespace Modules\Treatment\Http\Requests\Treatment;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatment_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            // 'treatment_plants_id'=>'required',
            'category_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            // 'treatment_plants_id.required' => ':attribute is required',
            'category_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            // 'treatment_plants_id' => 'Treatment Plans',
            'category_id' => 'Catgory',
        ];
    }
}
