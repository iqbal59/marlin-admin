<?php

namespace Modules\Treatment\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TreatmentCategory extends Model
{
    use HasFactory;
    protected $fillable = ['id','hospital_id','category_name','status','added_by'];
}
