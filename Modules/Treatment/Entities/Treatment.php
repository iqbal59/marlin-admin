<?php

namespace Modules\Treatment\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\TreatmentDetails\Entities\TreatmentDetails;

class Treatment extends Model
{
    use HasFactory;
    use Sluggable;


    protected $fillable = ['id','slug','diseases_id','treatment_category_id','name','title','description','image','logo','status','options','added_by'];

    public function treatmentDetails()
    {
        return $this->hasMany(TreatmentDetails::class, 'treatment_slug', 'slug');
    }

    protected static function newFactory()
    {
        return \Modules\Treatment\Database\factories\TreatmentFactory::new();
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}