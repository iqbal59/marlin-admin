<?php


namespace Modules\Faq\Repositories;
use Modules\Faq\Models\Faq;
use Illuminate\Database\Eloquent\Builder;

class FaqRepository implements FaqRepositoryInterface
{
    public function all()
    { 
        return Faq::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($faq =  Faq::create($input)) {
            return $faq;
        }
        return false;
    }

    public function get($id)
    { 
        return Faq::where('id',$id)->first();
    }

    public function update(array $input)
    { 
        $faq = Faq::find($input['id']); 
        unset($input['id']);
        if ($faq->update($input)) {
            return $faq;
        }
        return false;
    }

    public function delete(string $id)
    {
        $faq = Faq::find($id);
        return $faq->delete();
    }

    public function getTypeBasedFaq($type)
    {
        $faq = Faq::where('askable_type',$type)->take(4)->get();
        return $faq;
    }

    public function getFaq($id, $type)
    {
        $faq = Faq::where('askable_id',$id)->where('askable_type',$type)->get();
        return $faq;
    }

    public function getAllFaq($askable_type)
    { 
        $faq = Faq::where('askable_type',$askable_type)->take(5)->get();
        return $faq;
    }

    public function getAllTypeOfFaq()
    { 
        return Faq::take(5)->get();
    }
}