<?php

namespace Modules\Faq\Repositories;

interface FaqRepositoryInterface
{

    public function all();

    public function save(array $input);

    public function get($id);

    public function update(array $input);

    public function delete(string $id);

    public function getTypeBasedFaq($type);

    public function getFaq($id, $type);

    public function getAllFaq($askable_type);

    public function getAllTypeOfFaq();
    
}