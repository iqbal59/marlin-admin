@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
    Add Faq
        
    </div>

    <div class="card-body">
        <form action="{{ route("admin.faq.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
    <div class="row">

        <input type="hidden" value="{{!empty($is_Id) ? $is_Id : ''}}" name="is_Id">
        <input type="hidden" value="{{!empty($is_Type) ? $is_Type : ''}}" name="is_Type">
        <input type="hidden" value="{{!empty($retrn_url) ? $retrn_url : ''}}" name="retrn_url">
        <input type="hidden" value="{{!empty($retrn_prms) ? $retrn_prms : ''}}" name="retrn_prms">


        <div class="col-6">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
        </div>  
        <div class="col-6">
            <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                <label for="subject">Subject <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="subject" name="subject" class="form-control" value="{{old('subject')}}" >
                @if($errors->has('subject'))
                    <em class="invalid-feedback">
                        {{ $errors->first('subject') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                <label for="question">Question <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="question" name="question" class="form-control" value="{{old('question')}}" >
                @if($errors->has('question'))
                    <em class="invalid-feedback">
                        {{ $errors->first('question') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div>
        <div class="col-6">
            <div class="form-group {{ $errors->has('answerBy') ? 'has-error' : '' }}">
                <label for="answerBy">AnswerBy <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="answerBy" name="answerBy" class="form-control" value="{{old('answerBy')}}" >
                @if($errors->has('answerBy'))
                    <em class="invalid-feedback">
                        {{ $errors->first('answerBy') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group {{ $errors->has('solution') ? 'has-error' : '' }}">
                <label for="solution">Solution <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="solution" name="solution" class="form-control" value="{{old('solution')}}" >
                @if($errors->has('solution'))
                    <em class="invalid-feedback">
                        {{ $errors->first('solution') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div>
        <div class="col-6">
            <div class="form-group {{ $errors->has('entrydate') ? 'has-error' : '' }}">
                <label for="entrydate">Entry Date <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="date" id="entrydate" name="entrydate" class="form-control" value="{{old('entrydate')}}" >
                @if($errors->has('entrydate'))
                    <em class="invalid-feedback">
                        {{ $errors->first('entrydate') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div>
    </div>  
    @if($is_Type == "")
    <div class="row">
        <div class="col-6">
            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                <label for="type">Type <i class="mdi mdi-flag-checkered text-danger "></i></label>
                    <select id="type" name="type" class="js-states form-control">
                        <option value="" name="" >Select</option>
                        <option value="Hospital" name="Hospital" >Hospital</option>
                        <option value="Department" name="Department" >Department</option>
                        <option value="Doctor" name="Doctor" >Doctor</option>
                        <option value="Treatment" name="Treatment" >Treatment</option>
                    </select>  
                    @if($errors->has('type'))
                    <em class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </em>
                @endif             
            </div> 
        </div>    
        <div class="col-6">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> Name<i class="mdi mdi-flag-checkered text-danger "></i></label>           <div id="sucess-msg">     
                    <select id="faq_type_id" name="faq_type_id" class="js-states form-control">
                        <option value="">{{ trans('panel.select') }}</option>
                    </select></div>
                    @if($errors->has('faq_type_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('faq_type_id') }}
                    </em>
                    @endif 
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>  
    </div>
    @endif
    <div>
        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
    </div>
</form>
    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Department",
                allowClear: true
            });
            $("#type").select2({
                placeholder: "Select Type",
                allowClear: true
            });
            $("#faq_type_id").select2({
                placeholder: "Select Name",
                allowClear: true
            });
             
        });
        $('select[name="type"]').on('change', function () {        
                var type = this.value;
                $("#state-dropdown").html('');
                $.ajax({
                    url:"{{route('admin.faq.search')}}",
                    type: "POST",
                    data: {
                    type: type,
                    _token: '{{csrf_token()}}' 
                    },
                dataType : 'json',
                success: function(result){
                    $('#faq_type_id').empty();
                    $('#state-dropdown').html('<option value="">Select</option>'); 
                    if(type == "Hospital"){
                        $.each(result.states,function(key,value,type){  
                        $('select[name="faq_type_id"]').append('<option value="'+value.id+'">'+value.title+'</option');
                    });

                    }else{
                        $.each(result.states,function(key,value,type){  
                        $('select[name="faq_type_id"]').append('<option value="'+value.id+'">'+value.name+'</option');
                        });
                    }
                    
                }
            });
        });
</script>
@endsection
