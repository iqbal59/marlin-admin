@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
         View Faq
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr> 
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $faq->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            {{ $faq->title }}
                        </td>
                    </tr>
                    <tr>

                        <th>
                            Subject
                        </th>
                        <td> 
                            {{ $faq->subject }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Questions
                        </th>
                        <td>
                            {{ $faq->question }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Type
                        </th>
                        <td>
                            @if($faq->type == 1)
                            Hospital
                            @elseif($faq->type == 2)
                            Doctor
                            @else
                            Department
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Answer By
                        </th>
                        <td>
                            {{ $faq->answerBy }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Solution
                        </th>
                        <td>
                            {{ $faq->solution }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Entry Date
                        </th>
                        <td>
                            {{ $faq->entrydate }}
                        </td>
                    </tr>                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection