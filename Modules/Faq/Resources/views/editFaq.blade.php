@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Edit Faq
    </div>

    <div class="card-body">
        <form action="{{ route("admin.faq.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$faq->id}}">
            <input type="hidden" name="retrn_url" value="{{$retrn_url}}">
            <input type="hidden" name="retrn_prms" value="{{$retrn_prms}}">

            <div class="row">
                <div class="col-6">        
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title">Title <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($faq) ? $faq->title : '') }}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-6">   
                    <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                        <label for="subject">Subject <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="subject" name="subject" class="form-control" value="{{ old('subject', isset($faq) ? $faq->subject : '') }}" >
                        @if($errors->has('subject'))
                            <em class="invalid-feedback">
                                {{ $errors->first('subject') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-6"> 
                    <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                        <label for="question">Question <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="question" name="question" class="form-control" value="{{ old('question', isset($faq) ? $faq->question : '') }}" >
                        @if($errors->has('question'))
                            <em class="invalid-feedback">
                                {{ $errors->first('question') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div> 
                <div class="col-6">     
                    <div class="form-group {{ $errors->has('answerBy') ? 'has-error' : '' }}">
                        <label for="answerBy">Answer By <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="answerBy" name="answerBy" class="form-control" value="{{ old('answerBy', isset($faq) ? $faq->answerBy : '') }}" >
                        @if($errors->has('answerBy'))
                            <em class="invalid-feedback">
                                {{ $errors->first('answerBy') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div> 
            <div class="row">
                <div class="col-6">         
                    <div class="form-group {{ $errors->has('solution') ? 'has-error' : '' }}">
                        <label for="solution">Solution <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="solution" name="solution" class="form-control" value="{{ old('solution', isset($faq) ? $faq->solution : '') }}" >
                        @if($errors->has('solution'))
                            <em class="invalid-feedback">
                                {{ $errors->first('solution') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-6">         
                    <div class="form-group {{ $errors->has('entrydate') ? 'has-error' : '' }}">
                        <label for="entrydate">Entry Date <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="date" id="entrydate" name="entrydate" class="form-control" value="{{ old('entrydate', isset($faq) ? $faq->entrydate : '') }}" >
                        @if($errors->has('entrydate'))
                            <em class="invalid-feedback">
                                {{ $errors->first('entrydate') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                        </div>            
                    
                    <div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-6">  
                    <div class="form-group {{ $errors->has('hospital_id') ? 'has-error' : '' }}">
                        <label for="department"> Type<i class="mdi mdi-flag-checkered text-danger "></i>
                        </label>
                        <select id="type" name="type" class="js-states form-control">
                            <option value="1" {{ old('type', $faq->type) == 1 ? "selected" : "" }}>Hospital</option>
                            <option value="2" {{ old('type', $faq->type) == 2 ? "selected" : "" }}>Doctor</option>
                            <option value="3" {{ old('type', $faq->type) == 3 ? "selected" : "" }}>Department</option>
                        </select>            
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div> --}}
        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
        </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#type").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
        });
</script>
@endsection