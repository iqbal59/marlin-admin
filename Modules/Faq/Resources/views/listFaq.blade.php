@extends('layouts.admin')
@section('content')
@can('faq_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{route('admin.faq.add')}}">
                <i class="mdi mdi-plus"></i> 
               Add Faq
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
       List Faq
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            Title
                        </th>

                        <th>
                            Subject
                        </th>

                        {{-- <th>
                           Question
                        </th>

                        <th>
                           AnswerBy
                        </th>

                        <th>
                           Solution
                        </th>

                        <th>
                           Entry Date
                        </th> --}}
                        
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($faqs as $key => $faq)
                        <tr data-entry-id="{{ $faq->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $faq->title ?? '' }}
                            </td>

                            <td>
                                {{ $faq->subject ?? '' }}
                            </td>

                            {{-- <td>
                                {{ $faq->question ?? '' }}
                            </td>

                            <td>
                                {{ $faq->answerBy ?? '' }}
                            </td>
                            
                            <td>
                                {{ $faq->solution ?? '' }}
                            </td>
                            <td>
                                {{ $faq->entrydate ?? '' }}
                            </td> --}}
                            
                           
                            <td>
                              
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.faq.view', ['id' => $faq->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                             

                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.faq.edit', ['id' => $faq->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             

                                
                                    <form action="{{ route('admin.faq.delete',  ['id' => $faq->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$faq->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 10,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection