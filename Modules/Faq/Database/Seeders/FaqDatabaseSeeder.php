<?php

namespace Modules\Faq\Database\Seeders;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FaqDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '116',
                'title'      => 'faq_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '117',
                'title'      => 'faq_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '118',
                'title'      =>  'faq_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '119',
                'title'      => 'faq_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '200',
                'title'      => 'faq_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],

        ];

        Permission::insert($permissions);
    }
}
