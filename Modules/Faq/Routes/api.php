<?php

use Illuminate\Http\Request;
use Modules\Faq\Http\Controllers\Api\FaqController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/faq', function (Request $request) {
    return $request->user();
});

Route::prefix('faq')->group(function () {
    Route::get('/listFaq', [FaqController::class, 'listFaq']);
    Route::post('/view', [FaqController::class, 'view']);

});