<?php
use Modules\Faq\Http\Controllers\FaqController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('faq')->group(function() {
    Route::get('/', 'FaqController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'faq', 'as' => 'faq.'], function () {         
        Route::get('/', [FaqController::class, 'list'])->name('list');
        Route::get('add', [FaqController::class, 'add'])->name('add');
        Route::post('save', [FaqController::class, 'save'])->name('save');
        Route::get('edit', [FaqController::class, 'edit'])->name('edit');
        Route::post('update', [FaqController::class, 'update'])->name('update');
        Route::get('view', [FaqController::class, 'view'])->name('view');
        Route::get('delete', [FaqController::class, 'delete']) ->name('delete');
        Route::post('search', [FaqController::class, 'search'])->name('search');     

        
    });
});
