<?php

namespace Modules\Faq\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('faq_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
