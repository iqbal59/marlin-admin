<?php

namespace Modules\Faq\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class FaqListRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'type' => 'Type',
        ];
    }
}


