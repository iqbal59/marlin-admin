<?php

namespace Modules\Faq\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('faq_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
