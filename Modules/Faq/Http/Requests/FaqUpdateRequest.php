<?php

namespace Modules\Faq\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title'     => 'required',
            'subject'   => 'required',
            'question'  => 'required',
            'answerBy'  => 'required',
            'solution'  => 'required',
            'entrydate' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'    => ':attribute is required',
            'subject.required'  => ':attribute is required',
            'question.required' => ':attribute is required',
            'answerBy.required' => ':attribute is required',
            'solution.required' => ':attribute is required',
            'entrydate.required'=> ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title'     => 'Title',
            'subject'   => 'Subject',
            'question'  => 'Question',
            'answerBy'  => 'Answer By',
            'solution'  => 'Solution',
            'entrydate' => 'Entry Date',
        ];
    }
}
