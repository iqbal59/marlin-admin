<?php

namespace Modules\Faq\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('faq_create') ? true : false;
    }

    public function rules()
    { 
        if($this->retrn_url != null){
            return [
                'title'     => 'required',
                'subject'   => 'required',
                'question'  => 'required',
                'answerBy'  => 'required',
                'solution'  => 'required',
                'entrydate' => 'required',
            ];
        }else{
            return [
                'title'     => 'required',
                'subject'   => 'required',
                'question'  => 'required',
                'answerBy'  => 'required',
                'solution'  => 'required',
                'entrydate' => 'required',
                'type'      => 'required',
            ];
        }
    }

    public function messages()
    {
        if($this->retrn_url != null){
            return [
                'title.required'    => ':attribute is required',
                'subject.required'  => ':attribute is required',
                'question.required' => ':attribute is required',
                'answerBy.required' => ':attribute is required',
                'solution.required' => ':attribute is required',
                'entrydate.required'=> ':attribute is required',
            ];
        }else{
            return [
                'title.required'    => ':attribute is required',
                'subject.required'  => ':attribute is required',
                'question.required' => ':attribute is required',
                'answerBy.required' => ':attribute is required',
                'solution.required' => ':attribute is required',
                'entrydate.required'=> ':attribute is required',
                'type.required'=> ':attribute is required',
            ];
        }
    }

    public function attributes()
    {
        if($this->retrn_url != null){
            return [
                'title'     => 'Title',
                'subject'   => 'Subject',
                'question'  => 'Question',
                'answerBy'  => 'Answer By',
                'solution'  => 'Solution',
                'entrydate' => 'Entry Date',
            ];
        }else{
            return [
                'title'     => 'Title',
                'subject'   => 'Subject',
                'question'  => 'Question',
                'answerBy'  => 'Answer By',
                'solution'  => 'Solution',
                'entrydate' => 'Entry Date',
                'type'      => 'Type',

            ];
        }
    }
}
