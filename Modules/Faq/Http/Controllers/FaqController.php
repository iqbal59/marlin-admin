<?php

namespace Modules\Faq\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Blogs\Models\Blogs;
use Modules\Department\Models\Department;
use Modules\Doctor\Models\Doctor;
use Modules\Faq\Http\Requests\FaqAddRequest;
use Modules\Faq\Http\Requests\FaqSaveRequest;
use Modules\Faq\Http\Requests\FaqEditRequest;
use Modules\Faq\Http\Requests\FaqUpdateRequest;
use Modules\Faq\Http\Requests\FaqViewRequest;
use Modules\Faq\Http\Requests\FaqDeleteRequest;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Hospital\Entities\Hospital;
use Modules\Hospital\Models\Hospital as ModelsHospital;
use Modules\Treatment\Models\Treatment;

class FaqController extends Controller
{
    public function list(FaqRepository $faqRepo)
    {  
        $faqs = $faqRepo->all();   
        return view('faq::listFaq', compact('faqs'));                         
    }

    public function add(FaqAddRequest $request , FaqRepository $faqRepo)
    {  
        $is_Id  = $request->Id != '' ? $request->Id : '';
        $is_Type =  $request->type != '' ? $request->type : '';
        $retrn_url   = $request->retrn_url;
        $retrn_prms  = $request->retrn_prms;  
        return view('faq::addFaq', compact('is_Id','is_Type','retrn_url','retrn_prms'));                 
    }

    public function save(FaqSaveRequest $request, FaqRepository $faqRepo)
    {  
        if($request->retrn_url !=null){
            $inputData = [
                'title'    => $request->title,
                'subject'  => $request->subject,
                'question' => $request->question,
                'answerBy' => $request->answerBy,
                'solution' =>$request->solution,
                'entrydate'=>$request->entrydate,
                'askable_id'  =>$request->is_Id,
                'askable_type'=>$request->is_Type
            ];
        }else{ 
            $inputData = [
                'title'    => $request->title,
                'subject'  => $request->subject,
                'question' => $request->question,
                'answerBy' => $request->answerBy,
                'solution' =>$request->solution,
                'entrydate'=>$request->entrydate,
                'askable_id'  =>$request->faq_type_id,
                'askable_type'=>$request->type
            ];
        }
        
        $faq = $faqRepo->save($inputData); 
        if(!empty($request->retrn_prms)){ 
            connectify('success', 'Success!', 'Faq added successfully');               
            return redirect()
            ->route($request['retrn_url'],['id'=>$request['retrn_prms']]);
        } 
        else{ 
            connectify('success', 'Success!', 'Faq added successfully');               
            return redirect()
            ->route('admin.faq.list');
        }
    } 

    public function view(FaqViewRequest $request, FaqRepository $faqRepo)
    { 
        $faq = $faqRepo->get($request->id);  
        return view('faq::viewFaq', compact('faq'));
    }

    public function edit(FaqEditRequest $request, FaqRepository $faqRepo)
    {  
        $faq =$faqRepo->get($request->id);  
        $retrn_url  = $request->retrn_url != '' ? $request->retrn_url : '';
        $retrn_prms  = $request->retrn_prms != '' ? $request->retrn_prms : '';
        return view('faq::editFaq', compact('faq','retrn_url','retrn_prms'));
    }

    public function update(FaqUpdateRequest $request, FaqRepository $faqRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'title'  => $request->title,
            'subject'  => $request->subject,
            'question'  => $request->question,
            'answerBy'  => $request->answerBy,
            'solution'=>$request->solution,
            'entrydate'=>$request->entrydate,
            // 'type'     =>$request->type
        ];
        $faq = $faqRepo->update($inputData);
        connectify('success', 'Success!', 'Faq updated successfully');  
        
        if(!empty($request->retrn_prms)){  
            connectify('success', 'Success!', 'Faq updated successfully'); 
            return redirect()
            ->route($request->retrn_url,['id'=>$request->retrn_prms]);
        }else{
            return redirect()
            ->route('admin.faq.list');
        }
    }  

    public function delete(FaqRepository $faqRepo, FaqDeleteRequest $request)
    {   
        if ($faqRepo->delete($request->id)) {
            if(!empty($request->retrn_prms)){  
                connectify('success', 'Success!', 'Faq deleted successfully'); 
                return redirect()
                ->route($request->retrn_url,['id'=>$request->retrn_prms]);
            }else{
                connectify('success', 'Success!', 'Faq deleted successfully');               
                return redirect()
                ->route('admin.faq.list');
            }
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function search(Request $request)
    { 
        if($request->type == "Hospital"){ 
            $data['states'] = Hospital::all();  
        }elseif($request->type =="Department"){
            $data['states'] = Department::all();  
        }elseif($request->type =="Doctor"){
            $data['states'] = Doctor::all();  
        }elseif($request->type =="Blog"){
            $data['states'] = Blogs::all();  
        }else{
            $data['states'] = Treatment::all();  
        } 
        return response()->json($data);
    }
}
