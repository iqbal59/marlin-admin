<?php

namespace Modules\Faq\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Faq\Http\Requests\Api\FaqListRequest;
use Modules\Faq\Http\Requests\FaqAddRequest;
use Modules\Faq\Http\Requests\FaqSaveRequest;
use Modules\Faq\Http\Requests\FaqEditRequest;
use Modules\Faq\Http\Requests\FaqUpdateRequest;
use Modules\Faq\Http\Requests\FaqViewRequest;
use Modules\Faq\Http\Requests\FaqDeleteRequest;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Exception;

class FaqController extends Controller
{
    public function listFaq(Request $request,FaqRepository $faqRepo)
    {   
        try {
            $faqs = $faqRepo->getAllTypeOfFaq(); 
            if (!$faqs) {
                $response = ['status' => false, 'message' => 'No faq Available'];
                return response()->json($response, 200);
            }         
            $data = compact("faqs");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}
