<?php

namespace Modules\Faq\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;
    protected $table = 'faq';

    protected $fillable = [
        'id','title','subject','question','askable_id','askable_type','answerBy','solution','entrydate','status'
    ];

    
}