<?php

namespace Modules\Cms\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CmsListRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'category_id' => 'Category Id',
        ];
    }
}


