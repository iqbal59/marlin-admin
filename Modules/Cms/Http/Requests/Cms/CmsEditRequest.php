<?php

namespace Modules\Cms\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}