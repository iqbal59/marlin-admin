<?php

namespace Modules\Cms\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}