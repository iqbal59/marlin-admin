<?php

namespace Modules\Cms\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}