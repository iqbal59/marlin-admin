<?php

namespace Modules\Cms\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required',
            'type' => 'required',
            // 'meta_description' => 'required',
            // 'meta_tags' => 'required',
            // 'meta_keywords' => 'required',
            // 'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'category_id.required' => ':attribute is required',
            'type.required' => ':attribute is required',
            // 'meta_description.required' => ':attribute is required',
            // 'meta_tags.required' => ':attribute is required',
            // 'meta_keywords.required' => ':attribute is required',
            // 'image.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'category_id' => 'Category',
            'type' => 'Meta Title',
            // 'meta_description' => 'Meta Description',
            // 'meta_tags' => 'Meta Tags',
            // 'meta_keywords' => 'Meta Keywords',
        ];
    }
}