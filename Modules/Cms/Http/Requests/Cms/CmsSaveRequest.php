<?php

namespace Modules\Cms\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class CmsSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'category_id.required' => ':attribute is required',
            'type.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'category_id' => 'Cms Category',
            'type' => 'Type',
        ];
    }
}