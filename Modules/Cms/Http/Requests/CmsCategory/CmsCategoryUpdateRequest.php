<?php

namespace Modules\Cms\Http\Requests\CmsCategory;

use Illuminate\Foundation\Http\FormRequest;

class CmsCategoryUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title'
        ];
    }
}