<?php

namespace Modules\Cms\Http\Requests\CmsCategory;

use Illuminate\Foundation\Http\FormRequest;

class CmsCategoryDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_category_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}