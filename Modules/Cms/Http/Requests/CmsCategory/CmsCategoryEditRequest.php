<?php

namespace Modules\Cms\Http\Requests\CmsCategory;

use Illuminate\Foundation\Http\FormRequest;

class CmsCategoryEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}