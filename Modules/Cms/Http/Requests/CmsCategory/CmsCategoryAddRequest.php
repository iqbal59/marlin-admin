<?php

namespace Modules\Cms\Http\Requests\CmsCategory;

use Illuminate\Foundation\Http\FormRequest;

class CmsCategoryAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('cms_category_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}