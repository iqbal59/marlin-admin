<?php

namespace Modules\Cms\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Cms\Http\Controllers\Includes\Cms;
use Modules\Cms\Http\Controllers\Includes\CmsCategory;

class CmsController extends Controller
{
    use Cms;
    use CmsCategory;
}
