<?php

namespace Modules\Cms\Http\Controllers\Includes;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategoryAddRequest;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategorySaveRequest;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategoryEditRequest;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategoryUpdateRequest;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategoryViewRequest;
use Modules\Cms\Http\Requests\CmsCategory\CmsCategoryDeleteRequest;
use Modules\Cms\Repositories\Includes\CmsCategory\CmsCategoryRepositoryInterface as CmsCategoryRepository;
use Illuminate\Http\Request;
trait CmsCategory
{
    public function categoryList(CmsCategoryRepository $cmsCategoryRepo)
    {  
        $cmsCategory = $cmsCategoryRepo->all();                           
        return view('cms::cmsCategory.listCmsCategory', compact('cmsCategory'));           
    }

    public function add(CmsCategoryAddRequest $request)
    {  
        return view('cms::cmsCategory.addCmsCategory');
    }

    public function save(CmsCategorySaveRequest $request, CmsCategoryRepository $cmsCategoryRepo)
    {       
        $inputData = [
            'title'  => $request->title,
        ];
        $cmsCategory = $cmsCategoryRepo->save($inputData);  
        connectify('success', 'Success!', 'Cms Category added successfully');                              
        return redirect()
            ->route('admin.cmsCategory.categoryList');   
    } 

    public function view(CmsCategoryViewRequest $request,CmsCategoryRepository $cmsCategoryRepo)
    { 
        $cmsCategory = $cmsCategoryRepo->get($request->id);  
        return view('cms::cmsCategory.viewCmsCategory', compact('cmsCategory'));
    }

    public function edit(CmsCategoryEditRequest $request,CmsCategoryRepository $cmsCategoryRepo)
    {   
        $cmsCategory = $cmsCategoryRepo->get($request->id);  
        return view('cms::cmsCategory.editCmsCategory', compact('cmsCategory'));
    }

    public function update(CmsCategoryUpdateRequest $request, CmsCategoryRepository $cmsCategoryRepo)
    {   
        $inputData = [
            'id' => $request->id,            
            'title' => $request->title,
        ];

        $cmsCategory = $cmsCategoryRepo->update($inputData);
        connectify('success', 'Success!', 'Cms Category updated successfully');                              
        return redirect()
            ->route('admin.cmsCategory.categoryList');
    }

    public function delete(CmsCategoryRepository $cmsCategoryRepo, CmsCategoryDeleteRequest $request)
    { 
        if ($cmsCategoryRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Cms Category deleted successfully');                              
            return redirect()
            ->route('admin.cmsCategory.categoryList');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}