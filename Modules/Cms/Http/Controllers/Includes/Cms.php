<?php

namespace Modules\Cms\Http\Controllers\Includes;
use Modules\Cms\Http\Requests\Cms\CmsAddRequest;
use Modules\Cms\Http\Requests\Cms\CmsSaveRequest;
use Modules\Cms\Http\Requests\Cms\CmsEditRequest;
use Modules\Cms\Http\Requests\Cms\CmsUpdateRequest;
use Modules\Cms\Http\Requests\Cms\CmsViewRequest;
use Modules\Cms\Http\Requests\Cms\CmsDeleteRequest;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Modules\Cms\Repositories\Includes\CmsCategory\CmsCategoryRepositoryInterface as CmsCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface as BannerRepository;
use Modules\Gallery\Repositories\GalleryRepositoryInterface as GalleryRepository;
trait Cms
{
    
    public function cmsList(CmsRepository $cmsRepo, CmsCategoryRepository $cmsCategoryRepo ,Request $request)
    {   
        $cmsData  = $request->type != '' ? $cmsRepo->getByCategory($request->type) : $cmsRepo->all();
        $return_url = (!isset($request->type)) ? '': $request->type; 
        $categoryTitle = (!isset($request->type)) ? '': $cmsCategoryRepo->getCategory($request->type);   
 
        return view('cms::cms.listCms', compact('cmsData','return_url','categoryTitle'));           
    }

    public function categoryBasedList(Request $request, CmsRepository $cmsRepo)
    {    
        $cmsData = $cmsRepo->getByCategory($request->type);
        return view('cms::cms.listcategoryBasedList', compact('cmsData'));        
    }


    public function cmsTypeBasedList(CmsRepository $cmsRepo, Request $request)
    {   
        $cmsData =$cmsRepo->typeBaseData($request->type); 
        $type= $request->type;
        return view('cms::cms.listTypesCms', compact('cmsData','type'));           
    }    

    public function addCms(CmsAddRequest $request, CmsCategoryRepository $cmsCategoryRepo)
    {  
        $return_url = $request->retrn_prms != '' ? $request->retrn_prms : ''; 
        $cmsCategory = $cmsCategoryRepo->all(); 
        $type = $request->type;
        return view('cms::cms.addCms', compact('cmsCategory','return_url','type'));      
    }

    public function saveCms(CmsSaveRequest $request, CmsRepository $cmsRepo)
    {    
        $imageUrl=$request->hasFile('image')?$imageUrl = Storage::disk('public')->putFile('cmsimage/',$request->file('image')):$imageUrl="";
        $inputData = [
            'title'  => $request->title,
            'sub_title'  => $request->sub_title,
            'category_id'  => $request->category_id,
            'type'  => $request->type,
            'content'  => $request->content,
            'mini_content'  => $request->mini_content,
            'image'  => $imageUrl,
            
        ];
        $cms = $cmsRepo->save($inputData);    
        if(!empty($request->retrn_prms)){ 
            connectify('success', 'Success!', 'Cms added successfully');                              
            return redirect()
            ->route('admin.cms.cmsList',['type' => $request->retrn_prms]);
        }else{
            connectify('success', 'Success!', 'Cms added successfully');                              
            return redirect()
            ->route('admin.cms.cmsList');
        }   
    } 

    public function viewCms(CmsViewRequest $request,CmsRepository $cmsRepo)
    { 
        $cmsData = $cmsRepo->get($request->id);   
        return view('cms::cms.viewCms', compact('cmsData'));
    }

    public function editCms(GalleryRepository $galleryRepo, BannerRepository $bannerRepo, CmsEditRequest $request,CmsRepository $cmsRepo, CmsCategoryRepository $cmsCategoryRepo)
    {   
        $return_url   = $request->return_url;  
        $cmsData = $cmsRepo->get($request->id);  
        $cmsCategory = $cmsCategoryRepo->all();
        $retrn_prms = $request->retrn_prms;
        $banners = $bannerRepo->all();
        $galleries = $galleryRepo->all(); 
        $selectedMetaTags = json_decode($cmsData->meta_tags);     

        return view('cms::cms.editCms', compact('selectedMetaTags','galleries','banners','cmsData','cmsCategory','return_url','retrn_prms'));
    }

    public function updateCms(CmsUpdateRequest $request, CmsRepository $cmsRepo)
    {   
        // dd($request->all());  
        if($request->hasFile('file')){
            $fileUrl = Storage::disk('public')->putFile('cms_video_files/',$request->file('file'));
        }elseif($request->hasFile('file_image')){
            $fileUrl = Storage::disk('public')->putFile('cms_video_files/',$request->file('file_image'));

        }else{
            $fileUrl="";
        }
        
        $inputData = [
            'id' => $request->id,  
            'category_id'  => $request->category_id,      
            'title'  => $request->title,
            'meta_title'  => $request->meta_title,
            'meta_tags' =>json_encode($request->meta_tags),
            'meta_keywords'  => $request->meta_keywords,
            'meta_description'  => $request->meta_description,
            'content'  => $request->content,
            'type'  => $request->type,
            'banner_id'  => $request->banner_id,
            'gallery_id'  => $request->gallery_id,
            'file'  => $fileUrl,
            'auther'  => $request->auther,
            'given_by' =>$request->given_by,
            'date' =>$request->date,
        ];
        if(isset($request->link)){
            $inputData['link'] = $request->link;
        }      
        if(isset($request->file_type)){
            $inputData['file_type'] = $request->file_type;
        }
        if(isset($request->thumb_line_image)){
            $inputData['thumb_line_image'] = Storage::disk('public')->putFile('cms_thumb_line_image/',$request->file('thumb_line_image'));
        }
        if(isset($request->thumb_line_image1)){
            $inputData['thumb_line_image'] = Storage::disk('public')->putFile('cms_thumb_line_image/',$request->file('thumb_line_image1'));
        }
        if ($request->hasFile('image')) { 
            $this->_removeFile($cmsRepo, $request->id);                 
            $imageUrl = Storage::disk('public')->putFile('cmsimage/',$request->file('image'));
            $inputData['image'] = $imageUrl;
        }
        if ($request->hasFile('logo')) { 
            $this->_removeLogo($cmsRepo, $request->id);                 
            $logoUrl = Storage::disk('public')->putFile('cmslogo/',$request->file('logo'));
            $inputData['logo'] = $logoUrl;
        }
        if ($request->hasFile('absolute_image')) { 
            $this->_removeAbsoluteImage($cmsRepo, $request->id);                 
            $AbsoluteImageUrl = Storage::disk('public')->putFile('cmsAbsoluteimage/',$request->file('absolute_image'));
            $inputData['absolute_image'] = $AbsoluteImageUrl;
        }
        $cms = $cmsRepo->update($inputData);
        if(!empty($request->retrn_prms)){ 
            connectify('success', 'Success!', 'Cms updated successfully');                              
            return redirect()
            ->route('admin.cms.cmsList',['type' => $request->retrn_prms]);
        }
        if(!empty($request->return_url && $request->return_url == 1)){ 
            connectify('success', 'Success!', 'Cms updated successfully');       
            return redirect()
            ->route('admin.cms.cmsTypeBasedList',['type' => 1]);
        }
        if(!empty($request->return_url && $request->return_url == 2)){ 
            connectify('success', 'Success!', 'Cms updated successfully');       
            return redirect()
            ->route('admin.cms.cmsTypeBasedList',['type' => 2]);
        } 
        if(empty($request->return_url) && empty($request->retrn_prms)){
            connectify('success', 'Success!', 'Cms updated successfully');       
            return redirect()
            ->route('admin.cms.cmsList');
        }      
    }

    public function ckeditor_image_upload(Request $request)
    { 
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image successfully uploaded'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    public function removeFile(CmsRepository $cmsRepo,Request $request)
    {   
        if ($this->_removeFile($cmsRepo, $request->id)) {
            $cmsRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'Image deleted successfully');       
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    private function _removeFile($cmsRepo, $id)
    { 
        $image = $cmsRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }

    public function removeLogo(CmsRepository $cmsRepo,Request $request)
    {   
        if ($this->_removeLogo($cmsRepo, $request->id)) {
            $cmsRepo->resetLogo($request->id);
        }
        connectify('success', 'Success!', 'Logo deleteed successfully');       
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    private function _removeLogo($cmsRepo, $id)
    { 
        $image = $cmsRepo->get($id);
        if (Storage::disk('public')->delete($image->logo)) {
            return true;
        }
        return false;
    }

    public function deleteCms(CmsRepository $cmsRepo, CmsDeleteRequest $request)
    {  
        if ($cmsRepo->delete($request->id)) {
            if($request->return_url != null){ 
                connectify('success', 'Success!', 'Cms deleted successfully');       
                return redirect()
                ->route($request['return_url'],['type'=>$request['retrn_prms']]);
            }else{
                connectify('success', 'Success!', 'Cms deleted successfully');       
                return redirect()
                ->route('admin.cms.cmsList');
            }
        }
            return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function removeVideo(CmsRepository $cmsRepo,Request $request)
    {    
        $cmsRepo->resetVideo($request->id);
        connectify('success', 'Success!', 'Video deleted successfully');               
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    public function removeDoc(CmsRepository $cmsRepo,Request $request)
    {   
        if ($this->_removeDoc($cmsRepo, $request->id)) {
            $cmsRepo->removeDoc($request->id);
        }
        connectify('success', 'Success!', 'Doc deleted successfully');       
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    private function _removeDoc($cmsRepo, $id)
    { 
        $image = $cmsRepo->get($id);
        if (Storage::disk('public')->delete($image->file)) {
            return true;
        }
        return false;
    }

    public function removeImage(CmsRepository $cmsRepo,Request $request)
    {   
        if ($this->_removeImage($cmsRepo, $request->id)) {
            $cmsRepo->removeImage($request->id);
        }
        connectify('success', 'Success!', 'Logo deleteed successfully');       
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    private function _removeImage($cmsRepo, $id)
    { 
        $image = $cmsRepo->get($id);
        if (Storage::disk('public')->delete($image->thumb_line_image)) {
            return true;
        }
        return false;
    }

    public function removeAbsoluteImage(CmsRepository $cmsRepo,Request $request)
    {   
        if ($this->_removeAbsoluteImage($cmsRepo, $request->id)) {
            $cmsRepo->removeAbsoluteImage($request->id);
        }
        connectify('success', 'Success!', 'Absolute Image deleteed successfully');       
        return redirect()
            ->route('admin.cms.editCms', ['id' => $request->id]);
    }

    private function _removeAbsoluteImage($cmsRepo, $id)
    { 
        $image = $cmsRepo->get($id);
        if (Storage::disk('public')->delete($image->absolute_image)) {
            return true;
        }
        return false;
    }

    
}