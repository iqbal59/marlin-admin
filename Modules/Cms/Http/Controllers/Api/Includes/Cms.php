<?php

namespace Modules\Cms\Http\Controllers\Api\Includes;
use Modules\Cms\Http\Requests\Api\CmsListRequest;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Modules\Cms\Repositories\Includes\CmsCategory\CmsCategoryRepositoryInterface as CmsCategoryRepository;
use Carbon\Exceptions\Exception;
use Illuminate\Http\Request;
use Cohensive\Embed\Facades\Embed;
trait Cms
{ 
    /*awards-publication/specialization(5,10) */
    public function cmsView(CmsRepository $cmsRepo, CmsCategoryRepository $cmsCategoryRepo ,CmsListRequest $request)
    {     
        try {
            $categoryId = $request->category_id;
            $cms = $cmsRepo->getByCategory($categoryId);  
            if (!$cms) {
                $response = ['status' => false, 'message' => 'The selected cms id is invalid'];
                return response()->json($response, 200);
            }   
            $details = $cms->map(function ($cms, $key)   {  
                if($cms->link != null){
                    $iframe = Embed::make($cms->link)->parseUrl()->getIframe();  
                    $parts = parse_url($iframe); 
                    $embedUrl = trim($parts['path'],"<iframe src=");
                    $embedUrl = ltrim($embedUrl, '""');
                    $cms->link  = $embedUrl;
                    return $cms;
                }
                $cms->content=$cms->content?$cms->content:"";
            });       
            $data = compact("cms");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }   
    
    public function FeaturedIn(CmsRepository $cmsRepo)
    {   
        try {
            $slug = "featured_in";
            $featuredIn = $cmsRepo->getCmsData($slug);
            $data = compact("featuredIn");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }   

    public function AwardsandRecoganization(CmsRepository $cmsRepo)
    {    
        try {
            $AwardAndRecoganization = $cmsRepo->getByCategory(5);
            $data = compact("AwardAndRecoganization");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }   

    public function personalLoan(CmsRepository $cmsRepo)
    {   
        try {
            $slug = "personal_loan";
            $personalLoan = $cmsRepo->getCmsData($slug);
            $data = compact("personalLoan");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }   

    public function cmsList(Request $request, CmsRepository $cmsRepo)
    {   
        try {
            $slug = $request->slug;
            $cmsData = $cmsRepo->getCmsData($slug);
            $response = ['status' => true, 'data' => $cmsData, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }
    
    public function cmsDetails(Request $request, CmsRepository $cmsRepo)
    {    
        try {
            $cmsId = $request->cms_id;
            $cmsData = $cmsRepo->get($cmsId);        
            $response = ['status' => true, 'data' => $cmsData, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }         
    }
}