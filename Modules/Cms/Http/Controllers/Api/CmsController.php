<?php

namespace Modules\Cms\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Cms\Http\Controllers\Api\Includes\Cms;

class CmsController extends Controller
{
    use Cms;
}
