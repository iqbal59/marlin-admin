<?php

use Modules\Cms\Http\Controllers\CmsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('cms')->group(function() {
    Route::get('/', 'CmsController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::middleware('auth')->prefix('cmsCategory')->name('cmsCategory.')->group(function () {

        Route::get('/categoryList', [CmsController::class, 'categoryList'])->name('categoryList');
        Route::get('/add', [CmsController::class, 'add'])->name('add');
        Route::post('save', [CmsController::class, 'save'])->name('save');
        Route::get('edit', [CmsController::class, 'edit'])->name('edit');
        Route::post('update', [CmsController::class, 'update'])->name('update');
        Route::get('view', [CmsController::class, 'view'])->name('view');
        Route::get('delete', [CmsController::class, 'delete'])->name('delete');
            
    });  
    Route::middleware('auth')->prefix('cms')->name('cms.')->group(function () {
    
        Route::get('/cmsList', [CmsController::class, 'cmsList'])->name('cmsList');
        Route::get('/cmsTypeBasedList', [CmsController::class, 'cmsTypeBasedList'])->name('cmsTypeBasedList');
        Route::get('/addCms', [CmsController::class, 'addCms'])->name('addCms');
        Route::post('saveCms', [CmsController::class, 'saveCms'])->name('saveCms');
        Route::get('editCms', [CmsController::class, 'editCms'])->name('editCms');
        Route::post('updateCms', [CmsController::class, 'updateCms'])->name('updateCms');
        Route::get('viewCms', [CmsController::class, 'viewCms'])->name('viewCms');
        Route::get('deleteCms', [CmsController::class, 'deleteCms'])->name('deleteCms');
        Route::post('ckeditor_image_upload', [CmsController::class, 'ckeditor_image_upload'])->name('ckeditor_image_upload');
        Route::get('remove_file', [CmsController::class, 'removeFile'])->name('remove_file');
        Route::get('remove_logo', [CmsController::class, 'removeLogo'])->name('remove_logo');
        Route::get('remove_video', [CmsController::class, 'removeVideo'])->name('remove_video');
        Route::get('remove_image', [CmsController::class, 'removeImage'])->name('remove_image');
        Route::get('remove_doc', [CmsController::class, 'removeDoc'])->name('remove_doc');
        Route::get('remove_absolute_image', [CmsController::class, 'removeAbsoluteImage'])->name('remove_absolute_image');


        /*
        |--------------------------------------------------------------------------
        | Category Wise Listing
        */
        // Route::get('/patientStories', [CmsController::class, 'patientStories'])->name('patientStories');

        // Route::get('/patients_stories/{id?}', [CmsController::class, 'cmsList'])->name('list_patients_stories');
        // Route::get('/events/{id?}', [CmsController::class, 'cmsList'])->name('list_events');
        // Route::get('/blogs/{id?}', [CmsController::class, 'cmsList'])->name('list_blogs');
        // Route::get('/partners/{id?}', [CmsController::class, 'cmsList'])->name('list_partners');
        // Route::get('/awards/{id?}', [CmsController::class, 'cmsList'])->name('list_awards');
        // Route::get('/our-team/{id?}', [CmsController::class, 'cmsList'])->name('list_our_team');
        // Route::get('/patient-testimonial/{id?}', [CmsController::class, 'cmsList'])->name('list_patients_testimonial');
        // Route::get('/partners-testimonial/{id?}', [CmsController::class, 'cmsList'])->name('list_partners_testimonial');
    });  
});

