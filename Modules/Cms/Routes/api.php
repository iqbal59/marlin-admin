<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Cms\Http\Controllers\Api\CmsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/cms', function (Request $request) {
    return $request->user();
});

Route::prefix('cms')->group(function () {
    Route::post('/cmsView', [CmsController::class, 'cmsView']);
    Route::get('/FeaturedIn', [CmsController::class, 'FeaturedIn']);
    Route::get('/AwardsandRecoganization', [CmsController::class, 'AwardsandRecoganization']);
    Route::get('/personalLoan', [CmsController::class, 'personalLoan']);
    Route::post('/cmsList', [CmsController::class, 'cmsList']);
    Route::post('/cmsDetails', [CmsController::class, 'cmsDetails']);

});