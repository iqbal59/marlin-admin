<?php

namespace Modules\Cms\Repositories\Includes\CmsCategory;

interface CmsCategoryRepositoryInterface
{
    public function all();

    public function save(array $input);

    public function get($countryId);

    public function update(array $input);

    public function delete(string $id);

    public function getCategory($id);
}
