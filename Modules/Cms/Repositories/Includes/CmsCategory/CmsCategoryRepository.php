<?php

namespace Modules\Cms\Repositories\Includes\CmsCategory;

use App\Repositories\BaseRepository;
use Modules\Cms\Models\CmsCategory;
use Illuminate\Database\Eloquent\Builder;
use Modules\Cms\Repositories\Includes\CmsCategory\CmsCategoryRepositoryInterface;

class CmsCategoryRepository implements CmsCategoryRepositoryInterface
{
    public function all()
    {
        return CmsCategory::get();
    }

    public function save(array $input)
    {
        if ($cmsCategory =  CmsCategory::create($input)) {
            return $cmsCategory;
        }
        return false;
    }

    public function get($categoryId)
    {
        return CmsCategory::find($categoryId);
    }

    public function update(array $input)
    {
        $cmsCategory = CmsCategory::find($input['id']);
        unset($input['id']);
        if ($cmsCategory->update($input)) {
            return $cmsCategory;
        }
        return false;
    }

    public function delete(string $id)
    {
        $cmsCategory = CmsCategory::find($id);
        return $cmsCategory->delete();
    }
    
    public function getCategory($id)
    {
        return CmsCategory::find($id);
    }
}
