<?php

namespace Modules\Country\Repositories\Includes\State;

use App\Repositories\BaseRepository;
use Modules\Country\Models\State;
use Illuminate\Database\Eloquent\Builder;
use Modules\Country\Repositories\Includes\State\StateRepositoryInterface;

class StateRepository implements StateRepositoryInterface
{
    public function all()
    {
        return State::with(['country'])->get();
    }
    public function get($stateId)
    {
        return State::where('id', $stateId)->first();
        ;
    }
    public function save(array $input)
    {
        if ($state =  State::create($input)) {
            return $state;
        }
        return false;
    }
    public function update(array $input)
    {
        $state = State::find($input['id']);
        unset($input['id']);
        if ($state->update($input)) {
            return $state;
        }
        return false;
    }

    public function delete(string $id)
    {
        $state = State::find($id);
        return $state->delete();
    }

    public function getStates($id)
    {
        $state = State::where('country_id', $id)->get();
        return $state;
    }
}
