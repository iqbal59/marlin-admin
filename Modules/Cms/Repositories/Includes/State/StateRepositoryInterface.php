<?php

namespace Modules\Country\Repositories\Includes\State;

interface StateRepositoryInterface
{
    public function all();

    public function get($stateId);

    public function save(array $input);

    public function update(array $input);

    public function delete(string $id);
    
    public function getStates($id);
}
