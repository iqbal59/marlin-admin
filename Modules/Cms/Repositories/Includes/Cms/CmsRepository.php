<?php

namespace Modules\Cms\Repositories\Includes\Cms;

use App\Repositories\BaseRepository;
use Modules\Cms\Models\Cms;
use Illuminate\Database\Eloquent\Builder;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface;

class CmsRepository implements CmsRepositoryInterface
{
    public function all()
    {
        return Cms::with('cmscategory')->get();
    }

    public function typeBaseData($type)
    {
        return Cms::where('type', $type)->get();
    }

    public function get($Id)
    {
        return Cms::where('id', $Id)->first();
    }

    public function getByCategory($Id)
    {
        return Cms::with('cmscategory')->where('category_id', $Id)->get();
    }

    public function save(array $input)
    {
        if ($cms =  Cms::create($input)) {
            return $cms;
        }
        return false;
    }

    public function update(array $input)
    {
        $cms = Cms::find($input['id']);
        unset($input['id']);
        if ($cms->update($input)) {
            return $cms;
        }
        return false;
    }

    public function delete(string $id)
    {
        $cms = Cms::find($id);
        return $cms->delete();
    }

    public function resetFile(string $id)
    {
        $cms = Cms::find($id);
        $cms->image = '';
        return $cms->save();
    }

    public function resetLogo(string $id)
    {
        $cms = Cms::find($id);
        $cms->logo = '';
        return $cms->save();
    }

    public function resetVideo(string $id)
    {
        $cms = Cms::find($id);
        $cms->link = '';
        return $cms->save();
    }
    public function removeDoc(string $id)
    {
        $cms = Cms::find($id);
        $cms->file = '';
        return $cms->save();
    }

    public function removeImage(string $id)
    {
        $cms = Cms::find($id);
        $cms->thumb_line_image = '';
        return $cms->save();
    }

    public function removeAbsoluteImage(string $id)
    {
        $cms = Cms::find($id);
        $cms->absolute_image = '';
        return $cms->save();
    }

    public function successStories($successStory)
    {
        return Cms::whereIn('id', $successStory)->get();
    }

    public function specialFeatures($specialFeature)
    {
        return Cms::whereIn('id', $specialFeature)->get();
    }

    public function getSpecialization($ids)
    {
        return Cms::whereIn('id', $ids)->pluck('title');
    }

    public function getCmsData($slug)
    {
        return Cms::where('slug', $slug)->first();
    }
}