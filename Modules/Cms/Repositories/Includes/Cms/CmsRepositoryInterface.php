<?php

namespace Modules\Cms\Repositories\Includes\Cms;

interface CmsRepositoryInterface
{
    public function all();

    public function typeBaseData($type);

    public function get($Id);

    public function save(array $input);

    public function getByCategory($Id);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);

    public function resetLogo(string $id);

    public function resetVideo(string $id);

    public function removeDoc(string $id);

    public function removeImage(string $id);

    public function removeAbsoluteImage(string $id);

    public function successStories($successStory);

    public function specialFeatures($specialFeature);
    public function getSpecialization($ids);

    public function getCmsData($slug);
}