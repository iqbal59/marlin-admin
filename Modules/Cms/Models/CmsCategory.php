<?php

namespace Modules\Cms\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CmsCategory extends Model
{
    use HasFactory;
    protected $table = 'cms_categories';

    protected $fillable = [
        'id','title','parent_id','is_editable'
    ];
    public function cms()
    {
        return $this->hasMany('Modules\Cms\Models\Cms');
    }
   
}