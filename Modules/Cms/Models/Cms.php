<?php

namespace Modules\Cms\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Cms extends Model
{
    use HasFactory;
    use Sluggable;
    protected $table = 'cms';

    protected $fillable = [
        'id','slug','sub_title','title','file_type','file','link','logo','meta_title','meta_tags','meta_keywords','banner_id','gallery_id','meta_description','content','image','category_id','type','thumb_line_image','auther','given_by','date','absolute_image'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function cmscategory()
    {
        return $this->belongsTo('Modules\Cms\Models\CmsCategory',  'category_id', 'id');
    }

   
}