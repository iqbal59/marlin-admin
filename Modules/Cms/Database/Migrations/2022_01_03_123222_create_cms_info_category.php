<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Cms\Models\CmsCategory;
use Modules\Cms\Models\Cms;

class CreateCmsInfoCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        CmsCategory::create(['id' => 14, 'title' => 'Information', 'parent_id' => 1, 'is_editable' => 0]);
        Cms ::create(['slug' => 'personal loan', 'title' => 'Personal Loan', 'type' => 1,'category_id' => 14,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'credit cards', 'title' => 'Credit Cards', 'type' => 1,'category_id' => 14,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'travel loans', 'title' => 'Travel Loans', 'type' => 1,'category_id' => 14,'is_editable' => 0,'banner_id'=>1]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CmsCategory::where('id', 14)->delete();
    }
}
