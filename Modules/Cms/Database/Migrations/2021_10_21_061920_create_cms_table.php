<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->index('slug')->unique();
            $table->text('title')->nullable();
            $table->text('sub_title')->nullable();
            $table->text('content')->nullable();
            $table->text('mini_content')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_tags')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->integer('gallery_id')->nullable();
            $table->string('image')->nullable();
            $table->string('logo')->nullable();
            $table->boolean('status')->default(true);
            $table->string('order')->default(0);
            $table->integer('banner_id')->nullable();
            $table->integer('type');
            $table->integer('category_id')->nullable();
            $table->integer('parent_id')->default(0);
            $table->boolean('is_editable')->default(true);
            $table->string('file_type')->nullable();
            $table->string('file')->nullable();
            $table->string('link')->nullable();
            $table->string('thumb_line_image')->nullable();
            $table->string('auther')->nullable();
            $table->string('given_by')->nullable();
            $table->string('date')->nullable();
            $table->string('absolute_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms');
    }
}
