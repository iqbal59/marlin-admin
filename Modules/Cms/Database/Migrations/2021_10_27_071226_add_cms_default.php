<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Modules\Cms\Models\CmsCategory;

use Modules\Cms\Models\Cms;


class AddCmsDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Patient Stories Category
        CmsCategory::create(['id' => 2, 'title' => 'Patient Stories', 'parent_id' => 1, 'is_editable' => 0]);
        
        // Events Category
        CmsCategory::create(['id' => 3, 'title' => 'News & Events', 'parent_id' => 1, 'is_editable' => 0]);
        
        // Partners Category
        CmsCategory::create(['id' => 4, 'title' => 'Partners', 'parent_id' => 1, 'is_editable' => 0]);
        
        // Awards Category
        CmsCategory::create(['id' => 5, 'title' => 'Awards & Recognations', 'parent_id' => 1, 'is_editable' => 0]);
        
        // Executive Team Category
        CmsCategory::create(['id' => 6, 'title' => 'Executive Team', 'parent_id' => 1, 'is_editable' => 0]);

        // Students Testimonial Category
        CmsCategory::create(['id' => 7, 'title' => 'Partners Testimonial', 'parent_id' => 1, 'is_editable' => 0]);

        // Patient Testimonial Category
        CmsCategory::create(['id' => 8, 'title' => 'Patient Testimonial', 'parent_id' => 1, 'is_editable' => 0]);

         // Core Speciality
         CmsCategory::create(['id' => 9, 'title' => 'Core Speciality', 'parent_id' => 1, 'is_editable' => 0]);

        // Specialization
        CmsCategory::create(['id' => 10, 'title' => 'Specialization', 'parent_id' => 1, 'is_editable' => 0]);


        // Specialfeatures
        CmsCategory::create(['id' => 11, 'title' => 'Specialfeatures', 'parent_id' => 1, 'is_editable' => 0]);

         // Specialfeatures
         CmsCategory::create(['id' => 12, 'title' => 'Health Care Associates', 'parent_id' => 1, 'is_editable' => 0]);
        
         // About us
        CmsCategory::create(['id' => 13, 'title' => 'About Us', 'parent_id' => 1, 'is_editable' => 0]);


        /******************************************************************** */
       
        Cms ::create(['id' => 1, 'slug' => 'home', 'title' => 'Home', 'type' => 2,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['id' => 2, 'slug' => 'mma-biz', 'title' => 'MMA-Biz', 'type' => 2,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['id' => 3, 'slug' => 'about_us', 'title' => 'About-Us', 'type' => 1,'category_id' => 1,'parent_id'=>1,'is_editable' => 0]);
        Cms ::create(['id' => 4, 'slug' => 'featured_in', 'title' => 'Featured-In', 'type' => 1,'category_id' => 1,'parent_id'=>1,'is_editable' => 0]);
        Cms ::create(['id' => 5, 'slug' => 'our_healthcare_associates', 'title' => 'Our Healthcare Associates', 'type' => 1,'category_id' => 1,'parent_id'=>1,'is_editable' => 0]);
        Cms ::create(['id' => 6, 'slug' => 'partners', 'title' => 'Partners', 'type' => 1,'category_id' => 1,'parent_id'=>1,'is_editable' => 0]);
        Cms ::create(['id' => 7, 'slug' => 'map-section', 'title' => 'Map-Section', 'type' => 1,'category_id' => 1,'parent_id'=>1,'is_editable' => 0]);
        Cms ::create(['id' => 8, 'slug' => 'get_in_touch', 'title' => 'Get In Touch', 'type' => 1,'category_id' => 1,'parent_id'=>0,'is_editable' => 0]);
        Cms ::create(['id' => 9, 'slug' => 'quick_link', 'title' => 'Quick Link', 'type' => 1,'category_id' => 1,'parent_id'=>0,'is_editable' => 0]);
        Cms ::create(['id' => 10, 'slug' => 'footer_info', 'title' => 'Footer-Info', 'type' => 1,'category_id' => 1,'parent_id'=>0,'is_editable' => 0]);
        Cms ::create(['id' => 11, 'slug' => 'footer_news_letter', 'title' => 'Footer News Letter', 'type' => 1,'category_id' => 1,'parent_id'=>0,'is_editable' => 0]);
        Cms ::create(['id' => 12, 'slug' => 'copyright', 'title' => 'Copyright', 'type' => 1,'category_id' => 1,'parent_id'=>0,'is_editable' => 0]);
        Cms ::create(['id' => 13, 'slug' => 'let_us_work_together', 'title' => 'Let Us Work Together', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 14, 'slug' => 'medical_assistance', 'title' => 'Medical Assistance', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 15, 'slug' => 'health_insurance', 'title' => 'Health Insurance', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 16, 'slug' => 'wellness_assistance', 'title' => 'Wellness Assistance', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 17, 'slug' => 'protect_your_staff_health_and_look_after_your_business', 'title' => 'Protect Your Staff health and look after your business', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 18, 'slug' => 'benefits_of_corporate_tie-ups_with_us', 'title' => 'Benefits of Corporate tie-ups with us', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 19, 'slug' => 'how_it_works', 'title' => 'How it Works', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 20, 'slug' => 'meet_our_partners', 'title' => 'Meet Our Partners', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 21, 'slug' => 'partners_say', 'title' => 'Partners Say', 'type' => 1,'category_id' => 1,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 22, 'slug' => 'patients_photo', 'title' => 'Patients Photo', 'type' => 1,'category_id' => 5,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 23, 'slug' => 'membership_and_organization', 'title' => 'Membership And Organization', 'type' => 1,'category_id' => 5,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 24, 'slug' => 'award_and_honors', 'title' => 'Awards And Honors', 'type' => 1,'category_id' => 5,'parent_id'=>2,'is_editable' => 0]);
        Cms ::create(['id' => 25, 'slug' => 'newspaper/magazine', 'title' => 'Newspaper/Magazine', 'type' => 1,'category_id' => 5,'parent_id'=>2,'is_editable' => 0]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CmsCategory::where('id', 2)->delete();
        CmsCategory::where('id', 3)->delete();
        CmsCategory::where('id', 4)->delete();
        CmsCategory::where('id', 5)->delete();
        CmsCategory::where('id', 6)->delete();
        CmsCategory::where('id', 7)->delete();
        CmsCategory::where('id', 8)->delete();
        CmsCategory::where('id', 9)->delete();
        CmsCategory::where('id', 10)->delete();
        CmsCategory::where('id', 11)->delete();
        CmsCategory::where('id', 12)->delete();
        CmsCategory::where('id', 13)->delete();

        /******************************************************************** */
         

        Cms::where('id', 1)->delete();
        Cms::where('id', 2)->delete();
        Cms::where('id', 3)->delete();
        Cms::where('id', 4)->delete();
        Cms::where('id', 5)->delete();
        Cms::where('id', 6)->delete();
        Cms::where('id', 7)->delete();
        Cms::where('id', 8)->delete();
        Cms::where('id', 9)->delete();
        Cms::where('id', 10)->delete();
        Cms::where('id', 11)->delete();
        Cms::where('id', 12)->delete();
        Cms::where('id', 13)->delete();
        Cms::where('id', 14)->delete();
        Cms::where('id', 15)->delete();
        Cms::where('id', 16)->delete();
        Cms::where('id', 17)->delete();
        Cms::where('id', 18)->delete();
        Cms::where('id', 19)->delete();
        Cms::where('id', 20)->delete();
        Cms::where('id', 21)->delete();
        Cms::where('id', 22)->delete();
        Cms::where('id', 23)->delete();
        Cms::where('id', 24)->delete();
        Cms::where('id', 25)->delete();

    }
}
