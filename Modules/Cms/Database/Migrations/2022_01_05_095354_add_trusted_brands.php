<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Cms\Models\Cms;

class AddTrustedBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Cms ::create(['slug' => 'show case', 'title' => 'We Will Showcase Your Hospital In Middle East , CIS , Africa, And European Countries', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'online reputation', 'title' => 'Build your online reputation', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'international business', 'title' => 'Exponential Growth of International Business', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'branding and promotion', 'title' => 'Branding and promotion of your hospital', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'trusted brands', 'title' => 'Some trusted brands has put faith in us', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1]);
        Cms ::create(['slug' => 'attractive profile', 'title' => 'We build the most attractive profile', 'type' => 1,'category_id' => 1,'is_editable' => 0,'banner_id'=>1,'content'=>'<p>Brand</p><p>International Accreditations &amp; Certifications</p><p>Procedures offered at your facility</p><p>Top doctors and their specialities and experience Media &amp; Gallery</p><p>Options to send Enquires</p><p>Reviews and ratings</p>']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
