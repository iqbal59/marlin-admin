<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Cms extends Model
{
    use HasFactory;
    use Sluggable;


    protected $fillable = [
        'id',
        'slug',
        'title',
        'sub_title',
        'meta_title',
        'meta_tags',
        'meta_keywords',
        'meta_description',
        'content',
        'mini_content',
        'image',
        'logo',
        'gallery_id',
        'banner_id',
        'category_id',
        'type',
        'file_type',
        'file',
        'link',
        'thumb_line_image',
        'auther',
        'given_by',
        'date',
        'absolute_image',
    ];
    
    protected static function newFactory()
    {
        return \Modules\Cms\Database\factories\CmsFactory::new();
    }
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
