<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CmsCategory extends Model
{
    use HasFactory;

    protected $fillable = ['id','title','parent_id','is_editable'];
    
    protected static function newFactory()
    {
        return \Modules\Cms\Database\factories\CmsCategoryFactory::new();
    }
}
