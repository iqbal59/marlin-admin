@extends('layouts.admin')
@section('content')
@can('cms_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">

            <a class="btn btn-success" href="{{ route('admin.cms.addCms', ["type" => $type]) }}">
                <i class="mdi mdi-plus"></i> 
               {{ trans('panel.add_cms') }}  
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('panel.list_cms') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('panel.title') }}
                        </th>

                        <th>
                      {{ trans('panel.action') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($cmsData as $key => $cms)
                        <tr data-entry-id="{{ $cms->id }}">
                            <td>
 
                            </td>
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $cms->title ?? '' }}
                            </td>                           
                           
                            <td>                            
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.cms.viewCms', ['id' => $cms->id]) }}">
                                        {{ trans('global.view') }}
                                    </a>                             
                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.cms.editCms', ['id' => $cms->id,'return_url'=>$cms->type]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                                
                                    <form action="{{ route('admin.cms.deleteCms',  ['id' => $cms->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$cms->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection