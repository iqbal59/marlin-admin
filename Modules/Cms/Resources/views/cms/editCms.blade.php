@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<style>
.iframe-contr iframe{
    max-width: 100%;
    height: 275px;
}
.prevv img.preview {
    width: 100%;
    margin-top: 8px;
    height: 266px;
    object-fit: cover;
}
</style>


<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>
<div class="card">
    <div class="card-header">
      Edit Cms
    </div>

    <div class="card-body">
        <form action="{{ route("admin.cms.updateCms") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$cmsData->id}}">
            <input type="hidden" value="{{!empty($return_url) ? $return_url : ''}}" name="return_url">
            <input type="hidden" value="{{!empty($retrn_prms) ? $retrn_prms : ''}}" name="retrn_prms">
        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('panel.title') }} </label>
                        <i class="mdi mdi-flag-checkered text-danger "></i> 
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($cmsData) ? $cmsData->title : '') }}" required>
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div> 
            </div>
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    <label for="category_id">{{ trans('panel.category_id') }}</label> 
                    <i class="mdi mdi-flag-checkered text-danger "></i> 
                    <select class="js-states form-control" name="category_id" id="category_id" >
                        @foreach($cmsCategory as $data) 
                        @if($data->id == $cmsData->category_id)
                        <option value="{{$data->id}}" name="category_id" selected="">{{$data->title}} </option>    
                        @else
                        <option value="{{$data->id}}" name="state_id"> {{$data->title}}  </option>
                        @endif
                        @endforeach
                    </select>
                    @if($errors->has('category_id'))
                        <em class="invalid-feedback">
                            {{ $errors->first('category_id') }}
                        </em>
                    @endif
                </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                    <label for="type">Type</label> 
                    <i class="mdi mdi-flag-checkered text-danger "></i>
                    <select class="js-states form-control" name="type" id="type" > 
                        <option {{ ($cmsData->type) == 1 ? 'selected' : '' }}  value="1">Block</option>
                        <option {{ ($cmsData->type) == 2 ? 'selected' : '' }}  value="2">Pages</option> 
                    </select>
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
            
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                    <label for="meta_title">{{ trans('panel.meta_title') }} </label>
                    <input type="text" id="meta_title" name="meta_title" class="form-control" value="{{ old('meta_title', isset($cmsData) ? $cmsData->meta_title : '') }}" >
                    @if($errors->has('meta_title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('meta_title') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>
            
            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label for="content">{{ trans('panel.content') }} </label>
                <textarea class="ckeditor form-control" id="content" name="content" >
                {!! old('content', isset($cmsData) ? $cmsData->content : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                <label for="meta_description">{{ trans('panel.meta_description') }} </label>
                <textarea class="ckeditor form-control" id="meta_description" name="meta_description" >
                {!! old('meta_description', isset($cmsData) ? $cmsData->meta_description : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('meta_description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
        <div class="row">
            <div class="col-md-6">    
                <div class="form-group {{ $errors->has('meta_tags') ? 'has-error' : '' }}">
                    <label for="meta_tags">{{ trans('panel.meta_tags') }} </label>
                   


                    @if($selectedMetaTags == null)
                    <select id="meta_tags" name="meta_tags[]" multiple>
                    </select>    
                    @else 
                    <select id="meta_tags" name="meta_tags[]" multiple > 
                    @foreach($selectedMetaTags as $data)  
                    <option value="{{$data}}"selected>{{$data}}</option>
                    @endforeach
                    </select>                
                    @endif


                    @if($errors->has('meta_tags'))
                        <em class="invalid-feedback">
                            {{ $errors->first('meta_tags') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div> 
            <div class="col-md-6">  
                <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                    <label for="meta_keywords">{{ trans('panel.meta_keywords') }} </label>
                    <input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="{{ old('meta_keywords', isset($cmsData) ? $cmsData->meta_keywords : '') }}" >
                    @if($errors->has('meta_keywords'))
                        <em class="invalid-feedback">
                            {{ $errors->first('meta_keywords') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">        
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label for="image">{{ trans('panel.image') }} </label>  
                    @if(Storage::disk('public')->exists($cmsData->image)) 
                    <img class="preview" src="{{asset('storage/'.$cmsData->image)}}" alt="" width="150px">
                        <a href="{{route('admin.cms.remove_file',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                        <i class="mdi mdi-close  pink-text"></i>
                        </a>
                    @else
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                    <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                    @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
                    @endif
                </div>  
            </div> 
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                    <label for="logo">Logo </label>  
                    @if(Storage::disk('public')->exists($cmsData->logo)) 
                    <img class="preview" src="{{asset('storage/'.$cmsData->logo)}}" alt="" width="150px">
                        <a href="{{route('admin.cms.remove_logo',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                        <i class="mdi mdi-close  pink-text"></i>
                        </a>
                    @else
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">                
                    <input type="file" name="logo" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                    @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                    </div>  
                    @endif
                </div>  
            </div>
        </div>  
        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">
                    <label for="absolute_image">Absolute Image </label>  
                    @if(Storage::disk('public')->exists($cmsData->absolute_image)) 
                    <img class="preview" src="{{asset('storage/'.$cmsData->absolute_image)}}" alt="" width="150px">
                        <a href="{{route('admin.cms.remove_absolute_image',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                        <i class="mdi mdi-close  pink-text"></i>
                        </a>
                    @else
                    <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">                
                    <input type="file" name="absolute_image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                    @if($errors->has('absolute_image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('absolute_image') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                    </div>  
                    @endif
                </div>  
            </div>
            <div class="col-md-6">  
                <div class="form-group {{ $errors->has('auther') ? 'has-error' : '' }}">
                    <label for="auther">Auther </label>                    
                    <input type="text" id="auther" name="auther" class="form-control" value="{{ old('auther', isset($cmsData) ? $cmsData->auther : '') }}" >
                    @if($errors->has('auther'))
                        <em class="invalid-feedback">
                            {{ $errors->first('auther') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('given_by') ? 'has-error' : '' }}">
                    <label for="given_by">Given By </label>                    
                    <input type="text" id="given_by" name="given_by" class="form-control" value="{{ old('given_by', isset($cmsData) ? $cmsData->given_by : '') }}" >
                    @if($errors->has('given_by'))
                        <em class="invalid-feedback">
                            {{ $errors->first('given_by') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
            </div>
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                    <label for="date">Date </label>                    
                    <input type="date" id="date" name="date" class="form-control" value="{{ old('date', isset($cmsData) ? $cmsData->date : '') }}" >
                    @if($errors->has('date'))
                        <em class="invalid-feedback">
                            {{ $errors->first('date') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"> 
                <div class="form-group {{ $errors->has('banner_id') ? 'has-error' : '' }}">
                    <label for="banner_id">Banner</label> 
                        <select id="banner_id" name="banner_id" class="js-states form-control">
                            <option value=""></option>        
                            @foreach ($banners as $key => $value)
                            <option value="{{$value->id}}" {{(old('banner_id', $cmsData->banner_id) == $value->id ? 'selected' : '')}} > {{$value->title}} </option>
                            @endforeach
                        </select>
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
            <div class="col-md-6">    
            <div class="form-group {{ $errors->has('gallery_id') ? 'has-error' : '' }}">
                    <label for="gallery_id">Gallery</label> 
                        <select id="gallery_id" name="gallery_id" class="js-states form-control">
                            <option value=""></option>    
                            @foreach ($galleries as $key => $value)
                            <option value="{{$value->id}}" {{(old('gallery_id', $cmsData->gallery_id) == $value->id ? 'selected' : '')}} > {{$value->title}} </option>
                            @endforeach
                        </select>
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>  
            </div>
        </div> 
     
        <div class="row">
                @if($cmsData->file_type == null)
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name"> Type </label>
                            <select id="file_type" name="file_type" class="js-states form-control" onchange="change_type()">
                               <option value="">Choose:</option>
                                <option value="vimeo">Vimeo</option>
                                <option value="youtube">Youtube</option>
                                <option value="file">File</option>
                            </select>               
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>     
                </div>
                @endif
                <div class="col-md-6" id="toshow">
                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">Link<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                </div> 

                @if(($cmsData->file_type == "file") && (Storage::disk('public')->exists($cmsData->file))) 
                <div class="col-md-6">
                <label for="file"> File </label>      
                    <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$cmsData->file)}}" target="_blank">
                    <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                    </a>    
                    <a href="{{route('admin.cms.remove_doc',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                    <i class="menu-icon mdi mdi-close pink-text"></i>
                    </a>
                </div>
                @endif
                
                @if($cmsData->file_type == "file" && $cmsData->file=='' )
                <div class="col-md-6" >
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        <label for="file">  File <i class="mdi mdi-flag-checkered text-danger "></i>  </label><br>
                            <input class="form-group" type="file" name="file_image" /> 
                          
                            @if($errors->has('file'))
                            <em class="invalid-feedback">
                                {{ $errors->first('file') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                    </div>   
                </div>
                @endif 

                @if($cmsData->file_type == "vimeo" || $cmsData->file_type=='youtube' )
                @if($cmsData->link !='')
                <div class="col-md-6 iframe-contr" >
                <label for="file">  Video <i class="mdi mdi-flag-checkered text-danger "></i>  </label><br>
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    {!! Embed::make($cmsData->link)->parseUrl()->getIframe() !!}
                            <a href="{{route('admin.cms.remove_video',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                            <i class="menu-icon mdi mdi-close pink-text"></i>
                            </a>   
                    </div>
                </div>
                @else
                <div class="col-md-6">
                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">@if($cmsData->file_type == "vimeo") Vimeo Link* @else Youtube Link @endif</label>
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                </div> 
                @endif
                @endif
                @if($cmsData->file_type == "vimeo" || $cmsData->file_type=='youtube')
                @if($cmsData->thumb_line_image !='')
                <div class="col-md-6 prevv" >
                <label for="file">  Thumb Line Image <i class="mdi mdi-flag-checkered text-danger "></i>  </label><br>
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <img class="preview" src="{{asset('storage/'.$cmsData->thumb_line_image)}}" alt="" width="150px">
                                <a href="{{route('admin.cms.remove_image',['id'=>$cmsData->id])}}" class="confirm-delete text-danger">
                                <i class="menu-icon mdi mdi-close pink-text"></i>
                                </a>
                    </div>
                </div>
                @else
                <div class="col-md-6" >
                <div class="form-group {{ $errors->has('thumb_line_image') ? 'has-error' : '' }}">
                        <label for="thumb_line_image"> Thumb Line Images<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <input type="file" name="thumb_line_image1" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('thumb_line_image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('thumb_line_image') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                    </div>
                </div>
                @endif
                @endif

                <div class="col-md-6" id="toshow2">
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        <label for="file"> File<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <input type="file" name="file" id="input-file-now-banner" class="dropify validate" data-default-file="" accept="application/pdf,application/doc,application/docx" /> 
                            @if($errors->has('file'))
                            <em class="invalid-feedback">
                                {{ $errors->first('file') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                    </div>   
                </div>
            </div>

            <div class="row" >

                <div class="col-md-6" id="toshow1">
                    <div class="form-group {{ $errors->has('thumb_line_image') ? 'has-error' : '' }}">
                        <label for="thumb_line_image"> Thumb Line Images<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <input type="file" name="thumb_line_image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('thumb_line_image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('thumb_line_image') }}
                            </em>
                            @endif
                            <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                    </div>         
                </div>
                
                <!-- <div class="col-md-6 badge-contr">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" {{ old('status',$cmsData->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                            <span class="off">No</span>
                            <label class="custom-control-label" for="status"></label>
                            <span class="on">Yes</span>
                        </div>
                    </div> 
                </div>    -->
              
                <br>       

               
            </div>        
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
    $(document).ready(function() {
        $("#banner_id").select2({
            placeholder: "Select Banner",
            allowClear: true
        });
    });
    $(document).ready(function() {
        $("#file_type").select2({
            placeholder: "Select Type",
            allowClear: true
        });
    });
    $(document).ready(function() {
        $("#gallery_id").select2({
            placeholder: "Select Gallery",
            allowClear: true
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#type").select2({
            placeholder: "Select Type",
            allowClear: true
        });
    });
</script>
<script> 
    $("#toshow").hide(); 
    $("#toshow1").hide();  
    $("#toshow2").hide();  

    function change_type(){
        var type =$("#file_type").val();
            if(type == 'vimeo' || type =="youtube"){
                $("#toshow").show();
                $("#toshow1").show();
                $("#toshow2").hide();

            }else{
                $("#toshow").hide();
                $("#toshow1").hide();
                $("#toshow2").show();
            }    
    }   
</script>
<script>
    $('#meta_tags').select2({
  tags: true
});
</script>
@endsection