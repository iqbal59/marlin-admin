@extends('layouts.admin')
@section('content')
<style>
    .table.cms-table th, .table.cms-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
        {{ trans('panel.view_cms_category') }}
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped cms-table">
                <tbody>
                    <tr> 
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $cmsData->id }}
                        </td>
                    </tr>
                    @if($cmsData->title != null)
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $cmsData->title }}
                        </td>
                    </tr>
                    @endif
                    @if($cmsData->content != null)
                    <tr>
                        <th>
                        {{ trans('panel.content') }}
                        </th>
                        <td>
                        {!! $cmsData->content !!}  
                        </td>
                    </tr>
                    @endif
                    @if($cmsData->meta_title != null)
                    <tr>
                        <th>
                        {{ trans('panel.meta_title') }}
                        </th>
                        <td>
                            {{ $cmsData->meta_title }}
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th>
                        Type
                        </th>
                        <td>
                            @if($cmsData->type == 0)
                            Block
                            @else
                            Pages
                            @endif
                        </td>
                    </tr>
                    @if($cmsData->meta_description != null)
                    <tr>
                        <th>
                    {{ trans('panel.meta_description') }}
                        </th>
                        <td>
                            {!! $cmsData->meta_description !!}
                        </td>
                    </tr>
                    @endif
                    @if($cmsData->meta_tags != null)
                    <tr>
                        <th>
                        {{ trans('panel.meta_tags') }}
                        </th>
                        <td>
                            {{ $cmsData->meta_tags }}
                        </td>
                    </tr>
                    @endif
                    @if($cmsData->meta_keywords != null)
                    <tr>
                        <th>
                       {{ trans('panel.meta_keywords') }}
                        </th>
                        <td>
                            {{ $cmsData->meta_keywords }}
                        </td>
                    </tr>
                    @endif
                    @if($cmsData->image != null)
                    <tr>
                        <th>
                       {{ trans('panel.meta_image') }}
                        </th>
                        <td>
                        <img src="{{asset('storage/'.$cmsData->image)}}" alt="" width="500px">
                        </td>
                    </tr>
                   @endif
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection