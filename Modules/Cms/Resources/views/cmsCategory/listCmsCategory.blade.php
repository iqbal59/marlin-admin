@extends('layouts.admin')
@section('content')
<!-- @can('cms_category_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.cmsCategory.add") }}">
                <i class="mdi mdi-plus"></i> 
                Add Cms Category
            </a>
        </div>
    </div>
@endcan -->
<div class="card">
    <div class="card-header">
       List Cms Category
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cmsCategory as $key => $category)
                        <tr data-entry-id="{{ $category->id }}">
                            <td></td>
                            <td> {{ $key+1 }}</td>
                            <td> {{ $category->title ?? '' }}</td>   
                            <td> 
                            @can('cms_category_show')                               
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.cmsCategory.view', ['id' => $category->id]) }}">
                                    {{ trans('global.view') }}
                                </a>   
                            @endcan
                            @can('cms_category_edit')     
                                <a class="btn btn-xs btn-info d-none" href="{{route('admin.cmsCategory.edit', ['id' => $category->id]) }}">
                                    {{ trans('global.edit') }}
                                </a>                             
                            @endcan
                            @can('cms_category_delete')    
                                <form action="{{ route('admin.cmsCategory.delete',  ['id' => $category->id]) }}" 
                                    onsubmit="return confirm('{{ trans('global.areYouSure') }}');" 
                                    style="display:none;">
                                    <input type="hidden" name="id" value="{{$category->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            @endcan    
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection