<?php

namespace Modules\Country\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class State extends Model
{
    use HasFactory;

    protected $fillable = ['id','name','country_id','status'];
    
    protected static function newFactory()
    {
        return \Modules\Country\Database\factories\StateFactory::new();
    }
}
