@extends('layouts.admin')
@section('content')

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>


<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} City
    </div>

    <div class="card-body">
        <form action="{{ route("admin.city_update") }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{$city->id}}">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($city) ? $city->name : '') }}" >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>    
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> {{ trans('panel.country_name') }} <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <select class="js-states form-control" name="country_id" id="country_id"  >
                    @foreach($countryData as $data)
                    @if($data->id == $country->id) 
                    <option value="{{$data->id}}" name="country_id" selected=""> {{$data->name}}  </option>    
                    @else
                    <option value="{{$data->id}}" name="country_id"> {{$data->name}}  </option>
                    @endif
                    @endforeach
                </select>
                @if($errors->has('country_id'))
                <em class="invalid-feedback">{{ $errors->first('country_id') }}</em>
                @endif  
            </div> 
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> {{ trans('panel.country_name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <select class="js-states form-control" name="state_id" id="state_id">
                    @foreach($stateData as $data)
                    @if($data->id == $state->id) 
                    <option value="{{$data->id}}" name="state_id" selected=""> {{$data->name}}  </option>    
                    @else
                    <option value="{{$data->id}}" name="state_id"> {{$data->name}}  </option>
                    @endif
                    @endforeach
                </select>
                @if($errors->has('state_id'))
                <em class="invalid-feedback">{{ $errors->first('state_id') }}</em>
                @endif  
            </div> 
            
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $city->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>   
            <br>
           
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(document).ready(function() {

            $("#country_id").select2({
                placeholder: "Select Country",
                allowClear: true
            });
            $("#state_id").select2({
                placeholder: "Select State",
                allowClear: true
            });
           
            $('select[name="country_id"]').on('change', function () { 

                var country_id = this.value;
                // alert(country_id);
                $("#state-dropdown").html('');
                $.ajax({
                    url:"{{route('admin.city_search')}}",
                    type: "POST",
                    data: {
                    country_id: country_id,
                    _token: '{{csrf_token()}}' 
                    },
                dataType : 'json',
                success: function(result){
                    $('#state_id').empty();
                    $('#state-dropdown').html('<option value="">Select State</option>'); 
                    $.each(result.states,function(key,value){ 
                        $('select[name="state_id"]').append('<option value="'+value.id+'">'+value.name+'</option');
                    });
                }
            });
        }); });
        </script>

        @endsection