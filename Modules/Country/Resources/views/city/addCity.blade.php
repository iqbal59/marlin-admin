@extends('layouts.admin')

@push('vendor-style')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush
@push('vendor-script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endpush

@section('content')

<div class="card">
    <div class="card-header">
        Add City
    </div>

    <div class="card-body">
        <div class="col-md-6">
        <form action="{{ route("admin.city_save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('panel.city_name') }} <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}"  >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> {{ trans('panel.country_name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                    <select id="country_id" name="country_id" class="js-states form-control">
                        <option value="">{{ trans('panel.select') }}</option>
                        @foreach($country as $data) 
                        <option value="{{$data->id}}" name="country-dropdown" id="country-dropdown" onclick="myFunction()" >
                        {{$data->name}}
                        </option>
                        @endforeach
                    </select>  
                    @if($errors->has('country_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('country_id') }}
                    </em>
                    @endif             
               
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>                                     
            
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> {{ trans('panel.state_name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>           <div id="sucess-msg">     
                    <select id="state_id" name="state_id" class="js-states form-control">
                        <option value="">{{ trans('panel.select') }}</option>
                    </select></div>
                    @if($errors->has('state_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('state_id') }}
                    </em>
                    @endif 
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>                           
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#country_id").select2({
                placeholder: "Select Country",
                allowClear: true
            });
            $("#state_id").select2({
                placeholder: "Select State",
                allowClear: true
            });

            

            $('select[name="country_id"]').on('change', function () {        
                var country_id = this.value;
                $("#state-dropdown").html('');
                $.ajax({
                    url:"{{route('admin.city_search')}}",
                    type: "POST",
                    data: {
                    country_id: country_id,
                    _token: '{{csrf_token()}}' 
                    },
                dataType : 'json',
                success: function(result){
                    $('#state_id').empty();
                    $('#state-dropdown').html('<option value="">Select State</option>'); 
                    $.each(result.states,function(key,value){ 
                        $('select[name="state_id"]').append('<option value="'+value.id+'">'+value.name+'</option');
                    });
                }
            }); 
        });    
    });
</script>

@endsection