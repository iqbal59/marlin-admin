@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
       {{ trans('panel.view_city') }}
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    {{-- @php  dd($country); @endphp --}}
    <div class="card-body">
        @include('country::city.partials.tabSection')
    </div>
</div>
@endsection