@extends('layouts.admin')
@section('content')
<style>
.addMore{
  border: none; 
  height: 28px;
  background-color: #FF8000;
  transition: all ease-in-out 0.2s;
  cursor: pointer;
}
.addMore:hover{
  border: 1px solid #FF8000;
  background-color: #FF8000;
}
button:disabled,
button[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}

</style>
@can('city_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.city_add") }}">
                <i class="mdi mdi-plus"></i>
                {{ trans('panel.add_city') }} 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
       {{ trans('panel.add_city') }} 
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($cities as $key => $city)
                        <tr data-entry-id="{{ $city->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $city->name ?? '' }}</td>
                            <td>
                            @can('city_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.city_view', ['id' => $city->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan 
                            @can('city_edit')
                                    <a class="btn btn-xs btn-info" href="{{route('admin.city_edit', ['id' => $city->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan 
                            @can('city_delete')    
                                    <form action="{{ route('admin.city_delete',  ['id' => $city->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$city->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if($city->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this city has child!"> Delete</button>
                                        @endif 
                                    </form>
                            @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})
</script>
@endsection