<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>  {{ trans('cruds.user.fields.id') }}</th>
                <td>  {{$city->id}}</td>
            </tr>
            <tr>
                <th>  {{ trans('cruds.user.fields.name') }}</th>
                <td>  {{ $city->name }}</td>
            </tr>
            <tr>
                <th>  {{ trans('panel.country_name') }} </th>
                <td>  {{ $country->name }}</td>
            </tr>
            <tr>
                <th>  {{ trans('panel.state_name') }}  </th>
                <td>  {{ $state->name }}</td>
            </tr>
             
        </tbody>
    </table>
</div>