
<div class="card">
<div class="card-header">
        List Doctor  
        @can('doctor_create')
        <a class="btn btn-warning float-right" href="{{route('admin.doctor.add', ["retrn_url" => "admin.country_view" , "retrn_prms" => $country->id])}}">
            Add Doctor
        </a>
        @endcan  
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Doctor">
                <thead>
                    <tr>
                    <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>{{ trans('cruds.user.fields.name') }}</th>
                        <th>Status</th>
                        <th>Actions</th>
                        
                    </tr>
                </thead>
                <tbody>
                @foreach($doctors as $key => $doctor) 
                        <tr data-entry-id="{{ $doctor->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $doctor->name ?? '' }}</td>
                            <td>
                                @if($doctor->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                              <a class="btn btn-xs btn-primary" href="{{ route('admin.doctor.view', ['id' => $doctor->id]) }}">
                                  {{ trans('global.view') }}
                              </a> 

                              <a class="btn btn-xs btn-info" href="{{route('admin.doctor.edit', ['id' => $doctor->id, 'doctor_id' => $doctor->id, 'retrn_url' => "admin.country_view" , "retrn_prms" => $country->id ]) }}">
                                  {{ trans('global.edit') }}
                              </a>   
                              <form action="{{ route('admin.doctor.delete',  ['id' => $doctor->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                  <input type="hidden" name="id" value="{{$doctor->id}}">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="retrn_url" value="admin.country_view">
                                  <input type="hidden" name="retrn_prms" value="{{$country->id}}">
                                  <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                              </form>
                          </td>
                            
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
 
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('state_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    // text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    // className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  $('.datatable-Doctor:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection