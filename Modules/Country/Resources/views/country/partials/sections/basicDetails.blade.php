<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>  {{ trans('cruds.user.fields.id') }}</th>
                <td>  {{ $country->id }}</td>
            </tr>
            <tr>
                <th>  {{ trans('cruds.user.fields.name') }}</th>
                <td>  {{ $country->name }}</td>
            </tr>              
        </tbody>
    </table><br>

    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>  Total Hospitals </th>
                <td>  {{(count($coutryhospitals->hospitals))}}</td>
            </tr>
            <tr>
                <th>  Total Departments</th>
                <td>  {{count($coutryhospitals->departments)}}</td>
            </tr>
            <tr>
                <th>  Total Services</th>
                <td>  {{count($coutryhospitals->services)}}</td>
            </tr>

            <tr>
                <th>  Total Doctors</th>
                <td>  {{count($coutryhospitals->doctors)}}</td>
            </tr>
            <tr>
                <th>  Total Treaments</th>
                @if($coutryhospitals->treaments != null)
                <td> {{count($coutryhospitals->treaments)}}</td>
                @else
                <td> 0</td>
                @endif
            </tr>
                         
        </tbody>
    </table><br>

     
   
</div>