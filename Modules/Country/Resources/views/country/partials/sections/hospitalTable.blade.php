
<div class="card">
    <div class="card-header">
        List Hospitals  
        @can('hospital_create')
        <a class="btn btn-warning float-right" href="{{route('admin.hospital.add', ["retrn_url" => "admin.country_view" , "retrn_prms" => $country->id])}}">
            Add Hospitals
        </a>
        @endcan  
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                         
                        <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>{{ trans('cruds.user.fields.name') }}</th>
                        <th>Status</th>
                        <th>Actions</th>
                       
                    </tr>
                </thead>
                <tbody>
                @foreach($hospitals as $key => $hospital)
                         
                        <tr data-entry-id="{{ $hospital->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $hospital->title ?? '' }}</td>
                            <td>
                                @if($hospital->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                              <a class="btn btn-xs btn-primary" href="{{ route('admin.hospital.view', ['id' => $hospital->id]) }}">
                                  {{ trans('global.view') }}
                              </a> 

                              <a class="btn btn-xs btn-info" href="{{route('admin.hospital.edit', ['id' => $hospital->id, 'hospital_id' => $hospital->id, 'retrn_url' => "admin.country_view" , "retrn_prms" => $country->id ]) }}">
                                  {{ trans('global.edit') }}
                              </a>   

                              <form action="{{ route('admin.hospital.delete',  ['id' => $hospital->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                  <input type="hidden" name="id" value="{{$hospital->id}}">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="retrn_url" value="admin.country_view">
                                  <input type="hidden" name="retrn_prms" value="{{$country->id}}">

                                  @if($hospital->can_delete == true)    
                                      <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                      @else
                                      <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this department has child!"> Delete</button>
                                      @endif 
                              </form>
                          </td>
                           
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
 
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('state_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    // text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    // className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
        return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection