 
<div class="row">
    <div class="col-md-12">        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Basic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-department-tab" 
                    data-toggle="pill" 
                    href="#pills-department" 
                    role="tab" 
                    aria-controls="pills-department" 
                    aria-selected="true">Hospitals</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-doctor-tab" 
                    data-toggle="pill" 
                    href="#pills-doctor" 
                    role="tab" 
                    aria-controls="pills-doctor" 
                    aria-selected="true">Doctor</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('country::country.partials.sections.basicDetails')
            </div>
            <div class="tab-pane fade show" id="pills-department" role="tabpanel" aria-labelledby="pills-department-tab">
                @include('country::country.partials.sections.hospitalTable')
            </div>
            <div class="tab-pane fade show" id="pills-doctor" role="tabpanel" aria-labelledby="pills-doctor-tab">
                @include('country::country.partials.sections.doctorTable')
            </div>
        </div>
    </div>
</div>