@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        View Country
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    {{-- @php  dd($country); @endphp --}}
    <div class="card-body">
        @include('country::country.partials.tabSection')
    </div>
</div>
@endsection