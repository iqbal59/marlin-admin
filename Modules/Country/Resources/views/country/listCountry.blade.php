@extends('layouts.admin')
@section('content')
@can('country_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.country_add") }}">
                <i class="mdi mdi-plus"></i>
                {{ trans('panel.add_country') }}                                                                                                      
                
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        List Country     
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                @foreach($countries as $key => $country)
                        <tr data-entry-id="{{ $country->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $country->name ?? '' }}</td>
                            <td>
                                @can('country_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.country_view', ['id' => $country->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                                @endcan 
                                @can('country_edit')
                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.country_edit', ['id' => $country->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                                @endcan 
                                @can('country_delete')
                                    <form action="{{ route('admin.country_delete',  ['id' => $country->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$country->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if($country->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this country has child!"> Delete</button>
                                        @endif  
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 10,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection