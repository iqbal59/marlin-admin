@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('panel.add_country') }} 
    </div>

    <div class="card-body">
        <div class="col-md-6">
        <form action="{{ route("admin.country_save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}</label>
                <i class="mdi mdi-flag-checkered text-danger "></i>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>          
           
        
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>

    </div>
</div>
</div>
@endsection