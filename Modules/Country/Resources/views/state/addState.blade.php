@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
         {{ trans('panel.add_state') }}        
    </div>

    <div class="card-body">
        <form action="{{ route("admin.state_save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name"> {{ trans('panel.state_name') }} </label>
                <i class="mdi mdi-flag-checkered text-danger "></i>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
        
            <div class="col-md-6">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('panel.country_name') }} </label>
                <i class="mdi mdi-flag-checkered text-danger "></i>
                <select class="js-states form-control" name="country_id" id="country_id"  >
                <option value="">{{ trans('panel.select') }} </option>    
                @foreach($country as $data)
                    <option value="{{$data->id}}" name="country_id"> {{$data->name}}  </option>
                @endforeach
                </select>
                @if($errors->has('country_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('country_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            </div>
        </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
        $("#country_id").select2({
            placeholder: "Select Country",
            allowClear: true
        });
        
    });
</script>
@endsection