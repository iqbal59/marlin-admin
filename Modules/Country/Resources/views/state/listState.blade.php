@extends('layouts.admin')
<style>
.addMore{
  border: none; 
  height: 28px;
  background-color: #FF8000;
  transition: all ease-in-out 0.2s;
  cursor: pointer;
}
.addMore:hover{
  border: 1px solid #FF8000;
  background-color: #FF8000;
}
button:disabled,
button[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}

</style>
@section('content')
@can('state_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.state_add") }}">
                <i class="mdi mdi-plus"></i>
                {{ trans('panel.add_state') }} 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
       {{ trans('panel.list_state') }} 
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($states as $key => $state)
                        <tr data-entry-id="{{ $state->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $state->name ?? '' }}</td>
                            <td>
                            @can('state_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.state_view', ['id' => $state->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan 
                            @can('state_edit')                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.state_edit', ['id' => $state->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan
                            @can('state_delete')    
                                    <form action="{{ route('admin.state_delete',  ['id' => $state->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$state->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if($state->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this state has child!"> Delete</button>
                                        @endif 
                                    </form>
                            @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection