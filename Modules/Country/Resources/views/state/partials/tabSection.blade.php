 
<div class="row">
    <div class="col-md-12">        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Basic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-department-tab" 
                    data-toggle="pill" 
                    href="#pills-department" 
                    role="tab" 
                    aria-controls="pills-department" 
                    aria-selected="true">Hospitals</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('country::state.partials.sections.basicDetails')
            </div>
            <div class="tab-pane fade show" id="pills-department" role="tabpanel" aria-labelledby="pills-department-tab">
                @include('country::state.partials.sections.hospitalTable')
            </div>
        </div>
    </div>
</div>