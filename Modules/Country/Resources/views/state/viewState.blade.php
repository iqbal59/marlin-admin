@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        View State
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    {{-- @php  dd($state); @endphp --}}
    <div class="card-body">
        @include('country::state.partials.tabSection')
    </div>
</div>
@endsection