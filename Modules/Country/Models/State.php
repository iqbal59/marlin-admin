<?php

namespace Modules\Country\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;
    protected $table = 'states';

    protected $fillable = [
        'name','country_id','status'
    ];

    public function country()
    {
        return $this->belongsTo('Modules\Country\Models\Country', 'country_id', 'id');
    }

    public function citiy()
    {
        return $this->hasMany('Modules\Country\Models\City');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->citiy->count()
        ) {
            return false;
        }
        return true;
    }

    public function hospitals()
    {
        return $this->hasManyDeep('Modules\Hospital\Models\Hospital', ['Modules\Country\Models\State','Modules\Country\Models\City']);
    }

    public function statedoctors()
    {
        return $this->hasManyDeep('Modules\Doctor\Models\Doctor', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department']);
    }
}