<?php

namespace Modules\Country\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentJsonRelations\Relations\HasManyJson;
use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\HasManyDeepJson;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;


class Country extends Model
{
    use HasFactory, HasRelationships;
    protected $table = 'countries';

    protected $fillable = [
        'name','code','phonecode', 'status'
    ];

    protected $casts = [
        'medicines' => 'json'
     ];

    public function states()
    {
        return $this->hasMany('Modules\Country\Models\State');
    }

    public function hospitals()
    {
        return $this->hasManyDeep('Modules\Hospital\Models\Hospital', ['Modules\Country\Models\State','Modules\Country\Models\City']);
    }

    public function departments()
    {
        return $this->hasManyDeep('Modules\Department\Models\Department', ['Modules\Country\Models\State','Modules\Country\Models\City','Modules\Hospital\Models\Hospital']);
    }

    public function activeDepartments() 
    {
        return $this->hasManyDeep('Modules\Department\Models\Department', ['Modules\Country\Models\State','Modules\Country\Models\City','Modules\Hospital\Models\Hospital'])->where('departments.status',1);
    }

    public function services()
    {
        return $this->hasManyDeep('Modules\Services\Models\Services', ['Modules\Country\Models\State','Modules\Country\Models\City','Modules\Hospital\Models\Hospital']);
    }

    public function doctors()
    {
        return $this->hasManyDeep('Modules\Doctor\Models\Doctor', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department']);
    }

    public function telemedicinedoctors()
    {
        return $this->hasManyDeep('Modules\Doctor\Models\Doctor', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department'])->where('doctor.type',1);
    }

    public function treatments()
    {
        return $this->hasManyDeep('Modules\Treatment\Models\Treatment', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department']);
    }

    public function activeTreatments()
    {
        return $this->hasManyDeep('Modules\Treatment\Models\Treatment', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department'])->where('treatments.status',1);    
    }

    // public function medicines()
    // {
    //     return $this->hasManyDeep('Modules\Medicines\Models\Medicines', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Doctor\Models\Doctor','Modules\Diseases\Models\Diseases']);
    // }

    // public function medicines()
    // {
    //     return $this->hasManyDeep('Modules\Medicines\Models\Medicines', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department','Modules\Treatment\Models\Treatment','Modules\Diseases\Models\Diseases']);
    // }

    // public function groups()
    // { 
    //     return $this->hasManyDeepJson('Modules\Medicines\Models\Medicines', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department','Modules\Treatment\Models\Treatment','Modules\Diseases\Models\Diseases', 'medicines->diseases_id']); 
    // }
    

    // public function groups() 
    // { 
    //     return $this->HasManyDeepJson('Modules\Medicines\Models\Medicines', ['Modules\Country\Models\City','Modules\Hospital\Models\Hospital','Modules\Department\Models\Department','Modules\Treatment\Models\Treatment','Modules\Diseases\Models\Diseases', 'diseases->diseases_id']); 
    // }

    // public function groupsd()
    // {
    //     return $this->hasManyDeep(
    //         Modules\Medicines\Models\Medicines::class,
    //         [Modules\Country\Models\City::class, Modules\Hospital\Models\Hospital::class,Modules\Department\Models\Department::class,Modules\Treatment\Models\Treatment::class,Modules\Diseases\Models\Diseases::class],
    //         [null, null, null, null, null,'medicines->diseases_id']
    //     );
    // }


    public function getCanDeleteAttribute()
    {
        if (
            $this->states->count()
        ) {
            return false;
        }
        return true;
    }
   
}