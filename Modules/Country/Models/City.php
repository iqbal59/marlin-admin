<?php

namespace Modules\Country\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $table = 'cities';

    protected $fillable = [
        'name','country_id','state_id','status'
    ];

    public function hospitals()
    {
        return $this->hasMany('Modules\Hospital\Models\Hospital');
    }

    public function states()
    {
        return $this->belongsTo('Modules\Country\Models\State',  'state_id', 'id');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->hospitals->count()
        ) {
            return false;
        }
        return true;
    }
    


   
}