<?php

namespace Modules\Country\Repositories\Includes\Country;


use App\Repositories\BaseRepository;
use Modules\Country\Models\Country;
use Illuminate\Database\Eloquent\Builder;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface;


class CountryRepository implements CountryRepositoryInterface
{
    public function getForDatatable()
    {
        return Country::orderBy('id', 'DESC')->get();
    } 

    public function all()
    {
        return Country::orderBy('id', 'DESC')->get();
    } 

    public function getAll()
    {
        return Country::where('status',1)->get();
    } 

    public function save(array $input)
    {
        if ($country =  Country::create($input)) {
            return $country;
        }
        return false;
    }

    public function get($countryId)
    {
        return Country::find($countryId);
    } 

    public function update(array $input)
    {
        $country = Country::find($input['id']);
        unset($input['id']);
        if ($country->update($input)) {
            return $country;
        }
        return false;
    }

    public function delete(string $id)
    {
        $country = Country::find($id);
        return $country->delete();
    }

    public function getCountryBasedHospital($countryId)
    {
        // $hospitalData = Country::with('hospitals')->where('id',$countryId)->first();
        // return $hospitalData;
        $hospitalData = Country::whereHas('hospitals', function ($query) use ($countryId){
            $query->where('countries.id', $countryId);
        })
        ->with(['hospitals' => function($query) {
            $query->where('hospitals.status',1);
        }])->first();

        return $hospitalData;
    }

    public function getHospitalBasedonCountry($countryId)
    {
        $hospitalData = Country::with('hospitals')->where('id',$countryId)->first();
        return $hospitalData;
    }

    public function getSearchBasedDoctors($data,$searchText)
    {
        return Country::with('doctors')
                    ->whereRelation('doctors', 'doctor.name', 'like',  "%{$searchText}%")
                    ->get();            
    }

    public function getSearchBasedHospitals($data,$searchText)
    {
        return Country::with('hospitals')
                    ->whereRelation('hospitals', 'hospitals.title', 'like',  "%{$searchText}%")
                    ->get();            
    }

    public function getSearchBasedTreatments($data,$searchText)
    {
        return Country::with('treatments')
                    ->whereRelation('treatments', 'treatments.name', 'like',  "%{$searchText}%")
                    ->get();            
    }

    public function countryBasedDoctor($countryId)
    {
        $data = Country::where('id',$countryId)->first(); 
        return $data->doctors;
    }
    /**/
    
    public function getCountryBasedDoctors($countryId,$cityId,$experience,$stateId,$serachText)
    {
        // $data = Country::with(['doctors'])->where(function (Builder $query) use ($countryId) {
        //             if ($countryId != "") {
        //                 $query->where('id', '=', $countryId);
        //             }
        //         })      
        //         ->first();         
        // return $data->doctors ;   
        
        
        $data = Country::
        with(['doctors' => function($query) use ($serachText){
            $query->where('doctor.name', 'like', '%'.$serachText.'%')->where('doctor.status',1);
        }])
        ->whereHas('doctors', function ($query) use ($serachText){
            $query->where('doctor.name', 'like', '%'.$serachText.'%')->where('doctor.status',1);
        })
        ->where(function (Builder $query) use ($countryId) {
                        if ($countryId != "") {
                            $query->where('id', '=', $countryId);
                        }
                    })->first();     

        if($data != null){
            return $data->doctors ;   
        }else{ 
            return [];
        }    
    }

    public function countryBasedHospital($countryId,$cityId,$departmentId,$searchText)
    { 
        $hospitalData = Country::with('hospitals')->first();
        return $hospitalData;
    }  

    public function getCountryBasedTelemedicineDoctors($countryId,$cityId,$departmentId,$experience)
    {
        $data = Country::with(['doctors'])->where(function (Builder $query) use ($countryId) {
                    if ($countryId != "") {
                        $query->where('id', '=', $countryId);
                    }
                })      
                ->first();         
        return $data->telemedicinedoctors ;       
    
    }

    public function getPhoneCode()
    {
        $phoneCode = Country::select('id','phonecode')->get();
        return $phoneCode;
    }

    public function getCountryBasedHospitals($countryId,$cityId,$departmentId,$searchText)
    {
        if($searchText ==null){ 
            // $hospitalData = Country::with('hospitals')->where('id',$countryId)->first(); 
            // return $hospitalData;
            $hospitalData = Country::whereHas('hospitals', function ($query) use ($countryId){
                $query->where('countries.id',$countryId);
            })
            ->with(['hospitals' => function($query){
                $query->where('hospitals.status',1);
            }])->first();

            return $hospitalData;
        }
        else{ 
            $hospitalData = Country::whereHas('hospitals', function ($query) use ($searchText){
                $query->where('title', 'like', '%'.$searchText.'%');
            })
            ->with(['hospitals' => function($query) use ($searchText){
                $query->where('title', 'like', '%'.$searchText.'%')->where('hospitals.status',1);
            }])->first();
        
            return $hospitalData;
        }
    }

    public function getStateBasedHospital($cityId)
    {
        return $hospitalData = Country::with(['hospitals' => function($query) use ($cityId){
            $query->whereIn('city_id',$cityId);
        }])->first();
    }

    public function getCountryNameFromPhoneCode($phoneCode)
    {
        return Country::where('phonecode',$phoneCode)->first();
    }
}