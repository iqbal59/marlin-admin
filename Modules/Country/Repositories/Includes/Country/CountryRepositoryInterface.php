<?php

namespace Modules\Country\Repositories\Includes\Country;

interface CountryRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function getAll();

    public function save(array $input);

    public function get($countryId);

    public function update(array $input);

    public function delete(string $id);

    public function getCountryBasedHospital($countryId);

    public function getHospitalBasedonCountry($countryId);

    public function getSearchBasedDoctors($data,$searchText);

    public function getSearchBasedHospitals($data,$searchText);

    public function getSearchBasedTreatments($data,$searchText);

    public function countryBasedDoctor($countryId);

    /**/ 
    public function getCountryBasedDoctors($countryId,$cityId,$experience,$stateId,$serachText);

    public function countryBasedHospital($countryId,$cityId,$departmentId,$searchText);

    public function getCountryBasedTelemedicineDoctors($countryId,$cityId,$departmentId,$experience);

    public function getPhoneCode();

    public function getCountryBasedHospitals($countryId,$cityId,$departmentId,$searchText);

    public function getStateBasedHospital($cityId);

    public function getCountryNameFromPhoneCode($phoneCode);

}