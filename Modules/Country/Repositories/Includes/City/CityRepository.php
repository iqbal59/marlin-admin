<?php

namespace Modules\Country\Repositories\Includes\City;


use App\Repositories\BaseRepository;
use Modules\Country\Models\City;
use Illuminate\Database\Eloquent\Builder;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface;


class CityRepository implements CityRepositoryInterface
{
    public function getForDatatable()
    {
        return City::orderBy('id', 'DESC')->get();
    }

    public function all()
    {
        return City::where('status',1)->orderBy('id', 'DESC')->get();
    }
    public function get($cityId)
    { 
        return City::where('id',$cityId)->first();
    }
    public function save(array $input)
    { 
        if ($state =  City::create($input)) {
            return $state;
        }
        return false;
    }
    public function update(array $input)
    {
        $state = City::find($input['id']);
        unset($input['id']);
        if ($state->update($input)) {
            return $state;
        }
        return false;
    }

    public function delete(string $id)
    {
        $state = City::find($id);
        return $state->delete();
    }

    public function getCityByCountry($countryId)
    {
        return City::where('country_id',$countryId)->pluck('id')->toArray();
    }

    public function getActiveCityByCountry($countryId)
    {
        return City::where('status',1)->where('country_id',$countryId)->pluck('id')->toArray();
    }

    public function getCityByState($stateId)
    {
        return City::where('status',1)->where('state_id',$stateId)->pluck('id')->toArray();
    }

    public function getCountry($cityId)
    {
        return City::where('id',$cityId)->value('country_id');
    } 

    public function getCity($countryId,$stateId)
    {
        return City::where('status',1)->where('country_id',$countryId)->where('state_id',$stateId)->get();
    } 

    
     
}