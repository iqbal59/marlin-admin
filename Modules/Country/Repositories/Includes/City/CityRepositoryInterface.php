<?php

namespace Modules\Country\Repositories\Includes\City;

interface CityRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function get($stateId);

    public function save(array $input);

    public function update(array $input);

    public function delete(string $id);
    
    public function getCityByCountry($countryId);

    public function getActiveCityByCountry($countryId);

    public function getCityByState($stateId);

    public function getCountry($cityId);

    public function getCity($countryId,$stateId);
}