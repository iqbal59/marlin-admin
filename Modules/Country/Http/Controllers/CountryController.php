<?php

namespace Modules\Country\Http\Controllers;

use Modules\Country\Http\Controllers\Controller;
use Modules\Country\Http\Controllers\Country\Includes\Country;
use Modules\Country\Http\Controllers\Country\Includes\State;
use Modules\Country\Http\Controllers\Country\Includes\City;

class CountryController extends Controller
{
    use Country;
    use State;
    use City;
    
}

