<?php

namespace Modules\Country\Http\Controllers\Country\Includes;
use Modules\Country\Http\Requests\City\CityAddRequest;
use Modules\Country\Http\Requests\City\CitySaveRequest;
use Modules\Country\Http\Requests\City\CityEditRequest;
use Modules\Country\Http\Requests\City\CityUpdateRequest;
use Modules\Country\Http\Requests\City\CityViewRequest;
use Modules\Country\Http\Requests\City\CityDeleteRequest;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Country\Repositories\Includes\State\StateRepositoryInterface as StateRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Illuminate\Http\Request;
use Modules\Country\Models\State;
trait City
{
        public function listAllCity(CityRepository $cityRepo)
        {  
            $cities = $cityRepo->getForDatatable();                 
            return view('country::city.listCity', compact('cities'));           
        }
        public function viewCity(CityViewRequest $request,CityRepository $cityRepo, StateRepository $stateRepo, CountryRepository $countryepo, HospitalRepository $hospitalRepo)
        {
            $city = $cityRepo->get($request->id);   
            $state = $stateRepo->get($city->state_id);
            $country = $countryepo->get($city->country_id);  
            $hospitals = $hospitalRepo->getHospitals($city->id);          
            return view('country::city.viewCity', compact('city','country','state','hospitals'));
        }

        public function addCity(CityAddRequest $request,StateRepository $stateRepo, CityRepository $cityRepo, CountryRepository $countryepo)
        {  
            $city = $cityRepo->all();
            $country = $countryepo->all();
            $state = $stateRepo->all(); 
            return view('country::city.addCity',compact('country','city','state'));
        }

        public function saveCity(CitySaveRequest $request, CityRepository $cityRepo)
        {         
            $inputData = [
                'name'  => $request->name,
                'country_id'  => $request->country_id,
                'state_id'  => $request->state_id,
            ];
            $country = $cityRepo->save($inputData);   
            connectify('success', 'Success!', 'City added successfully');                   
            return redirect()
                ->route('admin.city_list');       
        } 

        public function editCity(CityEditRequest $request,CityRepository $cityRepo,  CountryRepository $countryRepo, StateRepository $stateRepo)
        {   
            $city = $cityRepo->get($request->id); 
            $state = $stateRepo->get($city->state_id);
            $country = $countryRepo->get($city->country_id);
            $countryData = $countryRepo->all();
            $stateData = $stateRepo->getStates($city->country_id);
            return view('country::city.editCity', compact('city','country','state','countryData','stateData'));
        }

        public function updateCity(CityUpdateRequest $request, CityRepository $cityRepo)
        {   
            $inputData = [
                'id' => $request->id,            
                'name' => $request->name,
                'country_id' => $request->country_id,
                'state_id' => $request->state_id,
            ];

            if(isset($request->status)){ 
                $inputData['status']=($request->status == "on")?1:0;
            }else{ 
                $inputData['status']='0';
            }
    
            $city = $cityRepo->update($inputData);
            connectify('success', 'Success!', 'City updated successfully');                   
            return redirect()
                ->route('admin.city_list');
        }

        public function deleteCity(CityRepository $cityRepo, CityDeleteRequest $request)
        { 
            if ($cityRepo->delete($request->id)) {
                connectify('success', 'Success!', 'City deleted successfully');                   
                return redirect()
                ->route('admin.city_list');
            }
            return response()->json(['status' => 0, 'message' => "failed"]);
        }
        public function search(Request $request)
        {  
            $data['states'] = State::where("country_id",$request->country_id)->get(["name","id"]);  
            return response()->json($data);
        }

}