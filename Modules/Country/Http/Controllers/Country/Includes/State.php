<?php

namespace Modules\Country\Http\Controllers\Country\Includes;
use Modules\Country\Http\Requests\City\CityAddRequest;
use Modules\Country\Http\Requests\State\StateSaveRequest;
use Modules\Country\Http\Requests\State\StateEditRequest;
use Modules\Country\Http\Requests\State\StateUpdateRequest;
use Modules\Country\Http\Requests\State\StateViewRequest;
use Modules\Country\Http\Requests\State\StateDeleteRequest;

use Modules\Country\Repositories\Includes\State\StateRepositoryInterface as StateRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
trait State
{
        public function listStates(StateRepository $stateRepo)
        {  
            $states = $stateRepo->all();                      
            return view('country::state.listState', compact('states'));           
        }

        public function viewStates(StateViewRequest $request, StateRepository $stateRepo, CountryRepository $countryepo,HospitalRepository $hospitalRepo, CityRepository $cityRepo)
        {
            $state = $stateRepo->get($request->id);
            $country = $countryepo->get($state->country_id); 
            $city = $cityRepo->getCityByState($state->id); 
            $hospitals = $hospitalRepo->getHospitalsBasedOnCountry($city);
            return view('country::state.viewState', compact('country','state','hospitals'));
        }

        public function addStates(CityAddRequest $request,StateRepository $stateRepo, CountryRepository $countryepo)
        {  
            // $country = $stateRepo->all();
            $country = $countryepo->all();
             
            return view('country::state.addState',compact('country'));
        }

        public function saveStates(StateSaveRequest $request, StateRepository $stateRepo)
        {            
            $inputData = [
                'name'  => $request->name,
                'country_id'  => $request->country_id,
            ];
            $country = $stateRepo->save($inputData); 
            connectify('success', 'Success!', 'State added successfully');                         
            return redirect()
                ->route('admin.state_list');
       
        } 

        public function editStates(StateEditRequest $request, CountryRepository $countryRepo, StateRepository $stateRepo)
        {  
            $state = $stateRepo->get($request->id);
            $country = $countryRepo->get($state->country_id);
            $countryData = $countryRepo->all();
            return view('country::state.editState', compact('country','state','countryData'));
        }

        public function updateStates(StateUpdateRequest $request, StateRepository $stateRepo)
        { 
            $inputData = [
                'id' => $request->id,            
                'name' => $request->name,
                'country_id' => $request->country_id,
            ];
            if(isset($request->status)){ 
                $inputData['status']=($request->status == "on")?1:0;
            }else{ 
                $inputData['status']='0';
            }
    

    
            $state = $stateRepo->update($inputData);
            connectify('success', 'Success!', 'State updated successfully');                         
            return redirect()
                ->route('admin.state_list');
        }

        public function deleteStates(StateRepository $stateRepo, StateDeleteRequest $request)
        { 
            if ($stateRepo->delete($request->id)) {
                connectify('success', 'Success!', 'State deleted successfully');                         
                return redirect()
                ->route('admin.state_list');
            }
                return response()->json(['status' => 0, 'message' => "failed"]);
        }

}