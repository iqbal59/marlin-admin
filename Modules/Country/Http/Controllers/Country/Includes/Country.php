<?php

namespace Modules\Country\Http\Controllers\Country\Includes;
use Modules\Country\Http\Requests\Country\Country\CountryAddRequest;
use Modules\Country\Http\Requests\Country\Country\CountrySaveRequest;
use Modules\Country\Http\Requests\Country\Country\CountryEditRequest;
use Modules\Country\Http\Requests\Country\Country\CountryUpdateRequest;
use Modules\Country\Http\Requests\Country\Country\CountryViewRequest;
use Modules\Country\Http\Requests\Country\Country\CountryDeleteRequest;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;

trait Country  
{
        public function listCountry(CountryRepository $countryRepo)
        {
            $countries = $countryRepo->getForDatatable();          
            return view('country::country.listCountry', compact('countries'));           
        }
    
        public function add(CountryAddRequest $request)
        {  
            return view('country::country.addCountry');
        }
    
        public function save(CountryRepository $countryRepo, CountrySaveRequest $request)
        {
            $inputData = [
                'name'  => $request->name,
            ];
            $country = $countryRepo->save($inputData);
            connectify('success', 'Success!', 'Country added successfully');               
            return redirect()
                ->route('admin.country_list');
        }
    
        public function edit(CountryEditRequest $request, CountryRepository $countryRepo)
        { 
            $country = $countryRepo->get($request->id);
            return view('country::country.editCountry', compact('country'));
        }
    
        public function update(CountryUpdateRequest $request, CountryRepository $countryRepo)
        { 
            $inputData = [
                'id' => $request->id,            
                'name' => $request->name,
            ];

            if(isset($request->status)){ 
                $inputData['status']=($request->status == "on")?1:0;
            }else{ 
                $inputData['status']='0';
            }
    
            $banner = $countryRepo->update($inputData);
            connectify('success', 'Success!', 'Country updated successfully');               
            return redirect()
                ->route('admin.country_list');
        }
    
        public function view(CountryViewRequest $request, CountryRepository $countryRepo, HospitalRepository $hospitalRepo, CityRepository $cityRepo)
        {
            $coutryhospitals = $countryRepo->getHospitalBasedonCountry($request->id);
            // dd($coutryhospitals->treatments);            
            $country = $countryRepo->get($request->id); 
            $city = $cityRepo->getCityByCountry($country->id); 
            $hospitals = $hospitalRepo->getHospitalsBasedOnCountry($city);   
            $doctors = $coutryhospitals->doctors;    
            return view('country::country.viewCountry', compact('doctors','country','hospitals','coutryhospitals'));
        }
    
        public function delete(CountryRepository $countryRepo, CountryDeleteRequest $request)
        {
            if ($countryRepo->delete($request->id)) {
                connectify('success', 'Success!', 'Country deleted successfully');               
                 return redirect()
                ->route('admin.country_list');
            }
                return response()->json(['status' => 0, 'message' => "failed"]);
        }
       

}