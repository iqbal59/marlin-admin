<?php

namespace Modules\Country\Http\Controllers\Api;

use Modules\Country\Http\Controllers\Controller;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Carbon\Exceptions\Exception;
use Illuminate\Http\Request;
use Modules\Country\Http\Requests\Api\SearchRequest;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Country\Repositories\Includes\State\StateRepositoryInterface as StateRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Treatment\Repositories\TreatmentCategoryRepositoryInterface as TreatmentCategoryRepository;

class CountryController extends Controller
{
    public function listCity(CityRepository $cityRepo)
    {
        try {
            $cities = $cityRepo->all();
            $data = compact("cities");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function homeSearch(SearchRequest $request, CountryRepository $countryRepo)
    {
        try {
            $countryId = $request->country_id;
            $tabId = $request->tab_id;
            $searchText = $request->search_text;
            /**doctors */
            if ($tabId == 1) {
                if ($searchText == null) {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $doctors = $coutryhospitals->doctors;
                } else {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $doctors = $countryRepo->getSearchBasedDoctors($coutryhospitals, $searchText);
                }
                $data = compact("doctors");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
            /**hospitals */
            if ($tabId == 2) {
                if ($searchText ==null) {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $hospitals = $coutryhospitals->hospitals;
                } else {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $hospitals = $countryRepo->getSearchBasedHospitals($coutryhospitals, $searchText);
                }
                $data = compact("hospitals");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
            /**treatment */
            if ($tabId == 3) {
                if ($searchText ==null) {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $treaments = $coutryhospitals->treatments;
                } else {
                    $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                    $treaments = $countryRepo->getSearchBasedTreatments($coutryhospitals, $searchText);
                }
                $data = compact("treaments");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function doctorSearch(Request $request, DoctorRepository $doctorRepo, CountryRepository $countryRepo, CityRepository $cityRepo, DepartmentRepository $deptRepo, HospitalRepository $hospitalRepo)
    {
        try {
            if ($request->tab_id == 1) {
                $countryId = $request->country_id;
                $cityId = $request->city_id;
                $experience = $request->experience;
                $stateId = $request->state_id;
                $searchText = $request->search_text;

                //tabid alone
                if ($countryId == null && $cityId == null && $experience == null && $stateId == null) {
                    $doctor = $doctorRepo->allDoctor($searchText);
                }
                //country id
                if ($countryId !=null && $cityId == null && $experience == null && $stateId ==null) {
                    $cityId = $cityRepo->getCityByCountry($countryId);
                    $hospitalId= $hospitalRepo->getHospitalsIds($cityId);
                    $doctor = $doctorRepo->hospitalDoctors($hospitalId);
                }
                //experience
                if ($experience != null && $countryId ==null && $cityId == null && $stateId ==null) {
                    $doctor = $doctorRepo->all();
                    $doctorIds = $doctor->where('experience', $experience)->pluck('id')->toArray();
                    $doctor = $doctorRepo->doctorsData($doctorIds, $searchText);
                }
                //country & exp

                if ($countryId !=null && $experience != null && $cityId == null && $stateId ==null) {
                    $doctor = $countryRepo->getCountryBasedDoctors($countryId, $cityId, $experience, $stateId, $searchText);
                    if (($doctor != [])) {
                        $doctorIds = $doctor->where('experience', $experience)->pluck('id')->toArray();
                        $doctor = $doctorRepo->doctorsData($doctorIds, $searchText);
                    } else {
                        $doctor = [];
                        $data = compact("doctor");
                        $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                        return response()->json($response, 200);
                    }
                }
                //country & state
                if ($countryId !=null && $stateId !=null && $experience == null && $cityId == null) {
                    $cityIds = $cityRepo->getCityByState($stateId);
                    $doctor =$doctorRepo->getCityDoctors($cityIds, $searchText);
                }
                //country & state & city
                if ($countryId !=null && $stateId !=null && $cityId != null && $experience == null) {
                    $doctor = $doctorRepo->getCategoryBasedDetails($countryId, $cityId, $dept="", $exp="", $searchText);
                }
                //country & state & exp
                if ($countryId !=null && $stateId !=null && $experience != null && $cityId == null) {
                    $cityIds = $cityRepo->getCityByState($stateId);
                    $doctor =$doctorRepo->getCityDoctorBaseOnExperience($cityIds, $experience, $searchText);
                }
                //country && state && city && exp

                if ($countryId !=null && $stateId !=null && $experience != null && $cityId != null) {
                    $doctor = $doctorRepo->getCategoryBasedDetails($countryId, $cityId, $dept="", $experience, $searchText);
                }

                $details = $doctor->map(function ($doctor, $key) use ($cityRepo, $countryRepo, $deptRepo, $hospitalRepo) {
                    $hospitalIds = json_decode($doctor->hospital_id);
                    $hospitalDetails = $hospitalRepo->get($hospitalIds['0']);
                    $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                    $country = $countryRepo->get($cityDetails->country_id);
                    $department=$doctor->department_id != 'null'?$deptRepo->getDepartmentName(json_decode($doctor->department_id)):[];
                    $doctor->city_name = $cityDetails->name;
                    $doctor->country_name = $country->name;
                    // dd(json_decode($doctor->department_id));
                    $doctor->department_id = json_decode($doctor->department_id);
                    $doctor->department_name = $department;
                    $doctor->hospital_logo = $hospitalDetails->logo;
                    $langId = json_decode($doctor->lang_id);
                    $langArray=[];
                    if ($langId != null) {
                        foreach ($langId as $lang) {
                            if ($lang ==1) {
                                $langArray[]="English";
                            } elseif ($lang ==2) {
                                $langArray[]="Hindi";
                            } elseif ($lang ==4) {
                                $langArray[]="Tamil";
                            } else {
                                $langArray[]="Others";
                            }
                        }
                    }
                    $doctor->languages  = $langArray;
                    return $doctor;
                });
                $data = compact("doctor");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }


    // public function doctorSearch(Request $request,DoctorRepository $doctorRepo,CountryRepository $countryRepo)
    // {
    //     try {
    //         if ($request->tab_id == 1) {
    //             $countryId = $request->country_id;
    //             $cityId = $request->city_id;
    //             $departmentId = $request->department_id;
    //             $experience = $request->experience;
    //             if ($countryId != null && $cityId ==null && $departmentId ==null && $experience == null) {
    //                 $doctor = $countryRepo->getCountryBasedDoctors($countryId, $cityId, $departmentId, $experience);
    //                 $data = compact("doctor");
    //                 $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //                 return response()->json($response, 200);
    //             }
    //             if ($countryId !=null && ($departmentId !=null || $experience != null) && $cityId == null) {
    //                 $doctor = $countryRepo->getCountryBasedDoctors($countryId, $cityId, $departmentId, $experience);

    //                 $doctorID = $doctor->where('department_id',$departmentId)->pluck('id')->toArray();
    //                 $doctorIds = $doctor->where('experience',$experience)->pluck('id')->toArray();

    //                 if (($departmentId != null && $experience == null)) {
    //                          $doctor = $doctorRepo->doctorsData($doctorID);
    //                         $data = compact("doctor");
    //                         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //                         return response()->json($response, 200);
    //                  }
    //                 if (($experience != null && $departmentId == null)) {
    //                         $doctor = $doctorRepo->doctorsData($doctorIds);
    //                         $data = compact("doctor");
    //                         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //                         return response()->json($response, 200);
    //                 }
    //                 if ($departmentId != null && $experience != null) {
    //                          $doctorId = array_intersect($doctorID, $doctorIds);
    //                         $doctor = $doctorRepo->doctorsData($doctorId);
    //                         $data = compact("doctor");
    //                         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //                         return response()->json($response, 200);
    //                 }

    //             }
    //             if ($countryId == null) {
    //                 $doctor = $doctorRepo->getCategoryBasedDetails($countryId, $cityId, $departmentId, $experience);
    //                 $data = compact("doctor");
    //                 $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //                 return response()->json($response, 200);
    //             }
    //         }
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }


    public function hospitalSearch(Request $request, DoctorRepository $doctorRepo, CityRepository  $cityRepo, DepartmentRepository $deptRepo, CountryRepository $countryRepo, HospitalRepository $hospitalRepo)
    {
        try {
            if ($request->tab_id ==2) {
                $countryId = $request->country_id;
                $departmentId = $request->department_id;
                $cityId = $request->city_id;
                $searchText =$request->search_text;

                $coutryhospitals = $countryRepo->getCountryBasedHospitals($countryId, $cityId, $departmentId, $searchText);
                //tabid alone
                if ($countryId == null && $cityId == null && $departmentId == null) {
                    $hospitalsdetails = $hospitalRepo->searchHospital($searchText);
                }

                // coutry alone
                if ($countryId != null && $cityId == null && $departmentId == null) {
                    $hospitalsdetails = $coutryhospitals;
                    $cityIds = $cityRepo->getActiveCityByCountry($countryId);
                    $hospitalsdetails = $hospitalRepo->getHospitalsBasedOnCountries($cityIds, $searchText);
                }
                // city alone
                // if($cityId != null && $countryId == null && $departmentId == null){
                //     $hospitalsdetails = $hospitalRepo->HospitalBasedOnCountry($cityId,$searchText);

                // }
                // //dept alone
                // if( $departmentId != null && $cityId == null && $countryId == null){
                //     $hospitalsdetails = $deptRepo->departmentHospital($departmentId,$searchText);

                // }
                // //country & city
                // if($cityId != null && $countryId != null && $departmentId == null){
                //     $hospitalsdetails = $hospitalRepo->HospitalBasedOnCountry($cityId,$searchText);

                // }
                // //country && dept..
                // if($countryId != null && $departmentId != null && $cityId == null){
                //     $cityId = $cityRepo->getCityByCountry($countryId);
                //     $hospitalId= $hospitalRepo->getHospitalsIds($cityId);
                //     $hospitalsdetails = $deptRepo->departmentBasedHospital($departmentId,$hospitalId,$searchText);

                // }
                // //city && dept
                // if($cityId != null && $countryId == null && $departmentId != null){
                //     $hospitalId = $hospitalRepo->getHospitalsBasedOnCountry1($cityId);
                //     $hospitalsdetails = $deptRepo->departmentBasedHospital($departmentId,$hospitalId,$searchText);
                // }
                // if($countryId !=null && $cityId !=null && $departmentId !=null){
                //      $hospitalId = $hospitalRepo->getHospitalsBasedOnCountry1($cityId);
                //     $hospitalsdetails = $deptRepo->departmentBasedHospital($departmentId,$hospitalId,$searchText);
                // }
                if ($hospitalsdetails == null) {
                    $hospitalsdetails =[];
                }

                $details = $hospitalsdetails->map(function ($hospital, $key) use ($cityRepo, $countryRepo, $deptRepo, $doctorRepo) {
                    $cityDetails = $cityRepo->get($hospital->city_id);
                    $country = $countryRepo->get($cityDetails->country_id);
                    $dept = $deptRepo->getDepartmentData($hospital->id);



                    $hospital->city_name = $cityDetails->name;
                    $hospital->country_name = $country->name;

                    if ($dept == null) {
                        $hospital->department_name=null;
                    } else {
                        $hospital->department_name = $dept->name;
                    }

                    //  $departmentCount = $deptRepo->getDepartmentsForHospitalCount($hospital->id);
                    $departmentCount = $hospital->department_id?count(json_decode($hospital->department_id)):0;
                    $hospital->departmentCount =  $departmentCount;

                    $hospital->departmentCount =  $departmentCount;
                    $hospital->doctorCount =  $doctorRepo->getHospitalCount($hospital->id);
                    return $hospital;
                });
                $data = compact("hospitalsdetails");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function telemedicinedoctorSearch(Request $request, DoctorRepository $doctorRepo, CountryRepository $countryRepo)
    {
        try {
            if ($request->tab_id == 5) {
                $countryId = $request->country_id;
                $cityId = $request->city_id;
                $departmentId = $request->department_id;
                $experience = $request->experience;
                $serachText ="";
                if ($countryId != null && $cityId ==null && $departmentId ==null && $experience == null) {
                    $doctor = $countryRepo->getCountryBasedTelemedicineDoctors($countryId, $cityId, $departmentId, $experience);
                    $data = compact("doctor");
                    $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                    return response()->json($response, 200);
                }
                if ($countryId !=null && ($departmentId !=null || $experience != null) && $cityId == null) {
                    $doctor = $countryRepo->getCountryBasedTelemedicineDoctors($countryId, $cityId, $departmentId, $experience);
                    $doctorID = $doctor->where('department_id', $departmentId)->pluck('id')->toArray();
                    $doctorIds = $doctor->where('experience', $experience)->pluck('id')->toArray();

                    if (($departmentId != null && $experience == null)) {
                        $doctor = $doctorRepo->doctorsData($doctorID, $serachText);
                        $data = compact("doctor");
                        $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                        return response()->json($response, 200);
                    }
                    if ($departmentId == null && $experience != null) {
                        $doctor = $doctorRepo->doctorsData($doctorIds, $serachText);
                        $data = compact("doctor");
                        $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                        return response()->json($response, 200);
                    }
                    if (($experience != null && $departmentId != null)) {
                        $doctorId = array_intersect($doctorID, $doctorIds);
                        $doctor = $doctorRepo->doctorsData($doctorId, $serachText);
                        $data = compact("doctor");
                        $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                        return response()->json($response, 200);
                    }
                }
                if ($countryId == null) {
                    $doctor = $doctorRepo->getCategoryBasedTelemedicineDoctor($countryId, $cityId, $departmentId, $experience);
                    $data = compact("doctor");
                    $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                    return response()->json($response, 200);
                }
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function listCountry(CountryRepository $countryRepo)
    {
        try {
            $country = $countryRepo->getAll();
            $data = compact("country");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function listCountryCode(CountryRepository $countryRepo)
    {
        try {
            $phoneCode = $countryRepo->getPhoneCode();
            $data = compact("phoneCode");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    // public function CountryBasedhospital(Request $request,CityRepository  $cityRepo,DepartmentRepository $deptRepo,CountryRepository $countryRepo,HospitalRepository $hospitalRepo)
    // {
    //     try {
    //         $countryId = $request->country_id;
    //         $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
    //         $countryHospitals = $coutryhospitals->hospitals;
    //         $countryHospitals = $countryHospitals->take(10);
    //         $data = compact("countryHospitals");
    //         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //         return response()->json($response, 200);
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }

    public function CountryBasedhospital(Request $request, CityRepository  $cityRepo, DepartmentRepository $deptRepo, CountryRepository $countryRepo, HospitalRepository $hospitalRepo)
    {
        try {
            $countryId = $request->country_id;
            $stateId = $request->state_id;
            if ($stateId == null) {
                $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                $countryHospitals = $coutryhospitals->hospitals;
                $countryHospitals = $countryHospitals->take(10);
                $data = compact("countryHospitals");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $cityId = $cityRepo->getCityByState($stateId);
                $statehospital = $hospitalRepo->getActiveHospitalsBasedOnCountries($cityId, $search="");
                $statehospital = $statehospital->take(10);
                $data = compact("statehospital");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function CountryBasedBestDoctor(Request $request, CityRepository  $cityRepo, DepartmentRepository $deptRepo, CountryRepository $countryRepo, HospitalRepository $hospitalRepo, DoctorRepository $doctorRepo)
    {
        try {
            $countryId = $request->country_id;
            if (!isset($countryId)) {
                $doctor = $doctorRepo->all();
                $topDoctors = $doctorRepo->topDoctorsBasedOnCountry($doctor->pluck('id'));
                $details = $topDoctors->map(function ($topDoctor, $key) use ($deptRepo, $hospitalRepo, $cityRepo) {
                    $hospitalIds = json_decode($topDoctor->hospital_id);
                    $hospitalDetails = $hospitalRepo->get($hospitalIds['0']);
                    $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                    $hospitalDetails->cityDetails = $cityDetails;
                    $topDoctor->hospitalDetails = $hospitalDetails;
                    $topDoctor->department_id = json_decode($topDoctor->department_id);
                    return $topDoctor;
                });
            } else {
                $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                $doctor = $doctorRepo->getHospitals($coutryhospitals);
                $hospitalId = $coutryhospitals->hospitals->pluck('id');
                $doctor = $doctorRepo->hospitalData($hospitalId->toArray());
                $topDoctors = $doctorRepo->topDoctorsBasedOnCountry($doctor->pluck('id'));
                $details = $topDoctors->map(function ($topDoctor, $key) use ($deptRepo, $hospitalRepo, $cityRepo) {
                    $hospitalIds = json_decode($topDoctor->hospital_id);
                    $hospitalDetails = $hospitalRepo->get($hospitalIds['0']);
                    $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                    $hospitalDetails->cityDetails = $cityDetails;
                    $topDoctor->hospitalDetails = $hospitalDetails;
                    $topDoctor->department_id = json_decode($topDoctor->department_id);
                    return $topDoctor;
                });
            }
            $data = compact("topDoctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    // public function CountryBasedBestDoctor(Request $request,CityRepository  $cityRepo,DepartmentRepository $deptRepo,CountryRepository $countryRepo,HospitalRepository $hospitalRepo, DoctorRepository $doctorRepo)
    // {
    //     try {
    //         $countryId = $request->country_id;
    //         $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
    //         $countryDoctor=$coutryhospitals->doctors;
    //         $topDoctors = $doctorRepo->topDoctorsBasedOnCountry($countryDoctor->pluck('id'));
    //         $details = $topDoctors->map(function ($topDoctor, $key) use($deptRepo,$hospitalRepo,$cityRepo) {
    //             $departmentDetails = $deptRepo->get($topDoctor->department_id);
    //             $hospitalDetails = $hospitalRepo->getData($departmentDetails->hospital_id);
    //             $cityDetails = $cityRepo->get($hospitalDetails->city_id);
    //             $hospitalDetails->cityDetails = $cityDetails;
    //             $topDoctor->hospitalDetails = $hospitalDetails;
    //             return $topDoctor;
    //         });
    //         $data = compact("topDoctors");
    //         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //         return response()->json($response, 200);
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }

    public function listState(Request $request, StateRepository $stateRepo)
    {
        try {
            $countryId = $request->country_id;
            $countryBasedState = $stateRepo->getStates($countryId);
            $data = compact("countryBasedState");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function listStateBasedCity(Request $request, CityRepository $cityRepo)
    {
        try {
            $countryId = $request->country_id;
            $stateId = $request->state_id;
            $city = $cityRepo->getCity($countryId, $stateId);
            $data = compact("city");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function stateBasedTreatments(Request $request, CityRepository  $cityRepo, DepartmentRepository $deptRepo, CountryRepository $countryRepo, HospitalRepository $hospitalRepo, DoctorRepository $doctorRepo, StateRepository $stateRepo, TreatmentRepository $treatmentRepo, TreatmentCategoryRepository $treatmentCategoryRepo)
    {
        try {
            $stateId = $request->state_id;
            $city = $cityRepo->getCityByState($stateId);

            if ($stateId == null || (!(isset($stateId)))) {
                $treatments = $treatmentCategoryRepo->all();
                $details = $treatments->map(function ($category, $key) use ($treatmentRepo) {
                    $treatments = $treatmentRepo->categoryBasedTreatment($category->id);
                    $category->treatments = $treatments;
                    return $category;
                });
            } else {
                $city = $cityRepo->getCityByState($stateId);
                $statehospitals = $hospitalRepo->getHospitalsBasedOnCity($city);
                $hospitalId=array();
                foreach ($statehospitals as $hos) {
                    $hospitalId[]=$hos->id;
                }

                $treatments = $treatmentCategoryRepo->aaaa($hospitalId);
                $details = $treatments->map(function ($category, $key) use ($treatmentRepo) {
                    $treatments = $treatmentRepo->categoryBasedTreatment($category->id);
                    $category->treatments = $treatments;
                    return $category;
                });
            }
            $treatmentsDetails = $treatments->map(function ($treatment, $key) {
                if ($treatment->options != null) {
                    $treatment->options = json_decode($treatment->options);
                } else {
                    $treatment->options =[];
                }
                $treatment->department_id = json_decode($treatment->department_id);
                return $treatment;
            });
            $data = compact("treatments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function StateBasedBestDoctor(Request $request, StateRepository $stateRepo, CityRepository  $cityRepo, DepartmentRepository $deptRepo, CountryRepository $countryRepo, HospitalRepository $hospitalRepo, DoctorRepository $doctorRepo)
    {
        try {
            $stateId = $request->state_id;
            $city = $cityRepo->getCityByState($stateId);
            $statehospitals = $hospitalRepo->getHospitalsBasedOnCity($city);
            $hospitalId=array();
            foreach ($statehospitals as $hos) {
                $hospitalId[]=$hos->id;
            }
            $department = $deptRepo->deptData($hospitalId);
            $departmentId = array();
            foreach ($department as $dept) {
                $departmentId[]=$dept->id;
            }
            $doctors = $doctorRepo->getDepartmentDoctor($departmentId);
            $doctorId = array();
            foreach ($doctors as $doctor) {
                $doctorId[]=$doctor->id;
            }
            $topDoctors = $doctorRepo->topDoctorsBasedOnCountry($doctorId);
            $details = $topDoctors->map(function ($topDoctor, $key) use ($deptRepo, $hospitalRepo, $cityRepo) {
                $departmentDetails = $deptRepo->get($topDoctor->department_id);
                $hospitalDetails = $hospitalRepo->getData($departmentDetails->hospital_id);
                $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                $hospitalDetails->cityDetails = $cityDetails;
                $topDoctor->hospitalDetails = $hospitalDetails;
                return $topDoctor;
            });
            $data = compact("topDoctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    // public function countryBasedDepartment(Request $request, CountryRepository $countryRepo)
    // {
    //     try {
    //         $countryId = $request->country_id;
    //         $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
    //         $countryBasedDepartments = $coutryhospitals->departments;
    //         $data = compact("countryBasedDepartments");
    //         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //         return response()->json($response, 200);
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }
    public function countryBasedDepartment(Request $request, CountryRepository $countryRepo, HospitalRepository $hospitalRepo, DepartmentRepository $deptRepo)
    {
        try {
            $countryId = $request->country_id;
            $cityId = $request->city_id;
            if ($countryId != null && $cityId == null) {
                $coutryhospitals = $countryRepo->getCountryBasedHospital($countryId);
                $countryBasedDepartments = $coutryhospitals->activeDepartments;
                $data = compact("countryBasedDepartments");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } elseif (($cityId != null && $countryId == null) || ($countryId !=null && $cityId !=null)) {
                $cityHospital =$hospitalRepo->getHospitals($cityId);
                $hospitalId = array();
                foreach ($cityHospital as $hospital) {
                    $hospitalId[] = $hospital->id;
                }
                $cityBasedDepartment = $deptRepo->deptData($hospitalId);
                $data = compact("cityBasedDepartment");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function countryBasedTreatments(Request $request, TreatmentRepository $treatmentRepo, CountryRepository $countryRepo)
    {
        try {
            $countryId = $request->country_id;
            $countryhospitals = $countryRepo->getCountryBasedHospital($countryId);
            $countryBasedTreatments = $countryhospitals->activeTreatments;
            $data = compact("countryBasedTreatments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}