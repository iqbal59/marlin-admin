<?php

namespace Modules\Country\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class CityEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('city_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}