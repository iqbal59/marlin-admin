<?php

namespace Modules\Country\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class CityViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('city_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}