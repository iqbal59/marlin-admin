<?php

namespace Modules\Country\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class CityAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('city_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}