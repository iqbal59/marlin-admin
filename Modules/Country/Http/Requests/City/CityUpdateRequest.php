<?php

namespace Modules\Country\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class CityUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('city_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'required|max:50|min:2',
            'country_id' => 'required',
            'state_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'name.max' => ':attribute must be maximum of 50 character',
            'name.min' => ':attribute must be minimum of 2 character',
            'country_id.required' => ':attribute is required',
            'state_id.required' => ':attribute is required',

        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'country_id' => 'Country Name',
            'state_id' => 'State Name',
        ];
    }
}