<?php

namespace Modules\Country\Http\Requests\Country\Country;

use Illuminate\Foundation\Http\FormRequest;

class CountryDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('country_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}