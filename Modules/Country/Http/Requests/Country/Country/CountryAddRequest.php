<?php

namespace Modules\Country\Http\Requests\Country\Country;

use Illuminate\Foundation\Http\FormRequest;

class CountryAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('country_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}