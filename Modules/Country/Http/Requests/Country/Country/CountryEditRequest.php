<?php

namespace Modules\Country\Http\Requests\Country\Country;

use Illuminate\Foundation\Http\FormRequest;

class CountryEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('country_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}