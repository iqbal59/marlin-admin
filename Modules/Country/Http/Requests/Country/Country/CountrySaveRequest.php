<?php

namespace Modules\Country\Http\Requests\Country\Country;

use Illuminate\Foundation\Http\FormRequest;

class CountrySaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('country_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'name.max' => ':attribute must be maximum of 50 character',
            'name.min' => ':attribute must be minimum of 2 character',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
        ];
    }
}