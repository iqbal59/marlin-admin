<?php

namespace Modules\Country\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;

class StateAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('state_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}