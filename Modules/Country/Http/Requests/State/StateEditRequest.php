<?php

namespace Modules\Country\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;

class StateEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('state_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}