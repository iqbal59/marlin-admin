<?php

namespace Modules\Country\Http\Requests\State;

use Illuminate\Foundation\Http\FormRequest;

class StateDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('state_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}