<?php

namespace Modules\Country\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'country_id' => 'required',
            'tab_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'country_id.required' => ':attribute is required',
            'tab_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'country_id' => 'Country Id',
            'tab_id' => 'Tab Id',
        ];
    }
}