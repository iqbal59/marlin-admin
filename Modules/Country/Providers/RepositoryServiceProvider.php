<?php

namespace Modules\Country\Providers;

use Illuminate\Support\ServiceProvider;
// use Modules\Country\Repositories\Country\CountryRepository;
// use Modules\Country\Repositories\Country\CountryRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    { 
        // $this->app->bind(CountryRepositoryInterface::class, CountryRepository::class);

    }
    public function boot()
    {
        //
    }
}