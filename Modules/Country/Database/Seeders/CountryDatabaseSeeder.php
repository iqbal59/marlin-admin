<?php

namespace Modules\Country\Database\Seeders;

use App\Models\Permission;
use Modules\Country\Models\Country;
use Modules\Country\Models\State;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CountryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '55',
                'title'      => 'country_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '56',
                'title'      => 'country_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '57',
                'title'      => 'country_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '58',
                'title'      => 'country_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '59',
                'title'      => 'country_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],

            [
                'id'         => '60',
                'title'      => 'state_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '61',
                'title'      => 'state_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '62',
                'title'      =>  'state_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '63',
                'title'      => 'state_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '64',
                'title'      => 'state_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '65',
                'title'      => 'city_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '66',
                'title'      => 'city_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '67',
                'title'      =>  'city_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '68',
                'title'      => 'city_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '69',
                'title'      => 'city_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
        ];
        
        Permission::insert($permissions);
        
        $countries = array(
            array('id' => 1,'code' => 'IN','name' => "India",'phonecode' => 91),         
            array('id' => 2,'code' => 'AE','name' => "United Arab Emirates",'phonecode' => 971),
        );
        Country::insert($countries);

        $states = array(
            array('name' => "Andaman and Nicobar Islands",'country_id' => 1),
            array('name' => "Andhra Pradesh",'country_id' => 1),
            array('name' => "Arunachal Pradesh",'country_id' => 1),
            array('name' => "Assam",'country_id' => 1),
            array('name' => "Bihar",'country_id' => 1),
            array('name' => "Chandigarh",'country_id' => 1),
            array('name' => "Chhattisgarh",'country_id' => 1),
            array('name' => "Dadra and Nagar Haveli",'country_id' => 1),
            array('name' => "Daman and Diu",'country_id' => 1),
            array('name' => "Delhi",'country_id' => 1),
            array('name' => "Goa",'country_id' => 1),
            array('name' => "Gujarat",'country_id' => 1),
            array('name' => "Haryana",'country_id' => 1),
            array('name' => "Himachal Pradesh",'country_id' => 1),
            array('name' => "Jammu and Kashmir",'country_id' => 1),
            array('name' => "Jharkhand",'country_id' => 1),
            array('name' => "Karnataka",'country_id' => 1),
            array('name' => "Kenmore",'country_id' => 1),
            array('name' => "Kerala",'country_id' => 1),
            array('name' => "Lakshadweep",'country_id' => 1),
            array('name' => "Madhya Pradesh",'country_id' => 1),
            array('name' => "Maharashtra",'country_id' => 1),
            array('name' => "Manipur",'country_id' => 1),
            array('name' => "Meghalaya",'country_id' => 1),
            array('name' => "Mizoram",'country_id' => 1),
            array('name' => "Nagaland",'country_id' => 1),
            array('name' => "Narora",'country_id' => 1),
            array('name' => "Natwar",'country_id' => 1),
            array('name' => "Odisha",'country_id' => 1),
            array('name' => "Paschim Medinipur",'country_id' => 1),
            array('name' => "Pondicherry",'country_id' => 1),
            array('name' => "Punjab",'country_id' => 1),
            array('name' => "Rajasthan",'country_id' => 1),
            array('name' => "Sikkim",'country_id' => 1),
            array('name' => "Tamil Nadu",'country_id' => 1),
            array('name' => "Telangana",'country_id' => 1),
            array('name' => "Tripura",'country_id' => 1),
            array('name' => "Uttar Pradesh",'country_id' => 1),
            array('name' => "Uttarakhand",'country_id' => 1),
            array('name' => "Vaishali",'country_id' => 1),
            array('name' => "West Bengal",'country_id' => 1),
            array('name' => "Abu Zabi",'country_id' => 2),
            array('name' => "Ajman",'country_id' => 2),
            array('name' => "Dubai",'country_id' => 2),
            array('name' => "Ras al-Khaymah",'country_id' => 2),
            array('name' => "Sharjah",'country_id' => 2),
            array('name' => "Sharjha",'country_id' => 2),
            array('name' => "Umm al Qaywayn",'country_id' => 2),
            array('name' => "al-Fujayrah",'country_id' => 2),
            array('name' => "ash-Shariqah",'country_id' => 2),
        );
        State::insert($states);
    }
}
