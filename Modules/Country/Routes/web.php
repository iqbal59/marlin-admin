<?php

/*
|--------------------------------------------------------------------------
| Country Routes
|--------------------------------------------------------------------------
|
*/
 use Modules\Country\Http\Controllers\CountryController;
//  use Illuminate\Routing\Route;
 
Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::middleware('auth')->prefix('country')->name('country_')->group(function () {

        Route::get('/', [CountryController::class, 'listCountry'])->name('list');
        Route::get('/add', [CountryController::class, 'add'])->name('add');
        Route::post('save', [CountryController::class, 'save'])->name('save');
        Route::get('edit', [CountryController::class, 'edit'])->name('edit');
        Route::post('update', [CountryController::class, 'update'])->name('update');
        Route::get('view', [CountryController::class, 'view'])->name('view');
        Route::get('delete', [CountryController::class, 'delete'])->name('delete');
            
    });
        
    Route::middleware('auth')->prefix('state')->name('state_')->group(function () {
    
        Route::get('/', [CountryController::class, 'listStates'])->name('list');
        Route::get('/add', [CountryController::class, 'addStates'])->name('add');
        Route::post('save', [CountryController::class, 'saveStates'])->name('save');
        Route::get('edit', [CountryController::class, 'editStates'])->name('edit');
        Route::post('update', [CountryController::class, 'updateStates'])->name('update');
        Route::get('view', [CountryController::class, 'viewStates'])->name('view');
        Route::get('delete', [CountryController::class, 'deleteStates'])->name('delete');       
    });
    
    Route::middleware('auth')->prefix('city')->name('city_')->group(function () {
    
        Route::get('/', [CountryController::class, 'listAllCity'])->name('list');
        Route::get('/add', [CountryController::class, 'addCity'])->name('add');
        Route::post('save', [CountryController::class, 'saveCity'])->name('save');
        Route::get('edit', [CountryController::class, 'editCity'])->name('edit');
        Route::post('update', [CountryController::class, 'updateCity'])->name('update');
        Route::get('view', [CountryController::class, 'viewCity'])->name('view');
        Route::get('delete', [CountryController::class, 'deleteCity'])->name('delete');   
        Route::post('search', [CountryController::class, 'search'])->name('search');     
    });
});