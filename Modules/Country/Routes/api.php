<?php

use Illuminate\Http\Request;
use Modules\Country\Http\Controllers\Api\CountryController;

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/country', function (Request $request) {
    return $request->user();
});

Route::prefix('country')->group(function () {
    Route::get('/listCity', [CountryController::class, 'listCity']);
    Route::post('/homeSearch', [CountryController::class, 'homeSearch']);
    Route::post('/doctorSearch', [CountryController::class, 'doctorSearch']);
    Route::post('/hospitalSearch', [CountryController::class, 'hospitalSearch']);
    Route::post('/telemedicinedoctorSearch', [CountryController::class, 'telemedicinedoctorSearch']);
    Route::get('/listCountry', [CountryController::class, 'listCountry']);
    Route::get('/listCountryCode', [CountryController::class, 'listCountryCode']);
    Route::post('/CountryBasedhospital', [CountryController::class, 'CountryBasedhospital']);
    Route::post('/CountryBasedBestDoctor', [CountryController::class, 'CountryBasedBestDoctor']);

    Route::post('/listState', [CountryController::class, 'listState']);
    Route::post('/listStateBasedCity', [CountryController::class, 'listStateBasedCity']);
    Route::post('/stateBasedTreatments', [CountryController::class, 'stateBasedTreatments']);

    Route::post('/StateBasedBestDoctor', [CountryController::class, 'StateBasedBestDoctor']);
    Route::post('/countryBasedDepartment', [CountryController::class, 'countryBasedDepartment']);
    Route::post('/countryBasedTreatments', [CountryController::class, 'countryBasedTreatments']);

});