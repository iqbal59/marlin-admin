@extends('layouts.admin')
@section('content')
@can('doctor_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{route('admin.doctor.add', ["retrn_url" => "admin.doctor.list","from" => "doctor" ])}}">
                <i class="mdi mdi-plus"></i> 
                {{ trans('panel.add_doctor') }} 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
       {{ trans('panel.list_doctor') }} 
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>

                        <th>
                            Status
                        </th>

                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($doctors as $key => $doctor)
                        <tr data-entry-id="{{ $doctor->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $doctor->name ?? '' }}</td>
                            <td>
                                @if($doctor->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            
                            <td>
                            @can('doctor_show')      
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.doctor.view', ['id' => $doctor->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan 
                            @can('doctor_edit')  
                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.doctor.edit', ['id' => $doctor->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan 
                            @can('doctor_delete')    
                                    <form action="{{ route('admin.doctor.delete',  ['id' => $doctor->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$doctor->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            @endcan 

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 10,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})
</script>
@endsection