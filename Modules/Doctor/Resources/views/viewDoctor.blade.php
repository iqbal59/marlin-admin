@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
    {{ trans('panel.view_doctor') }} 
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    {{-- @php  dd($doctor); @endphp --}}
    <div class="card-body">
        @include('doctor::partials.tabSection')
    </div>
</div>
@endsection