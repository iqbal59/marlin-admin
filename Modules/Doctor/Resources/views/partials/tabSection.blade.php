<div class="row">
    <div class="col-md-12">
         
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Basic</a>
            </li>
             
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-enquiry-tab" 
                    data-toggle="pill" 
                    href="#pills-enquiry" 
                    role="tab" 
                    aria-controls="pills-enquiry" 
                    aria-selected="true">Enquiry</a>
            </li>

            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-faq-tab" 
                    data-toggle="pill" 
                    href="#pills-faq" 
                    role="tab" 
                    aria-controls="pills-faq" 
                    aria-selected="true">Faq</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('doctor::partials.sections.basicDetails')
            </div>
          
            <div class="tab-pane fade show" id="pills-enquiry" role="tabpanel" aria-labelledby="pills-enquiry-tab">
                @include('doctor::partials.sections.enquiryTable')
            </div>

            <div class="tab-pane fade show" id="pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">
                @include('doctor::partials.sections.faqTable')
            </div>
        </div>
    </div>
</div>