<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
  color: orange;
}
</style> 
<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $doctor->id }}</td>
            </tr>
            <tr>
                <th>{{ trans('cruds.user.fields.name') }}</th>
                <td>{{ $doctor->name }}</td>
            </tr>
            @if($doctor->qualification !=null)
            <tr>
                <th>{{ trans('panel.qualification') }} </th>
                <td>{{ $doctor->qualification }}</td>
            </tr>
            @endif
            @if($doctor->achievements !=null)
            <tr>
                <th>Achievements </th>
                <td>{{ $doctor->achievements }}</td>
            </tr>
            @endif
            @if($doctor->meta_title !=null)
            <tr>
                <th>Meta Title  </th>
                <td>{{ $doctor->meta_title }}</td>
            </tr>
            @endif
            @if($doctor->meta_keywords !=null)
            <tr>
                <th>Meta Keyword  </th>
                <td>{{ $doctor->meta_keywords }}</td>
            </tr>
            @endif
            @if($doctor->meta_description !=null)
            <tr>
                <th>Meta description  </th>
                <td>{{ $doctor->meta_description }}</td>
            </tr>
            @endif
            @if($doctor->designation !=null)
            <tr>
                <th>Designation  </th>
                <td>{{ $doctor->designation }}</td>
            </tr>
            @endif
            
            @if($doctor->experience !=null)
            <tr>
                <th>{{ trans('panel.experience') }} </th>
                <td>{{ $doctor->experience }}</td>
            </tr>
            @endif
            @if($dept  !=null)
            <tr>
                <th> {{ trans('panel.list_department') }}  </th>
                <td> {{ $dept->name }}</td>
            </tr>
            @endif
            <tr>
                <th> {{ trans('panel.rating') }}</th>
                <td> 
                    @if($doctor->rating==null)
                        0
                    @else
                        @for ($i = 0; $i <$doctor->rating; $i++)
                            <span class="fa fa-star checked"></span>
                        @endfor
                    @endif
                </td>
            </tr>
             
            @if($doctor->image !='')
            <tr>
                <th>Image</th>
            <td>             
                <img src="{{asset('storage/'.$doctor->image)}}" alt="" width="500px">
            </td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif
           
            


        </tbody>
    </table>
<br>
    
</div>