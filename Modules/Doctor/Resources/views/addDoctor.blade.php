@extends('layouts.admin')
@section('content')
<style>
input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
}

.number-input {
    border: 2px solid #ddd;
    display: inline-flex;
}

.number-input,
.number-input * {
    box-sizing: border-box;
}

.number-input button {
    outline: none;
    -webkit-appearance: none;
    background-color: transparent;
    border: none;
    align-items: center;
    justify-content: center;
    width: 3rem;
    height: 2rem;
    cursor: pointer;
    margin: 0;
    position: relative;
}

.number-input button:before,
.number-input button:after {
    display: inline-block;
    position: absolute;
    content: '';
    width: 1rem;
    height: 2px;
    background-color: #212121;
    transform: translate(-50%, -50%);
}

.number-input button.plus:after {
    transform: translate(-50%, -50%) rotate(90deg);
}

.number-input input[type=number] {
    font-family: sans-serif;
    max-width: 3rem;
    padding: .5rem;
    border: solid #ddd;
    border-width: 0 2px;
    font-size: 1rem;
    height: 2rem;
    font-weight: bold;
    text-align: center;
}

.save-cn {
    margin-top: 10px;
}

i.mdi.mdi-minus {
    width: 2rem;
    text-align: center;
    line-height: 30px;
}

i.mdi.mdi-plus {
    width: 2rem;
    text-align: center;
    line-height: 30px;
}
</style>
<div class="card">
    <div class="card-header">
        {{ trans('panel.add_doctor') }}

    </div>

    <div class="card-body">
        <form action="{{ route('admin.doctor.save') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" value="{{!empty($retrn_url) ? $retrn_url : ''}}" name="retrn_url">
            <input type="hidden" value="{{!empty($retrn_prms) ? $retrn_prms : ''}}" name="retrn_prms">
            <input type="hidden" value="{{!empty($retrn_prms) ? $retrn_prms : ''}}" name="retrn_prms">

            <input type="hidden" value="{{$isTelemedicine}}" name="type">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('cruds.user.fields.name') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}">
                        @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                        <label for="qualification"> {{ trans('panel.qualification') }} </label>
                        <input type="text" id="qualification" name="qualification" class="form-control"
                            value="{{old('qualification')}}">
                        @if($errors->has('qualification'))
                        <em class="invalid-feedback">
                            {{ $errors->first('qualification') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                        <label for="designation"> Designation </label>
                        <input type="text" id="designation" name="designation" class="form-control"
                            value="{{old('designation')}}">
                        @if($errors->has('designation'))
                        <em class="invalid-feedback">
                            {{ $errors->first('designation') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('achievements') ? 'has-error' : '' }}">
                        <label for="achievements"> {{ trans('panel.achievements') }} </label>
                        <input type="text" id="achievements" name="achievements" class="form-control"
                            value="{{old('achievements')}}">
                        @if($errors->has('achievements'))
                        <em class="invalid-feedback">
                            {{ $errors->first('achievements') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('panel.rating') }} </label>
                        <select id="rating" name="rating" class="js-states form-control">
                            <option value="">Select rating</option>
                            <option value="1" @if(old('rating')==1) selected @endif>1</option>
                            <option value="2" @if(old('rating')==2) selected @endif>2</option>
                            <option value="3" @if(old('rating')==3) selected @endif>3</option>
                            <option value="4" @if(old('rating')==4) selected @endif>4</option>
                            <option value="5" @if(old('rating')==5) selected @endif>5</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('hospital_ids') ? 'has-error' : '' }}">
                        <label for="hospital_id">Hospital <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                        <select class="js-example-basic-multiple" name="hospital_ids[]" multiple="multiple"
                            id="hospital-select">
                            @foreach($hospitals as $hospital)
                            @if($selectedHospital != "")
                            <option value="{{$hospital->id}}" @if(in_array($hospital->id, $selectedHospital)) selected
                                @endif>{{$hospital->title}}</option>
                            @else
                            <option value="{{$hospital->id}}">
                                {{$hospital->title}}
                            </option>
                            @endif

                            @endforeach
                        </select>
                        @if($errors->has('hospital_ids'))
                        <em class="invalid-feedback">
                            {{ $errors->first('hospital_ids') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                        <label for="name"> {{ trans('panel.department') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i> </label>
                        <div id="sucess-msg">
                            <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                                multiple="multiple">
                                @if(isset($retrn_prms))
                                @foreach($alldept as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $hospitalDepartment)) selected
                                    @endif>{{$data->name}}</option>
                                @endforeach
                                @endif
                                <option value="">{{ trans('panel.select') }}</option>
                            </select>
                        </div>
                        @if($errors->has('department_id'))
                        <em class="invalid-feedback">
                            {{ $errors->first('department_id') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image">{{ trans('panel.image') }} (390 px * 450 px)</label>
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="experience">{{ trans('panel.experience') }} </label>
                        <br>
                        <div class="number-input">
                            <i class="mdi mdi-minus"
                                onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></i>
                            <input class="experience" min="0" name="experience" value="1" type="number">
                            <i class="mdi mdi-plus"
                                onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></i>
                        </div>
                    </div>
                </div>
            </div>



            <div>
                <input class="btn btn-danger save-cn" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent


<script>
$(document).ready(function() {
    $("#rating").select2({
        placeholder: "Select Rating",
        allowClear: true
    });
    $("#department_id").select2({
        placeholder: "Select Department",
        allowClear: true
    });
    $("#hospital_id").select2({
        placeholder: "Select Hospital",
        allowClear: true
    });
    $('#hospital-select').on('change', function() {
        //    alert($('#hospital-select').val())
        var hospital_id = $('#hospital-select').val();
        $("#state-dropdown").html('');
        $.ajax({
            url: "{{route('admin.doctor.search')}}",
            type: "POST",
            data: {
                hospital_id: hospital_id,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function(result) {
                $('#department_id').empty();
                $('#state-dropdown').html('<option value="">Select State</option>');
                $.each(result.dept, function(key, value) {
                    $('select[name="department_id[]"]').append(
                        '<option value="' + value.id + '">' + value.name +
                        '</option');
                });
            }
        });
    });
});
</script>
<script>
$(document).ready(function() {
    $('.minus').click(function() {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function() {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection