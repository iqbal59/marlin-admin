@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<html>

<body>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <style>
    input[type="number"] {
        -webkit-appearance: textfield;
        -moz-appearance: textfield;
        appearance: textfield;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
    }

    .number-input {
        border: 2px solid #ddd;
        display: inline-flex;
    }

    .number-input,
    .number-input * {
        box-sizing: border-box;
    }

    .number-input button {
        outline: none;
        -webkit-appearance: none;
        background-color: transparent;
        border: none;
        align-items: center;
        justify-content: center;
        width: 3rem;
        height: 2rem;
        cursor: pointer;
        margin: 0;
        position: relative;
    }

    .number-input button:before,
    .number-input button:after {
        display: inline-block;
        position: absolute;
        content: '';
        width: 1rem;
        height: 2px;
        background-color: #212121;
        transform: translate(-50%, -50%);
    }

    .number-input button.plus:after {
        transform: translate(-50%, -50%) rotate(90deg);
    }

    .number-input input[type=number] {
        font-family: sans-serif;
        max-width: 3rem;
        padding: .5rem;
        border: solid #ddd;
        border-width: 0 2px;
        font-size: 1rem;
        height: 2rem;
        font-weight: bold;
        text-align: center;
    }

    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }

    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }

    span.on {
        width: 95px;
        float: left;
    }

    .custom-switch {
        padding-left: 0;
    }

    .custom-switch .custom-control-label::before {
        width: 2rem;
    }

    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
    }

    .badge-contr span.off {
        width: 70px;
        float: left;
    }

    .badge-contr span.on {
        float: none;
    }

    .doctor-edit-cn span.select2-selection.select2-selection--multiple:after {
        border-color: #888 transparent transparent transparent;
        border-style: solid;
        border-width: 5px 4px 0 4px;
        height: 0;
        right: 5px;
        margin-left: -4px;
        margin-top: -2px;
        position: absolute;
        top: 50%;
        width: 0;
        content: "";
    }
    </style>
    @foreach($doctor->polls as $poll)
    @php $pollValue = $poll['value'];
    @endphp
    @endforeach
    <div class="card doctor-edit-cn">
        <div class="card-header">
            {{ trans('global.edit') }} Doctor
        </div>
        <div class="card-body">
            <form action="{{ route('admin.doctor.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$doctor->id}}">
                <input type="hidden" name="telemedicine" value="{{$isTelemedicine}}">
                <input type="hidden" name="retrnPrms" value="{{$retrnPrms}}">
                <input type="hidden" name="retrnUrl" value="{{$retrnUrl}}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.user.fields.name') }} <i
                                    class="mdi mdi-flag-checkered text-danger "></i></label>
                            <input type="text" id="name" name="name" class="form-control"
                                value="{{ old('name', isset($doctor) ? $doctor->name : '') }}">
                            @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                            <label for="qualification">{{ trans('panel.qualification') }}</label>
                            <input type="text" id="qualification" name="qualification" class="form-control"
                                value="{{ old('qualification', isset($doctor) ? $doctor->qualification : '') }}">
                            @if($errors->has('qualification'))
                            <em class="invalid-feedback">
                                {{ $errors->first('qualification') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                            <label for="content">Slug Url </label>

                            <input type="text" id="slug" name="slug" class="form-control"
                                value="{{ old('slug', isset($doctor) ? $doctor->slug : '') }}" required>
                            @if($errors->has('slug'))
                            <em class="invalid-feedback">
                                {{ $errors->first('slug') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('achievements') ? 'has-error' : '' }}">
                            <label for="achievements">{{ trans('panel.achievements') }} </label>
                            <input type="text" id="achievements" name="achievements" class="form-control"
                                value="{{ old('achievements', isset($doctor) ? $doctor->achievements : '') }}">
                            @if($errors->has('achievements'))
                            <em class="invalid-feedback">
                                {{ $errors->first('achievements') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('panel.rating') }}</i> </label>
                            <select id="rating" name="rating" class="js-states form-control">
                                <option value="1" @if(old('rating', $doctor->rating)==1) selected @endif>1</option>
                                <option value="2" @if(old('rating', $doctor->rating)==2) selected @endif>2</option>
                                <option value="3" @if(old('rating', $doctor->rating)==3) selected @endif>3</option>
                                <option value="4" @if(old('rating', $doctor->rating)==4) selected @endif>4</option>
                                <option value="5" @if(old('rating', $doctor->rating)==5) selected @endif>5</option>
                            </select>

                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Diseases </label>
                            @if($selecteValue == null)
                            <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                                @foreach($diseases as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                                @foreach($diseases as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteValue)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('hospital_ids') ? 'has-error' : '' }}">
                            <label for="hospital_id">Hospital <i class="mdi mdi-flag-checkered text-danger "></i>
                            </label>
                            <select class="js-example-basic-multiple" name="hospital_ids[]" multiple="multiple"
                                id="hospital-select">
                                @foreach ($hospitals as $key => $value)
                                <option value="{{$value->id}}" @if(in_array($value->id, $selectedHospitals)) selected
                                    @endif>{{$value->title}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('hospital_ids'))
                            <em class="invalid-feedback">{{ $errors->first('hospital_ids') }}</em>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                            <label for="name"> Department <i class="mdi mdi-flag-checkered text-danger "></i></label>
                            {{-- <select class="js-states form-control" name="department_id" id="department_id">
                                @foreach($department as $data)
                                @if($data->id == $doctor->department_id)
                                <option value="{{$data->id}}" name="department_id" selected=""> {{$data->name}}
                            </option>
                            @else
                            <option value="{{$data->id}}" name="department_id"> {{$data->name}} </option>
                            @endif
                            @endforeach
                            </select> --}}

                            @if($selectedDepartments == null)
                            <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                                multiple="multiple">
                                @foreach($hospitalDepartments as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                            @else
                            <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                                multiple="multiple">
                                @foreach($hospitalDepartments as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selectedDepartments)) selected
                                    @endif>{{$data->name}}</option>
                                @endforeach
                            </select>
                            @endif

                            @if($errors->has('department_id'))
                            <em class="invalid-feedback">{{ $errors->first('department_id') }}</em>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Specialization </label>
                            @if($selecteSpecialization == null)
                            <select class="js-example-basic-multiple" name="specialization[]" multiple="multiple">
                                @foreach($specialization as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="specialization[]" multiple="multiple">
                                @foreach($specialization as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteSpecialization)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="image">{{ trans('panel.image') }} </label>
                            @if(Storage::disk('public')->exists($doctor->image))
                            <img class="preview" src="{{asset('storage/'.$doctor->image)}}" alt="" width="150px">
                            <a href="{{route('admin.doctor.remove_file',['id'=>$doctor->id])}}"
                                class="confirm-delete text-danger">
                                <i class="material-icons pink-text">clear</i>
                            </a>
                            @else
                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                                    data-default-file="" />
                                @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="experience">{{ trans('panel.experience') }} </label>
                        <br>
                        <div class="number-input">
                            <i class="mdi mdi-minus"
                                onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></i>

                            <input class="experience" min="0" name="experience" type="number"
                                value="{{ old('experience', isset($doctor) ? $doctor->experience : '') }}">
                            <i class="mdi mdi-plus"
                                onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                            <label for="meta_title">Add Meta Title </label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"
                                value="{{ old('meta_title', isset($doctor) ? $doctor->meta_title : '') }}">
                            @if($errors->has('meta_title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_title') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                            <label for="meta_keywords">Add Meta Keyword </label>
                            <input type="text" id="meta_keywords" name="meta_keywords" class="form-control"
                                value="{{ old('meta_keywords', isset($doctor) ? $doctor->meta_keywords : '') }}">
                            @if($errors->has('meta_keywords'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_keywords') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                            <label for="meta_description">{{ trans('panel.meta_description') }} </label>
                            <input type="text" id="meta_description" name="meta_description" class="form-control"
                                value="{{ old('meta_description', isset($doctor) ? $doctor->meta_description : '') }}">
                            @if($errors->has('meta_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_description') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="Poll">Poll</label>
                        <div class="form-group">
                            <input type="number" id="poll" name="value" min="1" max="99"
                                value="{{ old('value', $pollValue ) }}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Problems </label>
                            @if($selecteProblems == null)
                            <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                                @foreach($problems as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                                @foreach($problems as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteProblems)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('designation') ? 'has-error' : '' }}">
                            <label for="designation">Designation</label>
                            <input type="text" id="designation" name="designation" class="form-control"
                                value="{{ old('designation', isset($doctor) ? $doctor->designation : '') }}">
                            @if($errors->has('designation'))
                            <em class="invalid-feedback">
                                {{ $errors->first('designation') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                            <label for="designation">Type</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" {{ old('type', $doctor->type == 1) == 1 ? "checked" : "" }}
                                    class="custom-control-input" id="type" name="type">

                                <span class="on">Normal</span>
                                <label class="custom-control-label" for="type"></label>
                                <span class="off">Telemedicine</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 badge-contr">
                        <div class="form-group {{ $errors->has('verified_badge') ? 'has-error' : '' }}">
                            <label for="designation">Verified Badge</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" {{ old('verified_badge', $doctor->verified_badge == 1) ?
                                "checked" : "" }} class="custom-control-input" id="verified_badge"
                                    name="verified_badge">
                                <span class="off">No</span>
                                <label class="custom-control-label" for="verified_badge"></label>
                                <span class="on">Yes</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Core Speciality </label>
                            @if($selecteCoreSpeciality == null)
                            <select class="js-example-basic-multiple" name="CoreSpeciality[]" multiple="multiple">
                                @foreach($coreSpeciality as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="CoreSpeciality[]" multiple="multiple">
                                @foreach($coreSpeciality as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteCoreSpeciality)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Speaks </label>
                            @if($selecteLang == null)
                            <select class="js-example-basic-multiple" name="langs[]" multiple="multiple">
                                <option value="1">English</option>
                                <option value="2">Hindi</option>
                                <option value="3">Tamil</option>
                                <option value="4">Arabic</option>
                                <option value="5">Other</option>
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="langs[]" multiple="multiple">
                                <option value="1" @if(in_array(1, $selecteLang)) selected @endif>English</option>
                                <option value="2" @if(in_array(2, $selecteLang)) selected @endif>Hindi</option>
                                <option value="3" @if(in_array(3, $selecteLang)) selected @endif>Tamil</option>
                                <option value="4" @if(in_array(4, $selecteLang)) selected @endif>Arabic</option>
                                <option value="5" @if(in_array(5, $selecteLang)) selected @endif>Other</option>
                            </select>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 badge-contr">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <label for="status">Status</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" {{ old('status', $doctor->status == 1) ? "checked" : "" }}
                                    class="custom-control-input" id="status" name="status">
                                <span class="off">No</span>
                                <label class="custom-control-label" for="status"></label>
                                <span class="on">Yes</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Treatment </label>
                            @if($selecteTreatments == null)
                            <select class="js-example-basic-multiple" name="treatments[]" multiple="multiple">
                                @foreach($treatments as $data)
                                <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="treatments[]" multiple="multiple">
                                @foreach($treatments as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteTreatments)) selected
                                    @endif>{{$data->name}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Patient Voice </label>
                            @if($selectepatientVoice == null)
                            <select class="js-example-basic-multiple" name="patientVoice[]" multiple="multiple">
                                @foreach($patientVoice as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="patientVoice[]" multiple="multiple">
                                @foreach($patientVoice as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selectepatientVoice)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Awards / Publication </label>
                            @if($selecteAwards == null)
                            <select class="js-example-basic-multiple" name="awards[]" multiple="multiple">
                                @foreach($awards as $data)
                                <option value="{{$data->id}}">{{$data->title}}</option>
                                @endforeach
                            </select>
                            @else
                            <select class="js-example-basic-multiple" name="awards[]" multiple="multiple">
                                @foreach($awards as $data)
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteAwards)) selected
                                    @endif>{{$data->title}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>

        </div>
    </div>
    @endsection
    @section('scripts')
    @parent

    <script>
    $(document).ready(function() {
        $("#rating").select2({
            placeholder: "Select Rating",
            allowClear: true
        });
        $("#department_id").select2({
            placeholder: "Select Department",
            allowClear: true
        });
        $("#diseases").select2({
            placeholder: "Select diseases",
            allowClear: true
        });
        $("#hospital_id").select2({
            placeholder: "Select Hospital",
            allowClear: true
        });
        $('#hospital-select').on('change', function() {
            // alert($('#hospital-select').val())
            var hospital_id = $('#hospital-select').val();
            $("#state-dropdown").html('');
            $.ajax({
                url: "{{route('admin.doctor.search')}}",
                type: "POST",
                data: {
                    hospital_id: hospital_id,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#department_id').empty();
                    $('#state-dropdown').html('<option value="">Select State</option>');
                    $.each(result.dept, function(key, value) {
                        $('select[name="department_id[]"]').append(
                            '<option value="' + value.id + '">' + value
                            .name + '</option');
                    });
                }
            });
        });

    });
    </script>
    <script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
    </script>
    @endsection