<?php


namespace Modules\Doctor\Repositories;

use Modules\Doctor\Models\Doctor;
use Modules\Country\Models\Country;
use Modules\Poll\Models\Poll;
use Illuminate\Database\Eloquent\Builder;
use Modules\Doctor\Entities\Doctor as EntitiesDoctor;

class DoctorRepository implements DoctorRepositoryInterface
{
    public function getForDatatable()
    {
        return Doctor::with('department', 'hospital')->orderBy('id', 'DESC')->get();
    }

    public function all()
    {
        return Doctor::where('status', 1)->with('department', 'hospital')->orderBy('id', 'DESC')->get();
    }

    public function getTelemedicineList()
    {
        return Doctor::where('type', 1)->orderBy('id', 'DESC')->get();
    }

    public function getActiveTelemedicineList()
    {
        // return Doctor::where('status',1)->where('type',1)->orderBy('rating', 'DESC')->get();
        $doctor = Doctor::select('doctor.*')
                    ->where('doctor.status', 1)
                    ->where('doctor.type', 1)
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $doctor;
    }

    public function save(array $input)
    {
        if ($doctor =  Doctor::create($input)) {
            return $doctor;
        }
        return false;
    }

    public function get($id)
    {
        return Doctor::with('diseases')->where('id', $id)->first();
    }

    public function getBySlug($slug)
    {
        return Doctor::where('slug', $slug)->where('status', 1)->first();
    }


    // public function getDoctor($id)
    // {
    //     return Doctor::where('department_id',$id)->get()->toArray();
    // }

    public function getDoctor($id)
    {
        return Doctor::with('hospital')->where('department_id', $id)->get();
    }

    public function departmentandCityBased($id, $cityId)
    {
        $doctor = Doctor::whereHas('hospital', function ($q) use ($cityId) {
            $q->where('city_id', '=', $cityId);
        })->where('department_id', $id)->get();

        return $doctor;
    }

    public function cityBased($cityId)
    {
        $doctor = Doctor::where('status', 1)->whereHas('hospital', function ($q) use ($cityId) {
            $q->where('city_id', '=', $cityId);
        })->get();
        return $doctor;
    }

    public function getDepartmentDoctor($departments)
    {
        return Doctor::where('status', 1)->whereIn('department_id', $departments)->get();
    }

    public function resetFile(string $id)
    {
        $doctor = Doctor::find($id);
        $doctor->image = '';
        return $doctor->save();
    }

    public function update(array $input)
    {
        $doctor = Doctor::find($input['id']);
        unset($input['id']);
        if ($doctor->update($input)) {
            return $doctor;
        }
        return false;
    }

    public function delete(string $id)
    {
        $doctor = Doctor::find($id);
        return $doctor->delete();
    }

    public function getDoctorCount()
    {
        $doctorCount = Doctor::count();
        return $doctorCount;
    }

    public function topDoctors()
    {
        $doctor = Doctor::select('doctor.*')
                    ->where('doctor.status', 1)
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $doctor;
    }


    public function topDoctorsBasedOnCountry($doctorId)
    {
        $doctor = Doctor::select('doctor.*')
                    ->whereIn('doctor.id', $doctorId)
                    ->where('doctor.status', 1)
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->take(10)
                    ->get();
        return $doctor;
    }

    public function similarDoctors($doctorId, $diseasesId)
    {
        $doctor = Doctor::where('status', 1)->where('diseases_id', 'like', '%'.$diseasesId. '%')->where('id', '!=', $doctorId)->get();
        return $doctor;
    }

    public function getBestDoctors($diseasesId)
    {
        $doctor = Doctor::select('doctor.*')
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->get();

        return $doctor;
    }

    public function bestDoctors($diseasesId)
    {
        $doctor = Doctor::select('doctor.*')
                    ->where('doctor.status', 1)
                    ->where('doctor.diseases_id', 'like', '%'.$diseasesId.'%')
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $doctor;
    }

    public function bestDoctorForProblems($problemsId)
    {
        $doctor = Doctor::select('doctor.*')
                    ->where('doctor.status', 1)
                    ->where('doctor.problems_id', 'like', '%'.$problemsId.'%')
                    ->join('polls', 'polls.pollable_id', '=', 'doctor.id')
                    ->where('polls.pollable_type', 'Modules\Doctor\Models\Doctor')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $doctor;
    }

    public function getDepartmentBasedDoctor($departments)
    {
        return Doctor::where('department_id', $departments)->get();
    }

    public function getDoctorBasedOnExperience($exp)
    {
        return Doctor::where('experience', $exp)->get();
    }

    public function getSpecificDoctors($clmn1, $clmn2, $data1, $data2)
    {
        return Doctor::where($clmn1, $data1)->where($clmn2, $data2)->get();
    }

    public function categoryBasedDoctor($cityId, $exp, $dept)
    {
        $doctor = Doctor::whereHas('hospital', function ($q) use ($cityId, $exp, $dept) {
            $q->where('city_id', '=', $cityId)->where('experience', '=', $exp)->where('department_id', $dept);
        })->get();
        return $doctor;
    }

    /** */
    public function getCategoryBasedDetails($countryId, $cityId, $departmentId, $experience, $searchText)
    {
        $doctor = Doctor::where('status', 1)->whereHas('hospital', function ($q) use ($cityId) {
            if ($cityId != "") {
                $q->where('city_id', '=', $cityId);
            }
        })
            ->where(function (Builder $query) use ($departmentId) {
                if ($departmentId != "") {
                    $query->where('department_id', '=', $departmentId);
                }
            })
            ->where(function (Builder $query) use ($experience) {
                if ($experience != "") {
                    $query->where('experience', '=', $experience);
                }
            })
            ->where(function (Builder $query) use ($searchText) {
                if ($searchText != "") {
                    $query->where('name', 'like', '%'.$searchText.'%');
                }
            })

            ->get();
        return $doctor;
    }

    public function doctorsData($doctorId, $searchText)
    {
        $doctor = Doctor::whereIn('id', $doctorId)
        ->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('name', 'like', '%'.$searchText.'%');
            }
        })->get();
        return $doctor;
    }

    public function getCategoryBasedTelemedicineDoctor($countryId, $cityId, $departmentId, $experience)
    {
        $doctor = Doctor::whereHas('hospital', function ($q) use ($cityId) {
            if ($cityId != "") {
                $q->where('city_id', '=', $cityId);
            }
        })
            ->where(function (Builder $query) use ($departmentId) {
                if ($departmentId != "") {
                    $query->where('department_id', '=', $departmentId);
                }
            })
            ->where(function (Builder $query) use ($experience) {
                if ($experience != "") {
                    $query->where('experience', '=', $experience);
                }
            })->where('type', 1)
            ->get();
        return $doctor;
    }

    public function getCityDoctors($cityId, $searchText)
    {
        $doctor = Doctor::where('status', 1)->whereHas('hospital', function ($q) use ($cityId, $searchText) {
            if ($cityId != "") {
                $q->whereIn('city_id', $cityId);
            }
            if ($searchText != "") {
                $q->where('doctor.name', 'like', '%'.$searchText.'%');
            }
        })->get();
        return $doctor;
    }


    public function getCityDoctorBaseOnExperience($cityId, $exp, $searchText)
    {
        $doctor = Doctor::where('status', 1)->whereHas('hospital', function ($q) use ($cityId) {
            if ($cityId != "") {
                $q->whereIn('city_id', $cityId);
            }
        })
        ->where(function (Builder $query) use ($exp) {
            if ($exp != "") {
                $query->where('experience', '=', $exp);
            }
        })
        ->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('name', 'like', '%'.$searchText.'%');
            }
        })

        ->get();
        return $doctor;
    }


    public function allDoctor($searchText)
    {
        return Doctor::where('status', 1)->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('name', 'like', '%'.$searchText.'%');
            }
        })->get();
    }

    public function getDepartmentTeliDoctor($departments)
    {
        return Doctor::where('status', 1)->where('type', 1)->whereIn('department_id', $departments)->get();
    }

    public function getDepartmentDoctorCount($id)
    {
        return Doctor::where('status', 1)->where('department_id', $id)->count();
    }

    public function getDoctorBasedDiseases($diseasesId)
    {
        $array = $diseasesId;
        $array = array_values(array_map('strval', $array));
        $doctor = Doctor::where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('diseases_id', $id);
            }
        })->get();
        return $doctor;
    }

    public function getDoctorBasedOnProblems($problemsId)
    {
        $array = $problemsId;
        $array = array_values(array_map('strval', $array));
        $doctor = Doctor::where('status', 1)->where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('problems_id', $id);
            }
        })->get();
        return $doctor;
    }

    public function hospitalDoctors($hospitalId)
    {
        $array = $hospitalId;
        $array = array_values(array_map('strval', $array));
        $doctor = Doctor::where('status', 1)->where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('hospital_id', $id);
            }
        })->get();
        return $doctor;
    }

    public function getTelemedicineDoctor($hospitalId)
    {
        return Doctor::where('status', 1)->where('type', 1)->WhereJsonContains('hospital_id', $hospitalId)->get();
    }

    public function searchBasedTeleDoctor($searchText)
    {
        $doctor = Doctor::where('status', 1)->where('name', 'like', '%'.$searchText. '%')->get();
        return $doctor;
    }


    public function cityBasedTeleDoctor($cityId)
    {
        $doctor = Doctor::where('status', 1)->where('city', 'like', '%'.$searchText. '%')->get();
        return $doctor;
    }


    public function getHospitals($hospitalId)
    {
        return Doctor::where('status', 1)->WhereJsonContains('hospital_id', $hospitalId)->get();
    }

    public function getHospitalCount($hospitalId)
    {
        $doctor = Doctor::whereRaw('json_contains(hospital_id, \'["' . $hospitalId . '"]\')')->get();
        return count($doctor);
        //    $doctor=Doctor::where('status',1)->WhereJsonContains('hospital_id', $hospitalId)->get();
        //    return count($doctor);
    }

    public function hospitalData($hospitalId)
    {
        $array = $hospitalId;
        $array = array_values(array_map('strval', $array));
        $doctor = Doctor::where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('hospital_id', $id);
            }
        })->get();

        return $doctor;
    }
}