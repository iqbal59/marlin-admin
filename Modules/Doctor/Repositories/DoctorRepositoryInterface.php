<?php

namespace Modules\Doctor\Repositories;

interface DoctorRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function getTelemedicineList();

    public function getActiveTelemedicineList();

    public function save(array $input);

    public function get($id);

    public function getBySlug($slug);

    public function getDoctor($id);

    public function resetFile(string $id);

    public function update(array $input);

    public function delete(string $id);

    public function getDoctorCount();

    public function topDoctors();

    public function topDoctorsBasedOnCountry($doctorId);

    public function similarDoctors($doctorId, $diseasesId);

    public function getDepartmentDoctor($departments);

    public function bestDoctors($diseasesId);

    public function bestDoctorForProblems($problemsId);

    public function departmentandCityBased($id, $cityId);

    public function cityBased($cityId);

    public function getDepartmentBasedDoctor($departments);

    public function getDoctorBasedOnExperience($exp);

    public function getSpecificDoctors($clmn1, $clmn2, $data1, $data2);

    public function categoryBasedDoctor($cityId, $exp, $dept);
    /**/
    public function getCategoryBasedDetails($countryId, $cityId, $departmentId, $experience, $searchText);

    public function doctorsData($doctorId, $searchText);

    public function getCategoryBasedTelemedicineDoctor($countryId, $cityId, $departmentId, $experience);

    public function getCityDoctors($cityId, $searchText);

    public function getCityDoctorBaseOnExperience($cityId, $exp, $searchText);

    public function allDoctor($searchText);

    public function getDepartmentTeliDoctor($departments);

    public function getDepartmentDoctorCount($id);

    public function getDoctorBasedDiseases($diseasesId);

    public function getDoctorBasedOnProblems($problemsId);

    public function hospitalDoctors($hospitalId);

    public function getTelemedicineDoctor($hospitalId);

    public function searchBasedTeleDoctor($searchText);

    public function getHospitals($hospitalId);

    public function getHospitalCount($hospitalId);

    public function hospitalData($hospitalId);
}