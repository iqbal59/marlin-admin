<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->string('name');
            $table->string('qualification')->nullable();
            $table->string('experience')->nullable();
            $table->string('designation')->nullable();
            $table->boolean('verified_badge')->nullable()->comment("0: No | 1: Yes");
            $table->string('image')->nullable();
            $table->string('achievements')->nullable();
            $table->boolean('is_top_pick')->default(false);
            $table->boolean('type')->nullable()->comment("0: Normal | 1: Telimdicine");
            $table->string('rating')->nullable();
            $table->integer('department_id')->nullable();
            $table->json('diseases_id')->nullable();
            $table->json('problems_id')->nullable();
            $table->json('treatment_id')->nullable();
            $table->json('core_specialty_id')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->json('lang_id')->nullable();
            $table->boolean('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor');
    }
}
