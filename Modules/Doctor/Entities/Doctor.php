<?php

namespace Modules\Doctor\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Doctor extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'doctor';

    protected $fillable = ['id','slug','name','designation','lang_id','qualification','core_specialty_id','problems_id','experience','verified_badge','image','achievements','type','rating','department_id','status','diseases_id','meta_title','meta_description','meta_keywords','treatment_id','patient_voice_id','awards_and_publication','specialization','hospital_id','added_by'];

    protected static function newFactory()
    {
        return \Modules\Doctor\Database\factories\DoctorFactory::new();
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}