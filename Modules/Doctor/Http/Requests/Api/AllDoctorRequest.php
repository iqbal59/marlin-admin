<?php

namespace Modules\Doctor\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class AllDoctorRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function attributes()
    {
        return [
            //
        ];
    }
}
