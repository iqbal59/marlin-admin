<?php

namespace Modules\Doctor\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class DoctorFaqRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'doctor_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'doctor_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'doctor_id' => 'Doctor Id',
        ];
    }
}
