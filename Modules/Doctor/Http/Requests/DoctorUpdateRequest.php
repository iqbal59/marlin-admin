<?php

namespace Modules\Doctor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            'hospital_ids' => 'required',
            'department_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'hospital_ids.required' => ':attribute is required',
            'department_id.required' => ':attribute is required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'hospital_ids' => 'Hospital',
            'hospital_ids.*' => 'Hospital',
            'department_id' => 'Department',
            'department_id.*' => 'Department',
        ];
    }
}
