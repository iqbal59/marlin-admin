<?php

namespace Modules\Doctor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
