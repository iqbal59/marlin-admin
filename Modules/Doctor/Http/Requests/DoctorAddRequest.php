<?php

namespace Modules\Doctor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
