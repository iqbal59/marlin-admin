<?php

namespace Modules\Doctor\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Doctor\Http\Requests\DoctorAddRequest;
use Modules\Doctor\Http\Requests\DoctorSaveRequest;
use Modules\Doctor\Http\Requests\DoctorEditRequest;
use Modules\Doctor\Http\Requests\DoctorUpdateRequest;
use Modules\Doctor\Http\Requests\DoctorViewRequest;
use Modules\Doctor\Http\Requests\DoctorDeleteRequest;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Enquiry\Repositories\EnquiryRepositoryInterface as EnquiryRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Poll\Models\Poll;
use Illuminate\Support\Facades\Storage;
use Modules\Problems\Repositories\Problems\ProblemsRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Modules\Cms\Repositories\Includes\CmsCategory\CmsCategoryRepositoryInterface as CmsCategoryRepository;
use Modules\Videos\Repositories\Includes\Videos\VideosRepositoryInterface as VideosRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Intervention\Image\ImageManagerStatic as Image;
use Modules\Department\Entities\Department;
use App\Repositories\User\UserRepositoryInterface as UserRepository;

class DoctorController extends Controller
{
    public function list(DoctorRepository $doctorRepo)
    {
        $doctors = $doctorRepo->getForDatatable();
        return view('doctor::listDoctor', compact('doctors'));
    }

    public function add(DoctorAddRequest $request, DepartmentRepository $deptRepo, CityRepository $cityRepo, HospitalRepository $hospitalRepo)
    {
        $is_dept_id  = $request->id != '' ? $deptRepo->get($request->id) : '';
        $departments = $deptRepo->all();
        $retrn_url   = $request->retrn_url;
        $retrn_prms  = $request->retrn_prms;
        $isTelemedicine  = $request->telemedicine != '' ? 1 : 0;
        $from = $request->from != '' ? $request->from: '';
        if ($from == "doctor" || $request->telemedicine == "on") {
            $dept = $deptRepo->all();
        } else {
            $dept  = $from != '' ? $deptRepo->getData($retrn_prms) : $deptRepo->getDepartmentsForHospital($retrn_prms);
            //dd($dept);
        }

        /*add doctor from country */
        if ($request->retrn_url == "admin.country_view") {
            $hospitalId = $hospitalRepo->getHospitalsIds($cityRepo->getCityByCountry($request->retrn_prms));
            $dept = $deptRepo->deptData($hospitalId);
        }
        $hospitals = $hospitalRepo->all();




        /**add doctor from hospital view*/
        if (isset($request->retrn_prms)) {
            $hospitalDepartment = $deptRepo->getDepartmentsForHospital($retrn_prms)->pluck('id')->toArray();
            $alldept = $deptRepo->all();
            $selectedHospital = array($retrn_prms);
        } else {
            $hospitalDepartment="";
            $alldept = $deptRepo->all();
            $selectedHospital="";
        }

        //dd($alldept);
        return view('doctor::addDoctor', compact('selectedHospital', 'alldept', 'hospitalDepartment', 'hospitals', 'dept', 'departments', 'is_dept_id', 'retrn_url', 'retrn_prms', 'isTelemedicine'));
    }

    public function save(DoctorSaveRequest $request, DoctorRepository $doctorRepo, $department_id = '')
    {
        $inputData = [
            'name'  => $request->name,
            'qualification'  => $request->qualification,
            'experience'  => $request->experience,
            'verified_badge'  => $request->verified_badge,
            'achievements'=>$request->achievements,
            'rating'=>$request->rating,
            'department_id'=>json_encode($request->department_id),
            'hospital_id' => json_encode($request->hospital_ids),
            'designation'=>$request->designation,
            'added_by'     => auth()->user()->id,
        ];

        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(390, 450);
            $image_resize->save(storage_path('app/public/doctorimage/' .$filename));
            $inputData['image'] = 'doctorimage/' .$filename;
        }
        $inputData['type']=($request->type == 1)?1:0;
        $doctor = $doctorRepo->save($inputData);
        $poll         = new Poll;
        $poll->value  = '1';
        $doctor->polls()->save($poll);

        if ($request->type ==1) {
            $request['retrn_url'] ="admin.teledoctor.teledoctorList";
        }

        if (!empty($request->retrn_prms)) {
            connectify('success', 'Success!', 'Doctor added successfully');
            return redirect()
            ->route($request['retrn_url'], ['id'=>$request['retrn_prms']]);
        } elseif (!empty($request->retrn_url)) {
            connectify('success', 'Success!', 'Doctor added successfully');
            return redirect()
            ->route($request['retrn_url']);
        } else {
            connectify('success', 'Success!', 'Doctor added successfully');
            return redirect()
            ->route('admin.doctor.list');
        }
    }

    public function view(EnquiryRepository $enquiryRepo, DoctorViewRequest $request, DoctorRepository $doctorRepo, DepartmentRepository $deptRepo, FaqRepository $faqRepo, UserRepository $userRepo, HospitalRepository $hospitalRepo)
    {
        $doctor = $doctorRepo->get($request->id);
        $hospital =  $hospitalRepo->hospitals(json_decode(($doctor->hospital_id)));

        // $deparments = $deptRepo->departments(json_decode(($doctor->department_id)));


        $dept = $deptRepo->get($doctor->department_id);
        $faqs = $faqRepo->getFaq($request->id, $type="Doctor");
        $enquiry = $enquiryRepo->getEnquiry($request->id, $type="Modules\Doctor\Models\Doctor");
        $addedBy = $userRepo->get($doctor->added_by);
        return view('doctor::viewDoctor', compact('addedBy', 'enquiry', 'doctor', 'dept', 'faqs', 'hospital'));
    }

    public function edit(CmsCategoryRepository $cmscategoryRepo, CmsRepository $cmsRepo, DoctorEditRequest $request, DoctorRepository $doctorRepo, DepartmentRepository $deptRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemRepo, TreatmentRepository $treatmentRepo, VideosRepository $videosRepo, HospitalRepository $hospitalRepo)
    {
        $doctor =$doctorRepo->get($request->id);
        $department = $deptRepo->getDepartmentsForHospital($doctor->hospital_id);
        $diseases = $diseasesRepo->all();
        $problems =  $problemRepo->all();
        $treatments = $treatmentRepo->allTreatments();
        $selecteValue = json_decode($doctor->diseases_id);
        $selecteProblems = json_decode($doctor->problems_id);
        $selecteTreatments = json_decode($doctor->treatment_id);
        $isTelemedicine  = $request->telemedicine != '' ? 1 : 0;
        $coreSpeciality = $cmsRepo->getByCategory(9);
        $selecteCoreSpeciality = json_decode($doctor->core_specialty_id);
        $selecteLang = json_decode($doctor->lang_id);
        $retrnPrms  = $request->retrn_prms != '' ? $request->retrn_prms : '';
        $retrnUrl = $request->retrn_url != '' ? $request->retrn_url : '';
        $patientVoice = $videosRepo->getPatientVoice();
        $selectepatientVoice = json_decode($doctor->patient_voice_id);

        $awards = $cmsRepo->getByCategory(5);
        $selecteAwards = json_decode($doctor->awards_and_publication);
        $specialization = $cmsRepo->getByCategory(10);
        $selecteSpecialization = json_decode($doctor->specialization);


        $selectedHospitals = json_decode($doctor->hospital_id);
        $selectedDepartments = json_decode($doctor->department_id);
        $hospitalDepartments = $deptRepo->deptData($selectedHospitals);



        $hospitals = $hospitalRepo->all();
        return view('doctor::editDoctor', compact('selectedDepartments', 'selectedHospitals', 'hospitalDepartments', 'hospitals', 'awards', 'selecteAwards', 'specialization', 'selecteSpecialization', 'treatments', 'selecteTreatments', 'retrnUrl', 'retrnPrms', 'selecteLang', 'selecteCoreSpeciality', 'coreSpeciality', 'isTelemedicine', 'doctor', 'problems', 'selecteProblems', 'department', 'diseases', 'selecteValue', 'patientVoice', 'selectepatientVoice'));
    }

    public function removeFile(DoctorRepository $doctorRepo, Request $request)
    {
        if ($this->_removeFile($doctorRepo, $request->id)) {
            $doctorRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly');
        return redirect()
            ->route('admin.doctor.edit', ['id' => $request->id]);
    }

    private function _removeFile($doctorRepo, $id)
    {
        $logo = $doctorRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }

    public function update(DoctorUpdateRequest $request, DoctorRepository $doctorRepo)
    {
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'qualification'  => $request->qualification,
            'slug'  => $request->slug,
            'experience'  => $request->experience,
            'achievements'=>$request->achievements,
            'rating'=>$request->rating,
            'department_id' => json_encode($request->department_id),
            'hospital_id'=>json_encode($request->hospital_ids),
            'diseases_id'=>$request->states,
            'problems_id'=>$request->problems,
            'meta_title'=>$request->meta_title,
            'meta_keywords'=>$request->meta_keywords,
            'meta_description'=>$request->meta_description,
            'designation'=>$request->designation,
            'core_specialty_id'=>$request->CoreSpeciality,
            'patient_voice_id'=>$request->patientVoice,
            'awards_and_publication'=>$request->awards,
            'lang_id'=>$request->langs,
            'treatment_id'=>$request->treatments,
            'specialization'=>$request->specialization,
        ];
        $inputData['type']=($request->type == "on")?1:0;
        $inputData['verified_badge']=($request->verified_badge == "on")?1:0;

        if (isset($request->status)) {
            $inputData['status']=($request->status == "on")?1:0;
        } else {
            $inputData['status']='0';
        }

        if ($request->hasFile('image')) {
            $this->_removeFile($doctorRepo, $request->id);
            // $imageUrl = Storage::disk('public')->putFile('doctorimage/',$request->file('image'));
            // $inputData['image'] = $imageUrl;
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(390, 450);
            $image_resize->save(storage_path('app/public/doctorimage/' .$filename));
            $inputData['image'] = 'doctorimage/' .$filename;
        }
        $poll     = new Poll;
        $doctor = $doctorRepo->update($inputData);
        $poll->pollable()->associate($doctor);
        $doctor->polls()->update(['value' => $request->value]);
        if (!empty($request->telemedicine) && $request->telemedicine == 1) {
            connectify('success', 'Success!', 'Doctor updated successfully');
            return redirect()
            ->route('admin.teledoctor.teledoctorList');
        } elseif (!empty($request->retrnUrl)) {
            $route = redirect()->route($request->retrnUrl, ['id' => $request->retrnPrms]);
            connectify('success', 'Success!', 'Doctor updated successfully');
            return $route;
        } else {
            connectify('success', 'Success!', 'Doctor updated successfully');
            return redirect()
            ->route('admin.doctor.list');
        }
    }

    public function delete(DoctorRepository $doctorRepo, DoctorDeleteRequest $request)
    {
        $route=!empty($request->telemedicine)?"admin.teledoctor.teledoctorList":"admin.doctor.list";
        if ($doctorRepo->delete($request->id)) {
            if (!empty($request->retrn_prms)) {
                connectify('success', 'Success!', 'Hospital Updated successfully');
                return redirect()
                ->route($request['retrn_url'], ['id' => $request->retrn_prms]);
            } else {
                connectify('success', 'Success!', 'Doctor deleted successfully');
                return redirect()
                ->route($route);
            }
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function teledoctorList(DoctorRepository $doctorRepo)
    {
        $doctors = $doctorRepo->getTelemedicineList();
        return view('doctor::listteleDoctor', compact('doctors'));
    }

    public function search(Request $request, DepartmentRepository $deptRepo)
    {
        $dept = $deptRepo->getDepartmentsForHospital($request->hospital_id);
        $data['dept'] = Department::where("status", '1')->get(["name","id"]);
        return response()->json($data);
    }
}