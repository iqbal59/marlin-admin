<?php

namespace Modules\Doctor\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Doctor\Http\Requests\Api\DoctorViewRequest;
use Modules\Doctor\Http\Requests\Api\DoctorFaqRequest;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Exception;
use Modules\Doctor\Models\Doctor;
use Modules\Enquiry\Models\Enquiry;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Modules\Videos\Repositories\Includes\Videos\VideosRepositoryInterface as VideosRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Diseases\Repositories\Tips\TipsRepositoryInterface as TipsRepository;
use Cohensive\Embed\Facades\Embed;

class DoctorController extends Controller
{
    public function list(DoctorRepository $doctorRepo)
    {
        try {
            $doctors = $doctorRepo->topDoctors();
            $details = $doctors->map(function ($doctors, $key) use ($doctorRepo) {
                $langId = json_decode($doctors->lang_id);
                $a=[];
                if ($langId != null) {
                    foreach ($langId as $lang) {
                        if ($lang ==1) {
                            $a[]="English";
                        } elseif ($lang ==2) {
                            $a[]="Hindi";
                        } elseif ($lang ==4) {
                            $a[]="Tamil";
                        } else {
                            $a[]="Others";
                        }
                    }
                }
                $doctors->languages  = $a;
                return $doctors;
            });
            $data = compact("doctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function view(DoctorViewRequest $request, DoctorRepository $doctorRepo, DepartmentRepository $deptRepo, FaqRepository $faqRepo, HospitalRepository $hospitalRepo, CityRepository $cityRepo, CountryRepository $countryRepo, CmsRepository $cmsRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctor = $doctorRepo->get($doctorId);
            $hospitalIs  = json_decode($doctor->hospital_id);
            $hospitalDetails = $hospitalRepo->get($hospitalIs[0]);
            $country = $cityRepo->get($hospitalDetails->city_id);
            $countryDetails = $countryRepo->get($country->country_id);
            $faqs = $faqRepo->getFaq($doctorId, $type="Doctor");
            $dept = $deptRepo->getDepartmentName(json_decode($doctor->department_id));
            $specialization = $cmsRepo->getSpecialization(json_decode($doctor->specialization));
            $doctor->department_name = $dept;
            $doctor->specialization_name = $specialization;
            $doctor->faqs = $faqs;
            $data = compact("doctor");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }

        // try {
        //     $doctorId = $request->doctor_id;
        //     $doctor = $doctorRepo->get($doctorId);
        //     $deptDetails = $deptRepo->get($doctor->department_id);
        //     $hospitalDetails = $hospitalRepo->get($deptDetails->hospital_id);
        //     $country = $cityRepo->get($hospitalDetails->city_id);
        //     $countryDetails = $countryRepo->get($country->country_id);
        //     $faqs = $faqRepo->getFaq($doctorId,$type="Doctor");
        //     if (!$doctor) {
        //         $response = ['status' => false, 'message' => 'The selected doctor id is invalid'];
        //         return response()->json($response, 200);
        //     }
        //     $dept = $deptRepo->get($doctor->department_id);
        //     $doctor->department_name = $dept->name;
        //     $doctor->faqs = $faqs;
        //     $doctor->countryDetails = $countryDetails;
        //     $data = compact("doctor");
        //     $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
        //     return response()->json($response, 200);
        // } catch (Exception $e) {
        //     $response = ['status' => false, 'message' => $e->getMessage()];
        //     return response()->json($response, 200);
        // }
    }


    public function slugview(DoctorViewRequest $request, DoctorRepository $doctorRepo)
    {
        try {
            $doctorId = $request->slug;
            $doctor = $doctorRepo->getBySlug($doctorId);

            $data = compact("doctor");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function similarDoctors(DoctorRepository $doctorRepo, DoctorViewRequest $request)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctor = $doctorRepo->get($doctorId);
            if ($doctor != null) {
                $similarDoctor = $doctorRepo->similarDoctors($doctorId, $doctor->diseases_id);
                if (!$similarDoctor) {
                    $response = ['status' => false, 'message' => 'No similar doctors available'];
                    return response()->json($response, 200);
                }
                $data = compact("similarDoctor");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'message' => 'Doctor id not available'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function telemedicineDoctors(DoctorRepository $doctorRepo, DepartmentRepository $deptRepo, HospitalRepository $hospitalRepo, CityRepository $cityRepo)
    {
        try {
            $telemedicineDoctors = $doctorRepo->getActiveTelemedicineList();
            $telemedicineDoctors = $telemedicineDoctors->take(4);
            $details = $telemedicineDoctors->map(function ($telemedicineDoctor, $key) use ($deptRepo, $hospitalRepo, $cityRepo) {
                $hospital = json_decode($telemedicineDoctor->hospital_id);
                $hospitalId = $hospital[0];
                $hospitalDetails = $hospitalRepo->getData($hospitalId);
                $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                $hospitalDetails->cityDetails = $cityDetails;
                $telemedicineDoctor->hospital = $hospitalDetails;
                $department=$telemedicineDoctor->department_id != 'null'?$deptRepo->getDepartmentName(json_decode($telemedicineDoctor->department_id)):[];
                $telemedicineDoctor->department_id = json_decode($telemedicineDoctor->department_id);
                $telemedicineDoctor->department_name = $department;
                return $telemedicineDoctor;
            });
            $data = compact("telemedicineDoctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function bestDiseasesDoctor(DoctorRepository $doctorRepo, Request $request)
    {
        try {
            $bestDoctors = $doctorRepo->bestDoctors($request->diseases_id);
            $data = compact("telemedicineDoctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
    public function teleDoctors(DoctorRepository $doctorRepo, Request $request, HospitalRepository $hospitalRepo, DepartmentRepository $departmentRepo)
    {
        try {
            $searchText = $request->search_text;
            $cityId = $request->city_id;
            if ((isset($searchText)) && (!(isset($cityId)))) {
                $telemedicineDoctors = $doctorRepo->searchBasedTeleDoctor($searchText);
                $details = $telemedicineDoctors->map(function ($telemedicineDoctor, $key) use ($departmentRepo, $hospitalRepo) {
                    $hospital = json_decode($telemedicineDoctor->hospital_id);
                    $hospitalDetails = $hospitalRepo->get($hospital['0']);
                    $telemedicineDoctor->hospital_logo = $hospitalDetails->logo;
                    $telemedicineDoctor->department_name = $departmentRepo->getDepartmentName(json_decode($telemedicineDoctor->department_id));
                });
            } else {
                $telemedicineDoctors = $doctorRepo->getActiveTelemedicineList();
                $details = $telemedicineDoctors->map(function ($telemedicineDoctor, $key) use ($departmentRepo, $hospitalRepo) {
                    $hospital = json_decode($telemedicineDoctor->hospital_id);
                    $hospitalDetails = $hospitalRepo->get($hospital['0']);
                    $telemedicineDoctor->hospital_logo = $hospitalDetails->logo;
                    $telemedicineDoctor->department_name = $departmentRepo->getDepartmentName(json_decode($telemedicineDoctor->department_id));
                });
            }
            $data = compact("telemedicineDoctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }












































    public function specialityDoctors(DoctorRepository $doctorRepo, Request $request, HospitalRepository $hospitalRepo, DepartmentRepository $departmentRepo)
    {
        try {
            $searchText = $request->search_text;
            $cityId = $request->city_id;
            if ((isset($searchText)) && (!(isset($cityId)))) {
                $doctors = $doctorRepo->allDoctor($searchText);
                $dcount = count($doctors);
                if (count($doctors) == 0) {
                    $hospitals = $hospitalRepo->searchHospital($searchText);
                    if (count($hospitals)!= 0) {
                        foreach ($hospitals as $hospital) {
                            $dept[] = $departmentRepo->getDepartmentsHospital($hospital->id);
                        }
                        $doctors = $doctorRepo->getDepartmentDoctor($dept);
                    } else {
                        $departments = $departmentRepo->searchBasedDepartment($searchText);
                        $doctors = $doctorRepo->getDepartmentDoctor($departments);
                    }
                }
            } elseif ((isset($cityId)) && (!(isset($searchText)))) {
                $doctors = $doctorRepo->cityBased($cityId);
            } elseif ((isset($cityId)) && ((isset($searchText)))) {
                $doctors = $doctorRepo->getCategoryBasedDetails($countryId="", $cityId, $departmentId="", $experience="", $searchText);
                if (count($doctors) == 0) {
                    $hospital = $hospitalRepo->HospitalBasedOnCountry($cityId, $searchText);
                    $hospitalId = $hospital->pluck('id')->toArray();
                    $dept = $departmentRepo->deptData($hospitalId);
                    $deptId = $dept->pluck('id')->toArray();
                    $doctors = $doctorRepo->getDepartmentDoctor($deptId);
                    if (count($doctors) == 0) {
                        $hospitalId = $hospitalRepo->getHospitalsBasedOnCountry1($cityId);
                        $dept = $departmentRepo->searchBasedDepartments($searchText, $hospitalId);
                        $doctors = $doctorRepo->getDepartmentDoctor($dept);
                    }
                }
            } else {
                $doctors = $doctorRepo->topDoctors();
            }
            $data = compact("doctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
        // try {
        //     $searchText = $request->search_text;
        //     $cityId = $request->city_id;
        //     if((isset($searchText)) && (!(isset($cityId)))){
        //         $doctors = $doctorRepo->allDoctor($searchText);
        //         $dcount = count($doctors);
        //         if(count($doctors) == 0){
        //             $hospitals = $hospitalRepo->searchHospital($searchText);
        //             if(count($hospitals)!= 0){
        //                 foreach($hospitals as $hospital)
        //                 {
        //                     $dept[] = $departmentRepo->getDepartmentsHospital($hospital->id);
        //                 }
        //                 $doctors = $doctorRepo->getDepartmentDoctor($dept);
        //             }else{
        //                $departments = $departmentRepo->searchBasedDepartment($searchText);
        //                $doctors = $doctorRepo->getDepartmentDoctor($departments);
        //             }
        //         }
        //     }
        //     elseif((isset($cityId)) && (!(isset($searchText)))){
        //        $doctors = $doctorRepo->cityBased($cityId);
        //     }elseif((isset($cityId)) && ((isset($searchText)))){
        //         $doctors = $doctorRepo->getCategoryBasedDetails($countryId="",$cityId,$departmentId="",$experience="",$searchText);
        //         if(count($doctors) == 0){
        //           $hospital = $hospitalRepo->HospitalBasedOnCountry($cityId,$searchText);
        //           $hospitalId = $hospital->pluck('id')->toArray();
        //           $dept = $departmentRepo->deptData($hospitalId);
        //           $deptId = $dept->pluck('id')->toArray();
        //           $doctors = $doctorRepo->getDepartmentDoctor($deptId);
        //           if(count($doctors) == 0){
        //               $hospitalId = $hospitalRepo->getHospitalsBasedOnCountry1($cityId);
        //               $dept = $departmentRepo->searchBasedDepartments($searchText,$hospitalId);
        //               $doctors = $doctorRepo->getDepartmentDoctor($dept);
        //           }
        //         }
        //     }else{
        //         $doctors = $doctorRepo->topDoctors();
        //     }
        //     $data = compact("doctors");
        //     $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
        //     return response()->json($response, 200);


        // } catch (Exception $e) {
        //     $response = ['status' => false, 'message' => $e->getMessage()];
        //     return response()->json($response, 200);
        // }
    }


    public function consultation(DoctorRepository $doctorRepo, TreatmentRepository $treatmentRepo, Request $request)
    {
        try {
            $doctorDetails = $doctorRepo->get($request->doctor_id);
            $treatmentId = $doctorDetails->treatment_id;
            if ($treatmentId != null) {
                $treatments = $treatmentRepo->getTreatments(json_decode($treatmentId));
            } else {
                $treatments =[];
            }
            $data = compact("treatments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    // public function doctorEnquiry(EnquiryAddRequest $request,Enquiry $enquiry)
    // {
    //     try {
    //         $doctor = Doctor::find($request->doctor_id);
    //         if($doctor != null){
    //             $enquiry = new Enquiry;
    //             $enquiry->enquiryable_id  = $request->doctor_id;
    //             $enquiry->enquiryable_type  = $request->name;
    //             $enquiry->email  = $request->email;
    //             $enquiry->contact  = $request->contact;
    //             $enquiry->name  = $request->name;
    //             $enquiry->message  = $request->message;
    //             $doctor->enquiries()->save($enquiry);

    //             $response = ['status' => true,'message' => 'Doctor Enquiry Added Successfully'];
    //             return response()->json($response, 200);
    //         }
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }

    public function doctorEnquiry(EnquiryAddRequest $request, Enquiry $enquiry)
    {
        try {
            $doctor = Doctor::find($request->doctor_id);
            if ($doctor != null) {
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->doctor_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $enquiry->reason = $request->reason;
                $enquiry->seen_doctor_before = $request->seen_doctor_before;
                $doctor->enquiries()->save($enquiry);
                $response = ['status' => true,'message' => 'Doctor Enquiry Added Successfully'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function telemedicinedoctorEnquiry(EnquiryAddRequest $request, Enquiry $enquiry)
    {
        try {
            $doctor = Doctor::where('type', 1)->find($request->telemedicine_doctor_id);
            if ($doctor != null) {
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->telemedicine_doctor_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $enquiry->is_tele_doctor  = '1';
                $doctor->enquiries()->save($enquiry);

                $response = ['status' => true,'message' => 'Telemedicine Doctor Enquiry Added Successfully'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function allDoctorFaq(FaqRepository $faqRepo)
    {
        try {
            $allDoctorFaq = $faqRepo->getAllFaq($askable_type="Doctor");
            $data = compact("allDoctorFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function DoctorFaq(DoctorFaqRequest $request, FaqRepository $faqRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorFaq = $faqRepo->getFaq($doctorId, $askable_type="Doctor");
            $data = compact("doctorFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function doctorsHospital(Request $request, DoctorRepository $doctorRepo, HospitalRepository $hospitalRepo, DepartmentRepository $deptRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorDetails = $doctorRepo->get($doctorId);
            $hospital = json_decode($doctorDetails->hospital_id);
            $hospitalId = $hospital[0];
            $hospitalDetails = $hospitalRepo->get($hospital[0]);
            $data = compact("hospitalDetails");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function doctorcoreSpeciality(Request $request, DoctorRepository $doctorRepo, CmsRepository $cmsRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorDetails = $doctorRepo->get($doctorId);
            if ($doctorDetails->core_specialty_id != null) {
                $coreSpecialityId = json_decode($doctorDetails->core_specialty_id);
                $coreSpeciality = $cmsRepo->specialFeatures($coreSpecialityId);
                $data = compact("coreSpeciality");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'data' => 'No data Found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function doctorPatientVoice(Request $request, DoctorRepository $doctorRepo, VideosRepository $videosRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorDetails = $doctorRepo->get($doctorId);
            if ($doctorDetails->patient_voice_id != null) {
                $patientvoiceId = json_decode($doctorDetails->patient_voice_id);
                $patientVoice = $videosRepo->doctorPatientVoice($patientvoiceId);
                $details = $patientVoice->map(function ($patientVoice, $key) {
                    if ($patientVoice->link != null) {
                        $iframe = Embed::make($patientVoice->link)->parseUrl()->getIframe();
                        $parts = parse_url($iframe);
                        $embedUrl = trim($parts['path'], "<iframe src=");
                        $embedUrl = ltrim($embedUrl, '""');
                        $patientVoice->link  = $embedUrl;
                        return $patientVoice;
                    }
                });
                $data = compact("patientVoice");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'data' => 'No data found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function awardsAndPublication(Request $request, DoctorRepository $doctorRepo, CmsRepository $cmsRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorDetails = $doctorRepo->get($doctorId);
            $awardsandPublicationId = json_decode($doctorDetails->awards_and_publication);
            if ($awardsandPublicationId != null) {
                $awardsandPublication = $cmsRepo->specialFeatures($awardsandPublicationId);
                $data = compact("awardsandPublication");
            } else {
                $response = ['status' => false,'data' => 'No data found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }

            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function specialization(Request $request, DoctorRepository $doctorRepo, CmsRepository $cmsRepo)
    {
        try {
            $doctorId = $request->doctor_id;
            $doctorDetails = $doctorRepo->get($doctorId);
            $specializationId = json_decode($doctorDetails->specialization);
            if ($specializationId != null) {
                $specialization = $cmsRepo->specialFeatures($specializationId);
                $data = compact("specialization");
            } else {
                $response = ['status' => false, 'message' => 'No Specialization Available for this doctor'];
                return response()->json($response, 200);
            }

            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function doctorAutocompleteSearch(Request $request, DoctorRepository $doctorRepo, CmsRepository $cmsRepo, DepartmentRepository $deptRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepo, TreatmentRepository $treatmentRepo, HospitalRepository $hospitalRepo, CityRepository $cityRepo, TipsRepository $tipsRepo)
    {
        try {
            $searchText = $request->search_text;
            $doctors = $doctorRepo->allDoctor($searchText);
            if (count($doctors) == 0) {
                $departmentID = $deptRepo->searchBasedDepartment($searchText);
                $doctors = $doctorRepo->getDepartmentDoctor($departmentID);
                if (count($doctors) == 0) {
                    /**disease */
                    $diseasesId = $diseasesRepo->searchBasedDiseases($searchText);
                    if (count($diseasesId) != 0) {
                        $doctors = $doctorRepo->getDoctorBasedDiseases($diseasesId);
                    }
                }
                if (count($doctors) == 0) {
                    /**problems */
                    $problemsId = $problemsRepo->searchBasedProblems($searchText);
                    if (count($problemsId) != 0) {
                        $doctors = $doctorRepo->getDoctorBasedOnProblems($problemsId);
                    }
                }
                if (count($doctors) == 0) {
                    /**treatment based */
                    $treatmentId = $treatmentRepo->searchBasedTreatments($searchText);
                    if (count($treatmentId) != 0) {
                        $deptId = $treatmentRepo->getTreatmentBasedDepartment($treatmentId);
                        $doctors = $doctorRepo->getDepartmentDoctor($deptId);
                    }
                }
            }

            $details = $doctors->map(function ($doctor, $key) use ($hospitalRepo, $deptRepo, $cityRepo, $tipsRepo) {
                // if($doctor->diseases_id != null){
                //     $disease = json_decode($doctor->diseases_id);
                //     $tips = $tipsRepo->getDiseasesTips($disease);
                // }else{
                //     $tips=[];
                // }
                $deptDetails = $deptRepo->get($doctor->department_id);
                $doctor->department_name = $deptDetails->name;
                $hospitalDetails = $hospitalRepo->getData($deptDetails->hospital_id);
                $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                $hospitalDetails->HospitalcityDetails = $cityDetails;
                $doctor->workingHospitalDetails = $hospitalDetails;
                // $doctor->HealthTips = $tips;
                return $doctor;
            });
            // $tips=array();
            if (count($doctors) !=0) {
                foreach ($doctors as $doc) {
                    if ($doc->diseases_id != null) {
                        $disease = json_decode($doc->diseases_id);
                        $tips[] = $tipsRepo->getDiseasesTips($disease);
                    } else {
                        $tips=[];
                    }
                }
            } else {
                $tips=[];
            }

            $data = compact("doctors");
            $response = ['status' => true, 'doctors' => $doctors,'tips' => $tips, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}