<?php

use Modules\Doctor\Http\Controllers\DoctorController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'doctor', 'as' => 'doctor.'], function () {         
        Route::get('/', [DoctorController::class, 'list'])->name('list');
        Route::get('add', [DoctorController::class, 'add'])->name('add');
        Route::post('save', [DoctorController::class, 'save'])->name('save');
        Route::get('edit', [DoctorController::class, 'edit'])->name('edit');
        Route::post('update', [DoctorController::class, 'update'])->name('update');
        Route::get('view', [DoctorController::class, 'view'])->name('view');
        Route::get('delete', [DoctorController::class, 'delete'])->name('delete');
        Route::get('remove_file', [DoctorController::class, 'removeFile'])->name('remove_file');
        Route::post('search', [DoctorController::class, 'search'])->name('search');     

    });
    Route::middleware('auth')->prefix('teledoctor')->name('teledoctor.')->group(function () {
        Route::get('/teledoctorList', [DoctorController::class, 'teledoctorList'])->name('teledoctorList');
    });
});

