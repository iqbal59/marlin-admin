<?php

use Illuminate\Http\Request;
use Modules\Doctor\Http\Controllers\Api\DoctorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/doctor', function (Request $request) {
    return $request->user();
});

Route::prefix('doctor')->group(function () {
    Route::get('/list', [DoctorController::class, 'list']);
    Route::post('/view', [DoctorController::class, 'view']);
    Route::post('/slugview', [DoctorController::class, 'slugview']);
    Route::post('/similarDoctors', [DoctorController::class, 'similarDoctors']);
    Route::get('/telemedicineDoctors', [DoctorController::class, 'telemedicineDoctors']);
    Route::post('/teleDoctors', [DoctorController::class, 'teleDoctors']);


    Route::post('/bestDiseasesDoctor', [DoctorController::class, 'bestDiseasesDoctor']);
    Route::post('/specialityDoctors', [DoctorController::class, 'specialityDoctors']);
    Route::post('/doctorEnquiry', [DoctorController::class, 'doctorEnquiry']);
    Route::post('/telemedicinedoctorEnquiry', [DoctorController::class, 'telemedicinedoctorEnquiry']);
    Route::post('/consultation', [DoctorController::class, 'consultation']);
    Route::get('/allDoctorFaq', [DoctorController::class, 'allDoctorFaq']);
    Route::post('/DoctorFaq', [DoctorController::class, 'DoctorFaq']);
    Route::post('/doctorsHospital', [DoctorController::class, 'doctorsHospital']);
    Route::post('/doctorcoreSpeciality', [DoctorController::class, 'doctorcoreSpeciality']);
    Route::post('/doctorPatientVoice', [DoctorController::class, 'doctorPatientVoice']);

    Route::post('/awardsAndPublication', [DoctorController::class, 'awardsAndPublication']);
    Route::post('/specialization', [DoctorController::class, 'specialization']);


    Route::post('/doctorAutocompleteSearch', [DoctorController::class, 'doctorAutocompleteSearch']);
});