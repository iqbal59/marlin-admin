<?php

namespace Modules\Doctor\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Enquiry\Models\Enquiry;
use Modules\Department\Models\Department;
use Modules\Poll\Models\Poll;
use Modules\Videos\Models\Videos;

class Doctor extends Model
{
    use HasFactory;
    use Sluggable;
    use \Znck\Eloquent\Traits\BelongsToThrough;


    protected $table = 'doctor';

    protected $fillable = [
        'name',
        'qualification',
        'slug',
        'experience',
        'designation',
        'lang_id',
        'core_specialty_id',
        'verified_badge',
        'problems_id',
        'image',
        'achievements',
        'type',
        'rating',
        'department_id',
        'status',
        'diseases_id',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'treatment_id',
        'patient_voice_id',
        'awards_and_publication',
        'specialization',
        'hospital_id',
        'added_by'
    ];


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function hospital()
    {
        return $this->belongsToThrough('Modules\Hospital\Models\Hospital', 'Modules\Department\Models\Department');
    }

    public function department()
    {
        return $this->belongsTo('Modules\Department\Models\Department', 'department_id', 'id');
    }

    public function diseases()
    {
        return $this->belongsTo('Modules\Diseases\Models\Diseases', 'diseases_id', 'id');
    }

    public function polls()
    {
        return $this->morphMany(Poll::class, 'pollable')->orderBy('value', 'DESC');
    }

    public function enquiries()
    {
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

    public function videos()
    {
        return $this->morphMany(Videos::class, 'videoable');
    }
}