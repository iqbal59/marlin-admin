<?php

namespace Modules\Banner\Http\Controllers\Api\Includes;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface as BannerContentRepository;
use Carbon\Exceptions\Exception;

trait BannerDescription
{
    public function list(BannerContentRepository $bannerContentRepo )
    {   
        try {
            $banner = $bannerContentRepo->all(); 
            $data = compact("banner");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }          
    }
    
}