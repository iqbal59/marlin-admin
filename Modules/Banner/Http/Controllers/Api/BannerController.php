<?php

namespace Modules\Banner\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Banner\Http\Controllers\Api\Includes\Banner;
use Modules\Banner\Http\Controllers\Api\Includes\BannerDescription;

class BannerController extends Controller
{
    // use Banner;
    use BannerDescription;
}
