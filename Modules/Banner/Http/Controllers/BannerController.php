<?php

namespace Modules\Banner\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Banner\Http\Controllers\Includes\Banner;
use Modules\Banner\Http\Controllers\Includes\BannerDescription;

class BannerController extends Controller
{
    use Banner;
    use BannerDescription;
}
