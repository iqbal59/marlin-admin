<?php

namespace Modules\Banner\Http\Controllers\Includes;
use Modules\Banner\Http\Requests\Banner\BannerAddRequest;
use Modules\Banner\Http\Requests\Banner\BannerSaveRequest;
use Modules\Banner\Http\Requests\Banner\BannerEditRequest;
use Modules\Banner\Http\Requests\Banner\BannerUpdateRequest;
use Modules\Banner\Http\Requests\Banner\BannerViewRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentSaveRequest;
use Modules\Cms\Http\Requests\Cms\CmsDeleteRequest;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface as BannerRepository;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface as BannerContentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
trait Banner
{
    public function bannerList(BannerRepository $bannerRepo)
    {   
        $bannerList = $bannerRepo->all();
        return view('banner::banner.listBanner', compact('bannerList'));           
    }

    public function addBanner(BannerAddRequest $request, BannerRepository $bannerRepo)
    {  
        return view('banner::banner.addBanner');      
    }

    public function saveBanner(BannerSaveRequest $request, BannerRepository $bannerRepo, BannerContentRepository $bannerContentRepo)
    {   
        $bannerimageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('bannerimage/',$request->file('image')):"";
        $absoluteimageUrl=$request->hasFile('absolute_image')?Storage::disk('public')->putFile('bannerabsoluteimage/',$request->file('absolute_image')):"";
         
        $bannerData = [
            'title'  => $request->title,
            'start_date'  => $request->start_date,
            'end_date'  => $request->end_date,
        ];  
        $banner = $bannerRepo->save($bannerData);    
        $bannerContent =[
            'banner_id'  => $banner->id,
            'title'  => $request->description_title,
            'content'  => $request->content,
            'caption'  => $request->caption,
            'image'  => $bannerimageUrl,
            'absolute_image'  => $absoluteimageUrl,
        ];  
        connectify('success', 'Success!', 'Banner added successfully');      
        return redirect()
            ->route('admin.banner.bannerList');
    } 

    public function viewBanner(BannerViewRequest $request,BannerRepository $bannerRepo, BannerContentRepository $bannerContentRepo)
    { 
        $banner = $bannerRepo->get($request->id);  
        $bannerContent = $bannerContentRepo->getBanner($banner->id); 
        return view('banner::banner.viewBanner', compact('banner','bannerContent'));
    }

    public function editBanner(BannerEditRequest $request,BannerRepository $bannerRepo, BannerContentRepository $bannerContentRepo)
    {   
        $banner = $bannerRepo->get($request->id);   
        $bannerContent = $bannerContentRepo->get($banner->id);
        return view('banner::banner.editBanner', compact('banner','bannerContent'));
    }

    public function updateBanner(BannerUpdateRequest $request, BannerRepository $bannerRepo, BannerContentRepository $bannerContentRepo)
    {  
        $bannerData = [
            'id' => $request->id,  
            'title'  => $request->title,     
            'start_date'  => $request->start_date,
            'end_date'  => $request->end_date, 
        ];

        if(isset($request->status)){ 
            $bannerData['status']=($request->status == "on")?1:0;
        }else{ 
            $bannerData['status']='0';
        }
        
        $banner = $bannerRepo->update($bannerData);  
        connectify('success', 'Success!', 'Banner updated successfully');      
        return redirect()
            ->route('admin.banner.bannerList');
    }

    public function deleteBanner(BannerRepository $bannerRepo, BannerContentRepository $bannerContentRepo, CmsDeleteRequest $request)
    { 
        if ($bannerRepo->delete($request->id) ) {
            connectify('success', 'Success!', 'Banner deleted successfuly');      
            return redirect()
            ->route('admin.banner.bannerList');
        }
            return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}