<?php

namespace Modules\Banner\Http\Controllers\Includes;
use Modules\Banner\Http\Requests\BannerContent\BannerContentAddRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentSaveRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentEditRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentUpdateRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentViewRequest;
use Modules\Banner\Http\Requests\BannerContent\BannerContentDeleteRequest;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface as BannerContentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
trait BannerDescription
{
    // public function categoryList(CmsCategoryRepository $cmsCategoryRepo)
    // {  
    //     $cmsCategory = $cmsCategoryRepo->all();                           
    //     return view('cms::cmsCategory.listCmsCategory', compact('cmsCategory'));           
    // }

    public function addBannerContent(BannerContentAddRequest $request)
    {   
        $bannerId = $request->id;
        return view('banner::bannerContent.addBannerContent',compact('bannerId'));

    }

    public function bannerContentSave(BannerContentSaveRequest $request, BannerContentRepository $bannerContentRepo)
    {  
        $bannerimageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('bannerimage/',$request->file('image')):"";
        $absoluteimageUrl=$request->hasFile('absolute_image')?Storage::disk('public')->putFile('bannerabsoluteimage/',$request->file('absolute_image')):"";
             
        $inputData = [
            'banner_id'  => $request->bannerId,
            'title'  => $request->title,
            'content'  => $request->content,
            'caption'  => $request->caption,
            'image' =>$bannerimageUrl,
            'absolute_image' =>$absoluteimageUrl
        ];
        $bannerContent = $bannerContentRepo->save($inputData);       
        connectify('success', 'Success!', 'Banner content added successfully'); 
        return redirect()
        ->route('admin.banner.viewBanner',['id'=>$request->bannerId]);
    } 

    public function bannerContentview(BannerContentViewRequest $request,BannerContentRepository $bannerContentRepo)
    {  
        $bannerContent = $bannerContentRepo->get($request->id);   
        return view('banner::bannerContent.viewBannerContent',compact('bannerContent'));

    }

    public function bannerContentedit(BannerContentEditRequest $request,BannerContentRepository $bannerContentRepo)
    {   
        $bannerContent = $bannerContentRepo->get($request->id);   
        return view('banner::bannerContent.editBannerContent', compact('bannerContent'));
    }

    public function bannerContentUpdate(BannerContentUpdateRequest $request, BannerContentRepository $bannerContentRepo)
    { 
        
        // $bannerimageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('bannerimage/',$request->file('image')):"";
        // $absoluteimageUrl=$request->hasFile('absolute_image')?Storage::disk('public')->putFile('bannerabsoluteimage/',$request->file('absolute_image')):"";
         
        $inputData = [
            'id' => $request->id,            
            'title' => $request->title,
            'content' => $request->content,
            'caption' => $request->caption,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
        

        if($request->hasFile('image')){
            $bannerimageUrl = Storage::disk('public')->putFile('bannerimage/',$request->file('image'));
            $inputData['image'] = $bannerimageUrl;
        }

        if($request->hasFile('absolute_image')){
            $absoluteimageUrl = Storage::disk('public')->putFile('bannerimage/',$request->file('absolute_image'));
            $inputData['absolute_image'] = $absoluteimageUrl;
        }

        $bannercontent = $bannerContentRepo->update($inputData); 
        connectify('success', 'Success!', 'Image deleted successfuly');      
        return redirect()
        ->route('admin.banner.viewBanner', ['id' => $bannercontent->banner_id]);

    }

    public function removeImage(BannerContentRepository $bannerContentRepo,Request $request)
    {  
        if ($this->_removeImage($bannerContentRepo, $request->id)) {
            $image = "image";
            $bannerContentRepo->removeImage($request->id,$image);
        }
        connectify('success', 'Success!', 'Image deleted successfuly');      
        return redirect()
        ->route('admin.bannerContent.bannerContentedit', ['id' => $request->id]);
    }

    private function _removeImage($bannerContentRepo, $id)
    {   
        $image = $bannerContentRepo->get($id); 
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }

    public function removeAbsoluteImage(BannerContentRepository $bannerContentRepo,Request $request)
    {    
        if ($this->_removeAbsoluteImage($bannerContentRepo, $request->id)) {
            $image="absolute_image";
            $bannerContentRepo->removeImage($request->id,$image);
        }       
        connectify('success', 'Success!', 'Image deleted successfuly');      
        return redirect()
        ->route('admin.bannerContent.bannerContentedit', ['id' => $request->id]);
    }

    private function _removeAbsoluteImage($bannerContentRepo, $id)
    {    
        $image = $bannerContentRepo->get($id);   
        if (Storage::disk('public')->delete($image->absolute_image)) {
            return true;
        }
        return false;
    }

    public function bannerContentdelete(BannerContentRepository $bannerContentRepo, BannerContentDeleteRequest $request)
    { 
        $banner=$bannerContentRepo->get($request->id);   
        if ($bannerContentRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Banner content deleted successfuly');      
            return redirect()
            ->route('admin.banner.viewBanner', ['id' => $banner->banner_id]);
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}