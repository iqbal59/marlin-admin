<?php

namespace Modules\Banner\Http\Requests\BannerContent;

use Illuminate\Foundation\Http\FormRequest;

class BannerContentAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('bannercontent_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}