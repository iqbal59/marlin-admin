<?php

namespace Modules\Banner\Http\Requests\BannerContent;

use Illuminate\Foundation\Http\FormRequest;

class BannerContentSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('bannercontent_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'image.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'image' => 'Image',
        ];
    }
}