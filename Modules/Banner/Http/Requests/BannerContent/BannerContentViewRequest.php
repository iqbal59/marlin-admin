<?php

namespace Modules\Banner\Http\Requests\BannerContent;

use Illuminate\Foundation\Http\FormRequest;

class BannerContentViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('bannercontent_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}

