<?php

namespace Modules\Banner\Http\Requests\BannerContent;

use Illuminate\Foundation\Http\FormRequest;

class BannerContentEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('bannercontent_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}