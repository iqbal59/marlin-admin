<?php

namespace Modules\Banner\Http\Requests\BannerContent;

use Illuminate\Foundation\Http\FormRequest;

class BannerContentDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('bannercontent_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}