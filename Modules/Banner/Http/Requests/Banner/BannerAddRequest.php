<?php

namespace Modules\Banner\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class BannerAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('banner_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}