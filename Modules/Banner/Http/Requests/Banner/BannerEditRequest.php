<?php

namespace Modules\Banner\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class BannerEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('banner_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}