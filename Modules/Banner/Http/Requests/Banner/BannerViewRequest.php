<?php

namespace Modules\Banner\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class BannerViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('banner_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}

