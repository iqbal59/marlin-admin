<?php

namespace Modules\Banner\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class BannerSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('banner_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
        ];
    }
}