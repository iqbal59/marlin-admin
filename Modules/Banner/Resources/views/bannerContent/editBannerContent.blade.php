@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<style>
    .iframe-contr iframe{
        max-width: 100%;
        height: 275px;
    }
    .prevv img.preview {
        width: 100%;
        margin-top: 8px;
        height: 266px;
        object-fit: cover;
    }
    </style>
    
    
    <style>
        input[type="number"] {
        -webkit-appearance: textfield;
        -moz-appearance: textfield;
        appearance: textfield;
        }
        
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
          -webkit-appearance: none;
        }
        
        .number-input {
          border: 2px solid #ddd;
          display: inline-flex;
        }
        
        .number-input,
        .number-input * {
          box-sizing: border-box;
        }
        
        .number-input button {
          outline:none;
          -webkit-appearance: none;
          background-color: transparent;
          border: none;
          align-items: center;
          justify-content: center;
          width: 3rem;
          height: 2rem;
          cursor: pointer;
          margin: 0;
          position: relative;
        }
        
        .number-input button:before,
        .number-input button:after {
          display: inline-block;
          position: absolute;
          content: '';
          width: 1rem;
          height: 2px;
          background-color: #212121;
          transform: translate(-50%, -50%);
        }
        .number-input button.plus:after {
          transform: translate(-50%, -50%) rotate(90deg);
        }
        
        .number-input input[type=number] {
          font-family: sans-serif;
          max-width: 3rem;
          padding: .5rem;
          border: solid #ddd;
          border-width: 0 2px;
          font-size: 1rem;
          height: 2rem;
          font-weight: bold;
          text-align: center;
        }
        i.mdi.mdi-minus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        i.mdi.mdi-plus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        span.on {
            width: 95px;
            float: left;
        }
        .custom-switch {
            padding-left: 0;
        }
        .custom-switch .custom-control-label::before{
            width: 2rem;
        }
        .custom-switch .custom-control-label::after {
            width: calc(1.3rem - 4px);
            }
        .badge-contr span.off {
            width: 70px;
            float: left;
        }
        .badge-contr span.on {
            float: none;
        }
        </style>

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} Banner Content
    </div>

    <div class="card-body">
        <form action="{{ route("admin.bannerContent.bannerContentUpdate") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$bannerContent->id}}">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($bannerContent) ? $bannerContent->title : '') }}"  >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            
            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label for="content">Content</label>
                <input type="text" id="content" name="content" class="form-control" value="{{ old('content', isset($bannerContent) ? $bannerContent->content : '') }}" >
                @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                <label for="caption">Caption</label>
                <input type="text" id="caption" name="caption" class="form-control" value="{{ old('caption', isset($bannerContent) ? $bannerContent->caption : '') }}" >
                @if($errors->has('caption'))
                    <em class="invalid-feedback">
                        {{ $errors->first('caption') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
 
            
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image"> {{ trans('panel.image') }} </label>                
                @if(Storage::disk('public')->exists($bannerContent->image)) 
                <img class="preview" src="{{asset('storage/'.$bannerContent->image)}}" alt="" width="150px">
                    <a href="{{route('admin.bannerContent.remove_image',['id'=>$bannerContent->id])}}" class="confirm-delete text-danger">
                    <i class="material-icons pink-text">clear</i>
                    </a>
                @else
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
                @endif
            </div>     
            
            <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">
                <label for="absolute_image"> Absolute Image</label>                
                @if(Storage::disk('public')->exists($bannerContent->absolute_image)) 
                <img class="preview" src="{{asset('storage/'.$bannerContent->absolute_image)}}" alt="" width="150px">
                    <a href="{{route('admin.bannerContent.remove_absolute_image',['id'=>$bannerContent->id])}}" class="confirm-delete text-danger">
                    <i class="material-icons pink-text">clear</i>
                    </a>
                @else
                <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">                
                <input type="file" name="absolute_image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('absolute_image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('absolute_image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
                @endif
            </div> 
            
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status',$bannerContent->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>   
          
            <br>   
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection