@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        Add Banner Content
    </div>

    <div class="card-body">
        <form action="{{ route("admin.bannerContent.bannerContentSave") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="bannerId" value={{$bannerId}}>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title"> {{ trans('panel.title') }}  <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content">Content </label>
                        <input type="text" id="content" name="content" class="form-control" value="{{old('content')}}"  >
                        @if($errors->has('content'))
                            <em class="invalid-feedback">
                                {{ $errors->first('content') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                            <label for="caption">Caption  </label>
                            <input type="text" id="caption" name="caption" class="form-control" value="{{old('caption')}}"  >
                            @if($errors->has('caption'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('caption') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image">  {{ trans('panel.image') }} <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">
                        <label for="absolute_image">  Absolute Image </label>
                        <input type="file" name="absolute_image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('absolute_image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('absolute_image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>
 
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.department.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>
@endsection
