@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        View Banner Content
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr> 
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>

                            {{ $bannerContent->id }}
                        </td>
                    </tr> 
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            {{ $bannerContent->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Content
                        </th>
                        <td>
                            {{ $bannerContent->content }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Caption
                        </th>
                        <td>
                        {{ $bannerContent->caption }}
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                         Image
                        </th>
                        <td>
                        <img src="{{asset('storage/'.$bannerContent->image)}}" alt="" width="500px">
                        </td>
                    </tr>
                     @if($bannerContent->absolute_image !="")
                    <tr>
                        <th>
                        Absolute Image
                        </th>
                        <td>
                        <img src="{{asset('storage/'.$bannerContent->absolute_image)}}" alt="" width="500px">
                        </td>
                    </tr>
                   @endif
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection