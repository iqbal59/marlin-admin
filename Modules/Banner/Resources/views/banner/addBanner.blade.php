@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
   {{ trans('panel.add_banner') }} 
        
    </div>

    <div class="card-body">
        <form action="{{ route("admin.banner.saveBanner") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">{{ trans('panel.title') }} </label>
                        <i class="mdi mdi-flag-checkered text-danger "></i>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                        <label for="start_date">Start Date  </label>
                        <input type="date" id="start_date" name="start_date" class="form-control" value="{{old('start_date')}}" >
                        @if($errors->has('start_date'))
                            <em class="invalid-feedback">
                                {{ $errors->first('start_date') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                        <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                            <label for="end_date">End Date  </label>
                            <input type="date" id="end_date" name="end_date" class="form-control" value="{{old('end_date')}}" >
                            @if($errors->has('end_date'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('end_date') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                </div>
            </div>

            <!-- <div class="form-group {{ $errors->has('description_title') ? 'has-error' : '' }}">
                <label for="description_title">*{{ trans('panel.banner_description_title') }} </label>
                <input type="text" id="description_title" name="description_title" class="form-control" value="{{old('description_title')}}" required>
                @if($errors->has('description_title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description_title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label for="content">*{{ trans('panel.content') }} </label>
                <input type="text" id="content" name="content" class="form-control" value="{{old('content')}}" required>
                @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
            <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                <label for="caption">*{{ trans('panel.caption') }} </label>
                <input type="text" id="caption" name="caption" class="form-control" value="{{old('caption')}}" required>
                @if($errors->has('caption'))
                    <em class="invalid-feedback">
                        {{ $errors->first('caption') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                <label for="meta_title">*{{ trans('panel.meta_title') }} </label>
                <input type="text" id="meta_title" name="meta_title" class="form-control" value="{{old('meta_title')}}" required>
                @if($errors->has('meta_title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('meta_tags') ? 'has-error' : '' }}">
                <label for="meta_tags">*{{ trans('panel.meta_tags') }} </label>
                <input type="text" id="meta_tags" name="meta_tags" class="form-control" value="{{old('meta_tags')}}" required>
                @if($errors->has('meta_tags'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_tags') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                <label for="meta_keywords">*{{ trans('panel.meta_keywords') }} </label>
                <input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="{{old('meta_keywords')}}" required>
                @if($errors->has('meta_keywords'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_keywords') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
            
            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                <label for="meta_description"> *{{ trans('panel.meta_description') }} </label>
                <textarea class="ckeditor form-control" id="meta_description" name="meta_description" value="{{old('meta_description')}}" required></textarea>       
                @if($errors->has('meta_description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label for="content"> *{{ trans('panel.content') }} </label>
                <textarea class="ckeditor form-control" id="content" name="content" value="{{old('content')}}" required></textarea>       
                @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image"> *{{ trans('panel.image') }} </label>
                <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('absolute_image') ? 'has-error' : '' }}">
                <label for="absolute_image"> *{{ trans('panel.absolue_image') }} </label>
                <input type="file" name="absolute_image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('absolute_image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  -->


            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.department.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>
@endsection
