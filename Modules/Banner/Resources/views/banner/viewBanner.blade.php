@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        View Banner
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div> 
    {{-- @php  dd($banner); @endphp --}}
    <div class="card-body">
        @include('banner::partials.tabSection')
    </div>
</div>
@endsection