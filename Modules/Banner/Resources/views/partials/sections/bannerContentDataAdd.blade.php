<div class="mb-2">
    <form action="{{ route("admin.banner.saveBannerContent") }}" method="POST" enctype="multipart/form-data">
    @csrf   
    <input type="hidden" value="{{$banner->id}}" name="banner_id" id="banner_id">
        <div class="form-group {{ $errors->has('description_title') ? 'has-error' : '' }}">
            <label for="description_title">* Title</label>
            @if($bannerContent == null)
            <input type="text" name="description_title" id="description_title" class="form-control">
            @else 
            <input type="text" value="{{ old('description_title',$bannerContent->title) }}" name="description_title" id="description_title" class="form-control">

            @endif
            @if($errors->has('description_title'))
                <em class="invalid-feedback">
                    {{ $errors->first('description_title') }}
                </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>  

        <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
            <label for="content">* Content</label>
            @if($bannerContent == null)
            <input type="text"  name="content" id="content" class="form-control">
            @else
            <input type="text" value="{{ old('content',$bannerContent->content) }}" name="content" id="content" class="form-control">
            @endif
            @if($errors->has('content'))
                <em class="invalid-feedback">
                    {{ $errors->first('content') }}
                </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>  

        <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
            <label for="caption">* Caption</label>
            @if($bannerContent == null)
            <input type="text" name="caption" id="caption" class="form-control">
            @else
            <input type="text" value="{{ old('caption',$bannerContent->caption) }}" name="caption" id="caption" class="form-control">
            @endif
            @if($errors->has('caption'))
                <em class="invalid-feedback">
                    {{ $errors->first('content') }}
                </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>  

        <div class="row">
            
        <div class="col-md-6">
            <div class="file-field input-field">
                <label>@lang('Image')</label>
                @if($bannerContent !=null) 
                @if(Storage::disk('public')->exists($bannerContent->image)) 
                <div class="row">
                    <div class="col s12">
                        <img class="preview" src="{{asset('storage/'.$bannerContent->image)}}" alt="" style="width:150px">
                        <a href="{{route('admin.banner.remove_file',['id'=>$bannerContent->banner_id])}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                    </div>
                </div>
                @else
                <div class="btn">
                    <input type="file" id="image" name="image">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif
                
                @else
                <div class="btn">
                    <input type="file" id="image" name="image">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif

                @error('image')<small class="form-text text-danger">{{ $message }}</small>@enderror
            </div> 
        </div>


            <div class="file-field input-field">
                <label>@lang('Absolute Image')</label>
                @if($bannerContent !=null) 
                @if(Storage::disk('public')->exists($bannerContent->absolute_image)) 
                <div class="row">
                    <div class="col s12">
                        <img class="preview" src="{{asset('storage/'.$bannerContent->absolute_image)}}" alt="" style="width:150px">
                        <a href="{{route('admin.banner.remove_absolute_file',['id'=>$bannerContent->banner_id])}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                    </div>
                </div>
                @else
                <div class="btn">
                    <input type="file" id="absolute_image" name="absolute_image">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif
                
                @else
                <div class="btn">
                    <input type="file" id="absolute_image" name="absolute_image">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif

                @error('image')<small class="form-text text-danger">{{ $message }}</small>@enderror
            </div> 
        </div>
        

        <div>
            <input class="btn btn-primary float-right" type="submit" value="{{ trans('global.save') }}">
        </div>
    </form>
</div>