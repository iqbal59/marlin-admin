<div class="mb-2">
  
    <table class="table table-bordered table-striped">
        <tbody> 
            <tr><input type="hidden" value="{{$banner->id}}" name="banner_id" value="banner_id">
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $banner->id }}</td>
            </tr>
            <tr> 
                <th>Title</th>
                <td>{{ $banner->title }}</td>
            </tr>  
            <tr> 
                <th>Start Date</th>
                <td>{{ $banner->start_date }}</td>
            </tr>   
            <tr> 
                <th>End Date</th>
                <td>{{ $banner->end_date }}</td>
            </tr>           
        </tbody>
    </table>
    
</div>