<?php

use Modules\Banner\Http\Controllers\BannerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::middleware('auth')->prefix('banner')->name('banner.')->group(function () {
        Route::get('/bannerList', [BannerController::class, 'bannerList'])->name('bannerList');
        Route::get('/addBanner', [BannerController::class, 'addBanner'])->name('addBanner');
        Route::post('saveBanner', [BannerController::class, 'saveBanner'])->name('saveBanner');
        Route::get('editBanner', [BannerController::class, 'editBanner'])->name('editBanner');
        Route::post('updateBanner', [BannerController::class, 'updateBanner'])->name('updateBanner');
        Route::get('viewBanner', [BannerController::class, 'viewBanner'])->name('viewBanner');
        Route::get('deleteBanner', [BannerController::class, 'deleteBanner'])->name('deleteBanner');        
    });

    Route::middleware('auth')->prefix('bannerContent')->name('bannerContent.')->group(function () {
        Route::get('/addBannerContent', [BannerController::class, 'addBannerContent'])->name('addBannerContent');
        Route::post('bannerContentSave', [BannerController::class, 'bannerContentSave'])->name('bannerContentSave');
        Route::get('bannerContentedit', [BannerController::class, 'bannerContentedit'])->name('bannerContentedit');
        Route::post('bannerContentUpdate', [BannerController::class, 'bannerContentUpdate'])->name('bannerContentUpdate');
        Route::get('bannerContentview', [BannerController::class, 'bannerContentview'])->name('bannerContentview');
        Route::get('bannerContentdelete', [BannerController::class, 'bannerContentdelete'])->name('bannerContentdelete');
        Route::get('remove_image', [BannerController::class, 'removeImage'])
                ->name('remove_image');
        Route::get('remove_absolute_image', [BannerController::class, 'removeAbsoluteImage'])
                ->name('remove_absolute_image');
        
    });
    

});
