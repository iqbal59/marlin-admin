<?php

namespace Modules\Banner\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerContent extends Model
{
    use HasFactory;
    protected $table = 'banner_content';

    protected $fillable = ['id','banner_id','images','size','status'];
   
}