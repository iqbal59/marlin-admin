<?php

namespace Modules\Banner\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $table = 'banner';

    protected $fillable = [
        'id','title','status','type','order','is_editable','start_date','end_date'
    ];

    public function services()
    {
        return $this->hasMany('Modules\Services\Models\Services');
    }

    public function videosection()
    {
        return $this->hasMany('Modules\Videos\Models\VideosSections');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->services->count()
            ||$this->videosection->count()
        ) {
            return false;
        }
        return true;
    }
   
}