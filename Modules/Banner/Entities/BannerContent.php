<?php

namespace Modules\Banner\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BannerContent extends Model
{
    use HasFactory;

    protected $fillable = ['id','banner_id','title','content','caption','status','image','absolute_image','order','content'];
    
    protected static function newFactory()
    {
        return \Modules\Banner\Database\factories\BannerDescriptionFactory::new();
    }
}
