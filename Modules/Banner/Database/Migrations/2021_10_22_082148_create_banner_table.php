<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Banner\Models\Banner;
class CreateBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('status')->default('1');
            $table->tinyInteger('type')->comment('1:Slider|2:Static')->default('1');
            $table->integer('order')->default(0);
            $table->boolean('is_editable')->default('1');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
        });
        Banner::create(['id' => 1, 'title' => 'Banner']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
        // Banner::where('id', 2)->delete();
    }
}
