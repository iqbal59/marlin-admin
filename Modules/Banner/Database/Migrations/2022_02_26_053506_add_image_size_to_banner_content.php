<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageSizeToBannerContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_content', function (Blueprint $table) {
            $table->string('images')->after('order')->nullable();
            $table->string('size')->after('images')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner_content', function (Blueprint $table) {
            $table->dropColumn('images'); 
            $table->dropColumn('size'); 
        });
    }
}
