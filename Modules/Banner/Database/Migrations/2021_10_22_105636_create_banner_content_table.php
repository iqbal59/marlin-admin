<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_content', function (Blueprint $table) {
            $table->id();
            $table->integer('banner_id');
            $table->text('title');
            $table->text('content')->nullable();
            $table->text('caption')->nullable();
            $table->boolean('status')->default('1');
            $table->string('image')->nullable();
            $table->string('absolute_image')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_content');
    }
}
