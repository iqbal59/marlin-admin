<?php

namespace Modules\Banner\Database\Seeders;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BannerDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '100',
                'title'      => 'banner_access',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '101',
                'title'      => 'banner_create',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '102',
                'title'      => 'banner_edit',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '103',
                'title'      => 'banner_show',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '104',
                'title'      => 'banner_delete',
                'created_at' => '2019-09-19 12:14:15',
                'updated_at' => '2019-09-19 12:14:15',
            ],
            [
                'id'         => '105',
                'title'      => 'bannercontent_access',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
            [
                'id'         => '106',
                'title'      => 'bannercontent_create',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
            [
                'id'         => '107',
                'title'      => 'bannercontent_edit',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
            [
                'id'         => '108',
                'title'      => 'bannercontent_show',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
            [
                'id'         => '109',
                'title'      => 'bannercontent_delete',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
        ];

        Permission::insert($permissions);
    }
}
