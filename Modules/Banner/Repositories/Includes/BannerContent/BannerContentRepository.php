<?php

namespace Modules\Banner\Repositories\Includes\BannerContent;

use App\Repositories\BaseRepository;
use Modules\Banner\Models\BannerContent;
use Illuminate\Database\Eloquent\Builder;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface;
class BannerContentRepository implements BannerContentRepositoryInterface
{
    public function save(array $input)
    {
        if ($bannerContent =  BannerContent::create($input)) {
            return $bannerContent;
        }
        return false;
    }

    public function get($bannerId)
    {
        return BannerContent::where('id',$bannerId)->first();
    }

    public function getAll($bannerId)
    {
        return BannerContent::where('id',$bannerId)->get();
    }

    public function getBanner($bannerId)
    {
        return BannerContent::where('banner_id',$bannerId)->get();
    }

    public function update(array $input)
    {
        $banner = BannerContent::find($input['id']);
        unset($input['id']);
        if ($banner->update($input)) {
            return $banner;
        }
        return false;
    }

    public function delete(string $id)
    {
        $bannerContent = BannerContent::where('id',$id)->first();     
        return $bannerContent->delete();
    }

    public function removeImage(string $id,$image)
    { 
        $bannerContent = BannerContent::where('id',$id)->first();
        if($image == "image"){
            $bannerContent->image = '';
        }else{
            $bannerContent->absolute_image = '';
        }
        
        return $bannerContent->save();
    }

    public function getBannerData($bannerId)
    {
        return BannerContent::where('id',$bannerId)->first();
    }

    public function all()
    {
        return BannerContent::get();
    }


    public function addImage($filePath, $id,$size)
    {  
        $image = new BannerContent();
        $image->images = $filePath;
        $image->size = $size;
        $image->banner_id = $id;
        $image->title = "content";
        $image->status = 1;
        $image->save();
        return $image->id;
    }

    public function deleteGallery(string $id)
    {
        $gallery = Blogs::find($id);
        return $galleryImages = BannerContent::where('banner_id',$gallery->id)->delete();
    }

    public function getImage($id)
    { 
        return BannerContent::find($id);
    }

    public function getImages($id)
    {
        return BannerContent::where('banner_id',$id)->get();
    }

    public function imageDelete(string $id)
    {
        $blog = BannerContent::find($id);
        return $blog->delete();
    }


}