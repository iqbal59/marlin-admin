<?php

namespace Modules\Banner\Repositories\Includes\BannerContent;

interface BannerContentRepositoryInterface
{
    public function save(array $input);

    public function get($bannerId);

    public function update(array $input);

    public function delete(string $id);

    public function removeImage(string $id,$image);

    public function getBannerData($bannerId);

    public function getAll($bannerId);

    public function getBanner($bannerId);

    public function all();

    public function addImage($filePath, $id,$size);

    public function deleteGallery(string $id);

    public function getImage($id);

    public function getImages($id);

    public function imageDelete(string $id);

}
