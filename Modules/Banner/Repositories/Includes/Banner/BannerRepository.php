<?php

namespace Modules\Banner\Repositories\Includes\Banner;

use App\Repositories\BaseRepository;
use Modules\Banner\Models\Banner;
use Illuminate\Database\Eloquent\Builder;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface;

class BannerRepository implements BannerRepositoryInterface
{
    public function all()
    {
        return Banner::orderBy('id', 'DESC')->get();
    }
    
    public function get($Id)
    {
        return Banner::where('id', $Id)->first();
    }

    public function save(array $input)
    {
        if ($banner =  Banner::create($input)) {
            return $banner;
        }
        return false;
    }
    public function update(array $input)
    {
        $banner = Banner::find($input['id']);
        unset($input['id']);
        if ($banner->update($input)) {
            return $banner;
        }
        return false;
    }

    public function delete(string $id)
    {
        $banner = Banner::find($id);
        return $banner->delete();
    }

    public function resetFile(string $id)
    {
        $banner = Banner::find($id);
        $banner->image = '';
        return $banner->save();
    }
}
