<?php

namespace Modules\Banner\Repositories\Includes\Banner;

interface BannerRepositoryInterface
{
    public function all();

    public function get($Id);

    public function save(array $input);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);
}
