<?php
use Modules\Services\Http\Controllers\ServicesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('services')->group(function() {
    Route::get('/', 'ServicesController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'services', 'as' => 'services.'], function () {         
            Route::get('/', [ServicesController::class, 'list'])->name('list');
            Route::get('add', [ServicesController::class, 'add'])->name('add');
            Route::post('save', [ServicesController::class, 'save'])->name('save');
            Route::get('edit', [ServicesController::class, 'edit'])->name('edit');
            Route::post('update', [ServicesController::class, 'update'])->name('update');
            Route::get('view', [ServicesController::class, 'view'])->name('view');
            Route::get('delete', [ServicesController::class, 'delete'])->name('delete');
            // Route::get('remove_file', [ServicesController::class, 'removeFile'])
            // ->name('remove_file'); 
            Route::get('remove_video', [ServicesController::class, 'removeVideo'])->name('remove_video');
            Route::get('remove_file', [ServicesController::class, 'removeFile'])->name('remove_file');
            // Route::get('remove_doc', [ServicesController::class, 'removeDoc'])->name('remove_doc');
            Route::get('removeImage', [ServicesController::class, 'removeImage'])->name('removeImage');

    });
});
