<?php

namespace Modules\Services\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Services extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable =  ['id','name','slug','caption','hospital_id','file','link','subtitle','description','banner_id','image','type','order','status','is_editable'];
    
    protected static function newFactory()
    {
        return \Modules\Services\Database\factories\ServicesFactory::new();
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
