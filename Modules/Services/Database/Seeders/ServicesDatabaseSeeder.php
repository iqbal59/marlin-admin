<?php

namespace Modules\Services\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class ServicesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id'         => '241',
                'title'      => 'services_create',
                'created_at' => '2019-11-04 12:14:15',
                'updated_at' => '2019-11-04 12:14:15',
            ],
            [
                'id'         => '242',
                'title'      => 'services_edit',
                'created_at' => '2019-11-04 12:14:15',
                'updated_at' => '2019-11-04 12:14:15',
            ],
            [
                'id'         => '243',
                'title'      =>  'services_show',
                'created_at' => '2019-11-04 12:14:15',
                'updated_at' => '2019-11-04 12:14:15',
            ],
            [
                'id'         => '244',
                'title'      => 'services_delete',
                'created_at' => '2019-11-04 12:14:15',
                'updated_at' => '2019-11-04 12:14:15',
            ],
            [
                'id'         => '245',
                'title'      => 'services_access',
                'created_at' => '2019-11-04 12:14:15',
                'updated_at' => '2019-11-04 12:14:15',
            ],

        ];
        
        Permission::insert($permissions);
    }
}
