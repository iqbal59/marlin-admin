@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} Services
    </div>

    <div class="card-body">
        <form action="{{ route("admin.services.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$services->id}}">
            <input type="hidden" name="hasHospital" value="{{!empty($hospital) ? 1 : 0}}">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($services) ? $services->name : '') }}"  >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                <label for="caption">Caption </label>
                <input type="text" id="caption" name="caption" class="form-control" value="{{ old('caption', isset($services) ? $services->caption : '') }}"  >
                @if($errors->has('caption'))
                    <em class="invalid-feedback">
                        {{ $errors->first('caption') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('subtitle') ? 'has-error' : '' }}">
                <label for="subtitle">Subtitle </label>
                <input type="text" id="subtitle" name="subtitle" class="form-control" value="{{ old('subtitle', isset($services) ? $services->subtitle : '') }}"  >
                @if($errors->has('subtitle'))
                    <em class="invalid-feedback">
                        {{ $errors->first('subtitle') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('banner_id') ? 'has-error' : '' }}">
                        <label for="banner_id"> Banner
                        </label>
                        
                        <select id="banner_id" name="banner_id" class="js-states form-control">
                        <option value=""> Banner </option>
                            @foreach($banners as $banner) 
                            @if($banner->id == $services->banner_id)
                            <option value="{{$banner->id}}" name="banner_id" id="banner_id" selected>{{$banner->title}}</option>
                            @else
                            <option value="{{$banner->id}}" name="banner_id" id="banner_id">{{$banner->title}}</option>
                            @endif
                            @endforeach
                        </select>               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>


                    <div class="form-group {{ $errors->has('hospital_id') ? 'has-error' : '' }}">
                        <label for="hospital_id">  Hospital
                        </label>
                        
                        <select id="hospital_id" name="hospital_id" class="js-states form-control">
                        <option value=""> Banner </option>
                            @foreach($hospitals as $hospital) 
                            @if($hospital->id == $services->hospital_id)
                            <option value="{{$hospital->id}}" name="hospital_id" id="hospital_id" selected>{{$hospital->title}}</option>
                            @else
                            <option value="{{$hospital->id}}" name="hospital_id" id="hospital_id">{{$hospital->title}}</option>
                            @endif
                            @endforeach
                        </select>               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>        

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($services) ? $services->description : '') !!}
                </textarea>   
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
               @if($services->type == "youtube" || $services->type =="vimeo")
                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">Link </label>
                       @if($services->link != "")
                        {!! Embed::make($services->link)->parseUrl()->getIframe() !!}
                            <a href="{{route('admin.services.remove_video',['id'=>$services->id])}}" class="confirm-delete text-danger">
                            <i class="menu-icon mdi mdi-close pink-text"></i>
                            </a>                           
                            @if($errors->has('link'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('link') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        @else
                        <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                        @endif    
                    </div> 
                @endif   

               @if($services->type == "file")
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label for="file">  File </label>                
                        @if(Storage::disk('public')->exists($services->file)) 
                        <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$services->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>    
                            <a href="{{route('admin.services.remove_file',['id'=>$services->id])}}" class="confirm-delete text-danger">
                                    <i class="menu-icon mdi mdi-close pink-text"></i>
                            </a>
                         @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                        <input type="file" name="file" id="input-file-now-banner" class="dropify validate" data-default-file="" accept="application/pdf,application/doc,application/docx" /> 
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                        @endif
                </div>
                @endif

                @if($services->type != "file")

                <div class="row">
                    <div class="col-md-6">
                
                    <label for="image"> Thumb Line Images </label>                
                        @if(Storage::disk('public')->exists($services->image)) 
                            <img class="preview" src="{{asset('storage/'.$services->image)}}" alt="" width="150px">
                                <a href="{{route('admin.services.removeImage',['id'=>$services->id])}}" class="confirm-delete text-danger">
                                <i class="menu-icon mdi mdi-close pink-text"></i>
                                </a>
                         @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                        @endif
                </div>
                @endif  
                <div class="col-md-6 badge-contr">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" {{ old('status',$services->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                            <span class="off">No</span>
                            <label class="custom-control-label" for="status"></label>
                            <span class="on">Yes</span>
                        </div>
                    </div> 
                </div>   
                </div>
                <br>       
                                                  
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#banner_id").select2({
                placeholder: "Select Banner",
                allowClear: true
            });
            $("#type").select2({
                placeholder: "Select Type",
                allowClear: true
            });
            $("#hospital_id").select2({
                placeholder: "Select Hospital",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection