@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        Add Services
    </div>

    <div class="card-body">
        <form action="{{ route("admin.services.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="hasHospital" value="{{!empty($hospital) ? $hospital->id : 0}}">
            <div class="row">            
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                        <label for="caption">Caption </label>
                        <input type="text" id="caption" name="caption" class="form-control" value="{{old('caption')}}" >
                        @if($errors->has('caption'))
                            <em class="invalid-feedback">
                                {{ $errors->first('caption') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>    
            <div class="row">
                <div class="col-md-6">
                <div class="form-group {{ $errors->has('subtitle') ? 'has-error' : '' }}">
                        <label for="subtitle">Subtitle </label>
                        <input type="text" id="subtitle" name="subtitle" class="form-control" value="{{old('subtitle')}}" >
                        @if($errors->has('subtitle'))
                            <em class="invalid-feedback">
                                {{ $errors->first('subtitle') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('hospital_id') ? 'has-error' : '' }}">
                        <label for="hospital_id"> Hospital
                        </label>
                            <select id="hospital_id" name="hospital_id" class="js-states form-control">
                                <option value=""> Hospital </option> 
                                @foreach($hospitals as $data) 
                                @if(!empty($hospital) && $hospital->id == $data->id)
                                <option value="{{$data->id}}" name="hospital_id" id="hospital_id" selected>                                   
                                {{$data->title}}
                                </option>
                                @else
                                <option value="{{$data->id}}" name="hospital_id" id="hospital_id">                                   
                                {{$data->title}}
                                </option>
                                @endif
                                @endforeach
                            </select>               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div> 
           
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Description </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            <div class="row">
                <div class="col-md-6">
                <div class="form-group {{ $errors->has('banner_id') ? 'has-error' : '' }}">
                        <label for="banner_id"> Banner
                        </label>
                            <select id="banner_id" name="banner_id" class="js-states form-control">
                                <option value=""> Banner </option> 
                                @foreach($banners as $banner) 
                                <option value="{{$banner->id}}" name="banner_id" id="banner_id">                                   
                                {{$banner->title}}
                                </option>
                                @endforeach
                            </select>               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name"> Type</label>
                            <select id="type" name="type" class="js-states form-control" onchange="change_type()">
                               <option value="">Choose:</option>
                                <option value="vimeo">Vimeo</option>
                                <option value="youtube">Youtube</option>
                                <option value="file">File</option>
                            </select>               
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>     
                 
                </div>
                
            </div>

            <div class="row">         
                <div class="col-md-6" id="toshow">
                    <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                        <label for="link">Link </label>
                        <input type="text" id="link" name="link" class="form-control" value="{{old('link')}}" >
                        @if($errors->has('link'))
                            <em class="invalid-feedback">
                                {{ $errors->first('link') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>    
                </div>

                <div class="col-md-6" id="toshow2">
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        <label for="file">  File </label>
                            <input type="file" name="file" id="input-file-now-banner" class="dropify validate" data-default-file="" accept="application/pdf,application/doc,application/docx" /> 
                                @if($errors->has('file'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('file') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                    </div>   
                </div>
                <div class="col-md-6" id="toshow1">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image"> Thumb Line Images </label>
                            <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" accept= 'image/jpeg , image/jpg, image/gif, image/png' />
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                    </div>         
                </div>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#banner_id").select2({
                placeholder: "Select Banner",
                allowClear: true
            });
            $("#type").select2({
                placeholder: "Select Type",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.treatment.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
<script> 
    $("#toshow").hide(); 
    $("#toshow1").hide();  
    $("#toshow2").hide();  

    function change_type(){
        var type =$("#type").val();
            if(type == 'vimeo' || type =="youtube"){
                $("#toshow").show();
                $("#toshow1").show();
                $("#toshow2").hide();

            }else{
                $("#toshow").hide();
                $("#toshow1").hide();
                $("#toshow2").show();
            }    
    }   
</script>
<script>
    $(document).ready(function() {
            $("#hospital_id").select2({
                placeholder: "Select Hospital",
                allowClear: true
            });
        });
</script>
@endsection
