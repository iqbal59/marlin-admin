@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th img, .table.video-table td img {
    width: 80px;
    height: 80px;
    border-radius: 10px;
}
#pills-tabContent .table th, .table td {
    white-space: normal;
    line-height: 24px;
}
</style>
<div class="card">
    <div class="card-header">
       View Services
    </div>
    <div class="card-body">
        <div class="mb-2">
        <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $services->name }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Caption
                        </th>
                        <td>
                            {{ $services->caption }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Subtitle
                        </th>
                        <td>
                            {{ $services->subtitle }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Type
                        </th>
                        <td>
                            {{ $services->type }}
                        </td>
                    </tr>
                    @if($banner != null)
                    <tr>
                        <th>
                            Banner
                        </th>
                        <td>
                        {{ $banner->title }}  
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {!! $services->description !!} 
                        </td>
                    </tr>

                    @if($services->link != null)
                    <tr>
                        <th>
                            Video
                        </th>
                        <td> 
                        {!! Embed::make($services->link)->parseUrl()->getIframe() !!}
                       
                        </td>
                    </tr>
                    @endif
                    @if($services->type =="youtube" || $services->type =="vimeo")
                    <tr>
                        <th>
                            Image
                        </th>
                        <td> 
                        <img src="{{asset('storage/'.$services->image)}}" alt="" width="1000px" height="500px">
                       
                        </td>
                    </tr>
                    @else 
                    <tr>
                        <th>
                           File
                        </th> 
                        <td> 
                            @if($services->file != "")                                                                                          
                            <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$services->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>  
                            @else
                            No File Uploaded
                            @endif                     
                        </td>
                    </tr>
                    @endif 

                     
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection