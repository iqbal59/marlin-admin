<?php


namespace Modules\Services\Repositories;
use Modules\Services\Models\Services;
use Illuminate\Database\Eloquent\Builder;

class ServicesRepository implements ServicesRepositoryInterface
{
    public function all()
    { 
        return Services::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($services =  Services::create($input)) {
            return $services;
        }
        return false;
    }

    public function get($id)
    { 
        return Services::where('id',$id)->first();
    }   


    public function resetVideo(string $id)
    {
        $services = Services::find($id);
        $services->link = '';
        return $services->save();
    }

    public function resetImage(string $id)
    {
        $services = Services::find($id);
        $services->image = '';
        return $services->save();
    }

    public function resetFile(string $id)
    {
        $services = Services::find($id);
        $services->file = '';
        return $services->save();
    }

    public function update(array $input)
    { 
        $services = Services::find($input['id']); 
        unset($input['id']);
        if ($services->update($input)) {
            return $services;
        }
        return false;
    }

    public function delete(string $id)
    {
        $services = Services::find($id);
        return $services->delete();
    }

    public function getServices($hospitalId)
    { 
        return Services::where('hospital_id',$hospitalId)->get();
    }  



}