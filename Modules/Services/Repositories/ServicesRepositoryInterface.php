<?php

namespace Modules\Services\Repositories;

interface ServicesRepositoryInterface
{

    public function all();

    public function save(array $input);

    public function get($id);
    
    public function resetVideo(string $id);

    public function resetImage(string $id);

    public function resetFile(string $id);

    public function update(array $input);

    public function delete(string $id);

    public function getServices($hospitalId);
    
}