<?php

namespace Modules\Services\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Services extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'services';
    protected $fillable =  ['id','name','slug','link','file','hospital_id','caption','subtitle','description','banner_id','image','type','order','status','is_editable'];
   
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function hospital()
    {
        return $this->belongsTo('Modules\Hospital\Models\Hospital',  'hospital_id', 'id');
    }

    public function banner()
    {
        return $this->belongsTo('Modules\Banner\Models\Banner',  'banner_id', 'id');
    }
}