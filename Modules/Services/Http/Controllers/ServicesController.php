<?php

namespace Modules\Services\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Services\Http\Requests\ServicesAddRequest;
use Modules\Services\Http\Requests\ServicesSaveRequest;
use Modules\Services\Http\Requests\ServicesEditRequest;
use Modules\Services\Http\Requests\ServicesUpdateRequest;
use Modules\Services\Http\Requests\ServicesViewRequest;
use Modules\Services\Http\Requests\ServicesDeleteRequest;
use Modules\Services\Repositories\ServicesRepositoryInterface as ServicesRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Banner\Repositories\Includes\Banner\BannerRepository;
use Illuminate\Support\Facades\Storage;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function list(ServicesRepository $servicesRepo)
    {  
        $services = $servicesRepo->all();     
        return view('services::listServices', compact('services'));                         
    }

    public function add(ServicesAddRequest $request , BannerRepository $bannerRepo ,HospitalRepository $hospitalRepo)
    {    
        $banners = $bannerRepo->all();
        $hospitals = $hospitalRepo->all();  
        $hospital = $request->id != '' ? $hospitalRepo->get($request->id) : '';
        return view('services::addServices', compact('banners','hospitals','hospital'));              
    }

    public function save(ServicesSaveRequest $request, ServicesRepository $servicesRepo)
    {  
        $imageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('services_image/',$request->file('image')):"";
        $fileUrl = $request->hasFile('file')?Storage::disk('public')->putFile('services_files/',$request->file('file')):""; 

        $inputData = [
            'name'  => $request->name,
            'caption'  => $request->caption,
            'subtitle'  => $request->subtitle,
            'description'  => $request->description,
            'banner_id'  => $request->banner_id,
            'type'  => $request->type,
            'link'  => $request->link,
            'file'  => $fileUrl,
            'image'  => $imageUrl,
            'hospital_id'  => $request->hospital_id,
        ];
        $services = $servicesRepo->save($inputData);  
        connectify('success', 'Success!', 'Services added successfully');        
        $route = !empty($request->hasHospital) ? redirect()->route('admin.hospital.view', ['id' => $request->hospital_id]) : redirect()->route('admin.services.list');    
        return $route;       


        // return redirect()
        // ->route('admin.services.list')
        // ->with('success', 'Services Added successfuly');   
    } 


    public function view(ServicesViewRequest $request,ServicesRepository $servicesRepo,BannerRepository $bannerRepo)
    { 
        $services = $servicesRepo->get($request->id);  
        $banner = $bannerRepo->get($services->banner_id);  
        return view('services::viewServices', compact('services','banner'));
    }

    public function edit(ServicesEditRequest $request, ServicesRepository $servicesRepo,BannerRepository $bannerRepo,HospitalRepository $hospitalRepo)
    {  
        $services =$servicesRepo->get($request->id);  
        $banners = $bannerRepo->all();  
        $hospitals = $hospitalRepo->all();  
        $hospital = $request->hospital_id != '' ? $hospitalRepo->get($request->hospital_id) : '';
        return view('services::editServices', compact('hospital','services','banners','hospitals'));
    }

    public function removeFile(ServicesRepository $servicesRepo,Request $request)
    {    
        if ($this->_removeFile($servicesRepo, $request->id)) {
            $servicesRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');        
        return redirect()
            ->route('admin.services.edit', ['id' => $request->id]);
    }

    public function removeVideo(ServicesRepository $servicesRepo,Request $request)
    {    
        $servicesRepo->resetVideo($request->id);
        connectify('success', 'Success!', 'Video deleted successfully');        
        return redirect()
            ->route('admin.services.edit', ['id' => $request->id]);
    }

    private function _removeFile($servicesRepo, $id)
    { 
        $image = $servicesRepo->get($id);
        if (Storage::disk('public')->delete($image->file)) {
            return true;
        }
        return false;
    }

    public function removeImage(ServicesRepository $servicesRepo,Request $request)
    {     
        if ($this->_removeImage($servicesRepo, $request->id)) {
            $servicesRepo->resetImage($request->id);
        }
        connectify('success', 'Success!', 'Image deleted successfully');        
        return redirect()
            ->route('admin.services.edit', ['id' => $request->id]);
    }

    private function _removeImage($servicesRepo, $id)
    { 
        $image = $servicesRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }


    public function update(ServicesUpdateRequest $request, ServicesRepository $servicesRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'caption'  => $request->caption,
            'subtitle'  => $request->subtitle,
            'description'  => $request->description,
            'banner_id'  => $request->banner_id,
            'hospital_id'  => $request->hospital_id,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }



        if ($request->link) { 
            $inputData['link'] = $request->link;
        }

        if ($request->hasFile('image')) { 
            $this->_removeFile($servicesRepo, $request->id);                 
            $imageUrl = Storage::disk('public')->putFile('video_image/',$request->file('image'));
            $inputData['image'] = $imageUrl;
        } 

        if ($request->hasFile('file')) { 
            $this->_removeFile($servicesRepo, $request->id);                 
            $fileUrl = Storage::disk('public')->putFile('services_files/',$request->file('file'));
            $inputData['file'] = $fileUrl;
        } 

        $services = $servicesRepo->update($inputData);
        $route = !empty($request->hasHospital) ? redirect()->route('admin.hospital.view', ['id' => $request->hospital_id]) : redirect()->route('admin.services.list');    
        connectify('success', 'Success!', 'Services updated successfully');        
        return $route;
    }  
    public function delete(ServicesRepository $servicesRepo, ServicesDeleteRequest $request)
    { 
        $route = !empty($request->hospital_id) ? redirect()->route('admin.hospital.view', ['id' => $request->hospital_id]) : redirect()->route('admin.services.list');    
        if ($servicesRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Services deleted successfully');        
            return $route;
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}
