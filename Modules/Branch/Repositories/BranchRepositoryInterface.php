<?php

namespace Modules\Branch\Repositories;

interface BranchRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($Id);

    public function update(array $input);

    public function delete(string $id);

}