<?php


namespace Modules\Branch\Repositories;
use Modules\Branch\Models\Branch;
use Illuminate\Database\Eloquent\Builder;

class BranchRepository implements BranchRepositoryInterface
{
    public function getForDatatable()
    { 
        return Branch::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Branch::where('status',1)->orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($branch =  Branch::create($input)) {
            return $branch;
        }
        return false;
    }

    public function get($Id)
    {
        return Branch::where('id',$Id)->first();
    }

    public function update(array $input)
    {
        $branch = Branch::find($input['id']); 
        unset($input['id']);
        if ($branch->update($input)) {
            return $branch;
        }
        return false;
    }

    public function delete(string $id)
    {
        $branch = Branch::find($id);
        return $branch->delete();
    }

    
}