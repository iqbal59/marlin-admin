@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>



<div class="card">
    <div class="card-header">
      Edit Branch
    </div>

    <div class="card-body">
        <form action="{{ route("admin.branch.update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$branch->id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name </label>
                        <i class="mdi mdi-flag-checkered text-danger "></i>
                        <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($branch) ? $branch->name : '') }}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                       
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">Email </label>
                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($branch) ? $branch->email : '') }}" >
                        @if($errors->has('email'))
                            <em class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </em>
                        @endif
                       
                    </div>
                </div>
            </div> 

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                        <label for="phone_no">Phone Number </label>
                        <input type="text" id="phone_no" name="phone_no" class="form-control" value="{{ old('phone_no', isset($branch) ? $branch->phone_no : '') }}" >
                        @if($errors->has('phone_no'))
                            <em class="invalid-feedback">
                                {{ $errors->first('phone_no') }}
                            </em>
                        @endif
                       
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
                        <label for="location">Address </label>
                        <input type="text" id="location" name="location" class="form-control" value="{{ old('location', isset($branch) ? $branch->location : '') }}" >
                        @if($errors->has('location'))
                            <em class="invalid-feedback">
                                {{ $errors->first('location') }}
                            </em>
                        @endif                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('map_url') ? 'has-error' : '' }}">
                        <label for="map_url">Map Url </label>
                        <input type="text" id="map_url" name="map_url" class="form-control" value="{{ old('map_url', isset($branch) ? $branch->map_url : '') }}" >
                        @if($errors->has('map_url'))
                            <em class="invalid-feedback">
                                {{ $errors->first('map_url') }}
                            </em>
                        @endif                       
                    </div>
                </div>
                    
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $branch->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>  
            </div>      

            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>
@endsection