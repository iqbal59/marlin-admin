@extends('layouts.admin')
@section('content')
 
<div class="card">
    <div class="card-header">
       View Branch
    </div>
    <div class="card-body">
        <div class="mb-2">
        <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $branch->name }}
                        </td>
                    </tr>
                    @if($branch->phone_no != null)
                    <tr>
                        <th>
                          Phone NUmber
                        </th>
                        <td>
                            {{ $branch->phone_no }}
                        </td>
                    </tr>
                    @endif
                    @if($branch->email != null)
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                        {{ $branch->email }}
                        </td>
                    </tr>
                    @endif
                    @if($branch->location != null)
                    <tr>
                        <th>
                            Address
                        </th>
                        <td>
                        {{ $branch->location }}
                        </td>
                    </tr>
                    @endif
                    @if($branch->map_url != null)
                    <tr>
                        <th>
                            Map Url
                        </th>
                        <td>
                        {{ $branch->map_url }}
                        </td>
                    </tr>
                    @endif

                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection