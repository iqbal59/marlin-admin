@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Add Branch
    </div>

    <div class="card-body">
        <form action="{{ route("admin.branch.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('cruds.user.fields.name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                            <label for="phone_no">Phone Number  </label>
                            <input type="text" id="phone_no" name="phone_no" class="form-control" value="{{old('phone_no')}}" >
                            @if($errors->has('phone_no'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('phone_no') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                    </div>
                </div>

             <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">Email  </label>
                        <input type="text" id="email" name="email" class="form-control" value="{{old('email')}}" >
                        @if($errors->has('email'))
                            <em class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
                        <label for="location">Address  </label>
                        <input type="text" id="location" name="location" class="form-control" value="{{old('location')}}" >
                        @if($errors->has('location'))
                            <em class="invalid-feedback">
                                {{ $errors->first('location') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
             </div>
             <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('map_url') ? 'has-error' : '' }}">
                        <label for="map_url">Map Url  </label>
                        <input type="text" id="map_url" name="map_url" class="form-control" value="{{old('map_url')}}" placeholder="https://www.google.co.in/maps/place/New+York,+NY,+USA/">
                        @if($errors->has('map_url'))
                            <em class="invalid-feedback">
                                {{ $errors->first('map_url') }}
                            </em>
                        @endif
                    </div> 
                </div>
             </div>
        
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
@endsection
