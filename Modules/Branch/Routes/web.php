<?php
use Modules\Branch\Http\Controllers\BranchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('branch')->group(function() {
    Route::get('/', 'BranchController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'branch', 'as' => 'branch.'], function () {    
        Route::get('/', [BranchController::class, 'list'])->name('list');
        Route::get('add', [BranchController::class, 'add'])->name('add');
        Route::post('save', [BranchController::class, 'save'])->name('save');
        Route::get('edit', [BranchController::class, 'edit'])->name('edit');
        Route::post('update', [BranchController::class, 'update'])->name('update');
        Route::get('view', [BranchController::class, 'view'])->name('view');
        Route::get('delete', [BranchController::class, 'delete'])->name('delete');
      
    });
});
