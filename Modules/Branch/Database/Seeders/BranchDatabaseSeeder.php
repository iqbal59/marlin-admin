<?php

namespace Modules\Branch\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class BranchDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '256',
                'title'      => 'branch_access',
                'created_at' => '2021-12-18 12:14:15',
                'updated_at' => '2021-12-18 12:14:15',
            ],
            [
                'id'         => '257',
                'title'      => 'branch_create',
                'created_at' => '2021-12-18 12:14:15',
                'updated_at' => '2021-12-18 12:14:15',
            ],
            [
                'id'         => '258',
                'title'      => 'branch_edit',
                'created_at' => '2021-12-18 12:14:15',
                'updated_at' => '2021-12-18 12:14:15',
            ],
            [
                'id'         => '259',
                'title'      => 'branch_show',
                'created_at' => '2021-12-18 12:14:15',
                'updated_at' => '2021-12-18 12:14:15',
            ],
            [
                'id'         => '260',
                'title'      => 'branch_delete',
                'created_at' => '2021-12-18 12:14:15',
                'updated_at' => '2021-12-18 12:14:15',
            ],
        ];

        Permission::insert($permissions);
    }
}
