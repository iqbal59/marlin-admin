<?php

namespace Modules\Branch\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('branch_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
