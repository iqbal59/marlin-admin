<?php

namespace Modules\Branch\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('branch_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
