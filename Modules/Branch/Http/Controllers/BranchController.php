<?php

namespace Modules\Branch\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Branch\Http\Requests\BranchAddRequest;
use Modules\Branch\Http\Requests\BranchSaveRequest;
use Modules\Branch\Http\Requests\BranchEditRequest;
use Modules\Branch\Http\Requests\BranchUpdateRequest;
use Modules\Branch\Http\Requests\BranchViewRequest;
use Modules\Branch\Http\Requests\BranchDeleteRequest;
use Modules\Branch\Repositories\BranchRepositoryInterface as BranchRepository;

class BranchController extends Controller
{
    public function list(BranchRepository $branchRepo)
    {  
        $branches = $branchRepo->getForDatatable();   
        return view('branch::listBranch', compact('branches'));                         
    }

    public function add(BranchAddRequest $request , BranchRepository $branchRepo)
    { 
        return view('branch::addBranch');               
    }

    public function save(BranchSaveRequest $request, BranchRepository $branchRepo)
    {    
        $inputData = [
            'name'  => $request->name,
            'phone_no'  => $request->phone_no,
            'email'  => $request->email,
            'location'  => $request->location,
            'map_url'  => $request->map_url
        ];
        $branch = $branchRepo->save($inputData);    
        connectify('success', 'Success!', 'Branch added successfully');    
        return redirect()
            ->route('admin.branch.list');
    } 

    public function view(BranchViewRequest $request,BranchRepository $branchRepo)
    {
        $branch = $branchRepo->get($request->id); 
        return view('branch::viewBranch', compact('branch'));
    }

    public function edit(BranchEditRequest $request, BranchRepository $branchRepo)
    {  
        $branch =$branchRepo->get($request->id);    
        return view('branch::editBranch', compact('branch'));
    }

    public function update(BranchUpdateRequest $request, BranchRepository $branchRepo)
    {     
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'email'  => $request->email,
            'phone_no'  => $request->phone_no,
            'location'=>$request->location,
            'map_url'=>$request->map_url,
        ];
        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
 
        $branch = $branchRepo->update($inputData);
        connectify('success', 'Success!', 'Branch updated successfully');    
        return redirect()
        ->route('admin.branch.list');
    }  
 
    public function delete(BranchRepository $branchRepo, BranchDeleteRequest $request)
    { 
        if ($branchRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Branch deleted successfully');    
            return redirect()
            ->route('admin.branch.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

  
}
