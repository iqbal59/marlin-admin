<?php

namespace Modules\Branch\Http\Controllers\Api;
use Illuminate\Routing\Controller;
use Modules\Branch\Repositories\BranchRepositoryInterface as BranchRepository;
use Carbon\Exceptions\Exception;

class BranchController extends Controller
{

    public function list(BranchRepository $branchRepo)
    {     
        try {
            $branches = $branchRepo->all();   
            $data = compact("branches");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }  
    } 

   
}
