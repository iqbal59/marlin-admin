<?php

namespace Modules\Branch\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Branch extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'phone_no',
        'email',
        'location',
        'status',
        'map_url'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Branch\Database\factories\BranchFactory::new();
    }
}
