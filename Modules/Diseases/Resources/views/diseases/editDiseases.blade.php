@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>


<div class="card">
    <div class="card-header">
       Edit Diseases
    </div>

    <div class="card-body">
        <form action="{{ route("admin.diseases.update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$diseases->id}}">
            <input type="hidden" name="created_by" value="{{$diseases->created_by}}">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title</label>
                <i class="mdi mdi-flag-checkered text-danger "></i>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('name', isset($diseases) ? $diseases->title : '') }}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            
            <!-- <div class="form-group {{ $errors->has('created_by') ? 'has-error' : '' }}">
                <label for="created_by">Created By</label>
                <input type="text" id="title" name="created_by" class="form-control" value="{{ old('created_by', isset($diseases) ? $diseases->created_by : '') }}" >
                @if($errors->has('created_by'))
                    <em class="invalid-feedback">
                        {{ $errors->first('created_by') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>   -->

            <div class="form-group {{ $errors->has('related') ? 'has-error' : '' }}">
                <label for="related">Related</label>
                <input type="text" id="title" name="related" class="form-control" value="{{ old('related', isset($diseases) ? $diseases->related : '') }}" >
                @if($errors->has('related'))
                    <em class="invalid-feedback">
                        {{ $errors->first('related') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }}</label>
                <textarea class="ckeditor form-control" id="description" name="description" >
                {!! old('description', isset($diseases) ? $diseases->description : '') !!}
                </textarea>      
                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('treatment_id') ? 'has-error' : '' }}">
                <label for="treatment_id">Treatment<i class="mdi mdi-flag-checkered text-danger "></i>
                </label>
                <select id="treatment_id" name="treatment_id" class="js-states form-control">
                        @foreach ($treatments as $key => $value)
                        <option value="{{$value->id}}" {{(old('treatment_id', $diseases->treatment_id) == $value->id ? 'selected' : '')}} > {{$value->name}} </option>
                        @endforeach
                </select>    
                @if($errors->has('treatment_id'))
                <em class="invalid-feedback">{{ $errors->first('treatment_id') }}</em>
                @endif                        
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
            
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status',$diseases->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>              
            <br>          

            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $(document).ready(function() {
        $("#hospitale_id").select2({
            placeholder: "Select Hospital",
            allowClear: true
        });
        $("#treatment_id").select2({
            placeholder: "Select Treatment",
            allowClear: true
        });
    });
</script>
@endsection


