<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #example_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }

    </style>
<div class="card">
    <div class="card-header">
        Tips
        <a class="btn btn-warning float-right" href="{{route('admin.tips.addTips',['diseases_id' => $diseases->id])}}">
            Add Tips
        </a>
    </div> 

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class="display nowrap table" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tips as $key => $tip)
                        <tr data-entry-id="{{ $tip->id }}">
                            <td>{{ $key+1 }}</td>
                            <td>{{ $tip->title ?? '' }}</td>
                            <td>
                                @if($tip->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.tips.viewTips', ['id' => $tip->id,'diseases_id' => $diseases->id ]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.tips.editTips', ['id' => $tip->id, 'diseases_id' => $diseases->id ]) }}">
                                    {{ trans('global.edit') }}
                                </a>   

                                <form action="{{ route('admin.tips.deleteTips',  ['id' => $tip->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$tip->id}}">
                                    <input type="hidden" name="diseases_id" value="{{$diseases->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
{{-- <script src="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css"></script> --}}
{{-- <script src="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css"></script> --}}
<script>
   $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection



