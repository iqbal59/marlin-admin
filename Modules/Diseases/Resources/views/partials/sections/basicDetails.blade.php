<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
  color: orange;
}
#pills-tabContent .table th, .table td {
    white-space: normal;
    line-height: 24px;
}
.table.video-table th, .table.video-table td {
    line-height: 24px;
    white-space: normal;
}
</style>
<div class="mb-2">
    <table class="table table-bordered table-striped video-table">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $diseases->id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ $diseases->title }}</td>
            </tr>
            @if($diseases->related != null)
            <tr>
                <th>Related </th>
                <td>{{ $diseases->related }}</td>
            </tr>
            @endif
            @if($diseases->description != null)
            <tr>
                <th>Description</th>
                <td>{!! $diseases->description !!} </td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif 
        </tbody>
    </table>
</div>