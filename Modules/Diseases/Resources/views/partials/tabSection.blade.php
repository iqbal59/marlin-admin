<div class="row">
    <div class="col-md-12">
         
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Basic</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-tips-tab" 
                    data-toggle="pill" 
                    href="#pills-tips" 
                    role="tab" 
                    aria-controls="pills-tips" 
                    aria-selected="true">Tips</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('diseases::partials.sections.basicDetails')
            </div>
            <div class="tab-pane fade show" id="pills-tips" role="tabpanel" aria-labelledby="pills-tips-tab">
            @include('diseases::partials.sections.tipsDetails')
            </div>
            
        </div>
    </div>
</div>