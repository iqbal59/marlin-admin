<?php

namespace Modules\Diseases\Database\Seeders;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DiseasesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '201',
                'title'      => 'diseases_create',
                'created_at' => '2019-10-28 12:14:15',
                'updated_at' => '2019-10-28 12:14:15',
            ],
            [
                'id'         => '202',
                'title'      => 'diseases_edit',
                'created_at' => '2019-10-28 12:14:15',
                'updated_at' => '2019-10-28 12:14:15',
            ],
            [
                'id'         => '203',
                'title'      =>  'diseases_show',
                'created_at' => '2019-10-28 12:14:15',
                'updated_at' => '2019-10-28 12:14:15',
            ],
            [
                'id'         => '204',
                'title'      => 'diseases_delete',
                'created_at' => '2019-10-28 12:14:15',
                'updated_at' => '2019-10-28 12:14:15',
            ],
            [
                'id'         => '205',
                'title'      => 'diseases_access',
                'created_at' => '2019-10-28 12:14:15',
                'updated_at' => '2019-10-28 12:14:15',
            ],
            [
                'id'         => '206',
                'title'      => 'tips_create',
                'created_at' => '2021-11-11 12:14:15',
                'updated_at' => '2021-11-11 12:14:15',
            ],
            [
                'id'         => '207',
                'title'      => 'tips_edit',
                'created_at' => '2021-11-11 12:14:15',
                'updated_at' => '2021-11-11 12:14:15',
            ],
            [
                'id'         => '208',
                'title'      =>  'tips_show',
                'created_at' => '2021-11-11 12:14:15',
                'updated_at' => '2021-11-11 12:14:15',
            ],
            [
                'id'         => '209',
                'title'      => 'tips_delete',
                'created_at' => '2021-11-11 12:14:15',
                'updated_at' => '2021-11-11 12:14:15',
            ],
            [
                'id'         => '210',
                'title'      => 'tips_access',
                'created_at' => '2021-11-11 12:14:15',
                'updated_at' => '2021-11-11 12:14:15',
            ],

        ];
        
        Permission::insert($permissions);
    }
}
