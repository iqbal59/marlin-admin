<?php

namespace Modules\Diseases\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Diseases extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'diseases';

    protected $fillable = ['id','slug','title','related','description','created_by','status','treatment_id','added_by'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

     
}