<?php

namespace Modules\Diseases\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tips extends Model
{
    use HasFactory;
    protected $table = 'diseases_tips';

    protected $fillable = ['id','diseases_id','description','image','title','added_by'];

}