<?php

namespace Modules\Diseases\Http\Requests\Diseases;

use Illuminate\Foundation\Http\FormRequest;

class DiseasesAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('diseases_create') ? true : false;
    }
    public function rules()
    {
        return [
           
        ];
    }
}
