<?php

namespace Modules\Diseases\Http\Requests\Diseases;

use Illuminate\Foundation\Http\FormRequest;

class DiseasesUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('diseases_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            // 'related' => 'required',
            'treatment_id' => 'required',
         ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            // 'related.required' => ':attribute is required',
            'treatment_id.required' => ':attribute is required',
         ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            // 'related' => 'Related',
            'treatment_id' => 'Treatment',
         ];
    }
}
