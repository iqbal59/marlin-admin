<?php

namespace Modules\Diseases\Http\Requests\Diseases;

use Illuminate\Foundation\Http\FormRequest;

class DiseasesViewRequest extends FormRequest
{
  /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
