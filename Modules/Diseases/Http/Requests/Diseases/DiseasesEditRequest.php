<?php

namespace Modules\Diseases\Http\Requests\Diseases;

use Illuminate\Foundation\Http\FormRequest;

class DiseasesEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('diseases_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
