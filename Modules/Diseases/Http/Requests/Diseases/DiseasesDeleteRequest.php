<?php

namespace Modules\Diseases\Http\Requests\Diseases;

use Illuminate\Foundation\Http\FormRequest;

class DiseasesDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('diseases_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
