<?php

namespace Modules\Diseases\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('tips_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
