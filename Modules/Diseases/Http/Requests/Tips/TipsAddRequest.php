<?php

namespace Modules\Diseases\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('tips_create') ? true : false;
    }
    public function rules()
    {
        return [
           
        ];
    }
}
