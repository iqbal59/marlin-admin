<?php

namespace Modules\Diseases\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('tips_create') ? true : false;
    }

    public function rules()
    {
        return [
            'description' => 'required',
            'title' => 'required',
            'image' => 'required',
         ];
    }

    public function messages()
    {
        return [
            'description.required' => ':attribute is required',
            'title.required' => ':attribute is required',
            'image.required' => ':attribute is required',
         ];
    }

    public function attributes()
    {
        return [
            'description' => 'Description',
            'title' => 'Title',
            'image' => 'Image',
         ];
    }
}
