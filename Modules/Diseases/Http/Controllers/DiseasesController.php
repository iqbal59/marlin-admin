<?php

namespace Modules\Diseases\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Diseases\Http\Controllers\Includes\Diseases;
use Modules\Diseases\Http\Controllers\Includes\Tips;

class DiseasesController extends Controller
{
    use Diseases;
    use Tips;
}

