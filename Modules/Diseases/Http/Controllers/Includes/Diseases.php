<?php

namespace Modules\Diseases\Http\Controllers\Includes;
use Modules\Diseases\Http\Requests\Diseases\DiseasesAddRequest;
use Modules\Diseases\Http\Requests\Diseases\DiseasesSaveRequest;
use Modules\Diseases\Http\Requests\Diseases\DiseasesEditRequest;
use Modules\Diseases\Http\Requests\Diseases\DiseasesUpdateRequest;
use Modules\Diseases\Http\Requests\Diseases\DiseasesViewRequest;
use Modules\Diseases\Http\Requests\Diseases\DiseasesDeleteRequest;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Diseases\Repositories\Tips\TipsRepositoryInterface as TipsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cohensive\Embed\Facades\Embed;
use Illuminate\Support\Facades\Auth;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
trait Diseases
{
   
    public function listdiseases(DiseasesRepository $diseasesRepo)
    {  
        $diseases = $diseasesRepo->getForDatatable();    
        return view('diseases::diseases.listDiseases', compact('diseases'));                         
    }

    public function addDiseases(DiseasesAddRequest $request , DiseasesRepository $diseasesRepo)
    {    
        return view('diseases::diseases.addDiseases');        
    }

    public function save(DiseasesSaveRequest $request, DiseasesRepository $diseasesRepo)
    {   
        $inputData = [
            'title'  => $request->title,
            'related'  => $request->related,
            'description'  => $request->description,
            'created_by'  =>Auth::user()->id,
            'added_by'     => auth()->user()->id,
        ];
        $diseases = $diseasesRepo->save($inputData);  
        connectify('success', 'Success!', 'Diseases added successfully');        
            return redirect()
            ->route('admin.diseases.listdiseases');
    } 


    public function view(DiseasesViewRequest $request,DiseasesRepository $diseasesRepo, TipsRepository $tipsRepo,UserRepository $userRepo)
    { 
        $diseases = $diseasesRepo->get($request->id);  
        $tips = $tipsRepo->getTips($request->id); 
        $addedBy = $userRepo->get($diseases->added_by); 
        return view('diseases::diseases.viewDiseases', compact('addedBy','diseases','tips'));
    }

    public function edit(DiseasesEditRequest $request, DiseasesRepository $diseasesRepo, TreatmentRepository $treatmentRepo)
    {  
        $diseases =$diseasesRepo->get($request->id);  
        $treatments = $treatmentRepo->allTreatments();
        return view('diseases::diseases.editDiseases', compact('diseases','treatments'));
    }


    public function update(DiseasesUpdateRequest $request, DiseasesRepository $diseasesRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'title'  => $request->title,
            'related'  => $request->related,
            'description'  => $request->description,
            'created_by'  => Auth::user()->id,
            'treatment_id'  => $request->treatment_id,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }

        $diseases = $diseasesRepo->update($inputData);
        connectify('success', 'Success!', 'Diseases updated successfully');        
        return redirect()
            ->route('admin.diseases.listdiseases');
    }  

    public function delete(DiseasesRepository $diseasesRepo, DiseasesDeleteRequest $request)
    { 
        if ($diseasesRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Diseases deleted successfully');        
            return redirect()
            ->route('admin.diseases.listdiseases');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    
}