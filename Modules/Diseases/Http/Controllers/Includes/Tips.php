<?php

namespace Modules\Diseases\Http\Controllers\Includes;

use App\Repositories\User\UserRepository;
use Modules\Diseases\Http\Requests\Tips\TipsAddRequest;
use Modules\Diseases\Http\Requests\Tips\TipsSaveRequest;
use Modules\Diseases\Http\Requests\Tips\TipsEditRequest;
use Modules\Diseases\Http\Requests\Tips\TipsUpdateRequest;
use Modules\Diseases\Http\Requests\Tips\TipsViewRequest;
use Modules\Diseases\Http\Requests\Tips\TipsDeleteRequest;
use Modules\Diseases\Repositories\Tips\TipsRepositoryInterface as TipsRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

trait Tips
{
    public function addTips(TipsAddRequest $request , TipsRepository $tipsRepo)
    {    
        $diseasesId = $request->diseases_id;
        return view('diseases::tips.addTips', compact('diseasesId'));                         
    }

    public function saveTips(TipsSaveRequest $request, TipsRepository $tipsRepo)
    {   
        $imageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('diseases_tips/',$request->file('image')):"";

        $inputData = [
            'diseases_id'  => $request->diseasesId,
            'description'  => $request->description,
            'title'  => $request->title,
            'image'        => $imageUrl,
            'added_by'     => auth()->user()->id,
        ];
        $tips = $tipsRepo->save($inputData);  
        connectify('success', 'Success!', 'Diseases added successfully');                   
        $route =redirect()->route('admin.diseases.view', ['id' => $request->diseasesId]);
        connectify('success', 'Success!', 'Tips added successfully'); 
        return $route;
    }

    public function viewTips(TipsViewRequest $request, TipsRepository $tipsRepo, DiseasesRepository $diseasesRepo,UserRepository $userRepo)
    {  
        $tips = $tipsRepo->get($request->id); 
        $diseases = $diseasesRepo->get($request->diseases_id); 
        $addedBy = $userRepo->get($tips->added_by); 
        return view('diseases::tips.viewTips', compact('addedBy','diseases','tips'));
    }

    public function editTips(TipsEditRequest $request, TipsRepository $tipsRepo)
    {   
        $diseasesId = $request->diseases_id;
        $tips =$tipsRepo->get($request->id);  
        return view('diseases::tips.editTips', compact('tips','diseasesId'));
    }

    public function removeFile(TipsRepository $tipsRepo,Request $request)
    {   
        if ($this->_removeFile($tipsRepo, $request->id)) {
            $tipsRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly'); 
        return redirect()
            ->route('admin.tips.editTips', ['id' => $request->id]);
    }

    private function _removeFile($tipsRepo, $id)
    { 
        $logo = $tipsRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }


    public function updateTips(TipsUpdateRequest $request, TipsRepository $tipsRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'description'  => $request->description,
            'title' => $request->title,
        ];

        // if ($request->hasFile('image')) { 
        //     $this->_removeFile($tipsRepo, $request->id);                 
        //     $imageUrl = Storage::disk('public')->putFile('diseases_tips/',$request->file('image'));
        //     $inputData['image'] = $imageUrl;
        // }   

        $tips = $tipsRepo->update($inputData);
        $route =redirect()->route('admin.diseases.view', ['id' => $request->diseasesId]);
        connectify('success', 'Success!', 'Tips updated successfully'); 
        return $route;
    }  

    public function deleteTips(TipsRepository $tipsRepo, TipsDeleteRequest $request)
    {   
        if ($tipsRepo->delete($request->id)) {
            $route =redirect()->route('admin.diseases.view', ['id' => $request->diseases_id]);
            connectify('success', 'Success!', 'Tips updated successfully'); 
            return $route;
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}