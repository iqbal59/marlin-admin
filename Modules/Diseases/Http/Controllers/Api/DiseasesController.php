<?php

namespace Modules\Diseases\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Carbon\Exceptions\Exception;
class DiseasesController extends Controller
{
    public function listDiseases(DiseasesRepository $diseasesRepo)
    {   
        try {
            $data = $diseasesRepo->all();   
            $response = ['diseases' => $data,'status' => true, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }                                  
    }  
}