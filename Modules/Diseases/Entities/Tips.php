<?php

namespace Modules\Diseases\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tips extends Model
{
    use HasFactory;

    protected $fillable = ['id','diseases_id','description','image','title','added_by'];

    
    protected static function newFactory()
    {
        return \Modules\Diseases\Database\factories\TipsFactory::new();
    }
}
