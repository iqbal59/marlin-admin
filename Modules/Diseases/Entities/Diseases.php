<?php

namespace Modules\Diseases\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Diseases extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['id','slug','title','related','description','created_by','status','treatment_id','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\Diseases\Database\factories\DiseasesFactory::new();
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
