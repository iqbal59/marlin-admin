<?php

use Illuminate\Http\Request;
use Modules\Diseases\Http\Controllers\Api\DiseasesController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/diseases', function (Request $request) {
    return $request->user();
});
Route::prefix('diseases')->group(function () {
    Route::get('/listDiseases', [DiseasesController::class, 'listDiseases']);
});