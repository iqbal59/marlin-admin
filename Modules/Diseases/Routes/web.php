<?php
use Modules\Diseases\Http\Controllers\DiseasesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('diseases')->group(function() {
    Route::get('/', 'DiseasesController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'diseases', 'as' => 'diseases.'], function () {         
        Route::get('/listdiseases', [DiseasesController::class, 'listdiseases'])->name('listdiseases');
        Route::get('addDiseases', [DiseasesController::class, 'addDiseases'])->name('addDiseases');
        Route::post('save', [DiseasesController::class, 'save'])->name('save');
        Route::get('edit', [DiseasesController::class, 'edit'])->name('edit');
        Route::post('update', [DiseasesController::class, 'update'])->name('update');
        Route::get('view', [DiseasesController::class, 'view'])->name('view');
        Route::get('delete', [DiseasesController::class, 'delete'])->name('delete');       
    });

    Route::group(['prefix' => 'tips', 'as' => 'tips.'], function () {         
        // Route::get('/listdiseases', [DiseasesController::class, 'listdiseases'])->name('listdiseases');
        Route::get('addTips', [DiseasesController::class, 'addTips'])->name('addTips');
        Route::post('saveTips', [DiseasesController::class, 'saveTips'])->name('saveTips');
        Route::get('editTips', [DiseasesController::class, 'editTips'])->name('editTips');
        Route::post('updateTips', [DiseasesController::class, 'updateTips'])->name('updateTips');
        Route::get('viewTips', [DiseasesController::class, 'viewTips'])->name('viewTips');
        Route::get('deleteTips', [DiseasesController::class, 'deleteTips'])->name('deleteTips');       
        Route::get('remove_file', [DiseasesController::class, 'removeFile'])->name('remove_file');
    
    });
});
