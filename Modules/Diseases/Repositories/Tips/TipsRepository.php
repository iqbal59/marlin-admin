<?php


namespace Modules\Diseases\Repositories\Tips;
use Modules\Diseases\Models\Tips;
use Illuminate\Database\Eloquent\Builder;

class TipsRepository implements TipsRepositoryInterface
{

    public function getTips($diseasesId)
    { 
        return Tips::where('diseases_id',$diseasesId)->orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($tips =  Tips::create($input)) {
            return $tips;
        }
        return false;
    }

    public function get($Id)
    {
        return Tips::where('id',$Id)->first();
    }

    
    public function update(array $input)
    {
        $tips = Tips::find($input['id']); 
        unset($input['id']);
        if ($tips->update($input)) {
            return $tips;
        }
        return false;
    }

    public function delete(string $id)
    {
        $diseases = Tips::find($id); 
        return $diseases->delete();
    }

    public function getDiseasesTips($diseasesId)
    { 
        return Tips::whereIn('diseases_id',$diseasesId)->get();
    }

    public function resetFile(string $id)
    {
        $tips = Tips::find($id);
        $tips->image = '';
        return $tips->save();
    }
    
}