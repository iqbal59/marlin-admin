<?php

namespace Modules\Diseases\Repositories\Tips;

interface TipsRepositoryInterface
{
    public function getTips($diseasesId);

    public function save(array $input);

    public function get($tipsId);

    public function update(array $input);

    public function delete(string $id);

    public function getDiseasesTips($diseasesId);

    public function resetFile(string $id);

}