<?php


namespace Modules\Diseases\Repositories\Diseases;
use Modules\Diseases\Models\Diseases;
use Illuminate\Database\Eloquent\Builder;

class DiseasesRepository implements DiseasesRepositoryInterface
{
    public function getForDatatable()
    { 
        return Diseases::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Diseases::orderBy('id', 'DESC')->get();
    }

    public function getAll()
    { 
        return Diseases::select('id','title','slug')->get();
    }

    public function getDiseases($slug)
    { 
        return Diseases::where('status',1)->where('slug',$slug)->first();
    }

    public function save(array $input)
    { 
        if ($diseases =  Diseases::create($input)) {
            return $diseases;
        }
        return false;
    }

    public function get($Id)
    {
        return Diseases::where('id',$Id)->first();
    }

    
    public function update(array $input)
    {
        $diseases = Diseases::find($input['id']); 
        unset($input['id']);
        if ($diseases->update($input)) {
            return $diseases;
        }
        return false;
    }

    public function delete(string $id)
    {
        $diseases = Diseases::find($id);
        return $diseases->delete();
    }

    public function searchBasedDiseases($searchText)
    { 
        return Diseases::where('status',1)->where('title', 'like', '%'.$searchText.'%')->pluck('id')->toArray();
    }

    public function diseases($diseasesId)
    { 
        return Diseases::where('status',1)->whereIn('id',$diseasesId)->pluck('slug')->toArray();
    }
    
}