<?php

namespace Modules\Diseases\Repositories\Diseases;

interface DiseasesRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function getAll();

    public function getDiseases($slug);

    public function save(array $input);

    public function get($deptId);

    public function update(array $input);

    public function delete(string $id);

    public function searchBasedDiseases($searchText);

    public function diseases($diseasesId);

}