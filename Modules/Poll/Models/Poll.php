<?php

namespace Modules\Poll\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Modules\Department\Models\Department;
use Modules\Doctor\Models\Doctor;

class Poll extends Model
{
    use HasFactory;
    protected $table = 'polls';

    protected $fillable = ['value','poll_id','poll_type'];
    
    /**
     * Get all of the owning commentable models.
     */
    public function pollable()
    {
        return $this->morphTo();
    }

}