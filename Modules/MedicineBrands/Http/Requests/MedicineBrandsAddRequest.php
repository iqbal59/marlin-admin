<?php

namespace Modules\MedicineBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineBrandsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicinebarnds_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
