<?php

namespace Modules\MedicineBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineBrandsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicinebarnds_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            // 'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',

        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            // 'description' => 'Description',
        ];
    }
}
