<?php

namespace Modules\MedicineBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineBrandsSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicinebarnds_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required',
        //     'description' => 'required',
        //     'logo' => 'required',
        //     'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
            // 'logo.required' => ':attribute is required',
            // 'image.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            // 'description' => 'Description',
            // 'logo' => 'Logo',
            // 'image' => 'Image',
        ];
    }
}
