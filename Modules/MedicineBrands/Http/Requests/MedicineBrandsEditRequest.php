<?php

namespace Modules\MedicineBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineBrandsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicinebarnds_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
