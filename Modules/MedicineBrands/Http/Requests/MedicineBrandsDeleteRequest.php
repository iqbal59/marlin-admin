<?php

namespace Modules\MedicineBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicineBrandsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicinebarnds_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
