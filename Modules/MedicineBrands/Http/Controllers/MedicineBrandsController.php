<?php

namespace Modules\MedicineBrands\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsAddRequest;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsSaveRequest;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsEditRequest;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsUpdateRequest;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsViewRequest;
use Modules\MedicineBrands\Http\Requests\MedicineBrandsDeleteRequest;
use Modules\MedicineBrands\Repositories\MedicineBrandsRepositoryInterface as MedicineBrandsRepository;
use Illuminate\Support\Facades\Storage;
use Modules\Poll\Models\Poll;
use App\Repositories\User\UserRepositoryInterface as UserRepository;

class MedicineBrandsController extends Controller
{
    public function list(MedicineBrandsRepository $medicinebrandRepo)
    {  
        $medicineBrands = $medicinebrandRepo->getForDatatable();    
        return view('medicinebrands::listMedicineBrands', compact('medicineBrands'));                         
    }

    public function add(MedicineBrandsAddRequest $request , MedicineBrandsRepository $medicinebrandRepo)
    {    
        return view('medicinebrands::addMedicineBrands');        
    }

    public function save(MedicineBrandsSaveRequest $request, MedicineBrandsRepository $medicinebrandRepo)
    {   
        $imageUrl=$request->hasFile('image')?Storage::disk('public')->putFile('medicine_brand_image/',$request->file('image')):"";
        $logoUrl=$request->hasFile('logo')?Storage::disk('public')->putFile('medicine_logo_image/',$request->file('logo')):"";

        $inputData = [
            'name'  => $request->name,
            'description'  => $request->description,
            'image'  => $imageUrl,
            'logo'  => $logoUrl,
            'added_by'     => auth()->user()->id,
        ];
        $medicinebrand = $medicinebrandRepo->save($inputData); 
        $poll         = new Poll;
        $poll->value  = '1';
        $medicinebrand->polls()->save($poll);
            connectify('success', 'Success!', 'MedicineBrand added successfully');        
            return redirect()
            ->route('admin.medicinebrands.list');
    } 


    public function view(MedicineBrandsViewRequest $request,MedicineBrandsRepository $medicinebrandRepo,UserRepository $userRepo)
    { 
        $medicineBrands = $medicinebrandRepo->get($request->id);   
        $addedBy = $userRepo->get($medicineBrands->added_by); 
        return view('medicinebrands::viewMedicineBrands', compact('medicineBrands','addedBy'));
    }

    public function edit(MedicineBrandsEditRequest $request, MedicineBrandsRepository $medicinebrandRepo)
    {  
        $medicineBrands =$medicinebrandRepo->get($request->id);   
        return view('medicinebrands::editMedicineBrands', compact('medicineBrands'));
    }


    public function update(MedicineBrandsUpdateRequest $request, MedicineBrandsRepository $medicinebrandRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'description'  => $request->description,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }


        if ($request->hasFile('image')) { 
            $this->_removeFile($medicinebrandRepo, $request->id);                 
            $imageUrl = Storage::disk('public')->putFile('medicine_brand_image/',$request->file('image'));
            $inputData['image'] = $imageUrl;
        } 
        
        if ($request->hasFile('logo')) { 
            $this->_removeLogo($medicinebrandRepo, $request->id);                 
            $logoUrl = Storage::disk('public')->putFile('medicine_logo_image/',$request->file('logo'));
            $inputData['logo'] = $logoUrl;
        } 

        $poll     = new Poll;
        // $hospital = $hosptlRepo->update($inputData);
        $medicineBrands = $medicinebrandRepo->update($inputData);


        $poll->pollable()->associate($medicineBrands);
        $medicineBrands->polls()->update(['value' => $request->value]);

        connectify('success', 'Success!', 'MedicineBrand updated successfully');        
        return redirect()
            ->route('admin.medicinebrands.list');
    }  

    public function removeFile(MedicineBrandsRepository $medicinebrandRepo,Request $request)
    {   
        if ($this->_removeFile($medicinebrandRepo, $request->id)) {
            $medicinebrandRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly');        
        return redirect()
            ->route('admin.medicinebrands.edit', ['id' => $request->id]);
    }

    private function _removeFile($medicinebrandRepo, $id)
    { 
        $logo = $medicinebrandRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }

    public function removeLogo(MedicineBrandsRepository $medicinebrandRepo,Request $request)
    {   
        if ($this->_removeLogo($medicinebrandRepo, $request->id)) {
            $medicinebrandRepo->resetLogo($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly');        
        return redirect()
            ->route('admin.medicinebrands.edit', ['id' => $request->id]);
    }

    private function _removeLogo($medicinebrandRepo, $id)
    { 
        $logo = $medicinebrandRepo->get($id);
        if (Storage::disk('public')->delete($logo->logo)) {
            return true;
        }
        return false;
    }

    public function delete(MedicineBrandsRepository $medicinebrandRepo, MedicineBrandsDeleteRequest $request)
    { 
        if ($medicinebrandRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Medicine Brand deleted successfuly');        
            return redirect()
            ->route('admin.medicinebrands.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}
