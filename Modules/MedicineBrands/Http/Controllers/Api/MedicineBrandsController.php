<?php

namespace Modules\MedicineBrands\Http\Controllers\Api;
use Illuminate\Routing\Controller; 
use Modules\MedicineBrands\Repositories\MedicineBrandsRepositoryInterface as MedicineBrandsRepository;
use Carbon\Exceptions\Exception;
use Modules\MedicineBrands\Models\MedicineBrands;
use Modules\Enquiry\Models\Enquiry;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
class MedicineBrandsController extends Controller
{
    public function topmedicineBrands(MedicineBrandsRepository $medicinebrandRepo)
    {          
        try {
                $topBrands = $medicinebrandRepo->topMedicineBrands();
                if (!$topBrands) {
                    $response = ['status' => false, 'message' => 'Medicine brands are not available'];
                    return response()->json($response, 200);
                }
                $data = compact("topBrands");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
           
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function topBrandEnquiry(EnquiryAddRequest $request,Enquiry $enquiry)
    {   
        try {
            $medicineBrand = MedicineBrands::find($request->medicine_brand_id);
             if($medicineBrand != null){
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->medicine_brand_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $medicineBrand->enquiries()->save($enquiry); 

                $response = ['status' => true,'message' => 'MedicineBrand Enquiry Added Successfully'];
                return response()->json($response, 200);
            }        
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}
