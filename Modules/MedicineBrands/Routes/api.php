<?php

use Illuminate\Http\Request;
use Modules\MedicineBrands\Http\Controllers\Api\MedicineBrandsController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/medicinebrands', function (Request $request) {
    return $request->user();
});

Route::prefix('medicinebrands')->group(function () {
    Route::get('/topmedicineBrands', [MedicineBrandsController::class, 'topmedicineBrands']);
    Route::post('/topBrandEnquiry', [MedicineBrandsController::class, 'topBrandEnquiry']);

});