<?php
use Modules\MedicineBrands\Http\Controllers\MedicineBrandsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('medicinebrands')->group(function() {
    Route::get('/', 'MedicineBrandsController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'medicinebrands', 'as' => 'medicinebrands.'], function () {         
            Route::get('/', [MedicineBrandsController::class, 'list'])->name('list');
            Route::get('add', [MedicineBrandsController::class, 'add'])->name('add');
            Route::post('save', [MedicineBrandsController::class, 'save'])->name('save');
            Route::get('edit', [MedicineBrandsController::class, 'edit'])->name('edit');
            Route::post('update', [MedicineBrandsController::class, 'update'])->name('update');
            Route::get('view', [MedicineBrandsController::class, 'view'])->name('view');
            Route::get('delete', [MedicineBrandsController::class, 'delete'])->name('delete');
            Route::get('remove_file', [MedicineBrandsController::class, 'removeFile'])
            ->name('remove_file'); 
            Route::get('remove_logo', [MedicineBrandsController::class, 'removeLogo'])
            ->name('remove_logo');
    });
});
