<?php

namespace Modules\MedicineBrands\Repositories;

interface MedicineBrandsRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($Id);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);

    public function resetLogo(string $id);

    public function topMedicineBrands();

}