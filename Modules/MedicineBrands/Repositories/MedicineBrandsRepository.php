<?php


namespace Modules\MedicineBrands\Repositories;
use Modules\MedicineBrands\Models\MedicineBrands;
use Illuminate\Database\Eloquent\Builder;

class MedicineBrandsRepository implements MedicineBrandsRepositoryInterface
{
    public function getForDatatable()
    { 
        return MedicineBrands::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return MedicineBrands::where('status',1)->orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($medicineBrands =  MedicineBrands::create($input)) {
            return $medicineBrands;
        }
        return false;
    }

    public function get($Id)
    {
        return MedicineBrands::where('id',$Id)->first();
    }

    public function update(array $input)
    {
        $medicineBrands = MedicineBrands::find($input['id']); 
        unset($input['id']);
        if ($medicineBrands->update($input)) {
            return $medicineBrands;
        }
        return false;
    }

    public function delete(string $id)
    {
        $medicineBrands = MedicineBrands::find($id);
        return $medicineBrands->delete();
    }

    public function resetFile(string $id)
    {
        $medicineBrands = MedicineBrands::find($id);
        $medicineBrands->image = '';
        return $medicineBrands->save();
    }

    public function resetLogo(string $id)
    {
        $medicineBrands = MedicineBrands::find($id);
        $medicineBrands->logo = '';
        return $medicineBrands->save();
    }

    public function topMedicineBrands()
    { 
        $medicineBrands = MedicineBrands::select('medicine_brands.*')
                    ->where('medicine_brands.status',1)
                    ->join('polls', 'polls.pollable_id', '=', 'medicine_brands.id')
                    ->where('polls.pollable_type','Modules\MedicineBrands\Models\MedicineBrands')
                    ->orderBy('polls.value','DESC')
                    ->get();
        return $medicineBrands;
    }
}