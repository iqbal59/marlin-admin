<?php

namespace Modules\MedicineBrands\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Poll\Models\Poll;
use Modules\Enquiry\Models\Enquiry;
class MedicineBrands extends Model
{
    use HasFactory;
    protected $table = 'medicine_brands';

    protected $fillable = ['name', 'description','logo','image','status','added_by'];

    public function polls()
    {
        return $this->morphMany(Poll::class, 'pollable');
    }

    public function enquiries()
    { 
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

    public function medicines()
    {
        return $this->hasMany('Modules\Medicines\Models\Medicines');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->medicines->count()
        ) {
            return false;
        }
        return true;
    }

    // public function getCanDeleteAttribute()
    // {
    //     if (
    //         $this->medicines->count()
    //     ) {
    //         return false;
    //     }
    //     return true;
    // }

  
}