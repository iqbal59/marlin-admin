<?php

namespace Modules\MedicineBrands\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicineBrands extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description','logo','image','status','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\MedicineBrands\Database\factories\MedicineBrandsFactory::new();
    }
}
