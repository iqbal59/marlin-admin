@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>
    
@foreach($medicineBrands->polls as $poll)
  @php  $pollValue =  $poll['value']; @endphp
@endforeach

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} Medicine Brands
    </div>

    <div class="card-body">
        <form action="{{ route("admin.medicinebrands.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$medicineBrands->id}}">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($medicineBrands) ? $medicineBrands->name : '') }}"  >
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($medicineBrands) ? $medicineBrands->description : '') !!}
                </textarea>   
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="row">
                <div class="col-md-6">
            
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image"> {{ trans('panel.image') }} </label>                
                        @if(Storage::disk('public')->exists($medicineBrands->image)) 
                            <img class="preview" src="{{asset('storage/'.$medicineBrands->image)}}" alt="" width="150px">
                                <a href="{{route('admin.medicinebrands.remove_file',['id'=>$medicineBrands->id])}}" class="confirm-delete text-danger">
                                    <i class="material-icons pink-text">clear</i>
                                </a>
                        @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                            <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                        </div>  
                        @endif
                    </div>    
                </div>  
                <div class="col-md-6">
            
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                        <label for="logo"> Logo </label>                
                        @if(Storage::disk('public')->exists($medicineBrands->logo)) 
                            <img class="preview" src="{{asset('storage/'.$medicineBrands->logo)}}" alt="" width="150px">
                                <a href="{{route('admin.medicinebrands.remove_logo',['id'=>$medicineBrands->id])}}" class="confirm-delete text-danger">
                                    <i class="material-icons pink-text">clear</i>
                                </a>
                        @else
                        <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">                
                            <input type="file" name="logo" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('logo'))
                            <em class="invalid-feedback">
                                {{ $errors->first('logo') }}
                            </em>
                        @endif
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                        </div>  
                        @endif
                    </div>  
                </div>
            </div>  
            <div class="row">
                <div class="col-md-6 badge-contr">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" {{ old('status', $medicineBrands->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                            <span class="off">No</span>
                            <label class="custom-control-label" for="status"></label>
                            <span class="on">Yes</span>
                        </div>
                    </div> 
                </div> 
                <div class="col-md-6">
                    <label for="Poll">Poll</label>
                    <div class="form-group">
                        <input type="number" id="poll" name="value" min="1" max="99" value="{{ old('value', $pollValue ) }}">
                    </div>
                </div>
            </div>
  
           
            <br>         
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection