@extends('layouts.admin')
@section('content')
<style>
        #pills-tabContent .table th, .table td {
        white-space: normal;
        line-height: 24px;
    }
    </style>
<div class="card">
    <div class="card-header">
       View Medicine Brand 
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $medicineBrands->name }}
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {!! $medicineBrands->description !!} 
                        </td>
                    </tr>

                    @if($medicineBrands->image !='')
                    <tr>
                        <th>Image</th>
                            <td>             
                                <img src="{{asset('storage/'.$medicineBrands->image)}}" alt="" width="500px">
                            </td>
                    </tr>  
                    @endif

                    @if($medicineBrands->logo !='')
                    <tr>
                        <th>Logo</th>
                            <td>             
                                <img src="{{asset('storage/'.$medicineBrands->logo)}}" alt="" width="500px">
                            </td>
                    </tr>  
                    @endif
                    @if($addedBy!= "")
                    <tr>
                        <th>Added By</th>
                        <td>{{ $addedBy->first_name }}</td>
                    </tr> 
                    @endif   
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection