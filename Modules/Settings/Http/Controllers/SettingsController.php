<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Settings\Http\Requests\SettingsViewRequest;
use Modules\Settings\Http\Requests\SettingsUpdateRequest;
use Modules\Settings\Http\Requests\SettingsRemoveFileRequest;

use Modules\Settings\Repositories\SettingsRepositoryInterface as SettingsRepository;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    public function view(SettingsViewRequest $request,SettingsRepository $settingsRepo)
    { 
        $settings = $settingsRepo->getAll()->keyBy('key'); 
        $selectedValue = json_decode($settings['meta_tags']->value);     
        return view('settings::viewSettings', compact('settings','selectedValue'));     
    }

    public function save(SettingsUpdateRequest $request, SettingsRepository $settingsRepo)
    {    
        $inputData = [
            'company_name' => $request->company_name,
            'company_description' => $request->company_description,
            'meta_tags' =>json_encode($request->meta_tags),
            'signature' => $request->signature,
            'site_email' => $request->site_email,
            'site_phone' => $request->site_phone,
            'site_copyright' => $request->site_copyright,
            'site_24_support' => $request->site_24_support,
            'site_appoinment' =>$request->site_appoinment,
            'facebook' =>$request->facebook,
            'instagram' =>$request->instagram,
            'youtube' =>$request->youtube,
            'linkedin' =>$request->linkedin,
            'twitter' =>$request->twitter,
            'countries_count' =>$request->countries_count,
            'doctors_count' =>$request->doctors_count,
            'patients_count' =>$request->patients_count,
            'map_url' =>$request->map_url,
            'lead_generated' =>$request->lead_generated,
            'countries_reached_digitally' =>$request->countries_reached_digitally,
            'healthcare_provider' =>$request->healthcare_provider,
            'top_medical_destination' =>$request->top_medical_destination,
            'unique_website_users' =>$request->unique_website_users,

        ];

        if ($request->hasFile('light_logo')) {
            $logo = $request->file('light_logo');
            $logoName = 'light_logo.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app'; 
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['light_logo'] = $file;
        }

        if ($request->hasFile('dark_logo')) {
            $logo = $request->file('dark_logo');
            $logoName = 'dark_logo.' . $logo->getClientOriginalExtension();  
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['dark_logo'] = $file;
        }

        if ($request->hasFile('brochure')) {
            $logo = $request->file('brochure');
            $logoName = 'brochure.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['brochure'] = $file;
        }

        if ($request->hasFile('videos_waterMark')) {
            $logo = $request->file('videos_waterMark');
            $logoName = 'videos_waterMark.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['videos_waterMark'] = $file;
        }

        if ($request->hasFile('login_page-background')) {
            $logo = $request->file('login_page-background');
            $logoName = 'login_page-background.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['login_page-background'] = $file;
        }

        if ($request->hasFile('register_page-background')) {
            $logo = $request->file('register_page-background');
            $logoName = 'register_page-background.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['register_page-background'] = $file;
        }

        if ($request->hasFile('default_user_avatar')) {
            $logo = $request->file('default_user_avatar');
            $logoName = 'default_user_avatar.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['default_user_avatar'] = $file;
        }

        if ($request->hasFile('default_user_cover')) {
            $logo = $request->file('default_user_cover');
            $logoName = 'default_user_cover.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['default_user_cover'] = $file;
        }

        if ($request->hasFile('fav_icon')) {
            $logo = $request->file('fav_icon');
            $logoName = 'fav_icon.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['fav_icon'] = $file;
        }

        if ($request->hasFile('default_banner_image')) {
            $logo = $request->file('default_banner_image');
            $logoName = 'default_banner_image.' . $logo->getClientOriginalExtension(); 
            $logoPath = 'app';
            $file = Storage::disk('public')->putFileAs($logoPath, $logo, $logoName);
            $inputData['default_banner_image'] = $file;
        }


        $settingsRepo->update($inputData);
        connectify('success', 'Success!', 'Settings updated successfully');               
        return redirect()
            ->route('admin.settings.view');
    }

    // public function removeLightLogo(SettingsRemoveFileRequest $request, SettingsRepository $settingsRepo)
    // { 
    //     $logo = $settingsRepo->getByKey('light_logo'); 
    //     if (Storage::disk('public')->delete($logo->value)) {
    //         $settingsRepo->resetValue($logo);
    //     }
    //     connectify('success', 'Success!', 'Light logo deleted successfully');               
    //     return redirect()
    //         ->route('admin.settings.view');
    // }
    
    // public function removeDarkLogo(SettingsRemoveFileRequest $request, SettingsRepository $settingsRepo)
    // { 
    //     $logo = $settingsRepo->getByKey('dark_logo'); 
    //     if (Storage::disk('public')->delete($logo->value)) {
    //         $settingsRepo->resetValue($logo);
    //     }
    //     connectify('success', 'Success!', 'Dark logo deleted successfully');               
    //     return redirect()
    //         ->route('admin.settings.view');
    // }
     
    // public function removeBrochureLogo(SettingsRemoveFileRequest $request, SettingsRepository $settingsRepo)
    // { 
    //     $logo = $settingsRepo->getByKey('brochure'); 
    //     if (Storage::disk('public')->delete($logo->value)) {
    //         $settingsRepo->resetValue($logo);
    //     }
    //     connectify('success', 'Success!', 'Brochure logo deleted successfully');               
    //     return redirect()
    //         ->route('admin.settings.view');
    // }

    public function removeImage(SettingsRemoveFileRequest $request, SettingsRepository $settingsRepo)
    { 
        $logo = $settingsRepo->getByKey($request->key);    
        if (Storage::disk('public')->delete($logo->value) == true) {
            $settingsRepo->resetValue($logo);
        }
        connectify('success', 'Success!', 'Image deleted successfully');               
        return redirect()
            ->route('admin.settings.view');
    }

    
}
