<?php

namespace Modules\Settings\Http\Controllers\Api; 
use Illuminate\Routing\Controller;
use Modules\Settings\Repositories\SettingsRepositoryInterface as SettingsRepository;
use Carbon\Exceptions\Exception;

class SettingsController extends Controller
{
    public function contactUs(SettingsRepository $settingsRepo)
    { 
        try {
            $settings = config('settings');    
            $email = $settings['site_email'];
            $customerSupport = $settings['site_24_support'];
            $siteAppointment= $settings['site_appoinment'];
            $data = compact("email","customerSupport","siteAppointment");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    } 
    public function listAllSettings(SettingsRepository $settingsRepo)
    {  
        try {
            $settings = config('settings');    
            $data = compact("settings");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }    
}
