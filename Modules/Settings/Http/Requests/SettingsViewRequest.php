<?php

namespace Modules\Settings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('settings_read') ? true : false;
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
