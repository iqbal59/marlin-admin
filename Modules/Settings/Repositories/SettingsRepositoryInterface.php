<?php

namespace Modules\Settings\Repositories;

interface SettingsRepositoryInterface
{
    public function getAll();

    public function settingsIn($keys);

    public function update(array $input);

    public function getByKey($key);

    public function resetValue($setting);

    public function autoLoad();
}