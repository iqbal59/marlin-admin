<?php


namespace Modules\Settings\Repositories;
use Modules\Settings\Models\Settings;
use Illuminate\Database\Eloquent\Builder;

class SettingsRepository implements SettingsRepositoryInterface
{

    public function getAll()
    {
        $settings = Settings::get();
        return $settings;
    }

    public function settingsIn($keys)
    {
        return Settings::whereIn('key', $keys)->get();
    }

    public function update(array $input)
    {
        foreach ($input as $key => $value) {
            $setting = Settings::where('key', $key)->first();
            if (!empty($setting)) {
                $setting->value = $value;
                $setting->save();
                cache()->forget('settings'); //To reflect the settings immediately
            }
        }
        return $this->settingsIn(array_keys($input));
    }


    public function getByKey($key)
    { 
        return Settings::where('key', $key)->first();
    }

    public function resetValue($setting)
    {
        if (is_string($setting)) {
            $setting = Settings::where('key', $setting)->first();
        }
        if (!empty($setting)) {
            $setting->value = '';
            $setting->save();
            cache()->forget('settings'); //To reflect the settings immediately
        }
    }

    public function autoLoad()
    { 
        $settings = Settings::get();
        return $settings;
    }

}