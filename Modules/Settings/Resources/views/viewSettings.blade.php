@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
       {{ trans('panel.settings') }} 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.settings.save") }}" method="POST" enctype="multipart/form-data">
            @csrf        
        <div class="row">
            <div class="col-md-4">
            <div class="file-field input-field">
                <label>@lang('Light Logo')</label>
                @if(Storage::disk('public')->exists($settings->get('light_logo')->value)) 
                <div class="row">
                    <div class="col s12">
                        <img class="preview" src="{{asset('storage/'.$settings->get('light_logo')->value)}}" alt="" style="width:150px">
                        <a href="{{route('admin.settings.removeImage',["key" => "light_logo"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                    </div>
                </div>
                @else
                <div class="btn">
                    <input type="file" id="light_logo" name="light_logo">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif
                @error('light_logo')<small class="form-text text-danger">{{ $message }}</small>@enderror
            </div>
            </div>

            <div class="col-md-4 file-field input-field">
               <label>@lang('Dark Logo')</label>
                @if(Storage::disk('public')->exists($settings->get('dark_logo')->value)) 
                <div class="row">
                    <div class="col s12">
                        <img class="preview" src="{{asset('storage/'.$settings->get('dark_logo')->value)}}" alt="" style="width:150px">
                        <a href="{{route('admin.settings.removeImage',["key" => "dark_logo"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                    </div>
                </div>
                @else
                <div class="btn">
                    <input type="file" id="dark_logo" name="dark_logo">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif
                @error('dark_logo')<small class="form-text text-danger">{{ $message }}</small>@enderror
            </div>
            <div class="col-md-4 file-field input-field"> 
              <label>@lang('Brochure')</label>
                @if(Storage::disk('public')->exists($settings->get('brochure')->value))  
                <div class="row"> 
                    <div class="col s12">
                        <img class="preview" src="{{asset('storage/'.$settings->get('brochure')->value)}}" alt="" style="width:150px">
                        <a href="{{route('admin.settings.removeImage',["key" => "brochure"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                    </div>
                </div>
                @else
                <div class="btn">
                    <input type="file" id="brochure" name="brochure">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
                @endif
                @error('brochure')<small class="form-text text-danger">{{ $message }}</small>@enderror
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                    <label for="company_name">{{ trans('panel.company_name') }} </label>
                    <input type="text" value="{{ old('company_name',$settings->get('company_name')->value) }}" name="company_name" id="company_name" class="form-control">
                    @if($errors->has('company_name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('company_name') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div> 
        </div>

        <div class="form-group {{ $errors->has('company_description') ? 'has-error' : '' }}">
            <label for="company_description">{{ trans('panel.company_description') }} </label>
            <textarea class="ckeditor form-control" id="company_description" name="company_description" required>
            {!! old('company_description',$settings->get('company_description')->value) !!}
            </textarea>      

            @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
            @if($errors->has('company_description'))
                <em class="invalid-feedback">
                    {{ $errors->first('company_description') }}
                </em>
            @endif
        </div>
        
        <div class="form-group {{ $errors->has('meta_tags') ? 'has-error' : '' }}">
            <label for="meta_tags">{{ trans('panel.meta_tags') }} </label>
            @if($selectedValue == null)
                <select id="meta_tags" name="meta_tags[]" multiple>
                </select>    
            @else 
                <select id="meta_tags" name="meta_tags[]" multiple > 
                @foreach($selectedValue as $data)  
                <option value="{{$data}}"selected>{{$data}}</option>
                @endforeach
                </select>                
            @endif
        </div> 

        <div class="form-group {{ $errors->has('signature') ? 'has-error' : '' }}">
            <label for="signature">{{ trans('panel.signature') }} </label>
            <textarea class="ckeditor form-control" id="signature" name="signature" required>
            {!! old('signature',$settings->get('signature')->value) !!}
            </textarea>      

            @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
            @if($errors->has('signature'))
                <em class="invalid-feedback">
                    {{ $errors->first('signature') }}
                </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site_email') ? 'has-error' : '' }}">
                    <label for="site_email">Site email </label>
                    <input type="email" value="{{ old('site_email',$settings->get('site_email')->value) }}" name="site_email" id="site_email" class="form-control">
                    @if($errors->has('site_email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('site_email') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
            
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site_phone') ? 'has-error' : '' }}">
                    <label for="site_phone">Site Phone </label>
                    <input type="number" value="{{ old('site_phone',$settings->get('site_phone')->value) }}" name="site_phone" id="site_phone" class="form-control">
                    @if($errors->has('site_phone'))
                        <em class="invalid-feedback">
                            {{ $errors->first('site_phone') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site_appoinment') ? 'has-error' : '' }}">
                    <label for="site_appoinment">Site Appoinment </label>
                    <input type="text" value="{{ old('site_appoinment',$settings->get('site_appoinment')->value) }}" name="site_appoinment" id="site_appoinment" class="form-control">
                    @if($errors->has('site_appoinment'))
                        <em class="invalid-feedback">
                            {{ $errors->first('site_appoinment') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site_copyright') ? 'has-error' : '' }}">
                    <label for="site_copyright">Site Copyright </label>
                    <input type="text" value="{{ old('site_copyright',$settings->get('site_copyright')->value) }}" name="site_copyright" id="site_copyright" class="form-control">
                    @if($errors->has('site_copyright'))
                        <em class="invalid-feedback">
                            {{ $errors->first('site_copyright') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('site_24_support') ? 'has-error' : '' }}">
                    <label for="site_24_support">Site 24 Support </label>
                    <input type="text" value="{{ old('site_24_support',$settings->get('site_24_support')->value) }}" name="site_24_support" id="site_24_support" class="form-control">
                    @if($errors->has('site_24_support'))
                        <em class="invalid-feedback">
                            {{ $errors->first('site_24_support') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('facebook') ? 'has-error' : '' }}">
                    <label for="facebook">Facebook </label>
                    <input type="text" value="{{ old('facebook',$settings->get('facebook')->value) }}" name="facebook" id="facebook" class="form-control">
                    @if($errors->has('facebook'))
                        <em class="invalid-feedback">
                            {{ $errors->first('facebook') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('instagram') ? 'has-error' : '' }}">
                    <label for="instagram">Instagram </label>
                    <input type="text" value="{{ old('instagram',$settings->get('instagram')->value) }}" name="instagram" id="instagram" class="form-control">
                    @if($errors->has('instagram'))
                        <em class="invalid-feedback">
                            {{ $errors->first('instagram') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>

            <div class="col-md-6">
                <div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
                    <label for="youtube">Youtube </label>
                    <input type="text" value="{{ old('youtube',$settings->get('youtube')->value) }}" name="youtube" id="youtube" class="form-control">
                    @if($errors->has('youtube'))
                        <em class="invalid-feedback">
                            {{ $errors->first('youtube') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('linkedin') ? 'has-error' : '' }}">
                    <label for="linkedin">Linkedin </label>
                    <input type="text" value="{{ old('linkedin',$settings->get('linkedin')->value) }}" name="linkedin" id="linkedin" class="form-control">
                    @if($errors->has('linkedin'))
                        <em class="invalid-feedback">
                            {{ $errors->first('linkedin') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('twitter') ? 'has-error' : '' }}">
                    <label for="twitter">Twitter </label>
                    <input type="text" value="{{ old('twitter',$settings->get('twitter')->value) }}" name="twitter" id="twitter" class="form-control">
                    @if($errors->has('twitter'))
                        <em class="invalid-feedback">
                            {{ $errors->first('twitter') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Videos WaterMark')</label>
                    @if(Storage::disk('public')->exists($settings->get('videos_waterMark')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('videos_waterMark')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "videos_waterMark"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="videos_waterMark" name="videos_waterMark">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('videos_waterMark')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div> 
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Login Page Background')</label>
                    @if(Storage::disk('public')->exists($settings->get('login_page-background')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('login_page-background')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "login_page-background"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="login_page-background" name="login_page-background">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('login_page-background')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div> 
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Register Page Background')</label>
                    @if(Storage::disk('public')->exists($settings->get('register_page-background')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('register_page-background')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "register_page-background"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="register_page-background" name="register_page-background">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('register_page-background')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div>   
       

        
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Default User Avatar')</label>
                    @if(Storage::disk('public')->exists($settings->get('default_user_avatar')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('default_user_avatar')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "default_user_avatar"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="default_user_avatar" name="default_user_avatar">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('default_user_avatar')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div> 
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Default User Cover')</label>
                    @if(Storage::disk('public')->exists($settings->get('default_user_cover')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('default_user_cover')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "default_user_cover"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="default_user_cover" name="default_user_cover">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('default_user_cover')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div> 
            <div class="col-md-4">
                <div class="file-field input-field">
                    <label>@lang('Fav Icon')</label>
                    @if(Storage::disk('public')->exists($settings->get('fav_icon')->value)) 
                    <div class="row">
                        <div class="col s12">
                            <img class="preview" src="{{asset('storage/'.$settings->get('fav_icon')->value)}}" alt="" style="width:150px">
                            <a href="{{route('admin.settings.removeImage',["key" => "fav_icon"] )}}" class="confirm-delete text-danger"><i class="fas fa-times"></i></a> 
                        </div>
                    </div>
                    @else
                    <div class="btn">
                        <input type="file" id="fav_icon" name="fav_icon">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                    @endif
                    @error('fav_icon')<small class="form-text text-danger">{{ $message }}</small>@enderror
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('countries_count') ? 'has-error' : '' }}">
                    <label for="countries_count">Countries Count </label>
                    <input type="text" value="{{ old('countries_count',$settings->get('countries_count')->value) }}" name="countries_count" id="countries_count" class="form-control">
                    @if($errors->has('countries_count'))
                    <em class="invalid-feedback">
                        {{ $errors->first('countries_count') }}
                    </em>
                    @endif
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('doctors_count') ? 'has-error' : '' }}">
                    <label for="doctors_count">Doctors Count </label>
                    <input type="text" value="{{ old('doctors_count',$settings->get('doctors_count')->value) }}" name="doctors_count" id="doctors_count" class="form-control">
                    @if($errors->has('doctors_count'))
                    <em class="invalid-feedback">
                        {{ $errors->first('doctors_count') }}
                    </em>
                    @endif
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('patients_count') ? 'has-error' : '' }}">
                    <label for="patients_count">Patients Count </label>
                    <input type="text" value="{{ old('patients_count',$settings->get('patients_count')->value) }}" name="patients_count" id="patients_count" class="form-control">
                    @if($errors->has('patients_count'))
                    <em class="invalid-feedback">
                        {{ $errors->first('patients_count') }}
                    </em>
                    @endif
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('map_url') ? 'has-error' : '' }}">
                    <label for="map_url">Map Url</label>
                    <input type="url" value="{{ old('map_url',$settings->get('map_url')->value) }}" name="map_url" id="map_url" class="form-control">
                    @if($errors->has('map_url'))
                    <em class="invalid-feedback">
                        {{ $errors->first('map_url') }}
                    </em>
                    @endif
                </div> 
            </div>
        </div>
        <br>
        <label for="map_url"><u>Some Numbers/Statistics</u></label>
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('lead_generated') ? 'has-error' : '' }}">
                    <label for="lead_generated">Leads Generated per Month </label>
                    <input type="text" value="{{ old('lead_generated',$settings->get('lead_generated')->value) }}" name="lead_generated" id="lead_generated" class="form-control">
                    @if($errors->has('lead_generated'))
                    <em class="invalid-feedback">
                        {{ $errors->first('lead_generated') }}
                    </em>
                    @endif
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('countries_reached_digitally') ? 'has-error' : '' }}">
                    <label for="countries_reached_digitally">Countries Reached Digitally </label>
                    <input type="text" value="{{ old('countries_reached_digitally',$settings->get('countries_reached_digitally')->value) }}" name="countries_reached_digitally" id="countries_reached_digitally" class="form-control">
                    @if($errors->has('countries_reached_digitally'))
                    <em class="invalid-feedback">
                        {{ $errors->first('countries_reached_digitally') }}
                    </em>
                    @endif
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('healthcare_provider') ? 'has-error' : '' }}">
                    <label for="healthcare_provider">Healthcare providers on panel </label>
                    <input type="text" value="{{ old('healthcare_provider',$settings->get('healthcare_provider')->value) }}" name="healthcare_provider" id="healthcare_provider" class="form-control">
                    @if($errors->has('healthcare_provider'))
                    <em class="invalid-feedback">
                        {{ $errors->first('healthcare_provider') }}
                    </em>
                    @endif
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('top_medical_destination') ? 'has-error' : '' }}">
                    <label for="top_medical_destination">Top Medical Destination </label>
                    <input type="text" value="{{ old('top_medical_destination',$settings->get('top_medical_destination')->value) }}" name="top_medical_destination" id="top_medical_destination" class="form-control">
                    @if($errors->has('top_medical_destination'))
                    <em class="invalid-feedback">
                        {{ $errors->first('top_medical_destination') }}
                    </em>
                    @endif
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('unique_website_users') ? 'has-error' : '' }}">
                    <label for="unique_website_users">Unique Website Users per month </label>
                    <input type="text" value="{{ old('unique_website_users',$settings->get('unique_website_users')->value) }}" name="unique_website_users" id="unique_website_users" class="form-control">
                    @if($errors->has('unique_website_users'))
                    <em class="invalid-feedback">
                        {{ $errors->first('unique_website_users') }}
                    </em>
                    @endif
                </div> 
            </div>
        </div>
        
        
        <br>
        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
        </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $('#meta_tags').select2({
  tags: true
});
</script>
@endsection