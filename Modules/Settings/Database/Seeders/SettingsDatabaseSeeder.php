<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;
use Modules\Settings\Models\Settings;
// use Modules\Settings\Entities\Settings;


class SettingsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '113',
                'title'      => 'settings_read',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '114',
                'title'      => 'settings_update',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '115',
                'title'      => 'settings_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
        ];
        
        Permission::insert($permissions);

        Settings::truncate();
       
        $settings = [
            [
                'id'         => '1',
                'key'        => 'light_logo',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '2',
                'key'        => 'dark_logo',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '3',
                'key'        => 'company_name',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '4',
                'key'        => 'company_description',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '5',
                'key'        => 'meta_tags',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '6',
                'key'        => 'brochure',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],
            [
                'id'         => '7',
                'key'        => 'signature',
                'created_at' => '2021-10-25 12:14:15',
                'updated_at' => '2021-10-25 12:14:15',
            ],        
            [
                'id'         => '8',
                'key'        => 'site_email',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '9',
                'key'        => 'site_phone',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '10',
                'key'        => 'site_copyright',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '11',
                'key'        => 'site_appoinment',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '12',
                'key'        => 'site_24_support',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '13',
                'key'        => 'currency',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '14',
                'key'        => 'facebook',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '15',
                'key'        => 'instagram',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '16',
                'key'        => 'youtube',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '17',
                'key'        => 'linkedin',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '18',
                'key'        => 'twitter',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '19',
                'key'        => 'videos_waterMark',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '20',
                'key'        => 'login_page-background',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '21',
                'key'        => 'register_page-background',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ],
            [
                'id'         => '22',
                'key'        => 'default_user_avatar',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '23',
                'key'        => 'default_user_cover',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '24',
                'key'        => 'fav_icon',
                'created_at' => '2021-11-13 12:14:15',
                'updated_at' => '2021-11-13 12:14:15',
            ], 
            [
                'id'         => '25',
                'key'        => 'countries_count',
                'created_at' => '2022-01-03 09:14:15',
                'updated_at' => '2021-01-03 09:14:15',
            ], 
            [
                'id'         => '26',
                'key'        => 'doctors_count',
                'created_at' => '2022-01-03 09:14:15',
                'updated_at' => '2022-01-03 09:14:15',
            ], 
            [
                'id'         => '27',
                'key'        => 'patients_count',
                'created_at' => '2022-01-03 09:14:15',
                'updated_at' => '2022-01-03 09:14:15',
            ], 
            [
                'id'         => '28',
                'key'        => 'map_url',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ], 
            [
                'id'         => '29',
                'key'        => 'lead_generated',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ],
            [
                'id'         => '30',
                'key'        => 'countries_reached_digitally',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ], 
            [
                'id'         => '31',
                'key'        => 'healthcare_provider',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ], 
            [
                'id'         => '32',
                'key'        => 'top_medical_destination',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ], 
            [
                'id'         => '33',
                'key'        => 'unique_website_users',
                'created_at' => '2022-01-03 04:14:15',
                'updated_at' => '2022-01-03 04:14:15',
            ], 
            [
                'id'         => '34',
                'key'        => 'default_banner_image',
                'created_at' => '2022-01-21 04:14:15',
                'updated_at' => '2022-01-21 04:14:15',
            ],  
        ];

        Settings::insert($settings);
    }
}
