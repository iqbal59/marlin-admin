<?php
use Modules\Settings\Http\Controllers\SettingsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {    
        Route::get('/', [SettingsController::class, 'view'])->name('view');
        Route::post('save', [SettingsController::class, 'save'])->name('save');
        Route::get('removeImage', [SettingsController::class, 'removeImage'])->name('removeImage');
    });
});