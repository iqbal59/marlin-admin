<?php

use Illuminate\Http\Request;
use Modules\Settings\Http\Controllers\Api\SettingsController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/settings', function (Request $request) {
    return $request->user();
});

Route::prefix('settings')->group(function () {
    Route::get('/contactUs', [SettingsController::class, 'contactUs']);
    Route::get('/listAllSettings', [SettingsController::class, 'listAllSettings']);
});
