<?php

namespace Modules\Problems\Http\Controllers\Includes;
use Modules\Problems\Http\Requests\Problems\ProblemsAddRequest;
use Modules\Problems\Http\Requests\Problems\ProblemsSaveRequest;
use Modules\Problems\Http\Requests\Problems\ProblemsEditRequest;
use Modules\Problems\Http\Requests\Problems\ProblemsUpdateRequest;
use Modules\Problems\Http\Requests\Problems\ProblemsViewRequest;
use Modules\Problems\Http\Requests\Problems\ProblemsDeleteRequest;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Problems\Repositories\Tips\TipsRepositoryInterface as TipsRepository;
use Illuminate\Support\Facades\Auth;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
trait Problems
{
    public function list(ProblemsRepository $problemsRepo)
    {  
        $problems = $problemsRepo->getForDatatable(); 
        return view('problems::problems.listProblems', compact('problems'));                         
    }

    public function add(ProblemsAddRequest $request , ProblemsRepository $problemsRepo)
    {    
        return view('problems::problems.addProblems');        
    }

    public function save(ProblemsSaveRequest $request, ProblemsRepository $problemsRepo)
    {    
        $inputData = [
            'title'  => $request->title,
            'related'  => $request->related,
            'description'  => $request->description,
            'created_by'  =>Auth::user()->id,
            'added_by'     => auth()->user()->id,
        ];
        $problems = $problemsRepo->save($inputData);  
        connectify('success', 'Success!', 'Problems added successfully');        
            return redirect()
            ->route('admin.problems.list');
    } 


    public function view(ProblemsViewRequest $request,ProblemsRepository $problemsRepo, TipsRepository $tipsRepo,UserRepository $userRepo)
    { 
        $problems = $problemsRepo->get($request->id); 
        $tips = $tipsRepo->getTips($request->id); 
        $addedBy = $userRepo->get($problems->added_by);  
        return view('problems::problems.viewProblems', compact('problems','tips','addedBy'));
    }

    public function edit(ProblemsEditRequest $request, ProblemsRepository $problemsRepo)
    {  
        $problems =$problemsRepo->get($request->id);  
        return view('problems::problems.editProblems', compact('problems'));
    }


    public function update(ProblemsUpdateRequest $request, ProblemsRepository $problemsRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'title'  => $request->title,
            'related'  => $request->related,
            'description'  => $request->description,
            'created_by'  => Auth::user()->id,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }

        $problems = $problemsRepo->update($inputData);
        connectify('success', 'Success!', 'Problems updated successfully');        
        return redirect()
            ->route('admin.problems.list');
    }  

    public function delete(ProblemsRepository $problemsRepo, ProblemsDeleteRequest $request)
    { 
        if ($problemsRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Problems deleted successfully');        
            return redirect()
            ->route('admin.problems.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }


    
}