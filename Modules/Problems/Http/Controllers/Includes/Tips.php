<?php

namespace Modules\Problems\Http\Controllers\Includes;
use Modules\Problems\Http\Requests\Tips\TipsAddRequest;
use Modules\Problems\Http\Requests\Tips\TipsSaveRequest;
use Modules\Problems\Http\Requests\Tips\TipsEditRequest;
use Modules\Problems\Http\Requests\Tips\TipsUpdateRequest;
use Modules\Problems\Http\Requests\Tips\TipsViewRequest;
use Modules\Problems\Http\Requests\Tips\TipsDeleteRequest;
use Modules\Problems\Repositories\Tips\TipsRepositoryInterface as TipsRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Illuminate\Http\Client\Request;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
trait Tips
{
    public function addProblemsTips(TipsAddRequest $request , TipsRepository $tipsRepo)
    {    
        $problemsId = $request->problems_id;
        return view('problems::tips.addTips', compact('problemsId'));                         
    }

    public function saveProblemsTips(TipsSaveRequest $request, TipsRepository $tipsRepo)
    {    
        $inputData = [
            'problems_id'  => $request->problemsId,
            'description'  => $request->description,
            'title'        => $request->title,
            'added_by'     => auth()->user()->id,
        ];
        $tips = $tipsRepo->save($inputData);  
        connectify('success', 'Success!', 'Tips added successfully');                   
        $route =redirect()->route('admin.problems.view', ['id' => $request->problemsId]);
        connectify('success', 'Success!', 'Tips added successfully'); 
        return $route;
    }

    public function viewTips(TipsViewRequest $request, TipsRepository $tipsRepo, ProblemsRepository $problemsRepo,UserRepository $userRepo)
    {  
        $tips = $tipsRepo->get($request->id); 
        $problems = $problemsRepo->get($request->problems_id); 
        $addedBy = $userRepo->get($tips->added_by);  
        return view('problems::tips.viewTips', compact('problems','tips','addedBy'));
    }

    public function editTips(TipsEditRequest $request, TipsRepository $tipsRepo)
    {   
        $problemsId = $request->problems_id;
        $tips =$tipsRepo->get($request->id);  
        return view('problems::tips.editTips', compact('tips','problemsId'));
    }

    public function updateTips(TipsUpdateRequest $request, TipsRepository $tipsRepo)
    {    
        $inputData = [
            'id'  => $request->id,
            'description'  => $request->description,
        ];
        $tips = $tipsRepo->update($inputData);
        $route =redirect()->route('admin.problems.view', ['id' => $request->problemsId]);
        connectify('success', 'Success!', 'Tips updated successfully'); 
        return $route;
    }  

    public function deleteTips(TipsRepository $tipsRepo, TipsDeleteRequest $request)
    {    
        if ($tipsRepo->delete($request->id)) {
            $route =redirect()->route('admin.problems.view', ['id' => $request->problems_id]);
            connectify('success', 'Success!', 'Tips updated successfully'); 
            return $route;
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}