<?php

namespace Modules\Problems\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Problems\Http\Controllers\Includes\Problems;
use Modules\Problems\Http\Controllers\Includes\Tips;

class ProblemsController extends Controller
{
    use Problems;
    use Tips;
}
