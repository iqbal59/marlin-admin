<?php

namespace Modules\Problems\Http\Requests\Problems;

use Illuminate\Foundation\Http\FormRequest;

class ProblemsViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('problems_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
