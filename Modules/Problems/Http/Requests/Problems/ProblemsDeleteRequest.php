<?php

namespace Modules\Problems\Http\Requests\Problems;

use Illuminate\Foundation\Http\FormRequest;

class ProblemsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('problems_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
