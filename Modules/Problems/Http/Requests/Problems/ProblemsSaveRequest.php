<?php

namespace Modules\Problems\Http\Requests\Problems;

use Illuminate\Foundation\Http\FormRequest;

class ProblemsSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('problems_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            // 'related' => 'required',
            // 'description' => 'required',
         ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            // 'related.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
         ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            // 'related' => 'Related',
            // 'description' => 'Description',
         ];
    }
}
