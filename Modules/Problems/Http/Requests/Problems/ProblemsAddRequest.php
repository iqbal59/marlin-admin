<?php

namespace Modules\Problems\Http\Requests\Problems;

use Illuminate\Foundation\Http\FormRequest;

class ProblemsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('problems_create') ? true : false;
    }
    public function rules()
    {
        return [
           
        ];
    }
}
