<?php

namespace Modules\Problems\Http\Requests\Problems;

use Illuminate\Foundation\Http\FormRequest;

class ProblemsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('problems_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
