<?php

namespace Modules\Problems\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('tips_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'description' => 'required',
         ];
    }

    public function messages()
    {
        return [
            'description.required' => ':attribute is required',
         ];
    }

    public function attributes()
    {
        return [
            'description' => 'Description',
         ];
    }
}
