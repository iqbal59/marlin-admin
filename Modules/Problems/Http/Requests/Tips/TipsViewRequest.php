<?php

namespace Modules\Problems\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsViewRequest extends FormRequest
{
    public function rules()
    {
        return [
            //
        ];
    }

    public function authorize()
    {
        return true;
    }
}
