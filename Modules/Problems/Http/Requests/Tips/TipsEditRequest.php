<?php

namespace Modules\Problems\Http\Requests\Tips;

use Illuminate\Foundation\Http\FormRequest;

class TipsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('tips_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
