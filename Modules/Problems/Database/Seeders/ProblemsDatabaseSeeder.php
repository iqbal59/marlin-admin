<?php

namespace Modules\Problems\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class ProblemsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '246',
                'title'      => 'problems_create',
                'created_at' => '2021-11-08 12:14:15',
                'updated_at' => '2021-11-08 12:14:15',
            ],
            [
                'id'         => '247',
                'title'      => 'problems_edit',
                'created_at' => '2021-11-08 12:14:15',
                'updated_at' => '2021-11-08 12:14:15',
            ],
            [
                'id'         => '248',
                'title'      =>  'problems_show',
                'created_at' => '2021-11-08 12:14:15',
                'updated_at' => '2021-11-08 12:14:15',
            ],
            [
                'id'         => '249',
                'title'      => 'problems_delete',
                'created_at' => '2021-11-08 12:14:15',
                'updated_at' => '2021-11-08 12:14:15',
            ],
            [
                'id'         => '250',
                'title'      => 'problems_access',
                'created_at' => '2021-11-08 12:14:15',
                'updated_at' => '2021-11-08 12:14:15',
            ],
            // [
            //     'id'         => '251',
            //     'title'      => 'tips_create',
            //     'created_at' => '2021-11-12 12:14:15',
            //     'updated_at' => '2021-11-12 12:14:15',
            // ],
            // [
            //     'id'         => '252',
            //     'title'      => 'tips_edit',
            //     'created_at' => '2021-11-12 12:14:15',
            //     'updated_at' => '2021-11-12 12:14:15',
            // ],
            // [
            //     'id'         => '253',
            //     'title'      =>  'tips_show',
            //     'created_at' => '2021-11-12 12:14:15',
            //     'updated_at' => '2021-11-12 12:14:15',
            // ],
            // [
            //     'id'         => '254',
            //     'title'      => 'tips_delete',
            //     'created_at' => '2021-11-12 12:14:15',
            //     'updated_at' => '2021-11-12 12:14:15',
            // ],
            // [
            //     'id'         => '255',
            //     'title'      => 'tips_access',
            //     'created_at' => '2021-11-12 12:14:15',
            //     'updated_at' => '2021-11-12 12:14:15',
            // ],

        ];
        
        Permission::insert($permissions);
    }
}
