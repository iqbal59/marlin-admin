<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
  color: orange;
}
#pills-tabContent .table th, .table td {
    white-space: normal;
    line-height: 24px;
}
</style> 
<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $problems->id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ $problems->title }}</td>
            </tr>
            @if($problems->related != null)
            <tr>
                <th>Related </th>
                <td>{{ $problems->related }}</td>
            </tr>
            @endif
            @if($problems->description != null)
            <tr>
                <th>Description</th>
                <td>{!! $problems->description !!} </td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif
        </tbody>
    </table>
</div>