@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th img, .table.video-table td img {
    width: 80px;
    height: 80px;
    border-radius: 10px;
}
.table.video-table th, .table.video-table td {
    line-height: 24px;
    white-space: normal;
}
</style> 
<div class="card">
    <div class="card-header">
       View Tips 
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Problem Name
                        </th>
                        <td>
                            {{ $problems->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                           Title
                        </th>
                        <td>
                            {{ $tips->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Tips Description
                        </th>
                        <td>
                            {!! $tips->description !!}
                        </td>
                    </tr>
                    @if($addedBy!= "")
                    <tr>
                        <th>Added By</th>
                        <td>{{ $addedBy->first_name }}</td>
                    </tr> 
                    @endif
                    
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection