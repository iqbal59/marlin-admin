@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
       Edit Tips
    </div>

    <div class="card-body">
        <form action="{{ route("admin.problemstips.updateTips") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="problemsId" value="{{$problemsId}}">
            <input type="hidden" name="id" value="{{$tips->id}}">

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('panel.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" 
                    name="title" 
                    class="form-control" 
                    value="{{ old('title', isset($tips) ? $tips->title : '') }}" 
                    required
                >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('panel.description') }} <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($tips) ? $tips->description : '') !!}
                </textarea>      
                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                 @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
         
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $(document).ready(function() {
        $("#hospitale_id").select2({
            placeholder: "Select Hospital",
            allowClear: true
        });
    });
</script>
@endsection