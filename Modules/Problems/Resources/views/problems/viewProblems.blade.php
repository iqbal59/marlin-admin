@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
    View Problems
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    {{-- @php  dd($problems); @endphp --}}
    <div class="card-body">
        @include('problems::partials.tabSection')
    </div>
</div>
@endsection