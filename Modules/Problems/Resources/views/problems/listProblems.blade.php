@extends('layouts.admin')
@section('content')
@can('problems_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.problems.add") }}">
                <i class="mdi mdi-plus"></i> 
                Add Problems
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
    List Problems
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"> </th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title</th>
                        <th>Related</th>
                        <th>
                            Status
                       </th>
                        <th>{{ trans('panel.action') }}</th>                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($problems as $key => $problem)
                        <tr data-entry-id="{{ $problem->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $problem->title ?? '' }}</td>
                            <td>{{ $problem->related ?? '' }}</td>
                            <td>
                                @if($problem->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                            @can('problems_show')        
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.problems.view', ['id' => $problem->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan 
                            @can('problems_edit')  
                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.problems.edit', ['id' => $problem->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan 
                            @can('problems_delete')      
                                    <form action="{{ route('admin.problems.delete',  ['id' => $problem->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$problem->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            @endcan 

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})
</script>
@endsection