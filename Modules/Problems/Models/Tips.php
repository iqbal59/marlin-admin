<?php

namespace Modules\Problems\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tips extends Model
{
    use HasFactory;
    protected $table = 'problems_tips';

    protected $fillable = ['id','problems_id','description','title','added_by'];

     

}