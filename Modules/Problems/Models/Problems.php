<?php

namespace Modules\Problems\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Problems extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'problems';

    protected $fillable = ['id','title','slug','related','description','created_by','status','added_by'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    
}