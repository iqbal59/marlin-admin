<?php

namespace Modules\Problems\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tips extends Model
{
    use HasFactory;

    protected $fillable = ['id','problems_id','description','title'];
    
    protected static function newFactory()
    {
        return \Modules\Problems\Database\factories\TipsFactory::new();
    }
}
