<?php

namespace Modules\Problems\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Problems extends Model
{
    use HasFactory;
    use Sluggable;


    protected $fillable = ['id','title','slug','related','description','created_by','status','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\Problems\Database\factories\ProblemsFactory::new();
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
