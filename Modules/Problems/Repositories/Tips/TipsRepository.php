<?php


namespace Modules\Problems\Repositories\Tips;
use Modules\Problems\Models\Tips;
use Illuminate\Database\Eloquent\Builder;

class TipsRepository implements TipsRepositoryInterface
{

    public function getTips($problemsId)
    { 
        return Tips::where('problems_id',$problemsId)->get();
    }

    public function save(array $input)
    { 
        if ($tips =  Tips::create($input)) {
            return $tips;
        }
        return false;
    }

    public function get($Id)
    {
        return Tips::where('id',$Id)->first();
    }

    
    public function update(array $input)
    {
        $tips = Tips::find($input['id']); 
        unset($input['id']);
        if ($tips->update($input)) {
            return $tips;
        }
        return false;
    }

    public function delete(string $id)
    {
        $tips = Tips::find($id);
        return $tips->delete();
    }
    
}