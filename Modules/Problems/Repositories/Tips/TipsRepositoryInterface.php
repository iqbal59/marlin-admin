<?php

namespace Modules\Problems\Repositories\Tips;

interface TipsRepositoryInterface
{
    public function getTips($problemsId);

    public function save(array $input);

    public function get($Id);

    public function update(array $input);

    public function delete(string $id);

}