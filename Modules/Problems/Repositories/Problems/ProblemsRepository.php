<?php


namespace Modules\Problems\Repositories\Problems;
use Modules\Problems\Models\Problems;
use Illuminate\Database\Eloquent\Builder;

class ProblemsRepository implements ProblemsRepositoryInterface
{
    public function getForDatatable()
    { 
        return Problems::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Problems::orderBy('id', 'DESC')->get();
    }

    public function getProblems($slug)
    { 
        return Problems::where('status',1)->where('slug',$slug)->first();
    }

    public function save(array $input)
    { 
        if ($problems =  Problems::create($input)) {
            return $problems;
        }
        return false;
    }

    public function get($Id)
    {
        return Problems::where('id',$Id)->first();
    }

    
    public function update(array $input)
    {
        $problems = Problems::find($input['id']); 
        unset($input['id']);
        if ($problems->update($input)) {
            return $problems;
        }
        return false;
    }

    public function delete(string $id)
    {
        $problems = Problems::find($id);
        return $problems->delete();
    }

    public function searchBasedProblems($searchText)
    {  
        return Problems::where('status',1)->where('title', 'like', '%'.$searchText.'%')->pluck('id')->toArray();
    }

    public function problems($problemsId)
    { 
        return Problems::whereIn('id',$problemsId)->pluck('slug')->toArray();
    }
    
}