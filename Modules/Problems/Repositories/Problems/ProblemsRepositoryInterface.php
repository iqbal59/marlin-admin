<?php

namespace Modules\Problems\Repositories\Problems;

interface ProblemsRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function getProblems($slug);

    public function save(array $input);

    public function get($deptId);

    public function update(array $input);

    public function delete(string $id);

    public function searchBasedProblems($searchText);

    public function problems($diseasesId);

}