<?php
use Modules\Problems\Http\Controllers\ProblemsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'problems', 'as' => 'problems.'], function () {         
        Route::get('/', [ProblemsController::class, 'list'])->name('list');
        Route::get('add', [ProblemsController::class, 'add'])->name('add');
        Route::post('save', [ProblemsController::class, 'save'])->name('save');
        Route::get('edit', [ProblemsController::class, 'edit'])->name('edit');
        Route::post('update', [ProblemsController::class, 'update'])->name('update');
        Route::get('view', [ProblemsController::class, 'view'])->name('view');
        Route::get('delete', [ProblemsController::class, 'delete'])->name('delete');       
    });
    Route::group(['prefix' => 'problemstips', 'as' => 'problemstips.'], function () {         
        Route::get('addProblemsTips', [ProblemsController::class, 'addProblemsTips'])->name('addProblemsTips');
        Route::post('saveProblemsTips', [ProblemsController::class, 'saveProblemsTips'])->name('saveProblemsTips');
        Route::get('editTips', [ProblemsController::class, 'editTips'])->name('editTips');
        Route::post('updateTips', [ProblemsController::class, 'updateTips'])->name('updateTips');
        Route::get('viewTips', [ProblemsController::class, 'viewTips'])->name('viewTips');
        Route::get('deleteTips', [ProblemsController::class, 'deleteTips'])->name('deleteTips');       
    });
});

