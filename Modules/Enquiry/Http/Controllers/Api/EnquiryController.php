<?php

namespace Modules\Enquiry\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Enquiry\Repositories\EnquiryRepositoryInterface as EnquiryRepository;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
use Modules\Enquiry\Http\Requests\Api\AddHospitalEnquiry;
use Modules\Enquiry\Models\Enquiry;
use Carbon\Exceptions\Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class EnquiryController extends Controller
{
    public function commonEnquiry(EnquiryAddRequest $request)
    {
        try {
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id  = rand(10, 100);
            $enquiry->enquiryable_type  = "normal";
            $enquiry->email  = $request->email;
            $enquiry->contact  = $request->contact;
            $enquiry->name  = $request->name;
            $enquiry->message  = $request->message;
            $enquiry->save();

            $response = ['status' => true,'message' => 'Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function contactEnquiry(EnquiryAddRequest $request)
    {
        try {
            $fileUrl=$request->hasFile('file') ? Storage::disk('public')->putFile('contact_api_file/', $request->file('file')) : "";
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id  = rand(10, 100);
            $enquiry->enquiryable_type  = "normal";
            $enquiry->email  = $request->email;
            $enquiry->contact  = $request->contact;
            $enquiry->name  = $request->name;
            $enquiry->message  = $request->message;
            $enquiry->file  = $fileUrl;
            $enquiry->country_code  = $request->country_code;
            $enquiry->diseases_id  = $request->diseases_id;
            $enquiry->save();

            $response = ['status' => true,'message' => 'Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function Mmma_Biz(EnquiryAddRequest $request)
    {
        try {
            $fileUrl=$request->hasFile('file') ? Storage::disk('public')->putFile('MmmaBiz_api_file/', $request->file('file')) : "";
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id  = rand(10, 100);
            $enquiry->enquiryable_type  = "normal";
            $enquiry->email  = $request->email;
            $enquiry->contact  = $request->contact;
            $enquiry->name  = $request->organization;
            $enquiry->location  = $request->location;
            $enquiry->file  = $fileUrl;
            $enquiry->save();

            $response = ['status' => true,'message' => 'Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function addHospitalEnquiry(AddHospitalEnquiry $request)
    {
        try {
            $fileUrl=$request->hasFile('file') ? Storage::disk('public')->putFile('addHospitalEnquiry/', $request->file('file')) : "";
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id  = rand(10, 100);
            $enquiry->enquiryable_type  = "vendor";
            $enquiry->name  = $request->hospital_name;
            $enquiry->contact  = $request->contact;
            $enquiry->location  = $request->location;
            $enquiry->file  = $fileUrl;
            $enquiry->email  = $request->email;
            $enquiry->save();
            $response = ['status' => true,'message' => 'Hospital Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function addDiseaseEnquiry(Request $request)
    {
        try {
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id     = rand(10, 100);
            $enquiry->enquiryable_type   = "disease";
            $enquiry->name               = $request->patient_name;
            $enquiry->contact            = $request->contact;
            $enquiry->location           = $request->location;
            $enquiry->save();
            $response                    = ['status' => true,'message' => 'Disease Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response                    = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function addBookingEnquiry(Request $request)
    {
        try {
            $enquiry = new Enquiry();
            $enquiry->enquiryable_id    = rand(10, 100);
            $enquiry->enquiryable_type  = "booking";
            $enquiry->name              = $request->patient_name;
            $enquiry->contact           = $request->contact;
            $enquiry->email             = $request->email;
            $enquiry->diseases_id       = $request->diseases_id;
            $enquiry->save();
            $response                   = ['status' => true,'message' => 'Booking Enquiry Added Successfully'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response                   = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}