<?php

namespace Modules\Enquiry\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Enquiry\Http\Requests\EnquiryViewRequest;
use Modules\Enquiry\Http\Requests\EnquiryDeleteRequest;
use Modules\Enquiry\Repositories\EnquiryRepositoryInterface as EnquiryRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;

class EnquiryController extends Controller
{
    public function list(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->all();
        return view('enquiry::listEnquiry', compact('enquiries'));
    }

    public function HospitalEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getHospitalEnquiry();
        return view('enquiry::listHospitalEnquiry', compact('enquiries'));
    }

    public function HospitalEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, HospitalRepository $hospitalRepo, DiseasesRepository $diseasesRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        $diseases = $diseasesRepo->get($enquires->diseases_id);
        $hospitalDetails = $hospitalRepo->get($enquires->enquiryable_id);
        return view('enquiry::viewHospitalEnquiry', compact('enquires', 'hospitalDetails', 'diseases'));
    }

    public function DoctorEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getDoctorEnquiry();
        return view('enquiry::listDoctorEnquiry', compact('enquiries'));
    }

    public function DoctorEnquiryview(DoctorRepository $doctorRepo, EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, TreatmentRepository $treatmentRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        $treatments = $treatmentRepo->get($enquires->reason);
        $doctorDetails = $doctorRepo->get($enquires->enquiryable_id);
        return view('enquiry::viewDoctorEnquiry', compact('enquires', 'treatments', 'doctorDetails'));
    }

    public function DepartmentEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getDepartmentEnquiry();
        return view('enquiry::listDepartmentEnquiry', compact('enquiries'));
    }

    public function DepartmentEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, TreatmentRepository $treatmentRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewDepartmentEnquiry', compact('enquires'));
    }

    public function TreatmentEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getTreatmentEnquiry();
        return view('enquiry::listTreatmentEnquiry', compact('enquiries'));
    }

    public function TreatmentEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, TreatmentRepository $treatmentRepo, DiseasesRepository $diseaseRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        $diseases = $diseaseRepo->get($enquires->diseases_id);
        $treatmentDetails = $treatmentRepo->get($enquires->enquiryable_id);
        return view('enquiry::viewTreatmentEnquiry', compact('enquires', 'diseases', 'treatmentDetails'));
    }

    public function MedicineEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getMedicineEnquiry();
        return view('enquiry::listMedicineEnquiry', compact('enquiries'));
    }

    public function MedicineEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, TreatmentRepository $treatmentRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewMedicineEnquiry', compact('enquires'));
    }

    public function CommonEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getCommonEnquiry();
        return view('enquiry::listCommonEnquiry', compact('enquiries'));
    }

    public function CommomnEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, DiseasesRepository $diseaseRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        $diseases = $diseaseRepo->get($enquires->diseases_id);
        return view('enquiry::viewCommonEnquiry', compact('enquires', 'diseases'));
    }

    public function TeleDoctorEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getTeleDoctorEnquiry();
        return view('enquiry::listTeleDoctorEnquiry', compact('enquiries'));
    }

    public function TeleDoctorEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewTeleDoctorEnquiry', compact('enquires'));
    }

    public function DiseaseEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getDiseaseEnquiry();
        return view('enquiry::listDiseaseEnquiry', compact('enquiries'));
    }

    public function DiseaseEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewDiseaseEnquiry', compact('enquires'));
    }

    public function BookingEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getBookingEnquiry();
        return view('enquiry::listBookingEnquiry', compact('enquiries'));
    }

    public function BookingEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewBookingEnquiry', compact('enquires'));
    }

    public function view(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewEnquiry', compact('enquires'));
    }

    public function delete(EnquiryRepository $enquiryRepo, EnquiryDeleteRequest $request)
    {
        if ($enquiryRepo->delete($request->id)) {
            if (!empty($request->retrn_prms)) {
                connectify('success', 'Success!', 'Enquiry deleted successfully');
                return redirect()
                ->route($request->retrn_url, ['id'=>$request->retrn_prms]);
            } else {
                connectify('success', 'Success!', 'Enquiry deleted successfully');
                return redirect()
                ->route($request->return_url);
            }
        }
    }

    public function hospitalAddEnquirylist(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->getHospitalAddEnquiry();
        return view('enquiry::listHospitalAddEnquiry', compact('enquiries'));
    }

    public function HospitalAddEnquiryview(EnquiryViewRequest $request, EnquiryRepository $enquiryRepo, TreatmentRepository $treatmentRepo)
    {
        $enquires = $enquiryRepo->get($request->id);
        return view('enquiry::viewHospitalAddEnquiry', compact('enquires'));
    }

    public function allEnquiryList(EnquiryRepository $enquiryRepo)
    {
        $enquiries = $enquiryRepo->all();
        return view('enquiry::listEnquiry', compact('enquiries'));
    }
}