<?php

namespace Modules\Enquiry\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnquiryViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('enquiry_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
