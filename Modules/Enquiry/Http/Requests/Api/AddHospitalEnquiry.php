<?php

namespace Modules\Enquiry\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class AddHospitalEnquiry extends FormRequest
{
    
    public function rules()
    {
        return [
            'hospital_name' => 'required|max:50|min:2',
            'contact' => 'required',
            'location' => 'required',
            'email' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'hospital_name.required' => ':attribute is required',
            'contact.required' => ':attribute is required',
            'location.required' => ':attribute is required',
            'email.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'hospital_name' => 'Hospital Name',
            'contact' => 'Contact',
            'location' => 'Location',
            'email' => 'Email',
        ];
    }
}
