<?php

namespace Modules\Enquiry\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Client\Request;

class EnquiryAddRequest extends FormRequest
{
    
    public function rules()
    {
        return [
            'email' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
        ];
    }
}
