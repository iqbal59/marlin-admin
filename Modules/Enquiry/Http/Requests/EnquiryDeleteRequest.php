<?php

namespace Modules\Enquiry\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnquiryDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('enquiry_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
