<?php

namespace Modules\Enquiry\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Enquiry extends Model
{
    use HasFactory;

    protected $fillable = ['id','name','email','contact','is_tele_doctor','file','query_type','query_id','message','reason','seen_doctor_before','country_code','location','diseases_id'];
    
    protected static function newFactory()
    {
        return \Modules\Enquiry\Database\factories\EnquiryFactory::new();
    }
}
