<?php

namespace Modules\Enquiry\Repositories;

interface EnquiryRepositoryInterface
{
    public function all();

    public function get($id);

    public function delete(string $id);

    public function getHospitalEnquiry();

    public function getDoctorEnquiry();

    public function getDepartmentEnquiry();

    public function getTreatmentEnquiry();

    public function getMedicineEnquiry();

    public function getDiseaseEnquiry();

    public function getBookingEnquiry();

    public function getCommonEnquiry();

    public function getTeleDoctorEnquiry();

    public function getHospitalAddEnquiry();

    public function getEnquiry($id, $type);
}