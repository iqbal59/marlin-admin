<?php

namespace Modules\Enquiry\Repositories;

use Modules\Enquiry\Models\Enquiry;
use Illuminate\Database\Eloquent\Builder;

class EnquiryRepository implements EnquiryRepositoryInterface
{
    public function all()
    {
        return Enquiry::orderBy('id', 'DESC')->get();
    }

    public function get($id)
    {
        return Enquiry::where('id', $id)->first();
    }

    public function delete(string $id)
    {
        $enquiry = Enquiry::find($id);
        return $enquiry->delete();
    }

    public function getHospitalEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'Modules\Hospital\Models\Hospital')->orderBy('id', 'DESC')->get();
    }

    public function getDoctorEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'Modules\Doctor\Models\Doctor')->orderBy('id', 'DESC')->get();
    }

    public function getDepartmentEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'Modules\Department\Models\Department')->orderBy('id', 'DESC')->get();
    }

    public function getTreatmentEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'Modules\Treatment\Models\Treatment')->orderBy('id', 'DESC')->get();
    }

    public function getDiseaseEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'disease')->orderBy('id', 'DESC')->get();
    }

    public function getBookingEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'booking')->orderBy('id', 'DESC')->get();
    }

    public function getMedicineEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'Modules\Medicines\Models\Medicines')->orderBy('id', 'DESC')->get();
    }

    public function getCommonEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'normal')->orderBy('id', 'DESC')->get();
    }

    public function getTeleDoctorEnquiry()
    {
        return Enquiry::where('is_tele_doctor', 1)->orderBy('id', 'DESC')->get();
    }

    public function getHospitalAddEnquiry()
    {
        return Enquiry::where('enquiryable_type', 'vendor')->orderBy('id', 'DESC')->get();
    }

    public function getEnquiry($id, $type)
    {
        return Enquiry::where('enquiryable_id', $id)->where('enquiryable_type', $type)->get();
    }
}