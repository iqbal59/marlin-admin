@extends('layouts.admin')
@section('content')
<style>
    .table.department-table th, .table.department-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
        View Department Enquiry
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped department-table">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $enquires->name }}
                        </td>
                    </tr>
                    <tr>

                        <th>
                        Email
                        </th>
                        <td> 
                            {{ $enquires->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Phone
                        </th>
                        <td>
                            {{ $enquires->contact }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                        Message
                        </th>
                        <td>
                            {{ $enquires->message }}
                        </td>
                    </tr>                
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection