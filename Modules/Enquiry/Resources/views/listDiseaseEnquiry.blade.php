@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       List Disease Enquiry
       
       
    </div>
 
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>

                        <th>
                            {{ trans('panel.contact') }} 
                        </th>

                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>                       
                       
                    </tr>
                </thead>
                <tbody>
                @foreach($enquiries as $key => $enquiry)
                        <tr data-entry-id="{{ $enquiry->id }}">
                            <td></td>
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $enquiry->name ?? '' }}
                            </td>
                            <td>
                                {{ $enquiry->contact ?? '' }}
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.enquiry.DiseaseEnquiryview', ['id' => $enquiry->id]) }}">
                                    {{ trans('global.view') }}
                                </a>                          

                                <form action="{{ route('admin.enquiry.delete',  ['id' => $enquiry->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$enquiry->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="return_url" value="admin.enquiry.DiseaseEnquirylist">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form> 
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'acs' ]],
        pageLength: 10,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection