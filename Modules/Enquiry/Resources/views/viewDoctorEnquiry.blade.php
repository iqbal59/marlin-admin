@extends('layouts.admin')
@section('content')
<style>
    .table.doctor-table th, .table.doctor-table td {
        line-height: 24px;
        white-space: normal;
    }
</style>
<div class="card">
    <div class="card-header">
        View Doctor Enquiry
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped doctor-table">
                <tbody> 
                    <tr>
                        <th>
                            From
                        </th>
                        <td>
                            {{ $doctorDetails->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $enquires->name }}
                        </td>
                    </tr>
                    <tr>

                        <th>
                        Email
                        </th>
                        <td> 
                            {{ $enquires->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Phone
                        </th>
                        <td>
                            {{ $enquires->contact }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                        Message
                        </th>
                        <td>
                            {{ $enquires->message }}
                        </td>
                    </tr>     
                    @if($enquires->reason != null)
                    <tr>
                        <th>
                        Reason for Visit
                        </th>
                        <td>
                            {{ $treatments->name }}
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th>
                        Seen doctor before ?
                        </th>
                        <td>
                            @if($enquires->seen_doctor_before == 0)
                            No
                            @else
                            Yes
                            @endif
                        </td>
                    </tr>
                   
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection