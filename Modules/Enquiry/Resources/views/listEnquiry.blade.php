@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        List Hospital Add Enquiry
       
       
    </div>
 
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>

                        <th>
                            {{ trans('panel.email') }} 
                        </th>

                        <th>
                            From
                        </th>

                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>                       
                       
                    </tr>
                </thead>
                <tbody>
                @foreach($enquiries as $key => $enquiry)
                        <tr data-entry-id="{{ $enquiry->id }}">
                            <td></td>
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $enquiry->name ?? '' }}
                            </td>
                            <td>
                                {{ $enquiry->email ?? '' }}
                            </td>
                            <td>
                                @if($enquiry->enquiryable_type =='normal')
                                Common Enquiry
                                @else
                                <?php
                                echo substr($enquiry->enquiryable_type, strrpos($enquiry->enquiryable_type, '\\' )+1)."\n";
                                ?>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.enquiry.view', ['id' => $enquiry->id]) }}">
                                    {{ trans('global.view') }}
                                </a>                          
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 10,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection