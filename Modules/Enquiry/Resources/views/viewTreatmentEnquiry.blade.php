@extends('layouts.admin')
@section('content')
<style>
    .table.treatment-table th, .table.treatment-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
        View Treatment Enquiry
    </div>
 
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped treatment-table">
                <tbody>
                    <tr>
                        <th>
                            From
                        </th>
                        <td>
                            {{ $treatmentDetails->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $enquires->name }}
                        </td>
                    </tr>
                    <tr>

                        <th>
                        Email
                        </th>
                        <td> 
                            {{ $enquires->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Phone
                        </th>
                        <td>
                            {{ $enquires->contact }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                        Message
                        </th>
                        <td>
                            {{ $enquires->message }}
                        </td>
                    </tr>  

                    <tr>
                        <th>
                        File
                        </th>
                        <td>  
                            @if($enquires->file != null)                                                                                         
                            <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$enquires->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>                       
                            @else
                            No file uploaded
                            @endif
                        </td>
                    </tr> 
                    @if($diseases != null)
                    <tr>
                        <th>
                        Diseases
                        </th>
                        <td>
                            {{ $diseases->title }}
                        </td>
                    </tr> 
                   @endif
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection