@extends('layouts.admin')
@section('content')
<style>
    .table.enquiry-table th, .table.enquiry-table td {
        line-height: 24px;
        white-space: normal;
    }
</style>
<div class="card">
    <div class="card-header">
        View Enquiry
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped enquiry-table">
                <tbody>
                    @if(isset($enquires->name)) 
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $enquires->name }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($enquires->email)) 
                    <tr>
                        <th>
                           Email
                        </th>
                        <td>
                            {{ $enquires->email }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($enquires->contact)) 
                    <tr>
                        <th>
                           Phone
                        </th>
                        <td>
                            {{ $enquires->contact }}
                        </td>
                    </tr>
                    @endif
                    @if(isset($enquires->message)) 
                    <tr>
                        <th>
                           Message
                        </th>
                        <td>
                            {{ $enquires->message }}
                        </td>
                    </tr>
                    @endif
                    @if($enquires->file != null) 
                    <tr>
                        <th>
                        File
                        </th>
                        <td>  
                            <a class="btn-floating mb-1 btn-flat waves-effect waves-light  pink accent-2 white-text" href="{{asset('storage/'.$enquires->file)}}" target="_blank">
                                <i class="menu-icon mdi mdi-download" style="color: #000;font-size: 24px;"></i>
                            </a>                       
                        </td>
                    </tr>
                    @endif

                    @if(isset($enquires->location)) 
                    <tr>
                        <th>
                           Location
                        </th>
                        <td>
                            {{ $enquires->location }}
                        </td>
                    </tr>
                    @endif

                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection