<?php

namespace Modules\Enquiry\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    use HasFactory;
    protected $table = 'enquiries';

    protected $fillable = [
        'name','email','contact','is_tele_doctor','query_type','query_id','file','message','reason','seen_doctor_before','country_code','location','diseases_id'
    ];
    
    public function enquiryable()
    { 
        return $this->morphTo();
    }
}