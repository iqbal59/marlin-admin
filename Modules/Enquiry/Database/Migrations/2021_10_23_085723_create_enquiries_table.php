<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table) {
            $table->id();            
            $table->integer('enquiryable_id');
            $table->string('enquiryable_type');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('message')->nullable();
            $table->string('is_tele_doctor')->default('0');
            $table->string('file')->nullable();
            $table->string('reason')->nullable();
            $table->boolean('seen_doctor_before')->default(0)->comment('0=>no,1=>yes');
            $table->boolean('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *c
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
