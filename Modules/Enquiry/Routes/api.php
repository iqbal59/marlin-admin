<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Enquiry\Http\Controllers\Api\EnquiryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/enquiry', function (Request $request) {
    return $request->user();
});

Route::prefix('enquiry')->group(function () {
    Route::post('/commonEnquiry', [EnquiryController::class, 'commonEnquiry']);
    Route::post('/contactEnquiry', [EnquiryController::class, 'contactEnquiry']);
    Route::post('/Mmma_Biz', [EnquiryController::class, 'Mmma_Biz']);
    Route::post('/addHospitalEnquiry', [EnquiryController::class, 'addHospitalEnquiry']);
    Route::post('/addDiseaseEnquiry', [EnquiryController::class, 'addDiseaseEnquiry']);
    Route::post('/addBookingEnquiry', [EnquiryController::class, 'addBookingEnquiry']);
});