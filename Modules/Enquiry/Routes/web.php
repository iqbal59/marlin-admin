<?php

use Modules\Enquiry\Http\Controllers\EnquiryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('enquiry')->group(function () {
    Route::get('/', 'EnquiryController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'enquiry', 'as' => 'enquiry.'], function () {
        Route::get('/', [EnquiryController::class, 'list'])->name('list');
        Route::get('HospitalEnquirylist', [EnquiryController::class, 'HospitalEnquirylist'])->name('HospitalEnquirylist');
        Route::get('HospitalEnquiryview', [EnquiryController::class, 'HospitalEnquiryview'])->name('HospitalEnquiryview');
        Route::get('DiseaseEnquirylist', [EnquiryController::class, 'DiseaseEnquirylist'])->name('DiseaseEnquirylist');
        Route::get('DiseaseEnquiryview', [EnquiryController::class, 'DiseaseEnquiryview'])->name('DiseaseEnquiryview');
        Route::get('BookingEnquirylist', [EnquiryController::class, 'BookingEnquirylist'])->name('BookingEnquirylist');
        Route::get('BookingEnquiryview', [EnquiryController::class, 'BookingEnquiryview'])->name('BookingEnquiryview');
        Route::get('DoctorEnquirylist', [EnquiryController::class, 'DoctorEnquirylist'])->name('DoctorEnquirylist');
        Route::get('DoctorEnquiryview', [EnquiryController::class, 'DoctorEnquiryview'])->name('DoctorEnquiryview');
        Route::get('DepartmentEnquirylist', [EnquiryController::class, 'DepartmentEnquirylist'])->name('DepartmentEnquirylist');
        Route::get('DepartmentEnquiryview', [EnquiryController::class, 'DepartmentEnquiryview'])->name('DepartmentEnquiryview');
        Route::get('TreatmentEnquirylist', [EnquiryController::class, 'TreatmentEnquirylist'])->name('TreatmentEnquirylist');
        Route::get('TreatmentEnquiryview', [EnquiryController::class, 'TreatmentEnquiryview'])->name('TreatmentEnquiryview');
        Route::get('MedicineEnquirylist', [EnquiryController::class, 'MedicineEnquirylist'])->name('MedicineEnquirylist');
        Route::get('MedicineEnquiryview', [EnquiryController::class, 'MedicineEnquiryview'])->name('MedicineEnquiryview');
        Route::get('CommonEnquirylist', [EnquiryController::class, 'CommonEnquirylist'])->name('CommonEnquirylist');
        Route::get('CommomnEnquiryview', [EnquiryController::class, 'CommomnEnquiryview'])->name('CommomnEnquiryview');
        Route::get('TeleDoctorEnquirylist', [EnquiryController::class, 'TeleDoctorEnquirylist'])->name('TeleDoctorEnquirylist');
        Route::get('TeleDoctorEnquiryview', [EnquiryController::class, 'TeleDoctorEnquiryview'])->name('TeleDoctorEnquiryview');
        Route::get('hospitalAddEnquirylist', [EnquiryController::class, 'hospitalAddEnquirylist'])->name('hospitalAddEnquirylist');
        Route::get('HospitalAddEnquiryview', [EnquiryController::class, 'HospitalAddEnquiryview'])->name('HospitalAddEnquiryview');

        Route::get('allEnquiryList', [EnquiryController::class, 'allEnquiryList'])->name('allEnquiryList');
        Route::get('view', [EnquiryController::class, 'view'])->name('view');
        Route::get('delete', [EnquiryController::class, 'delete'])->name('delete');
    });
});