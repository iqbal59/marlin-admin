<?php

use Modules\Department\Http\Controllers\DepartmentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------

|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'department', 'as' => 'department.'], function () {         
        Route::get('/', [DepartmentController::class, 'listDepartment'])
            ->name('list');
        Route::get('add/{id?}', [DepartmentController::class, 'add'])
            ->name('add');
        Route::post('save', [DepartmentController::class, 'save'])
            ->name('save');
        Route::get('edit/{hospital_id?}', [DepartmentController::class, 'edit'])
            ->name('edit');
        Route::post('update', [DepartmentController::class, 'update'])
            ->name('update');
        Route::get('view', [DepartmentController::class, 'view'])
            ->name('view');
        Route::get('delete', [DepartmentController::class, 'delete'])
            ->name('delete');
        Route::get('remove_file', [DepartmentController::class, 'removeFile'])
            ->name('remove_file');
        Route::get('remove_file_image', [DepartmentController::class, 'removeFileImage'])
            ->name('remove_file_image');
        Route::post('ckeditor_image_upload', [DepartmentController::class, 'ckeditor_image_upload'])
            ->name('ckeditor_image_upload');
        
    });
});

