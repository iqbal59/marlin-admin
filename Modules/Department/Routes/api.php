<?php

use Illuminate\Http\Request;
use Modules\Department\Http\Controllers\Api\DepartmentController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/department', function (Request $request) {
    return $request->user();
});

Route::prefix('department')->group(function () {
    Route::get('/listDepartments', [DepartmentController::class, 'listDepartments']);
    Route::post('/departmentEnquiry', [DepartmentController::class, 'departmentEnquiry']);

});