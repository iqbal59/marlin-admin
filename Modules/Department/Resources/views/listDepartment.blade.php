@extends('layouts.admin')
@section('content')
<style>
.addMore{
  border: none; 
  height: 28px;
  background-color: #FF8000;
  transition: all ease-in-out 0.2s;
  cursor: pointer;
}
.addMore:hover{
  border: 1px solid #FF8000;
  background-color: #FF8000;
}
button:disabled,
button[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}

</style>
@can('department_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.department.add") }}">
                <i class="mdi mdi-plus"></i> 
                {{ trans('panel.add_department') }} 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
    {{ trans('panel.list_department') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>

                        <th>
                        {{ trans('hospital') }}
                        </th>

                        <th>
                            Status
                        </th>

                        <th>
                       {{ trans('panel.action') }}
                        </th>
                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($departments as $key => $department)
                        <tr data-entry-id="{{ $department->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $department->name ?? '' }}</td>
                            <td>{{ $department->hospital->title ?? '' }}</td>
                            <td>
                                @if($department->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                            @can('department_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.department.view', ['id' => $department->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan 
                            @can('department_edit')
                              
                                    <a class="btn btn-xs btn-info" href="{{route('admin.department.edit', ['id' => $department->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan  
                            @can('department_delete')       
                                    <form action="{{ route('admin.department.delete',  ['id' => $department->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$department->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        @if($department->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this department has child!"> Delete</button>
                                        @endif                                    
                                    </form>
                            @endcan  
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
    });
})
</script>
@endsection