@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<html>
<body>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    .dept-edit-cn span.select2-selection.select2-selection--multiple:after {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 5px;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
    content: "";
}
    </style>

@foreach($department->polls as $poll)
  @php  $pollValue =  $poll['value'];  
@endphp
@endforeach
<div class="card dept-edit-cn">
    <div class="card-header">
         Department {{ trans('global.edit') }}  
    </div>

    <div class="card-body">
        <form action="{{ route("admin.department.update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="hasHospital" value="{{!empty($hospital) ? 1 : 0}}">
            <input type="hidden" name="id" value="{{$department->id}}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="department" name="name" class="form-control" value="{{ old('name', isset($department) ? $department->name : '') }}" required>
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
        </div>
        <div class="col-md-6">    
            <div class="form-group {{ $errors->has('caption') ? 'has-error' : '' }}">
                <label for="caption">{{ trans('panel.caption') }} </label>
                <input type="text" id="caption" name="caption" class="form-control" value="{{ old('caption', isset($department) ? $department->caption : '') }}" >
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label for="name">Diseases </label>
            @if($selecteValue == null)
                <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                    @foreach($diseases as $data)  
                    <option value="{{$data->id}}">{{$data->title}}</option>                 
                    @endforeach
                </select>
            @else
                <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                    @foreach($diseases as $data)  
                    <option value="{{$data->id}}" @if(in_array($data->id, $selecteValue)) selected @endif>{{$data->title}}</option>
                    @endforeach
                </select>
            @endif    
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('subtitle') ? 'has-error' : '' }}">
                <label for="subtitle"> {{ trans('panel.subtitle') }} </label>
                <input type="text" id="subtitle" name="subtitle" class="form-control" value="{{ old('subtitle', isset($department) ? $department->subtitle : '') }}" >
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
        </div> 
    </div>
    <div class="row">
        <div class="col-md-6">

            @php $hospital_id = !empty($hospital) ? $hospital->id : '' @endphp
            <div class="form-group {{ $errors->has('hospital_id') ? 'has-error' : '' }}">
                <label for="department"> Hospital</label>
                    <!-- <select id="hospitale_id" name="hospital_id" class="js-states form-control">
                        <option value="">Select Hospital</option>
                        @foreach($hospitals as $data) 
                        <option value="{{$data->id}}" {{old('hospital_id', $hospital_id) == $data->id ? 'selected' : ''}} 
                                name="hospital-dropdown" 
                                id="hospital-dropdown" 
                                onclick="myFunction()" 
                            >
                        {{$data->title}}
                        </option>
                        @endforeach
                    </select>    -->
                    <select id="hospital_id" name="hospital_id" class="js-states form-control">
                            @foreach ($hospitals as $key => $value)
                            <option value="{{$value->id}}" {{(old('hospital_id', $department->hospital_id) == $value->id ? 'selected' : '')}} > {{$value->title}} </option>
                            @endforeach
                        </select>
                    
                    
                    


                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                <label for="meta_title">Meta Title </label>
                <input type="text" id="meta_title" 
                    name="meta_title" 
                    class="form-control" 
                    value="{{ old('meta_title', isset($department) ? $department->meta_title : '') }}" 
                    
                >
                @if($errors->has('meta_title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
    </div>
     
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">* {{ trans('panel.description') }}</label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($department) ? $department->description : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                <!-- <input type="text" id="description" name="description" class="form-control" value="{{ old('description', isset($department) ? $department->description : '') }}" required> -->
                @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
         
    <div class="row">
        <div class="col-md-6">
            
            <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                <label for="logo">* {{ trans('panel.logo') }}</label>
                
                @if(Storage::disk('public')->exists($department->logo)) 
                <img class="preview" src="{{asset('storage/'.$department->logo)}}" alt="" width="150px">
                    <a href="{{route('admin.department.remove_file',['id'=>$department->id])}}" class="confirm-delete text-danger">
                    <i class="material-icons pink-text">clear</i>
                    </a>
                @else
                <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">                
                <input type="file" name="logo" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('logo'))
                    <em class="invalid-feedback">
                        {{ $errors->first('logo') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
                @endif
            </div> 
        </div>
        <div class="col-md-6">
        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image">* {{ trans('panel.image') }}</label>
               
                @if(Storage::disk('public')->exists($department->image)) 
                <img class="preview" src="{{asset('storage/'.$department->image)}}" alt="" width="150px">
                    <a href="{{route('admin.department.remove_file_image',['id'=>$department->id])}}" class="confirm-delete text-danger">
                    <i class="material-icons pink-text">clear</i>
                    </a>
                @else
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
                @endif
            </div> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                <label for="meta_keywords">Meta Keyword </label>
                <input type="text" id="meta_keywords" 
                    name="meta_keywords" 
                    class="form-control" 
                    value="{{ old('meta_keywords', isset($department) ? $department->meta_keywords : '') }}" 
                    
                >
                @if($errors->has('meta_keywords'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_keywords') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                <label for="meta_description">{{ trans('panel.meta_description') }} </label>
                <input type="text" id="meta_description" 
                    name="meta_description" 
                    class="form-control" 
                    value="{{ old('meta_description', isset($department) ? $department->meta_description : '') }}" 
                    
                >
                @if($errors->has('meta_description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-md-6">
            <label for="Poll">Poll</label>
            <div class="form-group">
                <input type="number" id="poll" name="value" min="1" max="99" value="{{ old('value', $pollValue ) }}">
            </div>
        </div>
        <div class="col-md-6">
                <label for="name">Problems  </label>
                @if($selecteProblems == null)
                    <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                        @foreach($problems as $data)  
                            <option value="{{$data->id}}">{{$data->title}}</option>                 
                        @endforeach
                    </select>
                @else
                    <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                        @foreach($problems as $data)  
                        <option value="{{$data->id}}" @if(in_array($data->id, $selecteProblems)) selected @endif>{{$data->title}}</option>
                @endforeach
            </select>
            @endif    
        </div>
    </div>
    <div class="col-md-6 badge-contr">
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
        <label for="status">Status</label>
            <div class="custom-control custom-switch">
                <input type="checkbox" {{ old('status', $department->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                <span class="off">No</span>
                <label class="custom-control-label" for="status"></label>
                <span class="on">Yes</span>
            </div>
        </div> 
    </div>   

    <br>
    
    
           
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
        autoParagraph = false;
    });
</script>
<script>
    $(document).ready(function() {
        $("#hospital_id").select2({
            placeholder: "Select Hospital",
            allowClear: true
        });
    });
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
 </script>
@endsection