<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #example2_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }

    </style>
<div class="card">
    <div class="card-header">
        View Doctors  
        @can('doctor_create')
        <a class="btn btn-warning float-right" href="{{route('admin.doctor.add', ["retrn_url" => "admin.department.view" , "retrn_prms" => $department->id,"from" => "department"])}}">
            Add Doctor
        </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example2" class="display nowrap table" style="width:100%">
                <thead>
                    <tr>
                        {{-- <th>{{ trans('cruds.user.fields.id') }}</th> --}}
                        <th>{{ trans('cruds.user.fields.name') }}</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($doctors as $key => $doctor)
                        <tr data-entry-id="{{ $doctor->id }}">
                            {{-- <td>{{ $key+1 }}</td> --}}
                            <td>{{ $doctor->name ?? '' }}</td>
                            <td>
                                @if($doctor->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.doctor.view', ['id' => $doctor->id]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.doctor.edit', ['id' => $doctor->id, 'retrn_url' => "admin.department.view" , "retrn_prms" => $department->id ]) }}">
                                    {{ trans('global.edit') }}
                                </a>   

                                <!-- <form action="{{ route('admin.doctor.delete',  ['id' => $doctor->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$doctor->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form> -->
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
   $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection



