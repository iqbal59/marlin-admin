<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="mb-2">
    <table class="table table-bordered table-striped video-table">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $department->id }}</td>
            </tr>
            <tr>
                <th>{{ trans('cruds.user.fields.name') }}</th>
                <td>{{ $department->name }}</td>
            </tr>
            @if($department->caption != "")
            <tr>
                <th>Caption</th>
                <td>{{ $department->caption }}</td>
            </tr>
            @endif
            @if($department->caption != "")
            <tr>
                <th>Description</th>
                <td> {!! $department->description  !!}</td>
            </tr>
            @endif
            @if($department->meta_title != "")
            <tr>
                <th>Meta Title</th>
                <td> {!! $department->meta_title  !!}</td>
            </tr>
            @endif
            @if($department->meta_title != "")
            <tr>
                <th>Meta Keyword</th>
                <td> {!! $department->meta_keywords  !!}</td>
            </tr>
            @endif
            
            @if($department->logo != null)
            <tr>
                <th>Logo</th>
                <td><img src="{{asset('storage/'.$department->logo)}}" alt="" width="500px"></td>
            </tr>
            @endif
            @if($department->image != null)
            <tr>
                <th>Image</th>
                <td><img src="{{asset('storage/'.$department->image)}}" alt="" width="500px"></td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif
        </tbody>
    </table>
</div>