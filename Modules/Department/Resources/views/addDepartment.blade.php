@extends('layouts.admin')
@section('content')

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<div class="card">
    <div class="card-header">
        {{ trans('panel.add_department') }}

    </div>

    <div class="card-body">
        <form action="{{ route('admin.department.save') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="hasHospital" value="{{!empty($hospital) ? 1 : 0}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}">
                        @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                    @php $hospital_id = !empty($hospital) ? $hospital->id : '' @endphp
                    <div class="form-group {{ $errors->has('hospital_id') ? 'has-error' : '' }}">
                        <label for="hospital">{{ trans('panel.hospital') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        </label>
                        <select id="hospitale_id" name="hospital_id" class="js-states form-control">
                            <option value=""> {{ trans('panel.select_hospital') }} </option>
                            @foreach($hospitals as $data)
                            <option value="{{$data->id}}" {{old('hospital_id', $hospital_id)==$data->id ? 'selected' :
                                ''}} name="hospital-dropdown" id="hospital-dropdown" onclick="myFunction()">
                                {{$data->title}}
                            </option>

                            @endforeach
                            @if($errors->has('hospital_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('hospital_id') }}
                            </em>
                            @endif
                        </select>
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>


            </div>



            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                        <label for="logo"> {{ trans('panel.logo') }}</label>
                        <input type="file" name="logo" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image"> {{ trans('panel.image') }}</label>
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        <label for="description">{{ trans('panel.description') }}</label>
                        <textarea class="ckeditor form-control" id="description" name="description"
                            value="{{old('description')}}"></textarea>
                        <!-- <input type="text" id="description" name="description" class="form-control" value="{{old('description')}}" required> -->
                        @if($errors->has('description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('description') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>



            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.ckeditor').ckeditor();
});
</script>
<script>
$(document).ready(function() {
    $("#hospitale_id").select2({
        placeholder: "Select Hospital",
        allowClear: true
    });
});
</script>
<script type="text/javascript">
CKEDITOR.replace('wysiwyg-editor', {
    filebrowserUploadUrl: "{{route('admin.department.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});
</script>
@endsection