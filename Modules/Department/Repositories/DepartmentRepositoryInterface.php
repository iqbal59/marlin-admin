<?php

namespace Modules\Department\Repositories;

interface DepartmentRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($deptId);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);

    public function resetFileImages(string $id);

    public function getImage($id, $type);

    public function getDepartmentName($departments);

    public function getDepartmentsForHospital($hospitalId);

    public function getDepartmentCount();

    public function getDepartmentsHospital($hospitalId);

    public function getDepartments($departments);

    public function getDepartment($departments);

    public function departmentHospital($deptId,$searchText);

    public function departmentBasedHospital($departments,$hospitalId,$searchText);

    public function getDepartmentData($hospitalId);

    public function getDepartmentsForHospitalCount($hospitalId);

    public function deptData($hospitalId);

    public function searchBasedDepartment($searchText);

    public function searchBasedDepartments($searchText,$hospitalId);
   
    public function getData($deptId);

    public function departments($id);

}