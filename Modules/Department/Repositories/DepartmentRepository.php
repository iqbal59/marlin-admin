<?php


namespace Modules\Department\Repositories;

// use Modules\Department\Entities\Department;
use Modules\Department\Models\Department;
use Illuminate\Database\Eloquent\Builder;

class DepartmentRepository implements DepartmentRepositoryInterface
{
    public function getForDatatable()
    {
        return Department::orderBy('id', 'DESC')->get();
    }

    public function all()
    {
        return Department::where('status', 1)->orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    {
        if ($department =  Department::create($input)) {
            return $department;
        }
        return false;
    }

    public function get($deptId)
    {
        return Department::with('doctors')->where('id', $deptId)->first();
    }

    public function getData($deptId)
    {
        return Department::where('id', $deptId)->get();
    }

    public function departments($id)
    {
        return Department::whereIn('id', $id)->get();
    }


    public function getDepartmentsForHospital($hospitalId)
    {
        return Department::where('hospital_id', $hospitalId)->orderBy('id', 'DESC')->get();
    }

    public function getDepartmentsHospital($hospitalId)
    {
        // return Department::where('status', 1)->where('hospital_id', $hospitalId)->pluck('id')->toArray();
        return Department::where('status', 1)->pluck('id')->toArray();
    }

    public function getDepartments($departments)
    {
        return Department::whereIn('id', $departments)->get();
    }

    public function getDepartmentName($departments)
    {
        return Department::whereIn('id', $departments)->pluck('name');
    }

    public function getDepartment($departments)
    {
        return Department::where('id', $departments)->first();
    }


    public function update(array $input)
    {
        $department = Department::find($input['id']);
        unset($input['id']);
        if ($department->update($input)) {
            return $department;
        }
        return false;
    }

    public function delete(string $id)
    {
        $department = Department::find($id);
        return $department->delete();
    }


    public function resetFile(string $id)
    {
        $department = Department::find($id);
        $department->logo = '';
        return $department->save();
    }

    public function resetFileImages(string $id)
    {
        $department = Department::find($id);
        $department->image = '';
        return $department->save();
    }

    public function getImage($id, $type)
    {
        $department = Department::where('id', $id)->value($type);
        return $department;
    }

    public function getDepartmentCount()
    {
        $departmentCount =Department::count();
        return $departmentCount;
    }

    public function departmentHospital($deptId, $searchText)
    {
        $hospital = Department::where('id', $deptId)->whereHas('hospital', function ($query) use ($searchText) {
            $query->where('title', 'like', '%'.$searchText.'%');
        })
        ->with(['hospital' => function ($query) use ($searchText) {
            $query->where('title', 'like', '%'.$searchText.'%');
        }])->get();

        return $hospital;
    }

    public function departmentBasedHospital($department, $hospitalId, $searchText)
    {
        // return Department::with('hospital')->where('id',$department)->whereIn('hospital_id',$hospitalId)->get();
        $hospital = Department::where('id', $department)
        ->whereIn('hospital_id', $hospitalId)
        ->whereHas('hospital', function ($query) use ($searchText) {
            $query->where('title', 'like', '%'.$searchText.'%');
        })
        ->with(['hospital' => function ($query) use ($searchText) {
            $query->where('title', 'like', '%'.$searchText.'%');
        }])->get();

        return $hospital;
    }

    public function getDepartmentData($hospitalId)
    {
        if (is_array($hospitalId)) {
            return Department::whereIn('hospital_id', $hospitalId)->first();
        } else {
            return Department::where('hospital_id', $hospitalId)->first();
        }
    }

    public function getDepartmentsForHospitalCount($hospitalId)
    {
        return Department::where('hospital_id', $hospitalId)->count();
    }

    public function deptData($hospitalId)
    {
        return Department::whereIn('hospital_id', $hospitalId)->get();
    }

    public function searchBasedDepartment($searchText)
    {
        return Department::where('status', 1)->where('name', 'like', '%'.$searchText.'%')->pluck('id')->toArray();
    }

    public function searchBasedDepartments($searchText, $hospitalId)
    {
        return Department::where('status', 1)->whereIn('hospital_id', $hospitalId)->where('name', 'like', '%'.$searchText.'%')->pluck('id')->toArray();
    }
}