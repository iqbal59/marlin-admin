<?php

namespace Modules\Department\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Hospital\Models\Hospital;


class Department extends Model
{
    use HasFactory;
    use Sluggable;


    protected $fillable = [
        'id',
        'slug',
        'name',
        'hospital_id',
        'problems_id',
        'caption',
        'subtitle',
        'description',
        'logo',
        'banner_id',
        'video_id',
        'image',
        'status',
        'diseases_id',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'added_by'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    
    protected static function newFactory()
    {
        return \Modules\Department\Database\factories\DepartmentFactory::new();
    }

    public function hospital()
    {
        return $this->belongsTo(Modules\Hospital\Models\Hospital::class, 'hospital_id', 'id');
    }


}
