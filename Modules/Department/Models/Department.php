<?php

namespace Modules\Department\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Poll\Models\Poll;
use Modules\Enquiry\Models\Enquiry;

class Department extends Model
{
    use HasFactory;
    use Sluggable;
    protected $table = 'departments';

    protected $fillable = ['id','slug','name','hospital_id','problems_id','caption','subtitle','description','logo','banner_id','video_id','image','status','diseases_id','meta_title','meta_description','meta_keywords','added_by'];


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'subtitle'
            ]
        ];
    }

    public function hospital()
    {
        return $this->belongsTo('Modules\Hospital\Models\Hospital', 'hospital_id', 'id');
    }

    public function doctors()
    {
        return $this->hasMany('Modules\Doctor\Models\Doctor');
    }

    public function facility()
    {
        return $this->hasMany('Modules\Facilities\Models\Facilities');
    }

    public function treatment()
    {
        return $this->hasMany('Modules\Treatment\Models\Treatment');
    }


    public function polls()
    {
        return $this->morphMany(Poll::class, 'pollable');
    }

    public function enquiries()
    {
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->doctors->count()
            ||$this->facility->count()
            // ||$this->treatment->count()
        ) {
            return false;
        }
        return true;
    }
}