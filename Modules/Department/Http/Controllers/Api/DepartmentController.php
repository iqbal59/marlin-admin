<?php

namespace Modules\Department\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Carbon\Exceptions\Exception;
use Modules\Department\Models\Department;
use Modules\Enquiry\Models\Enquiry;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
class DepartmentController extends Controller
{
    public function listDepartments(DepartmentRepository $deptRepo)
    {  
        try {
            $departments = $deptRepo->all(); 
            $data = compact("departments");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }                                  
    }  
    
    public function departmentEnquiry(EnquiryAddRequest $request,Enquiry $enquiry)
    {   
        try {
            $department = Department::find($request->department_id);
            if($department != null){
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->department_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $department->enquiries()->save($enquiry); 

                $response = ['status' => true,'message' => 'Department Enquiry Added Successfully'];
                return response()->json($response, 200);
            }        
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

}
