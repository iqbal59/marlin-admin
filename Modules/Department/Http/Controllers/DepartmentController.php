<?php

namespace Modules\Department\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Department\Http\Requests\DepartmentAddRequest;
use Modules\Department\Http\Requests\DepartmentSaveRequest;
use Modules\Department\Http\Requests\DepartmentEditRequest;
use Modules\Department\Http\Requests\DepartmentUpdateRequest;
use Modules\Department\Http\Requests\DepartmentViewRequest;
use Modules\Department\Http\Requests\DepartmentDeleteRequest;
use Modules\Poll\Models\Poll;
use Illuminate\Support\Facades\Storage;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepository;
use Intervention\Image\ImageManagerStatic as Image;
use App\Repositories\User\UserRepositoryInterface as UserRepository;

class DepartmentController extends Controller
{
    public function listDepartment(DepartmentRepository $deptRepo)
    {
        $departments = $deptRepo->getForDatatable();
        return view('department::listDepartment', compact('departments'));
    }

    public function add(DepartmentAddRequest $request, HospitalRepository $hospitalRepo)
    {
        $hospital = $request->id != '' ? $hospitalRepo->get($request->id) : '';
        $hospitals = $hospitalRepo->all();
        return view('department::addDepartment', compact('hospitals', 'hospital'));
    }

    public function save(DepartmentSaveRequest $request, DepartmentRepository $deptRepo)
    {
        $logoUrl=$request->hasFile('logo')?$logoUrl = Storage::disk('public')->putFile('departmentlogo/', $request->file('logo')):$logoUrl="";
        
        $inputData = [
            'name'  => $request->name,
            'description'  => $request->description,
            'logo'=>$logoUrl,
            'added_by'     => auth()->user()->id,
        ];

        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(180, 180);
            $image_resize->save(storage_path('app/public/departmentimage/' .$filename));
            $inputData['image'] = 'departmentimage/' .$filename;
        }

        $department = $deptRepo->save($inputData);
        
        $poll         = new Poll;
        $poll->value  = '1';
        $department->polls()->save($poll);
        //  $route = !empty($request->hasHospital) ? redirect()->route('admin.hospital.view', ['id' => $request->hospital_id]) : redirect()->route('admin.department.list');
       
        $route=redirect()->route('admin.department.list');
        connectify('success', 'Success!', 'Department added successfully');
        return $route;
    }

    public function view(DepartmentViewRequest $request, DepartmentRepository $deptRepo, DoctorRepository $doctorRepo, FaqRepository $faqRepo, UserRepository $userRepo)
    {
        $department = $deptRepo->get($request->id);
        $doctors = $doctorRepo->getDoctor($request->id);
        $faqs = $faqRepo->getFaq($request->id, $type="Department");
        $addedBy = $userRepo->get($department->added_by);
        return view('department::viewDepartment', compact('addedBy', 'department', 'doctors', 'faqs'));
    }

    public function edit(DepartmentEditRequest $request, DepartmentRepository $deptRepo, $hospital_id = '', HospitalRepository $hospitalRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepository)
    {
        $hospital = $request->hospital_id != '' ? $hospitalRepo->get($request->hospital_id) : '';
        $hospitals = $hospitalRepo->all();
        $department =$deptRepo->get($request->id);
        $diseases = $diseasesRepo->all();
        $problems = $problemsRepository->all();
        $selecteValue = json_decode($department->diseases_id);
        $selecteProblems = json_decode($department->problems_id);
        return view('department::editDepartment', compact('department', 'hospital', 'hospitals', 'diseases', 'selecteValue', 'problems', 'selecteProblems'));
    }

    public function update(DepartmentUpdateRequest $request, DepartmentRepository $deptRepo)
    {
        $inputData = [
            'id' => $request->id,
            'name' => $request->name,
            'caption' => $request->caption,
            'subtitle' => $request->subtitle,
            'description' => $request->description,
            'diseases_id' => json_encode($request->states),
            'problems_id' => json_encode($request->problems),
            'meta_title' => $request->meta_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'hospital_id'  => $request->hospital_id,
        ];

        if (isset($request->status)) {
            $inputData['status']=($request->status == "on")?1:0;
        } else {
            $inputData['status']='0';
        }


        if ($request->hasFile('logo')) {
            $this->_removeFile($deptRepo, $request->id);
            $logoUrl = Storage::disk('public')->putFile('departmentlogo/', $request->file('logo'));
            $inputData['logo'] = $logoUrl;
        }
        

        if ($request->hasFile('image')) {
            $this->_removeFileImages($deptRepo, $request->id);
            // $imageUrl = Storage::disk('public')->putFile('departmentimage/',$request->file('image'));
            // $inputData['image'] = $imageUrl;
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(180, 180);
            $image_resize->save(storage_path('app/public/departmentimage/' .$filename));
            $inputData['image'] = 'departmentimage/' .$filename;
        }

        $poll     = new Poll;
        $department = $deptRepo->update($inputData);
        $poll->pollable()->associate($department);
        $department->polls()->update(['value' => $request->value], );
        $route = !empty($request->hasHospital) ? redirect()->route('admin.hospital.view', ['id' => $request->hospital_id]) : redirect()->route('admin.department.list');
        connectify('success', 'Success!', 'Department updated successfully');
        return $route;
    }
   

    public function delete(DepartmentRepository $deptRepo, DepartmentDeleteRequest $request)
    {
        $department = $deptRepo->get($request->id);
        if ($department->doctors->count()) {
            connectify('danger', 'Warning!', 'This Department has transactions');
            return redirect()
                ->route('admin.department.list');
        } else {
            if ($deptRepo->delete($request->id)) {
                if ($request->retrn_prms != null) {
                    connectify('success', 'Success!', 'Department added successfully');
                    return redirect()
                    ->route($request['retrn_url'], ['id'=>$request['retrn_prms']]);
                } else {
                    connectify('success', 'Success!', 'Department deleted successfully');
                    return redirect()
                    ->route('admin.department.list');
                }
            }
            return response()->json(['status' => 0, 'message' => "failed"]);
        }
    }

    public function removeFile(DepartmentRepository $deptRepo, Request $request)
    {
        if ($this->_removeFile($deptRepo, $request->id)) {
            $deptRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly');
        return redirect()
            ->route('admin.department.edit', ['id' => $request->id]);
    }

    private function _removeFile($deptRepo, $id)
    {
        $logo = $deptRepo->get($id);
        if (Storage::disk('public')->delete($logo->logo)) {
            return true;
        }
        return false;
    }

    public function removeFileImage(DepartmentRepository $deptRepo, Request $request)
    {
        if ($this->_removeFileImages($deptRepo, $request->id)) {
            $deptRepo->resetFileImages($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfuly');
        return redirect()
            ->route('admin.department.edit', ['id' => $request->id]);
    }

    private function _removeFileImages($deptRepo, $id)
    {
        $image = $deptRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }
    public function ckeditor_image_upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $msg = 'Image successfully uploaded';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}