<?php

namespace Modules\Department\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('department_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            
           // 'hospital_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            
            'hospital_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            
            'hospital_id' => 'Hospital',
        ];
    }
}