<?php

namespace Modules\Department\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('department_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:50|min:2',
            // 'caption' => 'required',
            // 'subtitle' => 'required',
            // 'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            // 'caption.required' => ':attribute is required',
            // 'subtitle.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            // 'caption' => 'Caption',
            // 'subtitle' => 'Subtitle',
            // 'description' => 'Description',
        ];
    }
}
