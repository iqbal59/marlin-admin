<?php

namespace Modules\Department\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('department_create') ? true : false;
    }
    public function rules()
    {
        return [
           
        ];
    }
}
