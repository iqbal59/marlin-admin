<?php
use Modules\Hospital\Http\Controllers\HospitalController;

/*
|--------------------------------------------------------------------------
| Hospital Routes
|--------------------------------------------------------------------------

*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'hospital', 'as' => 'hospital.'], function () {    

        Route::get('/', [HospitalController::class, 'listHospital'])
            ->name('list');
        Route::get('add', [HospitalController::class, 'add'])
            ->name('add');
        Route::post('save', [HospitalController::class, 'save'])
            ->name('save');
        Route::get('edit', [HospitalController::class, 'edit'])
            ->name('edit');
        Route::post('update', [HospitalController::class, 'update'])
            ->name('update');
        Route::get('view', [HospitalController::class, 'view'])
            ->name('view');
        Route::get('delete', [HospitalController::class, 'delete'])
            ->name('delete');
        Route::get('remove_file', [HospitalController::class, 'removeFile'])
            ->name('remove_file');
        Route::get('remove_file_image', [HospitalController::class, 'removeFileImage'])
            ->name('remove_file_image');
        Route::get('remove_thumb_line_image', [HospitalController::class, 'removeThumblineImage'])
            ->name('remove_thumb_line_image');
    });
});
