<?php

use Illuminate\Http\Request;
use Modules\Hospital\Http\Controllers\Api\HospitalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/hospital', function (Request $request) {
    return $request->user();
});

Route::prefix('hospital')->group(function () {
    Route::post('/listHospital', [HospitalController::class, 'listHospital']);
    Route::post('/view', [HospitalController::class, 'view']);
    Route::post('/hospitalDoctors', [HospitalController::class, 'hospitalDoctors']);
    Route::post('/hospitalDepartments', [HospitalController::class, 'hospitalDepartments']);
    Route::post('/similarHospitals', [HospitalController::class, 'similarHospitals']);

    Route::post('/successStories', [HospitalController::class, 'successStories']);
    Route::post('/specialFeatures', [HospitalController::class, 'specialFeatures']);
    Route::post('/hospitalEnquiry', [HospitalController::class, 'hospitalEnquiry']);
    Route::post('/hospital-ExploreEnquiry', [HospitalController::class, 'hospitalExploreEnquiry']);

    Route::get('/allHospitalFaq', [HospitalController::class, 'allHospitalFaq']);
    Route::post('/HospitalFaq', [HospitalController::class, 'HospitalFaq']);

    Route::post('/hospitalTeliDoctors', [HospitalController::class, 'hospitalTeliDoctors']);
    Route::post('/hospitalAutocompleteSearch', [HospitalController::class, 'hospitalAutocompleteSearch']);

    //Get Hospital

    Route::post('/slugview', [HospitalController::class, 'slugview']);
});