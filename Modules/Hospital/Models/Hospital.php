<?php

namespace Modules\Hospital\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Modules\Department\Models\Department;
use Modules\Doctor\Models\Doctor;
use Modules\Poll\Models\Poll;
use Modules\Enquiry\Models\Enquiry;
use Modules\Country\Models\Country;

class Hospital extends Model
{
    use HasFactory;
    use \Znck\Eloquent\Traits\BelongsToThrough;
    use Sluggable;

    protected $table = 'hospitals';

    protected $fillable =  [
        'title',
        'city_id',
        'slug',
        'content',
        'mini_content',
        'credintials',
        'image','logo',
        'banner_id',
        'diseases_id',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'problems_id',
        'success_story_id',
        'special_feature',
        'status',
        'address',
        'rating',
        'verified_badge',
        'quality_approved_certificate',
        'thumbline_image',
        'link',
        'added_by',
        'department_id'
    ];
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function country()
    {
        return $this->belongsToThrough(Country::class, ['Modules\Country\Models\State', 'Modules\Country\Models\City' ]);
    }

    public function state()
    {
        return $this->belongsToThrough('Modules\Country\Models\State', 'Modules\Country\Models\City');
    }

    public function city()
    {
        return $this->belongsTo('Modules\Country\Models\City', 'city_id', 'id');
    }

    public function departments()
    {
        return $this->hasMany('Modules\Department\Models\Department');
    }

    public function doctors()
    {
        return $this->hasManyThrough(Doctor::class, Department::class);
    }

    public function polls()
    {
        return $this->morphMany(Poll::class, 'pollable');
    }

    public function enquiries()
    {
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

    public function services()
    {
        return $this->hasMany('Modules\Services\Models\Services');
    }

    public function getCanDeleteAttribute()
    {
        if (
            $this->departments->count()
            ||$this->services->count()
        ) {
            return false;
        }
        return true;
    }
}