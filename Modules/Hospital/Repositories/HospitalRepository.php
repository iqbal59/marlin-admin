<?php


namespace Modules\Hospital\Repositories;

use Modules\Hospital\Models\Hospital;
use Illuminate\Database\Eloquent\Builder;

class HospitalRepository implements HospitalRepositoryInterface
{
    public function getForDatatable()
    {
        return Hospital::orderBy('id', 'DESC')->get();
    }

    public function all()
    {
        return Hospital::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    {
        if ($hospital =  Hospital::create($input)) {
            return $hospital;
        }
        return false;
    }

    public function get($deptId)
    {
        return Hospital::with('departments')->where('id', $deptId)->first();
    }

    public function getHospital($id)
    {
        return Hospital::where('status', 1)->where('id', $id)->first();
    }

    public function hospitals($id)
    {
        return Hospital::whereIn('id', $id)->get();
    }


    public function update(array $input)
    {
        $hospital = Hospital::find($input['id']);
        unset($input['id']);
        if ($hospital->update($input)) {
            return $hospital;
        }
        return false;
    }

    public function delete(string $id)
    {
        $hospital = Hospital::find($id);
        return $hospital->delete();
    }

    public function resetFile(string $id)
    {
        $hospital = Hospital::find($id);
        $hospital->logo = '';
        return $hospital->save();
    }

    public function resetFileImages(string $id)
    {
        $hospital = Hospital::find($id);
        $hospital->image = '';
        return $hospital->save();
    }

    public function resetThumblineImages(string $id)
    {
        $hospital = Hospital::find($id);
        $hospital->thumbline_image = '';
        return $hospital->save();
    }

    public function getImage($id, $type)
    {
        $hospital = Hospital::where('id', $id)->value($type);
        return $hospital;
    }

    public function getHospitals($cityId)
    {
        $hospital= Hospital::where('status', 1)->where('city_id', $cityId)->get();
        return $hospital;
    }

    public function getHospitalsBasedOnCountry($city)
    {
        $hospitals = Hospital::whereIn('city_id', $city)->get();
        return $hospitals;
    }

    public function HospitalsBasedOnCountry($city)
    {
        $hospitals = Hospital::where('city_id', $city)->get();
        return $hospitals;
    }

    public function getHospitalCount()
    {
        $hospitalCount = Hospital::count();
        return $hospitalCount;
    }

    public function getData($deptId)
    {
        return Hospital::where('id', $deptId)->first();
    }

    public function getHospitalBySlug($slug)
    {
        return Hospital::where('slug', $slug)->where('status', 1)->first();
    }


    public function hospitalsBasedOnCountryandSpecilization($city, $diseasesId)
    {
        $hospitals = Hospital::whereIn('city_id', $city)->where('diseases_id', 'like', '%' . $diseasesId . '%')->get();
        return $hospitals;
    }

    public function getHospitalsBasedOnSpecification($diseasesId)
    {
        return Hospital::where('diseases_id', 'like', '%' . $diseasesId . '%')->get();
    }

    public function similarHospitals($hospitalId, $cityId)
    {
        $hospitals = Hospital::where('status', 1)->where('city_id', $cityId)->where('id', '!=', $hospitalId)->get();
        return $hospitals;
    }

    public function bestHospitals($diseasesId)
    {
        $hospital = Hospital::select('hospitals.*')
                    ->where('status', 1)
                    ->where('hospitals.diseases_id', 'like', '%'.$diseasesId.'%')
                    ->join('polls', 'polls.pollable_id', '=', 'hospitals.id')
                    ->where('polls.pollable_type', 'Modules\Hospital\Models\Hospital')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $hospital;
    }

    public function bestHospitalsForProblems($problemsId)
    {
        $hospital = Hospital::select('hospitals.*')
                    ->where('status', 1)
                    ->where('hospitals.problems_id', 'like', '%'.$problemsId.'%')
                    ->join('polls', 'polls.pollable_id', '=', 'hospitals.id')
                    ->where('polls.pollable_type', 'Modules\Hospital\Models\Hospital')
                    ->orderBy('polls.value', 'DESC')
                    ->get();
        return $hospital;
    }

    public function getHospitalsIds($city)
    {
        $hospitals = Hospital::whereIn('city_id', $city)->pluck('id')->toArray();

        return $hospitals;
    }

    public function getHospitalsBasedOnCountry1($city)
    {
        $hospitals = Hospital::where('status', 1)->where('city_id', $city)->pluck('id')->toArray();
        return $hospitals;
    }

    public function searchHospital($searchText)
    {
        return $orders = Hospital::where('status', 1)->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('title', 'like', '%'.$searchText.'%');
            }
        })->get();
    }

    public function HospitalBasedOnCountry($city, $searchText)
    {
        return $hospitals = Hospital::where('status', 1)->where('city_id', $city)
        ->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('title', 'like', '%'.$searchText.'%');
            }
        })->get();
    }

    // public function HospitalBasedOnCountries($city,$searchText)
    // {
    //     // $hospitals = Hospital::where('city_id', $city)->get();
    //     // return $hospitals;

    //     return $hospitals = Hospital::where('city_id', $city)
    //     ->where(function (Builder $query) use ($searchText) {
    //         if ($searchText != "") {
    //             $query->where('title', 'like',  '%'.$searchText.'%');
    //         }
    //     })->get();
    // }

    public function getHospitalsBasedOnCountries($city, $searchText)
    {
        return $hospital = Hospital::where('status', 1)->whereIn('city_id', $city)
        ->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('title', 'like', '%'.$searchText.'%');
            }
        })
        ->get();
    }

    public function getActiveHospitalsBasedOnCountries($city, $searchText)
    {
        return $hospital = Hospital::whereIn('city_id', $city)->where('status', 1)
        ->where(function (Builder $query) use ($searchText) {
            if ($searchText != "") {
                $query->where('title', 'like', '%'.$searchText.'%');
            }
        })
        ->get();
    }

    public function getHospitalsBasedOnCity($city)
    {
        return $hospital = Hospital::where('status', 1)->whereIn('city_id', $city)
        ->get();
    }

    public function getHospitalsBasedDiseases($diseasesId)
    {
        $array = $diseasesId;
        $array = array_values(array_map('strval', $array));
        $hospital = Hospital::where('status', 1)->where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('diseases_id', $id);
            }
        })->get();
        return $hospital;
    }

    public function getHospitalsBasedOnProblems($problemsId)
    {
        $array = $problemsId;
        $array = array_values(array_map('strval', $array));
        $hospital = Hospital::where('status', 1)->where(function ($query) use ($array) {
            foreach ($array as $id) {
                $query->orWhereJsonContains('problems_id', $id);
            }
        })->get();
        return $hospital;
    }
}