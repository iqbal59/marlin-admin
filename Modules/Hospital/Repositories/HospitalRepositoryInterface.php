<?php

namespace Modules\Hospital\Repositories;

interface HospitalRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function save(array $input);

    public function get($deptId);

    public function getHospital($id);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);

    public function resetFileImages(string $id);

    public function resetThumblineImages(string $id);

    public function getImage($id, $type);

    public function getHospitals($cityId);

    public function getHospitalsBasedOnCountry($city);

    public function HospitalsBasedOnCountry($city);

    public function getHospitalCount();

    public function getData($deptId);

    public function getHospitalBySlug($slug);

    public function hospitalsBasedOnCountryandSpecilization($city, $diseasesId);

    public function getHospitalsBasedOnSpecification($diseasesId);

    public function similarHospitals($hospitalId, $cityId);

    public function bestHospitals($diseasesId);

    public function bestHospitalsForProblems($problemsId);

    public function getHospitalsIds($city);

    public function getHospitalsBasedOnCountry1($city);

    public function searchHospital($searchText);

    public function HospitalBasedOnCountry($city, $searchText);

    // public function HospitalBasedOnCountries($city,$searchText);

    public function getHospitalsBasedOnCountries($city, $searchText);

    public function getActiveHospitalsBasedOnCountries($city, $searchText);

    public function getHospitalsBasedOnCity($city);

    public function getHospitalsBasedDiseases($diseasesId);

    public function getHospitalsBasedOnProblems($problemsId);

    public function hospitals($id);
}