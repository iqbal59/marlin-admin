<?php

namespace Modules\Hospital\Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class HospitalDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '50',
                'title'      => 'hospital_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '51',
                'title'      => 'hospital_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '52',
                'title'      =>  'hospital_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '53',
                'title'      => 'hospital_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '54',
                'title'      => 'hospital_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],

        ];
        
        Permission::insert($permissions);
    }
}
