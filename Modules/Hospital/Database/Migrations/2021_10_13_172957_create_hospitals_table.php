<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->text('title')->nullable();
            $table->text('content')->nullable();
            $table->text('mini_content')->nullable();
            $table->text('credintials')->nullable();
            $table->text('location')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0 => InActive, 1 => Active');
            $table->string('image')->nullable();
            $table->string('logo')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->integer('city_id')->unsigned()->index();
            $table->integer('banner_id')->unsigned()->index();
            $table->json('diseases_id')->nullable();
            $table->json('problems_id')->nullable();
            $table->json('success_story_id')->nullable();
            $table->json('special_feature')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals');
    }
}
