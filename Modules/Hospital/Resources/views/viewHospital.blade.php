@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('panel.view_hospital') }} 
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    
    <div class="card-body">
        
        @include('hospital::partials.tabSection')
        
    </div>
</div>
@endsection