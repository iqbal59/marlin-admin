@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>


<div class="card">
    <div class="card-header">
        {{ trans('panel.add_hospital') }}
    </div>

    <div class="card-body">
        <form action="{{ route('admin.hospital.save') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" value="{{!empty($retrn_url) ? $retrn_url : ''}}" name="retrn_url">
            <input type="hidden" value="{{!empty($retrn_prms) ? $retrn_prms : ''}}" name="retrn_prms">
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">{{ trans('cruds.user.fields.name') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}">
                        @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                        <label for="city">{{ trans('panel.city') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <select id="city_id" name="city_id" class="js-states form-control">
                            <option value="">Select City</option>
                            @foreach($city as $data)
                            <option value="{{$data->id}}" name="country-dropdown" id="country-dropdown"
                                onclick="myFunction()">
                                {{$data->name}}
                            </option>
                            @endforeach
                        </select>
                        @if($errors->has('city_id'))
                        <em class="invalid-feedback">
                            {{ $errors->first('city_id') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <input type="text" name="banner_id" value="">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content"> {{ trans('panel.description') }}</label>
                        <textarea class="ckeditor form-control" id="content" name="content"
                            value="{{old('content')}}"></textarea>
                        @if($errors->has('content'))
                        <em class="invalid-feedback">
                            {{ $errors->first('content') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('credintials') ? 'has-error' : '' }}">
                        <label for="credintials"> Credentials</label>
                        <textarea class="ckeditor form-control" id="credintials" name="credintials"
                            value="{{old('credintials')}}"></textarea>
                        @if($errors->has('credintials'))
                        <em class="invalid-feedback">
                            {{ $errors->first('credintials') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image">{{ trans('panel.image') }} (370 px * 240 px)</label>
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                        <label for="logo">{{ trans('panel.logo') }} </label>
                        <input type="file" name="logo" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label for="address">Address</label>
                        <input type="text" id="address" name="address" class="form-control" value="{{old('address')}}">
                        @if($errors->has('address'))
                        <em class="invalid-feedback">
                            {{ $errors->first('address') }}
                        </em>
                        @endif
                    </div>
                </div>

                <div class="col-md-6">

                    <div class="form-group {{ $errors->has('banner_id') ? 'has-error' : '' }}">
                        <label for="banner">Banner<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <select id="banner_id" name="banner_id" class="js-states form-control">
                            <option value="">Select Banner</option>
                            @foreach($banner as $data)
                            <option value="{{$data->id}}" name="country-dropdown" id="country-dropdown"
                                onclick="myFunction()">
                                {{$data->title}}
                            </option>
                            @endforeach
                        </select>
                        @if($errors->has('banner_id'))
                        <em class="invalid-feedback">
                            {{ $errors->first('banner_id') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>


            <div class="row">


                <div class="col-md-6">

                    <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                        <label for="banner">Departments<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <div id="sucess-msg">
                            <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                                multiple="multiple">
                                <option value="">Select Department</option>
                                @foreach($departments as $department)
                                <option value="{{$department->id}}">
                                    {{$department->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->has('department_id'))
                        <em class="invalid-feedback">
                            {{ $errors->first('department_id') }}
                        </em>
                        @endif
                    </div>
                </div>
            </div>




            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>

    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
$(document).ready(function() {
    $("#city_id").select2({
        placeholder: "Select City",
        allowClear: true
    });
});
$(document).ready(function() {
    $("#banner_id").select2({
        placeholder: "Select Banner",
        allowClear: true
    });

    $("#department_id").select2({
        placeholder: "Select Department",
        allowClear: true
    });
});
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.ckeditor').ckeditor();
});
</script>
@endsection