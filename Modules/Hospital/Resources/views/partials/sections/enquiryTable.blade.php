<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #enquiry_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }

    </style>
<div class="card">
    <div class="card-header">
        Enquiry of {{ $hospital->title }}
    </div> 

    <div class="card-body">
        <div class="table-responsive">
            <table id="enquiry" class="display nowrap table" style="width:100%">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($enquiry as $key => $enq)
                        <tr >
                            <td>{{ $enq->email ?? '' }}</td>

                           
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.enquiry.HospitalEnquiryview', ['id' => $enq->id]) }}">
                                    {{ trans('global.view') }}
                                </a>  

                                <form action="{{ route('admin.enquiry.delete',  ['id' => $enq->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$enq->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="retrn_url" value="admin.hospital.view">
                                    <input type="hidden" name="retrn_prms" value={{$hospital->id}}>
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
   $(document).ready(function() {
    $('#enquiry').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection



