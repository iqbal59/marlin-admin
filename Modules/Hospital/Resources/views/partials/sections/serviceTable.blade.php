<div class="card">
    <div class="card-header">
        Services of {{ $hospital->title }}
        @can('services_create')
        <a class="btn btn-warning float-right" href="{{route('admin.services.add', ['id' => $hospital->id])}}">
            Add Services
        </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="tnc table table-bordered table-striped table-hover datatable datatable-Services">
                <thead>
                    <tr>
                        <th class="tn2">{{ trans('cruds.user.fields.id') }}</th>
                        <th class="tn3">{{ trans('cruds.user.fields.name') }}</th>
                        <th class="tn4">Status</th>
                        <th class="tn5">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($services as $key => $service)
                        <tr data-entry-id="{{ $service->id }}">
                            <td class="tn2">{{ $key+1 }}</td>
                            <td class="tn3">{{ $service->name ?? '' }}</td>
                            <td class="tn4">
                                @if($service->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td class="tn5">
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.services.view', ['id' => $service->id]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.services.edit', ['id' => $service->id , 'hospital_id' => $hospital->id]) }}">
                                    {{ trans('global.edit') }}
                                </a>   

                                <form action="{{ route('admin.services.delete',  ['id' => $service->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$service->id}}">
                                    <input type="hidden" name="hospital_id" value="{{$hospital->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  $('.datatable-Services:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection

<style>
    .dataTables_scrollHeadInner{
        width: 100% !important;
    }
.tn1{
    width: 10%;
}
.tn2{
    width: 10%;
}

.tn3{
    width: 20%;
}

.tn4{
    width: 30%;
}

.tn5{
    width: 30%;
}


</style>