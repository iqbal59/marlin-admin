<style>
.table.video-table th, .table.video-table td {
    line-height: 24px;
    white-space: normal;
}
</style>
<div class="mb-2">
    <table class="table table-bordered table-striped video-table">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $hospital->id }}</td>
            </tr>
            <tr>
                <th>{{ trans('cruds.user.fields.name') }}</th>
                <td>{{ $hospital->title }}</td>
            </tr>
            <tr>
                <th>Country</th>
                <td>{{ $hospital->country->name }}</td>
            </tr>
            <tr>
                <th>City</th>
                <td>{{ $hospital->city->name }}</td>
            </tr>
            @if($hospital->content != "")
            <tr>
                <th>Description</th>
                <td>{!! $hospital->content !!}</td>
            </tr>
            @endif
            @if($hospital->min_content != "")
            <tr>
                <th>Short Description</th>
                <td>{!! $hospital->min_content !!}</td>
            </tr>
            @endif
            @if($hospital->credintials != "")
            <tr>
                <th>Credentials</th>
                <td>{!! $hospital->credintials !!}</td>
            </tr>
            @endif
            @if($hospital->meta_title != "")
            <tr>
                <th>Meta Title</th>
                <td>{{ $hospital->meta_title }}</td>
            </tr>
            @endif
            @if($hospital->meta_description != "")
            <tr>
                <th>Meta Description</th>
                <td>{{ $hospital->meta_description }}</td>
            </tr>
            @endif
            @if($hospital->meta_keywords != "")
            <tr>
                <th>Meta Keywords</th>
                <td>{{ $hospital->meta_keywords }}</td>
            </tr> 
            @endif
            @if($hospital->address != "")
            <tr>
                <th>Address</th>
                <td>{{ $hospital->address }}</td>
            </tr> 
            @endif
            @if($hospital->rating != "")
            <tr>
                <th>Rating</th>
                <td>{{ $hospital->rating }}</td>
            </tr> 
            @endif
            @if($hospital->logo != "")
            <tr>
                <th>Logo</th>
                <td><img src="{{asset('storage/'.$hospital->logo)}}" alt="" width="500px"></td>
            </tr>
            @endif
            @if($hospital->image != "")
            <tr>
                <th>Image</th>
                <td><img src="{{asset('storage/'.$hospital->image)}}" alt="" width="500px"></td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif
        </tbody>
    </table><br>
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>  Total Departments </th>
                <td>  {{(count($departments))}}</td>
            </tr>
            <tr>
                <th>  Total Services </th>
                <td>  {{(count($services))}}</td>
            </tr>
        </tbody>
    </table><br>
</div>