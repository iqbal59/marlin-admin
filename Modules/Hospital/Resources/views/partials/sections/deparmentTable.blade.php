<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #example_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }

    </style>
<div class="card">
    <div class="card-header">
        Departments of {{ $hospital->title }}
        @can('department_create')
        <a class="btn btn-warning float-right" href="{{route('admin.department.add', ['id' => $hospital->id])}}">
            Add Department
        </a>
        @endcan
    </div> 

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class="display nowrap table" style="width:100%">
                <thead>
                    <tr>
                        {{-- <th>{{ trans('cruds.user.fields.id') }}</th> --}}
                        <th>{{ trans('cruds.user.fields.name') }}</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($departments as $key => $department)
                        <tr >
                            {{-- <td>{{ $key+1 }}</td> --}}
                            <td>{{ $department->name ?? '' }}</td>
                            <td>
                                @if($department->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.department.view', ['id' => $department->id]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.department.edit', ['id' => $department->id, 'hospital_id' => $hospital->id ]) }}">
                                    {{ trans('global.edit') }}
                                </a>   
                                <form action="{{ route('admin.department.delete',  ['id' => $department->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="id" value="{{$department->id}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    @if($department->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        <input type="hidden" name="retrn_url" value="admin.hospital.view">
                                        <input type="hidden" name="retrn_prms" value={{$hospital->id}}>
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this department has child!"> Delete</button>
                                        @endif 
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
   $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection



