<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #example2_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }

    </style>
<div class="card">
    <div class="card-header">
        Faq of Hospital
        @can('faq_create')
        <a class="btn btn-warning float-right" href="{{route('admin.faq.add', ['Id' => $hospital->id,'type'=>'Hospital','retrn_url'=> "admin.hospital.view",'retrn_prms' => $hospital->id ])}}">
            Add Faq
        </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example2" class="display nowrap table" style="width:100%">
                <thead>
                    <tr>
                        {{-- <th>{{ trans('cruds.user.fields.id') }}</th> --}}
                        <th>Title</th>
                        {{-- <th>Subject</th>
                        <th>Question</th>
                        <th>Answer By</th>
                        <th>Date</th>   --}}
                        <th>Actions</th> 
                    </tr>
                </thead>
                <tbody>
                    @foreach($faqs as $key => $faq)
                        <tr data-entry-id="{{ $faq->id }}">
                            {{-- <td>{{ $key+1 }}</td> --}}
                            <td>{{ $faq->title ?? '' }}</td>
                            {{-- <td>{{ $faq->subject ?? '' }}</td>
                            <td>{{ $faq->question ?? '' }}</td>
                            <td>{{ $faq->answerBy ?? '' }}</td>
                            <td>{{ $faq->entrydate ?? '' }}</td>         --}}
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.faq.view', ['id' => $faq->id]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.faq.edit', ['id' => $faq->id, 'hospital_id' => $faq->id ,'retrn_url'=> "admin.hospital.view",'retrn_prms' => $hospital->id]) }}">
                                    {{ trans('global.edit') }}
                                </a>   

                                <form action="{{ route('admin.faq.delete',  ['id' => $faq->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$faq->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="retrn_url" value="admin.hospital.view">
                                    <input type="hidden" name="retrn_prms" value={{$hospital->id}}>
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>                    
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
   $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection



