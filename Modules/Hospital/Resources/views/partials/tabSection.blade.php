<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-department-tab" 
                    data-toggle="pill" 
                    data-pillid="departments"
                    href="#pills-department" 
                    role="tab" 
                    aria-controls="pills-department" 
                    aria-selected="false">Departments</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-doctor-tab" 
                    data-toggle="pill" 
                    href="#pills-doctor" 
                    role="tab" 
                    aria-controls="pills-doctor" 
                    aria-selected="false">Doctors</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-service-tab" 
                    data-toggle="pill" 
                    href="#pills-service" 
                    role="tab" 
                    aria-controls="pills-service" 
                    aria-selected="false">Services</a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-enquiry-tab" 
                    data-toggle="pill" 
                    href="#pills-enquiry" 
                    role="tab" 
                    aria-controls="pills-enquiry" 
                    aria-selected="false">Enquiry</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-faq-tab" 
                    data-toggle="pill" 
                    href="#pills-faq" 
                    role="tab" 
                    aria-controls="pills-faq" 
                    aria-selected="false">Faq</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('hospital::partials.sections.basicDetails')
            </div>
            <div class="tab-pane fade show" id="pills-department" role="tabpanel" aria-labelledby="pills-department-tab">
                @include('hospital::partials.sections.deparmentTable')
            </div>
            <div class="tab-pane fade" id="pills-doctor" role="tabpanel" aria-labelledby="pills-doctor-tab">
                @include('hospital::partials.sections.doctorTable')
            </div>
            <div class="tab-pane fade" id="pills-service" role="tabpanel" aria-labelledby="pills-service-tab">
                @include('hospital::partials.sections.serviceTable')
            </div>
            <div class="tab-pane fade" id="pills-enquiry" role="tabpanel" aria-labelledby="pills-enquiry-tab">
                @include('hospital::partials.sections.enquiryTable')
            </div>
            <div class="tab-pane fade" id="pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">
                @include('hospital::partials.sections.faqTable')
            </div>
        </div>
    </div>
</div>