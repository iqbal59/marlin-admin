@extends('layouts.admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<html>

<body>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <style>
    input[type="number"] {
        -webkit-appearance: textfield;
        -moz-appearance: textfield;
        appearance: textfield;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
    }

    .number-input {
        border: 2px solid #ddd;
        display: inline-flex;
    }

    .number-input,
    .number-input * {
        box-sizing: border-box;
    }

    .number-input button {
        outline: none;
        -webkit-appearance: none;
        background-color: transparent;
        border: none;
        align-items: center;
        justify-content: center;
        width: 3rem;
        height: 2rem;
        cursor: pointer;
        margin: 0;
        position: relative;
    }

    .number-input button:before,
    .number-input button:after {
        display: inline-block;
        position: absolute;
        content: '';
        width: 1rem;
        height: 2px;
        background-color: #212121;
        transform: translate(-50%, -50%);
    }

    .number-input button.plus:after {
        transform: translate(-50%, -50%) rotate(90deg);
    }

    .number-input input[type=number] {
        font-family: sans-serif;
        max-width: 3rem;
        padding: .5rem;
        border: solid #ddd;
        border-width: 0 2px;
        font-size: 1rem;
        height: 2rem;
        font-weight: bold;
        text-align: center;
    }

    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }

    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }

    span.on {
        width: 95px;
    }

    .custom-switch {
        padding-left: 0;
    }

    .custom-switch .custom-control-label::before {
        width: 2rem;
    }

    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
    }

    .badge-contr span.off {
        width: 70px;
        float: left;
    }

    .badge-contr span.on {
        float: none;
    }

    .hospital-edit-cn span.select2-selection.select2-selection--multiple:after {
        border-color: #888 transparent transparent transparent;
        border-style: solid;
        border-width: 5px 4px 0 4px;
        height: 0;
        right: 5px;
        margin-left: -4px;
        margin-top: -2px;
        position: absolute;
        top: 50%;
        width: 0;
        content: "";
    }

    span.off {
        width: 70px;
        float: left;
    }
    </style>
    @foreach($hospital->polls as $poll)
    @php $pollValue = $poll['value']; @endphp
    @endforeach

    <div class="card hospital-edit-cn">
        <div class="card-header">
            {{ trans('global.edit') }} {{ trans('panel.hospital') }}
        </div>

        <div class="card-body">
            <form action="{{ route('admin.hospital.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$hospital->id}}">
                <input type="hidden" name="retrn_url" value="{{$retrn_url}}">
                <input type="hidden" name="retrn_prms" value="{{$retrn_prms}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="title">{{ trans('panel.title') }}<i
                                    class="mdi mdi-flag-checkered text-danger "></i></label>
                            <input type="text" id="title" name="title" class="form-control"
                                value="{{ old('title', isset($hospital) ? $hospital->title : '') }}" required>
                            @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                            <label for="city_id">City <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                            <select id="city_id" name="city_id" class="js-city form-control">
                                @foreach($city as $data)
                                <option value="{{$data->id}} " {{ old('city_id', $hospital->city_id) == $data->id ?
                                    "selected" : "" }} name="country-dropdown" id="country-dropdown"
                                    onclick="myFunction()">
                                    {{$data->name}}
                                </option>
                                @endforeach
                            </select>
                            @if($errors->has('city_id'))
                            <em class="invalid-feedback">{{ $errors->first('city_id') }}</em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </div>
                </div>


                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    <label for="content">Slug Url </label>

                    <input type="text" id="slug" name="slug" class="form-control"
                        value="{{ old('slug', isset($hospital) ? $hospital->slug : '') }}" required>
                    @if($errors->has('slug'))
                    <em class="invalid-feedback">
                        {{ $errors->first('slug') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>


                <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                    <label for="content">{{ trans('panel.description') }} </label>
                    <textarea class="ckeditor form-control" id="content" name="content" required>
                {!! old('content', isset($hospital) ? $hospital->content : '') !!}
                </textarea>

                    @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>

                {{-- <div class="form-group {{ $errors->has('min_content') ? 'has-error' : '' }}">
                <label for="min_content"> Short Description </label>
                <textarea class="ckeditor form-control" id="min_content" name="min_content" required>
                {!! old('min_content', isset($hospital) ? $hospital->mini_content : '') !!}
                </textarea>

                @error('min_content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('min_content'))
                <em class="invalid-feedback">
                    {{ $errors->first('min_content') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
        </div> --}}
        <div class="form-group {{ $errors->has('quality_approved_certificate') ? 'has-error' : '' }}">
            <label for="quality_approved_certificate">Qwality Approved Certificate </label>
            <textarea class="ckeditor form-control" id="quality_approved_certificate"
                name="quality_approved_certificate" required>
                @if($hospital->quality_approved_certificate == null)
                <p><img alt="" src="https://astermimskottakkal.ipixsolutions.net/storage/cms_files/certification-placeholder.png" style="height:117px; width:117px" /></p>                @endif
                </textarea>

            @error('quality_approved_certificate')<small class="form-text text-danger">{{ $message
                        }}</small>@enderror
            @if($errors->has('quality_approved_certificate'))
            <em class="invalid-feedback">
                {{ $errors->first('quality_approved_certificate') }}
            </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                    <label for="logo"> {{ trans('panel.logo') }} </label>

                    @if(Storage::disk('public')->exists($hospital->logo))
                    <img class="preview" src="{{asset('storage/'.$hospital->logo)}}" alt="" width="150px">
                    <a href="{{route('admin.hospital.remove_file',['id'=>$hospital->id])}}"
                        class="confirm-delete text-danger">
                        <i class="material-icons pink-text">clear</i>
                    </a>
                    @else
                    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                        <input type="file" name="logo" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('logo'))
                        <em class="invalid-feedback">
                            {{ $errors->first('logo') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label for="image">Image </label>
                    @if(Storage::disk('public')->exists($hospital->image))
                    <img class="preview" src="{{asset('storage/'.$hospital->image)}}" alt="" width="150px">
                    <a href="{{route('admin.hospital.remove_file_image',['id'=>$hospital->id])}}"
                        class="confirm-delete text-danger">
                        <i class="material-icons pink-text">clear</i>
                    </a>
                    @else
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                    <label for="meta_title">Meta Title </label>
                    <input type="text" id="meta_title" name="meta_title" class="form-control"
                        value="{{ old('meta_title', isset($hospital) ? $hospital->meta_title : '') }}">
                    @if($errors->has('meta_title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_title') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                    <label for="meta_keywords">Meta Keyword </label>
                    <input type="text" id="meta_keywords" name="meta_keywords" class="form-control"
                        value="{{ old('meta_keywords', isset($hospital) ? $hospital->meta_keywords : '') }}">
                    @if($errors->has('meta_keywords'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_keywords') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
        </div>
        <div class="form-group {{ $errors->has('credintials') ? 'has-error' : '' }}">
            <label for="credintials">Credentials</label>
            <textarea class="ckeditor form-control" id="credintials" name="credintials" required>
                {!! old('credintials', isset($hospital) ? $hospital->credintials : '') !!}
                </textarea>
            @error('credintials')<small class="form-text text-danger">{{ $message }}</small>@enderror
            @if($errors->has('credintials'))
            <em class="invalid-feedback">
                {{ $errors->first('credintials') }}
            </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>

        <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
            <label for="meta_description">Meta Description</label>
            <textarea class="ckeditor form-control" id="meta_description" name="meta_description" required>
                {!! old('meta_description', isset($hospital) ? $hospital->meta_description : '') !!}
                </textarea>
            @error('meta_description')<small class="form-text text-danger">{{ $message }}</small>@enderror
            @if($errors->has('meta_description'))
            <em class="invalid-feedback">
                {{ $errors->first('meta_description') }}
            </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="name">Diseases</label>
                @if($selecteValue == null)
                <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                    @foreach($diseases as $data)
                    <option value="{{$data->id}}">{{$data->title}}</option>
                    @endforeach
                </select>
                @else
                <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                    @foreach($diseases as $data)
                    <option value="{{$data->id}}" @if(in_array($data->id, $selecteValue)) selected
                        @endif>{{$data->title}}</option>
                    @endforeach
                </select>
                @endif
            </div>
            <div class="col-md-6">
                <label for="name">Problems </label>
                @if($selecteProblems == null)
                <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                    @foreach($problems as $data)
                    <option value="{{$data->id}}">{{$data->title}}</option>
                    @endforeach
                </select>
                @else
                <select class="js-example-basic-multiple" name="problems[]" multiple="multiple">
                    @foreach($problems as $data)
                    <option value="{{$data->id}}" @if(in_array($data->id, $selecteProblems)) selected
                        @endif>{{$data->title}}</option>
                    @endforeach
                </select>
                @endif
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label for="Poll">Poll</label>
                <div class="form-group">
                    <input type="number" id="poll" name="value" min="1" max="99"
                        value="{{ old('value', $pollValue ) }}">
                </div>
            </div>
            <div class="col-md-6">
                <label for="name">Patient Stories </label>
                @if($selectepatientStories == null)
                <select class="js-example-basic-multiple" name="patientstories[]" multiple="multiple">
                    @foreach($patientStories as $data)
                    <option value="{{$data->id}}">{{$data->title}}</option>
                    @endforeach
                </select>
                @else
                <select class="js-example-basic-multiple" name="patientstories[]" multiple="multiple">
                    @foreach($patientStories as $data)
                    <option value="{{$data->id}}" @if(in_array($data->id, $selectepatientStories)) selected
                        @endif>{{$data->title}}</option>
                    @endforeach
                </select>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label for="name">Special Features </label>
                @if($selecteSpecialFeature == null)
                <select class="js-example-basic-multiple" name="specialfeature[]" multiple="multiple">
                    @foreach($specialFeature as $data)
                    <option value="{{$data->id}}">{{$data->title}}</option>
                    @endforeach
                </select>
                @else
                <select class="js-example-basic-multiple" name="specialfeature[]" multiple="multiple">
                    @foreach($specialFeature as $data)
                    <option value="{{$data->id}}" @if(in_array($data->id, $selecteSpecialFeature)) selected
                        @endif>{{$data->title}}</option>
                    @endforeach
                </select>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Address</label>
                    <input type="text" id="address" name="address" class="form-control"
                        value="{{ old('address', isset($hospital) ? $hospital->address : '') }}">
                    @if($errors->has('address'))
                    <em class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </em>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('banner_id') ? 'has-error' : '' }}">
                    <label for="banner_id">Banner <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                    <select id="banner_id" name="banner_id" class="js-city form-control">
                        @foreach($banner as $data)
                        <option value="{{$data->id}} " {{ old('banner_id', $hospital->banner_id) == $data->id ?
                                    "selected" : "" }} name="country-dropdown" id="country-dropdown"
                            onclick="myFunction()">
                            {{$data->title}}
                        </option>
                        @endforeach
                    </select>
                    @if($errors->has('banner_id'))
                    <em class="invalid-feedback">{{ $errors->first('banner_id') }}</em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('panel.rating') }}</i> </label>
                    <select id="rating" name="rating" class="js-states form-control">
                        <option value="1" @if(old('rating', $hospital->rating)==1) selected @endif>1</option>
                        <option value="2" @if(old('rating', $hospital->rating)==2) selected @endif>2</option>
                        <option value="3" @if(old('rating', $hospital->rating)==3) selected @endif>3</option>
                        <option value="4" @if(old('rating', $hospital->rating)==4) selected @endif>4</option>
                        <option value="5" @if(old('rating', $hospital->rating)==5) selected @endif>5</option>
                    </select>

                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('thumbline_image') ? 'has-error' : '' }}">
                    <label for="thumbline_image">Thumbline Image </label>
                    @if(Storage::disk('public')->exists($hospital->thumbline_image))
                    <img class="preview" src="{{asset('storage/'.$hospital->thumbline_image)}}" alt="" width="150px">
                    <a href="{{route('admin.hospital.remove_thumb_line_image',['id'=>$hospital->id])}}"
                        class="confirm-delete text-danger">
                        <i class="material-icons pink-text">clear</i>
                    </a>
                    @else
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <input type="file" name="thumbline_image" id="input-file-now-banner" class="dropify validate"
                            data-default-file="" />
                        @if($errors->has('thumbline_image'))
                        <em class="invalid-feedback">
                            {{ $errors->first('thumbline_image') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                    <label for="link"> Link </label>
                    <input type="text" id="link" name="link" class="form-control"
                        value="{{ old('link', isset($hospital) ? $hospital->link : '') }}" placeholder="youtube link">
                    @if($errors->has('link'))
                    <em class="invalid-feedback">
                        {{ $errors->first('link') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('verified_badge') ? 'has-error' : '' }}">
                    <label for="designation">MMA certified</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('verified_badge', $hospital->verified_badge == 1) ?
                                "checked" : "" }} class="custom-control-input" id="verified_badge"
                            name="verified_badge">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="verified_badge"></label>
                        <span class="on">Yes</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $hospital->status == 1) ? "checked" : "" }}
                            class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                    <label for="name"> Department <i class="mdi mdi-flag-checkered text-danger "></i></label>


                    @if($selectedDepartments == null)
                    <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                        multiple="multiple">
                        @foreach($department as $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                    @else
                    <select id="department_id" name="department_id[]" class="js-example-basic-multiple"
                        multiple="multiple">
                        @foreach($department as $data)
                        <option value="{{$data->id}}" @if(in_array($data->id, $selectedDepartments)) selected
                            @endif>{{$data->name}}</option>
                        @endforeach
                    </select>
                    @endif

                    @if($errors->has('department_id'))
                    <em class="invalid-feedback">{{ $errors->first('department_id') }}</em>
                    @endif
                </div>
            </div>
        </div>

        <br>
        <div>
            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
        </div>
        </form>


    </div>
    </div>
    @endsection
    @section('scripts')
    @parent
    <script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
    </script>
    <script>
    $(document).ready(function() {
        $("#city_id").select2({
            placeholder: "Select City",
            allowClear: true
        });
    });
    $(document).ready(function() {
        $("#banner_id").select2({
            placeholder: "Select Banner",
            allowClear: true
        });
        $("#rating").select2({
            placeholder: "Select Rating",
            allowClear: true
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.ckeditor').ckeditor();
    });
    </script>
    @endsection