<?php

namespace Modules\Hospital\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Hospital extends Model
{
    use HasFactory;

    protected $fillable = ['title','address','city_id','problems_id','mini_content','content','credintials','image','logo','banner_id','diseases_id','meta_title','meta_description','meta_keywords','success_story_id','special_feature','status','rating','verified_badge','quality_approved_certificate','thumbline_image','link'];
    
    protected static function newFactory()
    {
        return \Modules\Hospital\Database\factories\HospitalFactory::new();
    }
}
