<?php

namespace Modules\Hospital\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('hospital_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
