<?php

namespace Modules\Hospital\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('hospital_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:50|min:2',
            'city_id' => 'required',
            'banner_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'city_id.required' => 'Select Your :attribute',
            'banner_id.required' => 'Select Your :attribute',

        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'city_id' => 'City',
            'banner_id' => 'Banner',

        ];
    }
}
