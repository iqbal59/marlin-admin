<?php

namespace Modules\Hospital\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('hospital_edit') ? true : false;
    }

    public function rules()
    { 
        if($this->type == null){
            return [
                'title' => 'required|max:50|min:2',
                'city_id' => 'required',
                'banner_id' => 'required',
            ];
        }else{
            return [
                'title' => 'required|max:50|min:2',
                'city_id' => 'required',
                'banner_id' => 'required',
                'thumbline_image' => 'required',
                'link' => 'required'
            ];
        }
        
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'city_id.required' => 'Select Your :attribute',
            'banner_id.required' => 'Select Your :attribute',
            'thumbline_image.required' => 'Select Your :attribute',
            'link.required' => 'Select Your :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'link' => 'Link',
            'thumbline_image' => 'Thumbline Image',
            'title' => 'Title',
            'city_id' => 'City',
            'banner_id' => 'Banner',
        ];
    }
}
