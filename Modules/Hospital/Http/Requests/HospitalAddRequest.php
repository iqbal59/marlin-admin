<?php

namespace Modules\Hospital\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HospitalAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('hospital_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
