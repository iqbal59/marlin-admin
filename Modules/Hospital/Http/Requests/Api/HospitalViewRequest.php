<?php

namespace Modules\Hospital\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class HospitalViewRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //'hospital_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'hospital_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'hospital_id' => 'Doctor Id',
        ];
    }
}
