<?php

namespace Modules\Hospital\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Hospital\Models\Hospital;
use Modules\Enquiry\Models\Enquiry;
use Modules\Hospital\Http\Requests\Api\HospitalViewRequest;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
use Modules\Hospital\Http\Requests\Api\HospitalFaqRequest;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Services\Repositories\ServicesRepositoryInterface as ServicesRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Exception;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Illuminate\Support\Facades\Storage;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface as BannerRepository;
use Modules\Country\Repositories\Includes\State\StateRepositoryInterface as StateRepository;
use Modules\Country\Repositories\Includes\Country\CountryRepositoryInterface as CountryRepository;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface as BannerContentRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Settings\Repositories\SettingsRepositoryInterface as SettingsRepository;
use Cohensive\Embed\Facades\Embed;

class HospitalController extends Controller
{
    public function listHospital(HospitalRepository $hosptlRepo, Request $request, CityRepository $cityRepo)
    {
        try {
            if ((isset($request->country_id)) && (isset($request->city_id)) && (isset($request->diseases_id))) {
                $cityDetails = $cityRepo->get($request->city_id);
                if ($cityDetails->country_id == $request->country_id) {
                    $city[]=$request->city_id;
                    $diseasesId = json_encode($request->diseases_id);
                    $hospitals = $hosptlRepo->hospitalsBasedOnCountryandSpecilization($city, $diseasesId);
                } else {
                    $response = ['status' => false, 'message' => 'Invalid city id'];
                    return response()->json($response, 200);
                }
            } elseif (isset($request->country_id) && (isset($request->city_id))) {
                $cityDetails = $cityRepo->get($request->city_id);
                if ($cityDetails->country_id == $request->country_id) {
                    $city[]=$request->city_id;
                    $hospitals = $hosptlRepo->getHospitalsBasedOnCountry($city);
                } else {
                    $response = ['status' => false, 'message' => 'Invalid city id'];
                    return response()->json($response, 200);
                }
            } elseif (isset($request->country_id) && (isset($request->diseases_id))) {
                $city = $cityRepo->getActiveCityByCountry($request->country_id);
                $diseasesId = json_encode($request->diseases_id);
                $hospitals = $hosptlRepo->hospitalsBasedOnCountryandSpecilization($city, $diseasesId);
            } elseif (isset($request->city_id) && (isset($request->diseases_id))) {
                $city[]=$request->city_id;
                $diseasesId = json_encode($request->diseases_id);
                $hospitals = $hosptlRepo->hospitalsBasedOnCountryandSpecilization($city, $diseasesId);
            } elseif (isset($request->country_id)) {
                $city = $cityRepo->getActiveCityByCountry($request->country_id);
                $hospitals = $hosptlRepo->getHospitalsBasedOnCountry($city);
            } elseif (isset($request->city_id)) {
                $hospitals = $hosptlRepo->getHospitals($request->city_id);
            } elseif (isset($request->diseases_id)) {
                $diseasesId = json_encode($request->diseases_id);
                $hospitals = $hosptlRepo->getHospitalsBasedOnSpecification($diseasesId);
            } else {
                $hospitals = $hosptlRepo->all();
            }
            $data = compact("hospitals");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }


    public function view(HospitalViewRequest $request, HospitalRepository $hosptlRepo, DepartmentRepository $deptRepo, ServicesRepository $serviceRepo, FaqRepository $faqRepo, CityRepository $cityRepo, StateRepository $stateRepo, CountryRepository $countryRepo, BannerContentRepository $bannerContentRepo, BannerRepository $bannerRepo, SettingsRepository $settingsRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospital = $hosptlRepo->getData($hospitalId);
            if (!$hospital) {
                $response = ['status' => false, 'message' => 'The selected hospital id is invalid'];
                return response()->json($response, 200);
            }
            $cityDetails = $cityRepo->get($hospital->city_id);
            $stateDetails = $stateRepo->get($cityDetails->state_id);
            $countryDetails = $countryRepo->get($cityDetails->country_id);
            $hospital->city_name = $cityDetails->name;
            $hospital->state_name = $stateDetails->name;
            $hospital->country_name = $countryDetails->name;
            $bannerId = $hospital->banner_id;

            $bannerDetails = $bannerRepo->get($bannerId);
            // $today = date('Y-m-d');
            // if($today >= $bannerDetails->start_date && $today <= $bannerDetails->end_date){
            $bannerContent = $bannerContentRepo->getBanner($bannerId);
            $hospital->bannerContent = $bannerContent;
            // }else{
            //     $defaultBannerImage = $settingsRepo->getByKey('default_banner_image');
            //     $hospital->bannerContent = $defaultBannerImage;
            // }
            $data = compact("hospital");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }


    public function slugview(HospitalViewRequest $request, HospitalRepository $hosptlRepo)
    {
        try {
            $slug = $request->slug;
            $hospital = $hosptlRepo->getHospitalBySlug($slug);
            if (!$hospital) {
                $response = ['status' => false, 'message' => 'The selected hospital id is invalid'];
                return response()->json($response, 200);
            }

            $data = compact("hospital");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function hospitalDoctors(HospitalViewRequest $request, HospitalRepository $hosptlRepo, DepartmentRepository $deptRepo, DoctorRepository $doctorRepo, FaqRepository $faqRepo, CityRepository $cityRepo, CountryRepository $countryRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $doctors = $doctorRepo->getHospitals($hospitalId);
            $details = $doctors->map(function ($doctors, $key) use ($hospitalId, $deptRepo, $hosptlRepo, $cityRepo, $countryRepo) {
                $hospitalDetails = $hosptlRepo->get($hospitalId);
                $city = $cityRepo->get($hospitalDetails->city_id);
                $country = $countryRepo->get($city->country_id);
                $doctors->country_name  = $country->name;
                return $doctors;
            });


            // $departments = $deptRepo->getDepartmentsHospital($hospitalId);
            // $doctors = $doctorRepo->getDepartmentDoctor($departments);
            // $details = $doctors->map(function ($doctors, $key) use($deptRepo,$hosptlRepo,$cityRepo,$countryRepo)  {
            //     $department = $deptRepo->get($doctors->department_id);
            //     $hospital= $hosptlRepo->get($department->hospital_id);
            //     $city = $cityRepo->get($hospital->city_id);
            //     $country = $countryRepo->get($city->country_id);
            //     $doctors->department  = $department->name;
            //     $doctors->country_name  = $country->name;
            //     return $doctors;
            // });
            $data = compact("doctors");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
        // try {
        //     $hospitalId = $request->hospital_id;
        //     $departments = $deptRepo->getDepartmentsHospital($hospitalId);
        //     $doctors = $doctorRepo->getDepartmentDoctor($departments);
        //     $details = $doctors->map(function ($doctors, $key) use($deptRepo,$hosptlRepo,$cityRepo,$countryRepo)  {
        //         $department = $deptRepo->get($doctors->department_id);
        //         $hospital= $hosptlRepo->get($department->hospital_id);
        //         $city = $cityRepo->get($hospital->city_id);
        //         $country = $countryRepo->get($city->country_id);
        //         $doctors->department  = $department->name;
        //         $doctors->country_name  = $country->name;
        //         return $doctors;
        //     });

        //     $data = compact("doctors");
        //     $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
        //     return response()->json($response, 200);
        // } catch (Exception $e) {
        //     $response = ['status' => false, 'message' => $e->getMessage()];
        //     return response()->json($response, 200);
        // }
    }

    public function hospitalDepartments(HospitalViewRequest $request, HospitalRepository $hosptlRepo, DepartmentRepository $deptRepo, DoctorRepository $doctorRepo, FaqRepository $faqRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospital = $hosptlRepo->getData($hospitalId);

            $department_ids=$hospital->department_id?json_decode($hospital->department_id):array();
            // $departments = $deptRepo->getDepartmentsHospital($department_ids);
            $dept = $deptRepo->getDepartments($department_ids);
            $data = compact("dept");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function similarHospitals(HospitalRepository $hosptlRepo, HospitalViewRequest $request, CityRepository $cityRepo, StateRepository $stateRepo, CountryRepository $countryRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospital = $hosptlRepo->getData($hospitalId);
            $similarHospitals = $hosptlRepo->similarHospitals($hospitalId, $hospital->city_id);
            $details = $similarHospitals->map(function ($similarHospitals, $key) use ($cityRepo, $stateRepo, $countryRepo) {
                $cityDetails = $cityRepo->get($similarHospitals->city_id);
                $stateDetails = $stateRepo->get($cityDetails->state_id);
                $countryDetails = $countryRepo->get($cityDetails->country_id);
                $similarHospitals->city_name  = $cityDetails->name;
                $similarHospitals->state_name  = $stateDetails->name;
                $similarHospitals->country_name  = $countryDetails->name;
                return $similarHospitals;
            });
            $data = compact("similarHospitals");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function successStories(HospitalRepository $hosptlRepo, HospitalViewRequest $request, CmsRepository $cmsRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospital = $hosptlRepo->getData($hospitalId);
            if ($hospital->success_story_id != "null") {
                $successStory =json_decode($hospital->success_story_id);
                $successStories = $cmsRepo->successStories($successStory);
                $details = $successStories->map(function ($successStories, $key) {
                    if ($successStories->link != null) {
                        $iframe = Embed::make($successStories->link)->parseUrl()->getIframe();
                        $parts = parse_url($iframe);
                        $embedUrl = trim($parts['path'], "<iframe src=");
                        $embedUrl = ltrim($embedUrl, '""');
                        $successStories->link  = $embedUrl;
                        return $successStories;
                    }
                });
                $data = compact("successStories");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function specialFeatures(HospitalRepository $hosptlRepo, HospitalViewRequest $request, CmsRepository $cmsRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospital = $hosptlRepo->getData($hospitalId);
            $feature =json_decode($hospital->special_feature);
            if ($feature != null) {
                $specialFeature = $cmsRepo->specialFeatures($feature);
                $data = compact("specialFeature");
                $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
                return response()->json($response, 200);
            } else {
                $response = ['status' => false, 'data' => 'No data found', 'message' => 'Failed'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function hospitalEnquiry(EnquiryAddRequest $request, Enquiry $enquiry)
    {
        try {
            $hospital = Hospital::find($request->hospital_id);
            if ($hospital != null) {
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->hospital_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $hospital->enquiries()->save($enquiry);

                $response = ['status' => true,'message' => 'Hospital Enquiry Added Successfully'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function hospitalExploreEnquiry(EnquiryAddRequest $request, Enquiry $enquiry)
    {
        try {
            $fileUrl=$request->hasFile('file')?Storage::disk('public')->putFile('hospitalexplore_api_file/', $request->file('file')):"";
            $hospital = Hospital::find($request->hospital_id);
            if ($hospital != null) {
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->hospital_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $enquiry->file = $fileUrl;
                $enquiry->country_code  = $request->country_code;
                $enquiry->diseases_id  = $request->diseases_id;
                $hospital->enquiries()->save($enquiry);

                $response = ['status' => true,'message' => 'Hospital Enquiry Added Successfully'];
                return response()->json($response, 200);
            }
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function allHospitalFaq(FaqRepository $faqRepo)
    {
        try {
            $allHospitalFaq = $faqRepo->getAllFaq($askable_type="Hospital");
            $data = compact("allHospitalFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function HospitalFaq(HospitalFaqRequest $request, FaqRepository $faqRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $hospitalFaq = $faqRepo->getFaq($hospitalId, $askable_type="Hospital");
            $data = compact("hospitalFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function hospitalTeliDoctors(HospitalViewRequest $request, HospitalRepository $hosptlRepo, DepartmentRepository $deptRepo, DoctorRepository $doctorRepo, FaqRepository $faqRepo, CityRepository $cityRepo, CountryRepository $countryRepo)
    {
        try {
            $hospitalId = $request->hospital_id;
            $telemedicneDoctor = $doctorRepo->getTelemedicineDoctor($hospitalId);
            $details = $telemedicneDoctor->map(function ($doctors, $key) use ($hospitalId, $deptRepo, $hosptlRepo, $cityRepo, $countryRepo) {
                $hospitalDetails = $hosptlRepo->get($hospitalId);
                $city = $cityRepo->get($hospitalDetails->city_id);
                $country = $countryRepo->get($city->country_id);
                $doctors->country_name  = $country->name;
                return $doctors;
            });
            $data = compact("telemedicneDoctor");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
        // try {
        //     $hospitalId = $request->hospital_id;
        //     $departments = $deptRepo->getDepartmentsHospital($hospitalId);
        //     $doctors = $doctorRepo->getDepartmentTeliDoctor($departments);
        //     $details = $doctors->map(function ($doctors, $key) use($deptRepo,$hosptlRepo,$cityRepo,$countryRepo)  {
        //         $department = $deptRepo->get($doctors->department_id);
        //         $hospital= $hosptlRepo->get($department->hospital_id);
        //         $city = $cityRepo->get($hospital->city_id);
        //         $country = $countryRepo->get($city->country_id);
        //         $doctors->department  = $department->name;
        //         $doctors->country_name  = $country->name;
        //         return $doctors;
        //     });

        //     $data = compact("doctors");
        //     $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
        //     return response()->json($response, 200);
        // } catch (Exception $e) {
        //     $response = ['status' => false, 'message' => $e->getMessage()];
        //     return response()->json($response, 200);
        // }
    }

    public function hospitalAutocompleteSearch(Request $request, HospitalRepository $hospitalRepo, DoctorRepository $doctorRepo, CmsRepository $cmsRepo, DepartmentRepository $deptRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepo, TreatmentRepository $treatmentRepo)
    {
        try {
            $searchText = $request->search_text;
            $hospital = $hospitalRepo->searchHospital($searchText);
            if (count($hospital) == 0) { /**department based*/
                $departmentID = $deptRepo->searchBasedDepartment($searchText);
                $departmentDetails = $deptRepo->get($departmentID);
                if ($departmentDetails != null) {
                    $hospital = $hospitalRepo->getHospital($departmentDetails->hospital_id);
                }
                if (count($hospital) == 0) { /**diseases based */
                    $diseasesId = $diseasesRepo->searchBasedDiseases($searchText);
                    if (count($diseasesId) != 0) {
                        $hospital = $hospitalRepo->getHospitalsBasedDiseases($diseasesId);
                    }
                }
                if (count($hospital) == 0) { /**problems based */
                    $problemsId = $problemsRepo->searchBasedProblems($searchText);
                    if (count($problemsId) != 0) {
                        $hospital = $hospitalRepo->getHospitalsBasedOnProblems($problemsId);
                    }
                }
                if (count($hospital) == 0) {/**treatment based */
                    $treatmentId = $treatmentRepo->searchBasedTreatments($searchText);
                    if (count($treatmentId) != 0) {
                        $deptId = $treatmentRepo->getTreatmentBasedDepartment($treatmentId);
                        $departmentDetails = $deptRepo->get($deptId);
                        $hospital = $hospitalRepo->getHospital($departmentDetails->hospital_id);
                    }
                }
            }
            $response = ['status' => true, 'hospital' => $hospital, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}