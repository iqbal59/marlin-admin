<?php

namespace Modules\Hospital\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Hospital\Models\Hospital;
use Modules\Poll\Models\Poll;
use Modules\Hospital\Http\Requests\HospitalAddRequest;
use Modules\Hospital\Http\Requests\HospitalSaveRequest;
use Modules\Hospital\Http\Requests\HospitalEditRequest;
use Modules\Hospital\Http\Requests\HospitalUpdateRequest;
use Modules\Hospital\Http\Requests\HospitalViewRequest;
use Modules\Hospital\Http\Requests\HospitalDeleteRequest;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Storage;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Services\Repositories\ServicesRepositoryInterface as ServicesRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;
use Modules\Banner\Repositories\Includes\Banner\BannerRepositoryInterface as BannerRepository;
use Intervention\Image\ImageManagerStatic as Image;
use Modules\Enquiry\Repositories\EnquiryRepositoryInterface as EnquiryRepository;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
use Illuminate\Support\Facades\DB;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;

class HospitalController extends Controller
{
    public function listHospital(HospitalRepository $hosptlRepo)
    {
        $hospitals = $hosptlRepo->getForDatatable();
        return view('hospital::listHospital', compact('hospitals'));
    }

    public function add(HospitalAddRequest $request, CityRepository $cityRepo, BannerRepository $bannerRepo, DepartmentRepository $departmentRepo)
    {
        $retrn_url   = $request->retrn_url;
        $retrn_prms  = $request->retrn_prms;
        $city = $cityRepo->all();
        $banner = $bannerRepo->all();
        $departments = $departmentRepo->all();
        return view('hospital::addHospital', compact('city', 'retrn_url', 'retrn_prms', 'banner', 'departments'));
    }
    public function save(HospitalSaveRequest $request, HospitalRepository $hosptlRepo, CityRepository $cityRepo, Poll $poll)
    {
        $logoUrl=$request->hasFile('logo')?Storage::disk('public')->putFile('hospitallogo/', $request->file('logo')):"";
        $inputData = [
            'title'        => $request->title,
            'content'      => $request->content,
            'city_id'      => $request->city_id,
            'department_id'=>json_encode($request->department_id),
            'credintials'  => $request->credintials,
            'logo'         => $logoUrl,
            'banner_id'    => $request->banner_id,
            'address'      => $request->address,
            'added_by'     => auth()->user()->id,
        ];

        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(370, 240);
            // Storage::disk('public')->makeDirectory('hospitalimage/');
            $image_resize->save(storage_path('app/public/hospitalimage/' .$filename));
            $inputData['image'] = 'hospitalimage/' .$filename;
        }

        $hospital     = $hosptlRepo->save($inputData);
        $poll         = new Poll;
        $poll->value  = '1';
        $hospital->polls()->save($poll);

        if (!empty($request->retrn_prms)) {
            connectify('success', 'Success!', 'Hospital added successfully');
            return redirect()
            ->route($request['retrn_url'], ['id'=>$request['retrn_prms']]);
        } elseif (!empty($request->retrn_url)) {
            connectify('success', 'Success!', 'Hospital added successfully');
            return redirect()
            ->route($request['retrn_url']);
        } else {
            connectify('success', 'Success!', 'Hospital added successfully');
            return redirect()
            ->route('admin.hospital.list');
        }
    }

    public function view(DoctorRepository $doctorRepo, EnquiryRepository $enquiryRepo, HospitalViewRequest $request, HospitalRepository $hosptlRepo, DepartmentRepository $departmentRepo, ServicesRepository $serviceRepo, FaqRepository $faqRepo, UserRepository $userRepo)
    {
        $hospital = $hosptlRepo->get($request->id);
        $departments = $departmentRepo->getDepartmentsForHospital($request->id);
        $services = $serviceRepo->getServices($request->id);
        $faqs = $faqRepo->getFaq($request->id, $type="Hospital");
        $enquiry = $enquiryRepo->getEnquiry($request->id, $type="Modules\Hospital\Models\Hospital");
        $addedBy = $userRepo->get($hospital->added_by);

        $doctor = $doctorRepo->hospitalData([$hospital->id]);

        return view('hospital::viewHospital', compact('doctor', 'addedBy', 'enquiry', 'hospital', 'departments', 'services', 'faqs'));
    }


    public function edit(HospitalEditRequest $request, HospitalRepository $hosptlRepo, CityRepository $cityRepo, DiseasesRepository $diseasesRepo, ProblemsRepository $problemsRepo, CmsRepository $cmsRepo, BannerRepository $bannerRepo, DepartmentRepository $deptRepo)
    {
        $city     =  $cityRepo->all();
        $department = $deptRepo->all();
        $banner   = $bannerRepo->all();
        $hospital =  $hosptlRepo->get($request->id);
        $selectedDepartments = json_decode($hospital->department_id);
        $diseases = $diseasesRepo->all();
        $problems = $problemsRepo->all();
        $patientStories = $cmsRepo->getByCategory(2);
        $specialFeature = $cmsRepo->getByCategory(11);
        $selecteValue = json_decode($hospital->diseases_id);
        $selecteProblems = json_decode($hospital->problems_id);
        $selectepatientStories = json_decode($hospital->success_story_id);
        $selecteSpecialFeature = json_decode($hospital->special_feature);
        $retrn_url = $request->retrn_url != '' ? $request->retrn_url : '';
        $retrn_prms = $request->retrn_prms != '' ? $request->retrn_prms : '';
        return view('hospital::editHospital', compact('retrn_url', 'retrn_prms', 'banner', 'specialFeature', 'selecteSpecialFeature', 'selectepatientStories', 'patientStories', 'hospital', 'city', 'diseases', 'selecteValue', 'selecteProblems', 'problems', 'department', 'selectedDepartments'));
    }


    public function update(HospitalUpdateRequest $request, HospitalRepository $hosptlRepo)
    {
        $inputData = [
            'id' => $request->id,
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
            'diseases_id' => json_encode($request->states),
            'problems_id' => json_encode($request->problems),
            'success_story_id' => json_encode($request->patientstories),
            'special_feature' => json_encode($request->specialfeature),
            'meta_title' => $request->meta_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'mini_content' =>$request->min_content,
            'title'        => $request->title,
            'content'      => $request->content,
            'city_id'      => $request->city_id,
            'credintials'  => $request->credintials,
            'address'  => $request->address,
            'banner_id'  => $request->banner_id,
            'rating'  => $request->rating,
            'quality_approved_certificate'  => $request->quality_approved_certificate,
            'type' => $request->type,
            'link' => $request->link,
            'department_id'=>json_encode($request->department_id),


        ];

        $inputData['verified_badge']=($request->verified_badge == "on")?1:0;

        if (isset($request->status)) {
            $inputData['status']=($request->status == "on")?1:0;
        } else {
            $inputData['status']='0';
        }

        if ($request->hasFile('logo')) {
            $this->_removeFile($hosptlRepo, $request->id);
            $logoUrl = Storage::disk('public')->putFile('hospitallogo/', $request->file('logo'));
            $inputData['logo'] = $logoUrl;
        }

        if ($request->hasFile('thumbline_image')) {
            $this->_removeThumblineImage($hosptlRepo, $request->id);
            $thumblineUrl = Storage::disk('public')->putFile('hospitalthumbline_image/', $request->file('thumbline_image'));
            $inputData['thumbline_image'] = $thumblineUrl;
        }


        if ($request->hasFile('image')) {
            $this->_removeFileImages($hosptlRepo, $request->id);
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(370, 240);
            $image_resize->save(storage_path('app/public/hospitalimage/' .$filename));
            // $imageUrl = Storage::disk('public')->putFile('hospitalimage/',$request->file('image'));
            // $inputData['image'] = $imageUrl;
            $inputData['image'] = 'hospitalimage/' .$filename;
        }

        $poll     = new Poll;
        $hospital = $hosptlRepo->update($inputData);
        //var_dump($inputData);
        $poll->pollable()->associate($hospital);
        $hospital->polls()->update(['value' => $request->value]);


        if (!empty($request->retrn_prms)) {
            connectify('success', 'Success!', 'Hospital Updated successfully');
            return redirect()
            ->route($request['retrn_url'], ['id' => $request->retrn_prms]);
        } else {
            connectify('success', 'Success!', 'Hospital Updated successfully');
            return redirect()
                ->route('admin.hospital.list');
        }
    }


    public function delete(HospitalRepository $hosptlRepo, HospitalDeleteRequest $request)
    {
        $hospital = $hosptlRepo->get($request->id);
        if ($hospital->departments->count()) {
            connectify('danger', 'Warning!', 'This Hospital has transactions');
            return redirect()
                ->route('admin.hospital.list');
        } else {
            if ($hosptlRepo->delete($request->id)) {
                if (!empty($request->retrn_prms)) {
                    connectify('success', 'Success!', 'Hospital Updated successfully');
                    return redirect()
                    ->route($request['retrn_url'], ['id' => $request->retrn_prms]);
                } else {
                    connectify('success', 'Success!', 'Hospital Deleted successfully');
                    return redirect()
                    ->route('admin.hospital.list');
                }
            }
            return response()->json(['status' => 0, 'message' => "failed"]);
        }
    }

    public function removeFile(HospitalRepository $hosptlRepo, Request $request)
    {
        if ($this->_removeFile($hosptlRepo, $request->id)) {
            $hosptlRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'Image Deleted successfully');
        return redirect()
            ->route('admin.hospital.edit', ['id' => $request->id]);
    }

    private function _removeFile($hosptlRepo, $id)
    {
        $logo = $hosptlRepo->get($id);
        if (Storage::disk('public')->delete($logo->logo)) {
            return true;
        }
        return false;
    }

    public function removeFileImage(HospitalRepository $hosptlRepo, Request $request)
    {
        if ($this->_removeFileImages($hosptlRepo, $request->id)) {
            $hosptlRepo->resetFileImages($request->id);
        }
        connectify('success', 'Success!', 'Image Deleted successfully');
        return redirect()
            ->route('admin.hospital.edit', ['id' => $request->id]);
    }

    private function _removeFileImages($hosptlRepo, $id)
    {
        $image = $hosptlRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }

    public function removeThumblineImage(HospitalRepository $hosptlRepo, Request $request)
    {
        if ($this->_removeThumblineImage($hosptlRepo, $request->id)) {
            $hosptlRepo->resetThumblineImages($request->id);
        }
        connectify('success', 'Success!', 'Image Deleted successfully');
        return redirect()
            ->route('admin.hospital.edit', ['id' => $request->id]);
    }

    private function _removeThumblineImage($hosptlRepo, $id)
    {
        $image = $hosptlRepo->get($id);
        if (Storage::disk('public')->delete($image->thumbline_image)) {
            return true;
        }
        return false;
    }

    public function search(Request $request)
    {
        $data['states'] = Hospital::where("city_id", $request->city_id)->get(["title","id"]);
        return response()->json($data);
    }
}