<?php

namespace Modules\TreatmentDetails\Repositories;

interface TreatmentDetailsRepoInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function update(array $input);

    public function get($slug);

    public function getTreatmentDetails($slug);

    public function delete(string $id);

    public function aboutTreatment(string $slug);
}