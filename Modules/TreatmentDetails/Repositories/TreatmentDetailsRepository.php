<?php 

namespace Modules\TreatmentDetails\Repositories;

use Modules\TreatmentDetails\Entities\TreatmentDetails;
use Modules\Treatment\Entities\Treatment;
use Modules\TreatmentDetails\Repositories\TreatmentDetailsRepoInterface;

class TreatmentDetailsRepository implements TreatmentDetailsRepoInterface
{
    public function getForDatatable()
    { 
        return TreatmentDetails::with('treatments')->orderBy('id', 'DESC')->get()->toArray();
    }

    public function all()
    { 
        return Treatment::orderBy('id', 'DESC')->get()->toArray();
    }

    public function save(array $input)
    { 
        if ($disease_details =  TreatmentDetails::create($input)) {
            return $disease_details;
        }
        return false;
    }

    public function update(array $input)
    { 
        $disease_details = TreatmentDetails::find($input['id']); 
        unset($input['id']);
        if ($disease_details->update($input)) {
            return $disease_details;
        }
        return false;
    }

    public function get($id)
    { 
        return TreatmentDetails::with('treatments')->where('id',$id)->first();
    }

    public function getTreatmentDetails($slug)
    {
        $disease_details = TreatmentDetails::where('treatment_slug',$slug)->get();
        return $disease_details;
    }

    public function delete(string $id)
    {
        $disease_details = TreatmentDetails::find($id);
        return $disease_details->delete();
    }

    public function aboutTreatment($slug)
    {
        $disease_details = Treatment::with(['treatmentDetails' => function ($query) {
            $query->orderBy('order_number', 'ASC');
        }])->where('slug', $slug)->get();
        return $disease_details;
    }
}