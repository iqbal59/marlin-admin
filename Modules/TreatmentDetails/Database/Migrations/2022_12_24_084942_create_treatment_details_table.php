<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatment_details', function (Blueprint $table) {
            $table->id();
            $table->string('treatment_slug');
            $table->string('index_title');
            $table->integer('order_number')->nullable();
            $table->string('details_type')->nullable();
            $table->string('content_image')->nullable();
            $table->json('tab_details')->nullable();
            $table->json('general_details')->nullable();
            $table->json('item_list')->nullable();
            $table->integer('section_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatment_details');
    }
}
