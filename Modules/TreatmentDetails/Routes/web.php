<?php
use Modules\TreatmentDetails\Http\Controllers\Api\TreatmentDetailsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'treatmentdetails', 'as' => 'treatmentdetails.'], function () {       
        Route::get('/treatment-details/list', [TreatmentDetailsController::class, 'list'])->name('treatment-details');  
        Route::get('add', [TreatmentDetailsController::class, 'add'])->name('add');
        Route::post('save', [TreatmentDetailsController::class, 'save'])->name('save');
        Route::get('edit', [TreatmentDetailsController::class, 'edit'])->name('edit');
        Route::post('update', [TreatmentDetailsController::class, 'update'])->name('update');
        Route::get('view', [TreatmentDetailsController::class, 'view'])->name('view');
        Route::get('delete', [TreatmentDetailsController::class, 'delete'])->name('delete');
    });
});
