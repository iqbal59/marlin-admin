<?php
namespace Modules\TreatmentDetails\Http\Controllers\Includes;

use Closure;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Modules\Doctor\Entities\Doctor;
use Modules\TreatmentDetails\Repositories\TreatmentDetailsRepository as TreatmentDetailsRepo;
use Modules\TreatmentFaq\Repositories\TreatmentFaqRepository as TreatmentFaqRepo;
use Modules\TreatmentDetails\Entities\TreatmentDetails;
trait TreatmentDetailsTrait 
{
    public function list(TreatmentDetailsRepo $TreatmentDetailsRepo)
    {  
        $treatment_details = $TreatmentDetailsRepo->getForDatatable();   
        return  view('treatmentdetails::listTreatmentDetails', compact('treatment_details'));
    }

    public function add(TreatmentDetailsRepo $TreatmentDetailsRepo)
    { 
        $treatmentList = $TreatmentDetailsRepo->all();  
        return view('treatmentdetails::addTreatmentDetails', compact('treatmentList'));               
    }

    public function save(TreatmentDetailsRepo $TreatmentDetailsRepo, Request $request)
    {   
        // $request->validate([
        //     'order_number' => 'required|unique:treatment_details|max:255',
        // ]);
        $inputData = [
            'treatment_slug'        => $request->treatment_slug,
            'index_title'           => $request->title,
            'order_number'          => $request->order_number,
            'details_type'          => $request->choose_Item,
            'general_details'       => json_encode($request->general_description),
            'tab_details'           => json_encode($request->tabs),
            'item_list'             => json_encode($request->itemList)
        ];
        if($request->hasFile('content_image')) {
            $image         = $request->file('content_image');
            $filename      = $image->getClientOriginalName();
            $image_resize  = Image::make($image->getRealPath());              
            $image_resize->resize(380, 270); 
            $image_resize->save(public_path('uploads/treatmentdetailsimage/' .$filename)); 
            $inputData['content_image'] = 'treatmentdetailsimage/' .$filename;           
        }

        $Treatment_details = $TreatmentDetailsRepo->save($inputData);   
        connectify('success', 'Success!', 'Treatment Details added successfully');    
        return redirect()
            ->route('admin.treatmentdetails.treatment-details');
    }

    public function edit(Request $request, TreatmentDetailsRepo $TreatmentDetailsRepo)
    {  
        $treatment_details = $TreatmentDetailsRepo->get($request->id);    
        $treatmentList = $TreatmentDetailsRepo->all();  
        return view('treatmentdetails::editTreatmentDetails', compact('treatment_details','treatmentList'));
    }

    public function update(Request $request, TreatmentDetailsRepo $TreatmentDetailsRepo)
    {   
        // $validated = $request->validate([
        //     'order_number' => 'required|unique:treatment_details|max:255',
        // ]);
        $general_details = ($request->general_details_new != NULL) ? $request->general_details_new : $request->general_details;
        
        $inputData = [
            'id'                    => $request->id,
            'treatment_slug'        => $request->treatment_slug,
            'index_title'           => $request->title,
            'order_number'          => $request->order_number,
            'details_type'          => $request->choose_Item,
            'general_details'       => ($request->choose_Item != 'general') ? Null : json_encode($general_details),
            'tab_details'           => ($request->choose_Item != 'tab') ? Null : json_encode($request->tabs),
            'item_list'             => ($request->choose_Item != 'list_item') ? Null : json_encode($request->itemList)
        ];

        if ($request->hasFile('general_image')) {
            $this->_removeFile($TreatmentDetailsRepo, $request->id);                 
            $image        = $request->file('general_image');
            $filename     = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(380, 270); 
            $image_resize->save(public_path('uploads/treatmentdetailsimage/' .$filename));         
            $inputData['content_image'] = 'treatmentdetailsimage/' .$filename;     
        } 
        
        if ($request->hasFile('general_image_new')) {
            $this->_removeFile($TreatmentDetailsRepo, $request->id);                 
            $image        = $request->file('general_image_new');
            $filename     = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(380, 270); 
            $image_resize->save(public_path('uploads/treatmentdetailsimage/' .$filename));         
            $inputData['content_image'] = 'treatmentdetailsimage/' .$filename;     
        } 

        $treatment_details = $TreatmentDetailsRepo->update($inputData);
        connectify('success', 'Success!', 'Treatment Details updated successfully');    
        return redirect()
            ->route('admin.treatmentdetails.treatment-details');
    }  

    public function view(Request $request, TreatmentDetailsRepo $TreatmentDetailsRepo)
    { 
        $treatment_details = $TreatmentDetailsRepo->get($request->id);  
        return view('treatmentdetails::viewTreatmentDetails', compact('treatment_details'));
    }

    public function delete(TreatmentDetailsRepo $TreatmentDetailsRepo, Request $request)
    { 
        if ($TreatmentDetailsRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Treatment Details deleted successfully');    
            return redirect()
            ->route('admin.treatmentdetails.treatment-details');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    private function _removeFile($TreatmentDetailsRepo, $id)
    { 
        $treatment_image = $TreatmentDetailsRepo->get($id);
        if (Storage::disk('public')->delete($treatment_image->image)) {
            return true;
        }
        return false;
    }

    public function aboutTreatment(TreatmentDetailsRepo $TreatmentDetailsRepo, TreatmentFaqRepo $TreatmentFaqRepo, $slug)
    {
        $about_Treatment = $TreatmentDetailsRepo->aboutTreatment($slug);
       
        $treatment_faq   = $TreatmentFaqRepo->getTreatmentFaq($slug);
        $disease_id      = $about_Treatment[0]->id;
        $doctors         = Doctor::get();
        
        $relatedDoctors = $doctors->filter(function ($value) use ($disease_id) {
            $diseases = json_decode($value->diseases_id);
                foreach($diseases as $dis){
                    return $dis == $disease_id;
                }
        })->values();;

        $data = [
                'datas'     => $about_Treatment[0],
                'faqs'     => $treatment_faq,
                'doctors'  => $relatedDoctors
        ];
        return response()->json( $data, 200);
    }
}