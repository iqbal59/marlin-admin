<?php
namespace Modules\TreatmentDetails\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\TreatmentDetails\Http\Controllers\Includes\TreatmentDetailsTrait;

class TreatmentDetailsController extends Controller
{
    use TreatmentDetailsTrait;
}
