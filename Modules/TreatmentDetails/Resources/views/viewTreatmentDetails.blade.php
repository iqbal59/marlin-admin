@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
         View Disease Details
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr> 
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $treatment_details->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Index Title
                        </th>
                        <td>
                            {{ $treatment_details->index_title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Details Type
                        </th>
                        <td> 
                            {{ $treatment_details->details_type }}
                        </td>
                    </tr>  
                    <tr>
                        <th>
                            Order Number
                        </th>
                        <td> 
                            {{ $treatment_details->order_number }}
                        </td>
                    </tr>  
                    @if ($treatment_details->details_type == 'general')
                    <tr>
                        <th>
                            Description
                        </th>
                        <td> 
                            {!! $treatment_details->general_details !!}
                        </td>
                    </tr>  
                    @elseif($treatment_details->details_type == 'list_item')

                    @php
                        $items = json_decode($treatment_details->item_list,true);
                    @endphp
                        <tr>
                            <th colspan="2">
                                Items
                            </th>
                        </tr> 
                        @foreach ($items  as $key => $item)
                        <tr>
                            <td colspan="2"> 
                                {{$key+1}} . {{ $item }}
                            </td>
                        </tr>
                        @endforeach
                    @elseif($treatment_details->details_type == 'tab')
                    @php
                        $tabItems = json_decode($treatment_details->tab_details,true);             
                    @endphp
                    <tr>
                        <th colspan="2">
                            Tab Details
                        </th>
                    </tr> 
                    <tr>
                        <th>
                            Tab Title
                        </th>
                        <th>
                            Tab Description
                        </th>
                    </tr>
                   
                    @foreach ($tabItems as $a=> $items)
                    @php
                        $tab_count = count($items);
                        $tab_id = $tabItems["'tab_id'"];
                        $tab_title = $tabItems["'tab_title'"];
                        $tab_description = $tabItems["'tab_description'"];
                    @endphp
                    @endforeach
                    @for ($tabs=0; $tabs < $tab_count; $tabs++)
                        @if ($tab_title[$tabs] != NULL)
                        <tr>
                            <td>
                                {{$tabs+1}}. {{$tab_title[$tabs]}}
                            </td>
                            <td>
                                {!! $tab_description[$tabs] !!}
                            </td>
                        </tr>
                        @endif
                    @endfor
                    @endif            
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection