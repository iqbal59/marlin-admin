@extends('layouts.admin')
@section('content')
@can('treatmentdetails_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.treatmentdetails.add") }}">
                <i class="mdi mdi-plus"></i>
                {{ trans('panel.add_treatment_details') }} 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('panel.list_treatment_details') }} 
   
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.treatment_title') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.details_type') }}
                        </th>
                        
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($treatment_details as $key => $treatment_detail) 
                        <tr data-entry-id="{{ $treatment_detail['id'] }}">
                            <td></td> 
                            <td>{{ $key+1 }}</td>
                            <td>{{ $treatment_detail['index_title'] }}</td>
                            <td>{{ $treatment_detail['treatments']['name'] }}</td>
                            <td>{!! $treatment_detail['details_type'] !!}</td>                 
                            <td>   
                            @can('treatmentfaq_show')           
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.treatmentdetails.view', ['id' => $treatment_detail['id']]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan
                            @can('treatmentfaq_edit')  
                                    <a class="btn btn-xs btn-info" href="{{route('admin.treatmentdetails.edit', ['id' => $treatment_detail['id']]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan
                            @can('treatmentfaq_delete')  
                                    <form action="{{ route('admin.treatmentdetails.delete',  ['id' => $treatment_detail['id']]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$treatment_detail['id']}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})
</script>
@endsection