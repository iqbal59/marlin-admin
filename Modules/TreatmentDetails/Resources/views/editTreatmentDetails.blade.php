@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        {{ trans('panel.edit_treatment_details') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentdetails.update") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <input type="hidden" name="id" value="{{$treatment_details['id']}}">
                <input type="hidden" id="detailsType" value="{{$treatment_details->details_type}}">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('treatment_plants_id') ? 'has-error' : '' }}">
                        <label for="treatment_slug"> Treatment <i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <select class="js-states form-control" name="treatment_slug" id="treatment_slug">
                            @foreach($treatmentList as $data)
                            @if($data['slug'] === $treatment_details->treatment_slug)
                            <option value="{{$data['slug']}}" name="treatment_slug" selected=""> {{$data['name']}}
                            </option>
                            @else
                            <option value="{{$data['slug']}}" name="treatment_slug"> {{$data['name']}} </option>
                            @endif
                            @endforeach
                        </select>
                        @if($errors->has('treatment_plants_id'))
                        <em class="invalid-feedback">{{ $errors->first('treatment_plants_id') }}</em>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">{{ trans('cruds.user.fields.title') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control"
                            value="{{ old('title', isset($treatment_details) ? $treatment_details->index_title : '') }}">
                        @if($errors->has('title'))
                        <em class="invalid-feedback">
                            {{ $errors->first('title') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
                        <label for="order">{{ trans('cruds.user.fields.order') }}<i
                                class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="number" id="order" name="order_number" class="form-control"
                            placeholder="Please put order number(ASC/DESC) "
                            value="{{ old('title', isset($treatment_details) ? $treatment_details->order_number : '') }}">
                        @if($errors->has('order_number'))
                        <em class="invalid-feedback">
                            {{ $errors->first('order_number') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            {{-- <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
            <label for="description">Treatment Description </label>
            <textarea class="ckeditor form-control" id="description" name="description">
                    {!! old('description', isset($treatment_details) ? $treatment_details->content_description : '') !!}
                </textarea>
            @if($errors->has('description'))
            <em class="invalid-feedback">
                {{ $errors->first('description') }}
            </em>
            @endif
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
    </div> --}}
    @if ($treatment_details->details_type == 'general')
    <div>
        <input type="radio" class="btn-check display-details-type indexRadio" checked value="general" name="choose_Item"
            id="general" autocomplete="off">
        <label class="btn btn-secondary" for="general">General</label>

        <input type="radio" class="btn-check display-details-type indexRadio" value="tab" name="choose_Item" id="tab"
            autocomplete="off">
        <label class="btn btn-secondary" for="tab">Tab</label>

        <input type="radio" class="btn-check display-details-type indexRadio" value="list_item" name="choose_Item"
            id="list_item" autocomplete="off">
        <label class="btn btn-secondary" for="list_item">List Item</label>
    </div>

    <div class="general-details" id="generalDetails">
        <strong>General Description</strong>
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('general_image') ? 'has-error' : '' }}">
                <label for="general_image"> {{ trans('panel.general_image') }} </label>
                <div class="general_image my-2">
                    <img src="{{ asset('uploads/'.$treatment_details->content_image) }}" height="100" width="100" alt=""
                        srcset="">
                </div>
                <input type="file" name="general_image" id="input-file-now-banner" class="dropify validate"
                    data-default-file="" />
                @if($errors->has('general_image'))
                <em class="invalid-feedback">
                    {{ $errors->first('general_image') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('general_details') ? 'has-error' : '' }}">
                <label for="general_details"> {{ trans('panel.general_details') }} </label>

                <textarea class="ckeditor form-control" name="general_details">
                            {!! old('general_details', isset($treatment_details) ? json_decode($treatment_details->general_details) : '') !!}
                        </textarea>
                @if($errors->has('general_details'))
                <em class="invalid-feedback">
                    {{ $errors->first('general_details') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
    </div>


    @elseif ($treatment_details->details_type == 'tab')
    <div>
        <input type="radio" class="btn-check display-details-type indexRadio" value="general" name="choose_Item"
            id="general" autocomplete="off">
        <label class="btn btn-secondary" for="general">General</label>

        <input type="radio" class="btn-check display-details-type indexRadio" checked value="tab" name="choose_Item"
            id="tab" autocomplete="off">
        <label class="btn btn-secondary" for="tab">Tab</label>

        <input type="radio" class="btn-check display-details-type indexRadio" value="list_item" name="choose_Item"
            id="list_item" autocomplete="off">
        <label class="btn btn-secondary" for="list_item">List Item</label>
    </div>

    <div class="tab-details" id="tabDetails">
        <strong>Tab Details</strong>
        <table style="width: 100%" class="countTabNumber" id="appendTabElement">
            @php
            $tabItems = json_decode($treatment_details->tab_details,true);
            @endphp

            <input type="hidden" id="countTab" value="{{ (int)(count($tabItems)) }}">
            @foreach ($tabItems as $a => $items)
            @php
            $tab_count = count($items);
            $tab_id = $tabItems["'tab_id'"];
            $tab_title = $tabItems["'tab_title'"];
            $tab_description = $tabItems["'tab_description'"];
            @endphp
            @endforeach
            @for ($tabs=0; $tabs < $tab_count; $tabs++) @if ($tab_title[$tabs] !=NULL) <tr class="removeTab">
                <td style="width:100%">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('tab_id') ? 'has-error' : '' }}">
                                <label for="tab_id">{{ trans('cruds.user.fields.tab_id') }}<i
                                        class="mdi mdi-flag-checkered text-danger "></i></label>
                                <input type="text" id="tab_id" name="tabs['tab_id'][]" class="form-control"
                                    value="{{old('tab_id', isset($tab_id) ? $tab_id[$tabs] : '')}}">
                                @if($errors->has('tab_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('tab_id') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('tab_title') ? 'has-error' : '' }}">
                                <label for="tab_title">{{ trans('cruds.user.fields.title') }}<i
                                        class="mdi mdi-flag-checkered text-danger "></i></label>
                                <input type="text" id="tab_title" name="tabs['tab_title'][]" class="form-control"
                                    value="{{ old('tab_title', isset($tab_title) ? $tab_title[$tabs] : '') }}">
                                @if($errors->has('tab_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('tab_title') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('tab_description') ? 'has-error' : '' }}">
                        <label for="tab_description">Treatment Description </label>
                        <textarea class="ckeditor form-control" id="tab_description" name="tabs['tab_description'][]"
                            value="{{old('tab_description', isset($tab_description) ? $tab_description[$tabs] : '')}}"
                            required>
                                    {!! old('tab_details', isset($tab_description) ? $tab_description[$tabs] : '') !!}
                                </textarea>
                        @if($errors->has('tab_description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('tab_description') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                    <div>
                        <span class="btn btn-danger pull-right remove-tab mb-2">
                            Remove Tab
                        </span>
                    </div>
                </td>
                </tr>
                @endif
                @endfor
        </table>
        <span class="btn btn-primary pull-right" id="addTab">
            add Tab
        </span>
    </div>
    @elseif ($treatment_details->details_type == 'list_item')
    <div>
        <input type="radio" class="btn-check display-details-type indexRadio" value="general" name="choose_Item"
            id="general" autocomplete="off">
        <label class="btn btn-secondary" for="general">General</label>

        <input type="radio" class="btn-check display-details-type indexRadio" value="tab" name="choose_Item" id="tab"
            autocomplete="off">
        <label class="btn btn-secondary" for="tab">Tab</label>

        <input type="radio" class="btn-check display-details-type indexRadio" checked value="list_item"
            name="choose_Item" id="list_item" autocomplete="off">
        <label class="btn btn-secondary" for="list_item">List Item</label>
    </div>

    <div class="list-item" id="itemDetails">
        <strong>List Items</strong>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">{{ trans('cruds.user.fields.title') }}<i
                    class="mdi mdi-flag-checkered text-danger "></i></label>

            <table style="width: 100%" id="appendElement">
                @php
                $items = json_decode($treatment_details->item_list,true);
                @endphp
                @foreach ($items as $key => $item)
                <tr class='removeCol'>
                    <td style="width: 90%">
                        <div id="inputItems">
                            <input type="text" id="itemInput remove_project_file" name="itemList[]" class="form-control"
                                value="{{old('name', isset($item) ? $item : '')}}">
                        </div>
                        @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </td>
                    <td style="width: 10%">
                        <span class="btn btn-danger pull-right remove-button mb-2">
                            Remove Item
                        </span>
                    </td>
                </tr>
                @endforeach
            </table>
            <span class="btn btn-primary mt-2" id="addBtn">
                add Item
            </span>
        </div>
    </div>
    @endif

    <div class="display-details-type">
        <div class="general-details" id="generalDetailsNew">
            <strong>General Details</strong>
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('general_image_new') ? 'has-error' : '' }}">
                    <label for="general_image_new"> {{ trans('panel.general_image') }} </label>
                    <input type="file" name="general_image_new" id="input-file-now-banner" class="dropify validate"
                        data-default-file="" />
                    @if($errors->has('general_image_new'))
                    <em class="invalid-feedback">
                        {{ $errors->first('general_image_new') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </div>
            <div class="form-group {{ $errors->has('general_details') ? 'has-error' : '' }}">
                <label for="general_details_new"> {{ trans('panel.general_details') }} </label>
                <textarea class="ckeditor form-control" name="general_details_new">
                            {!! old('content_description') !!}
                        </textarea>
                @if($errors->has('general_details_new'))
                <em class="invalid-feedback">
                    {{ $errors->first('general_details_new') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
        </div>
        <div class="tab-details" id="tabDetailsNew">
            <strong>Tab Details</strong>
            <table style="width: 100%" id="appendTabElement">
                <tr>
                    <td style="width:90%">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('tab_id') ? 'has-error' : '' }}">
                                    <label for="tab_id">{{ trans('cruds.user.fields.tab_id') }}<i
                                            class="mdi mdi-flag-checkered text-danger "></i></label>
                                    <input type="text" id="tab_id" name="tabs['tab_id'][]" class="form-control"
                                        value="{{old('tab_id')}}">
                                    @if($errors->has('tab_id'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('tab_id') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.name_helper') }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('tab_title') ? 'has-error' : '' }}">
                                    <label for="tab_title">{{ trans('cruds.user.fields.title') }}<i
                                            class="mdi mdi-flag-checkered text-danger "></i></label>
                                    <input type="text" id="tab_title" name="tabs['tab_title'][]" class="form-control"
                                        value="{{old('tab_title')}}">
                                    @if($errors->has('tab_title'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('tab_title') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.name_helper') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('tab_description') ? 'has-error' : '' }}">
                            <label for="tab_description">Faq Description </label>
                            <textarea class="ckeditor form-control" id="tab_description"
                                name="tabs['tab_description'][]" value="{{old('tab_description')}}" required></textarea>
                            @if($errors->has('tab_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('tab_description') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
            <span class="btn btn-primary pull-right addTab" id="addTab">
                add Tab
            </span>
        </div>

        <div class="list-item" id="itemDetailsNew">
            <strong>List Items</strong>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('cruds.user.fields.title') }}<i
                        class="mdi mdi-flag-checkered text-danger "></i></label>

                <table style="width: 100%" id="appendElement">
                    <tr>
                        <td style="width: 90%">
                            <div id="inputItems">
                                <input type="text" id="itemInput remove_project_file" name="itemList[]"
                                    class="form-control" value="{{old('name')}}">
                            </div>
                            @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </td>
                        <td style="width: 10%">

                        </td>
                    </tr>
                </table>
                <span class="btn btn-primary mt-2" id="addBtn">
                    add Item
                </span>
            </div>
        </div>
    </div>

    <div>
        <input class="btn btn-success" type="submit" value="{{ trans('global.save') }}">
    </div>
    </form>


</div>
</div>

@endsection
@section('scripts')

@parent

<script>
$(document).ready(function() {
    $("#generalDetailsNew").hide()
    $("#tabDetailsNew").hide()
    $("#itemDetailsNew").hide()
    let items = document.querySelectorAll('.indexRadio');
    let detailsType = document.getElementById("detailsType").value;
    let countTab = document.getElementById("countTab")?.value;

    items.forEach(function(item) {
        item.addEventListener('click', function(e) {
            let value = e.target.value;
            if (detailsType == "general") {
                if (value == "general") {
                    $("#generalDetails").show()
                    $("#generalDetailsNew").hide()
                    $("#tabDetailsNew").hide()
                    $("#itemDetailsNew").hide()
                } else if (value == "tab") {
                    $("#generalDetails").hide()
                    $("#tabDetailsNew").show()
                    $("#itemDetailsNew").hide()
                } else if (value == "list_item") {
                    $("#generalDetails").hide()
                    $("#itemDetailsNew").show()
                    $("#tabDetailsNew").hide()
                }
            } else if (detailsType == "tab") {
                if (value == "general") {
                    $("#tabDetails").hide()
                    $("#generalDetailsNew").show()
                    $("#tabDetailsNew").hide()
                    $("#itemDetailsNew").hide()
                } else if (value == "tab") {
                    $("#generalDetailsNew").hide()
                    $("#tabDetails").show()
                    $("#tabDetailsNew").hide()
                    $("#itemDetailsNew").hide()
                } else if (value == "list_item") {
                    $("#tabDetails").hide()
                    $("#itemDetailsNew").show()
                    $("#generalDetailsNew").hide()
                }
            } else if (detailsType == "list_item") {
                if (value == "general") {
                    $("#tabDetailsNew").hide()
                    $("#itemDetails").hide()
                    $("#generalDetailsNew").show()
                } else if (value == "tab") {
                    $("#tabDetailsNew").show()
                    $("#itemDetails").hide()
                    $("#generalDetailsNew").hide()
                } else if (value == "list_item") {
                    $("#itemDetails").show()
                    $("#generalDetailsNew").hide()
                    $("#tabDetailsNew").hide()
                    $("#itemDetailsNew").hide()
                }
            }
        })
    });

    $("#addBtn").click(function() {
        $("#appendElement").append(`
            <tr class='removeCol'>
                <td style="width: 90%"> 
                    <div id="inputItems">
                        <input type="text" id="itemInput" name="itemList[]" class="form-control" value="{{old('name')}}">
                    </div>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </td>
                <td style="width: 10%">
                    <span class="btn btn-danger pull-right remove-button" >
                        Remove Item
                    </span>
                </td>
            </tr>`);
    });

    var i = parseInt(countTab);

    var tabHtml = `
        <div class="removeTab">
            <tr>
                <td style="width:100%">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('tab_id') ? 'has-error' : '' }}">
                                <label for="tab_id">{{ trans('cruds.user.fields.tab_id') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                                <input type="text" id="tab_id" name="tabs['tab_id'][]" class="form-control tab_id" value="{{old('tab_id')}}">
                                @if($errors->has('tab_id'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('tab_id') }}
                                    </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('tab_title') ? 'has-error' : '' }}">
                                <label for="tab_title">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                                <input type="text" id="tab_title" name="tabs['tab_title'][]" class="form-control" value="{{old('tab_title')}}" >
                                @if($errors->has('tab_title'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('tab_title') }}
                                    </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.user.fields.name_helper') }}
                                </p>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('tab_description') ? 'has-error' : '' }}">
                        <label for="tab_description">Faq Description </label>
                        <textarea class="ckeditor form-control" id="tab_description" name="tabs['tab_description'][]" value="{{old('tab_description')}}" required></textarea>       
                        @if($errors->has('tab_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('tab_description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <span class="btn btn-danger pull-right remove-tab mb-2" >
                        Remove Tab
                    </span>
                </td>     
            </tr>
        </div>`;

    $("#addTab").click(function() {
        $("#appendTabElement").append(tabHtml);
    });

    $(document).on('click', '.remove-button', function() {
        $(this).parents('.removeCol').remove();
    });

    $(document).on('click', '.remove-tab', function() {
        $(this).parents('.removeTab').remove();
    });

    $("#treatment_slug").select2({
        placeholder: "Select Category",
        allowClear: true
    });
});
</script>

@endsection