@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        {{ trans('panel.add_disease_details') }} 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentdetails.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">         
                <div class="col-md-6">
                <label for="treatment_slug">Treatment <i class="mdi mdi-flag-checkered text-danger "></i> </label> 
                    <select id="treatment_slug" name="treatment_slug" class="js-states form-control">
                        @foreach($treatmentList as $data)  
                            <option value="{{$data['slug']}}">{{$data['name']}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('treatment_slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('treatment_slug') }}
                        </em>
                    @endif     
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('order') ? 'has-error' : '' }}">
                        <label for="order">{{ trans('cruds.user.fields.order') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="number"  placeholder="Please put order number(ASC/DESC)" id="order" name="order_number" class="form-control" value="{{old('order')}}" >
                        @if($errors->has('order_number'))
                            <em class="invalid-feedback">
                                {{ $errors->first('order_number') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>
            {{-- <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Content Description </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  --}}
            <div>
                <input type="radio" class="btn-check display-details-type indexRadio" checked value="general" name="choose_Item" id="general" autocomplete="off">
                <label class="btn btn-secondary" for="general">General</label>
                
                <input type="radio" class="btn-check display-details-type indexRadio" value="tab" name="choose_Item" id="tab" autocomplete="off">
                <label class="btn btn-secondary" for="tab">Tab</label>
                        
                <input type="radio" class="btn-check display-details-type indexRadio" value="list_item" name="choose_Item" id="list_item" autocomplete="off">
                <label class="btn btn-secondary" for="list_item">List Item</label>
            </div>

            <div class="display-details-type">
                <div class="general-details" id="generalDetails">
                    <strong>General Details</strong>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('content_image') ? 'has-error' : '' }}">
                            <label for="content_image"> {{ trans('panel.content_image') }} </label>
                            <input type="file" name="content_image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                            @if($errors->has('content_image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('content_image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                    </div>
                    <div class="form-group {{ $errors->has('general_description') ? 'has-error' : '' }}">
                        <label for="general_description">General Description </label>
                        <textarea class="ckeditor form-control" id="general_description" name="general_description" value="{{old('general_description')}}" required></textarea>       
                        @if($errors->has('general_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('general_description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="tab-details" id="tabDetails">
                    <strong>Tab Details</strong>
                    <table style="width: 100%" id="appendTabElement">
                        <tr>
                            <td style="width:90%">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('tab_id') ? 'has-error' : '' }}">
                                            <label for="tab_id">{{ trans('cruds.user.fields.tab_id') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                                            <input type="text" id="tab_id" name="tabs['tab_id'][]" class="form-control" value="{{old('tab_id')}}" >
                                            @if($errors->has('tab_id'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('tab_id') }}
                                                </em>
                                            @endif
                                            <p class="helper-block">
                                                {{ trans('cruds.user.fields.name_helper') }}
                                            </p>
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('tab_title') ? 'has-error' : '' }}">
                                            <label for="tab_title">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                                            <input type="text" id="tab_title" name="tabs['tab_title'][]" class="form-control" value="{{old('tab_title')}}" >
                                            @if($errors->has('tab_title'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('tab_title') }}
                                                </em>
                                            @endif
                                            <p class="helper-block">
                                                {{ trans('cruds.user.fields.name_helper') }}
                                            </p>
                                        </div> 
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('tab_description') ? 'has-error' : '' }}">
                                    <label for="tab_description">Faq Description </label>
                                    <textarea class="ckeditor form-control" id="tab_description" name="tabs['tab_description'][]" value="{{old('tab_description')}}" required></textarea>       
                                    @if($errors->has('tab_description'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('tab_description') }}
                                        </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.name_helper') }}
                                    </p>
                                </div>
                            </td>
                            <td style="width:3%">

                            </td>
                        </tr>
                    </table>
                    <span class="btn btn-primary pull-right" id="addTab">
                        add Tab
                    </span>
                </div>
                
                <div class="list-item" id="itemDetails">
                    <strong>List Items</strong>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>

                        <table style="width: 100%" id="appendElement">
                            <tr>
                                <td style="width: 90%"> 
                                    <div id="inputItems">
                                        <input type="text" id="itemInput remove_project_file" name="itemList[]" class="form-control" value="{{old('name')}}">
                                    </div>
                                    @if($errors->has('itemList'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('itemList') }}
                                        </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.user.fields.name_helper') }}
                                    </p>
                                </td>
                                <td style="width: 10%">
                                    
                                </td>
                            </tr>
                        </table>
                        <span class="btn btn-primary mt-2" id="addBtn">
                            add Item
                        </span>
                    </div> 
                </div>
            </div>

            <div>
                <input class="btn btn-success" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>

@endsection
@section('scripts')


@parent

<script>
$(document).ready(function() {
    $("#tabDetails").hide()
    $("#itemDetails").hide()
    let items = document.querySelectorAll('.indexRadio');

    items.forEach(function(item){
        item.addEventListener('click',function(e){
            let value = e.target.value;
            if(value == "general"){
                $("#generalDetails").show(200)
                $("#tabDetails").hide(200)
                $("#itemDetails").hide(200)
            }else if(value == "tab"){
                $("#generalDetails").hide(200)
                $("#tabDetails").show(200)
                $("#itemDetails").hide(200)

            }else{
                $("#generalDetails").hide(200)
                $("#tabDetails").hide(200)
                $("#itemDetails").show(200)
            }
        })
    });

    $("#addBtn").click(function(){
        $("#appendElement").append(`
            <tr class='removeCol'>
                <td style="width: 90%"> 
                    <div id="inputItems">
                        <input type="text" id="itemInput" name="itemList[]" class="form-control" value="{{old('name')}}">
                    </div>
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </td>
                <td style="width: 10%">
                    <span class="btn btn-danger pull-right remove-button" >
                        Remove Item
                    </span>
                </td>
            </tr>`);
    });

    $("#addTab").click(function(){
        $("#appendTabElement").append(`
        <div class="removeTab"><tr>
            <td style="width:100%">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('tab_id') ? 'has-error' : '' }}">
                            <label for="tab_id">{{ trans('cruds.user.fields.tab_id') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <input type="text" id="tab_id" name="tabs['tab_id'][]" class="form-control" value="{{old('tab_id')}}">
                            @if($errors->has('tab_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('tab_id') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('tab_title') ? 'has-error' : '' }}">
                            <label for="tab_title">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <input type="text" id="tab_title" name="tabs['tab_title'][]" class="form-control" value="{{old('tab_title')}}" >
                            @if($errors->has('tab_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('tab_title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div> 
                    </div>
                </div>
                <div class="form-group {{ $errors->has('tab_description') ? 'has-error' : '' }}">
                    <label for="tab_description">Faq Description </label>
                    <textarea class="ckeditor form-control" id="tab_description" name="tabs['tab_description'][]" value="{{old('tab_description')}}" required></textarea>       
                    @if($errors->has('tab_description'))
                        <em class="invalid-feedback">
                            {{ $errors->first('tab_description') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 100%">
                <span class="btn btn-danger pull-right remove-tab mb-2" >
                    Remove Tab
                </span>
            </td>     
        </tr></div>`);
    });

    $(document).on('click', '.remove-button', function(){  
            $(this).parents('.removeCol').remove();
    }); 

    $(document).on('click', '.remove-tab', function(){  
            $(this).closest('.removeTab').remove();
    }); 

    $("#treatment_slug").select2({
        placeholder: "Select Category",
        allowClear: true
    });
});
</script>

@endsection
