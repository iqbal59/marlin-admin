<?php

namespace Modules\TreatmentDetails\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Treatment\Entities\Treatment;

class TreatmentDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'treatment_slug',
        'index_title',
        'details_type',
        'content_image',
        'tab_details',
        'general_details',
        'item_list',
        "order_number",
        'section_order'
    ];

    public function treatments()
    {
        return $this->belongsTo(Treatment::class, 'treatment_slug', 'slug');
    }
    
    protected static function newFactory()
    {
        return \Modules\TreatmentDetails\Database\factories\TreatmentDetailsFactory::new();
    }
}
