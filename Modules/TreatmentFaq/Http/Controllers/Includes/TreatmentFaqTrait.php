<?php

namespace Modules\TreatmentFaq\Http\Controllers\Includes;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\TreatmentFaq\Entities\TreatmentFaq;
use Modules\TreatmentFaq\Repositories\TreatmentFaqRepository as TreatmentFaqRepo;

trait TreatmentFaqTrait
{ 
    public function list(TreatmentFaqRepo $TreatmentFaqRepo)
    {  
        $treatment_faqs = $TreatmentFaqRepo->getForDatatable();   
        return  view('treatmentfaq::listTreatmentFaq', compact('treatment_faqs'));
    }

    public function add(TreatmentFaqRepo $TreatmentFaqRepo)
    { 
        $treatmentList = $TreatmentFaqRepo->all();  
        return view('treatmentfaq::addTreatmentFaq', compact('treatmentList'));               
    }

    public function save(TreatmentFaqRepo $TreatmentFaqRepo, Request $request)
    {   
        $inputData = [
            'treatment_slug'   =>$request->treatment_slug,
            'faq_title'        =>$request->name,
            'faq_description'  => $request->description 
        ];
       
        $treatment_faqs = $TreatmentFaqRepo->save($inputData);    
        connectify('success', 'Success!', 'Faq added successfully');    
        return redirect()
            ->route('admin.treatmentfaq.treatment-faq');
    }

    public function edit(Request $request, TreatmentFaqRepo $TreatmentFaqRepo)
    {  
        $treatment_faq = $TreatmentFaqRepo->get($request->id);    
        $treatmentList = $TreatmentFaqRepo->all();  
        return view('treatmentfaq::editTreatmentFaq', compact('treatment_faq','treatmentList'));
    }

    public function update(Request $request, TreatmentFaqRepo $TreatmentFaqRepo)
    {   
        $inputData = [
            'id'               => $request->id,
            'treatment_slug'   => $request->treatment_slug,
            'faq_title'        => $request->title,
            'faq_description'  => $request->description 
        ];
        $TreatmentFaqRepo->update($inputData);
        connectify('success', 'Success!', 'Faq Updated successfully');    
        return redirect()
            ->route('admin.treatmentfaq.treatment-faq');
    }  

    public function view(Request $request, TreatmentFaqRepo $TreatmentFaqRepo)
    { 
        $faq = $TreatmentFaqRepo->get($request->id);  
        return view('treatmentfaq::viewTreatmentFaq', compact('faq'));
    }

    public function delete(TreatmentFaqRepo $TreatmentFaqRepo, Request $request)
    { 
        if ($TreatmentFaqRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Faq deleted successfully');    
            return redirect()
            ->route('admin.treatmentfaq.treatment-faq');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}        