<?php
namespace Modules\TreatmentFaq\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\TreatmentFaq\Http\Controllers\Includes\TreatmentFaqTrait;

class TreatmentFaqController extends Controller
{
    use TreatmentFaqTrait;
}
