<?php

use Illuminate\Http\Request;
use Modules\TreatmentDetails\Http\Controllers\Api\TreatmentDetailsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/treatmentfaq', function (Request $request) {
    return $request->user();
});

Route::get('/about-treatment/{slug}', [TreatmentDetailsController::class, 'aboutTreatment']);