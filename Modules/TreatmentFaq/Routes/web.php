<?php

use Modules\TreatmentFaq\Http\Controllers\Api\TreatmentFaqController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'treatmentfaq', 'as' => 'treatmentfaq.'], function () {       
        Route::get('/treatment-faq/list', [TreatmentFaqController::class, 'list'])->name('treatment-faq');  
        Route::get('add', [TreatmentFaqController::class, 'add'])->name('add');
        Route::post('save', [TreatmentFaqController::class, 'save'])->name('save');
        Route::get('edit', [TreatmentFaqController::class, 'edit'])->name('edit');
        Route::post('update', [TreatmentFaqController::class, 'update'])->name('update');
        Route::get('view', [TreatmentFaqController::class, 'view'])->name('view');
        Route::get('delete', [TreatmentFaqController::class, 'delete'])->name('delete');
    });
});