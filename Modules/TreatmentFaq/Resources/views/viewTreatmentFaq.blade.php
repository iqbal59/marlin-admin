@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
         View Faq
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr> 
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $faq->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            {{ $faq->faq_title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Treatment
                        </th>
                        <td> 
                            {{ $faq->treatments->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Description
                        </th>
                        <td> 
                            {!! $faq->faq_description !!}
                        </td>
                    </tr>              
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection