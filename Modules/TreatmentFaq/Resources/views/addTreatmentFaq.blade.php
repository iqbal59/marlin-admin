@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        {{ trans('panel.add_treatment_faq') }} 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentfaq.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">         
                <div class="col-md-6">
                <label for="treatment_slug">Treatment <i class="mdi mdi-flag-checkered text-danger "></i> </label> 
                    <select id="treatment_slug" name="treatment_slug" class="js-states form-control">
                        @foreach($treatmentList as $data)  
                            <option value="{{$data['slug']}}">{{$data['name']}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('treatment_slug'))
                        <em class="invalid-feedback">
                            {{ $errors->first('treatment_slug') }}
                        </em>
                    @endif     
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">{{ trans('cruds.user.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Faq Description </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
        $("#treatment_slug").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>

@endsection
