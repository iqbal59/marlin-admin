@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    .treatment-edit-cn span.select2-selection.select2-selection--multiple:after {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 5px;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
    content: "";
}
    </style>

<div class="card treatment-edit-cn">
    <div class="card-header">
        {{ trans('global.edit') }} Treatment 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentfaq.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$treatment_faq['id']}}">
            
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('treatment_plants_id') ? 'has-error' : '' }}">
                    <label for="treatment_slug"> Treatment <i class="mdi mdi-flag-checkered text-danger "></i></label> 
                    <select class="js-states form-control" name="treatment_slug" id="treatment_slug">
                        @foreach($treatmentList as $data)  
                        @if($data['slug'] == $treatment_faq->treatment_slug) 
                        <option value="{{$data['slug']}}" name="treatment_slug" selected=""> {{$data['name']}}  </option>    
                        @else
                        <option value="{{$data['slug']}}" name="treatment_slug"> {{$data['name']}}  </option>
                        @endif
                        @endforeach
                    </select>
                    @if($errors->has('treatment_plants_id'))
                    <em class="invalid-feedback">{{ $errors->first('treatment_plants_id') }}</em>
                    @endif  
                </div> 
            </div>
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('panel.title') }} </label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($treatment_faq) ? $treatment_faq->faq_title : '') }}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description" >
                {!! old('description', isset($treatment_faq) ? $treatment_faq->faq_description : '') !!}
                </textarea>   
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>      
            <br>         
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#treatment_slug").select2({
                placeholder: "Select Category",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
 </script>
 <script>
     $(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: [',', ' ']
})
 </script>
@endsection