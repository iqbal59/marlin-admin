<?php 

namespace Modules\TreatmentFaq\Repositories;

interface TreatmentFaqRepoInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function update(array $input);

    public function get($slug);

    public function getTreatmentFaq($slug);

    public function delete(string $id);
}