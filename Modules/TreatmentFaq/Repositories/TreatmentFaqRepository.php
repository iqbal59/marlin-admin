<?php


namespace Modules\TreatmentFaq\Repositories;
use Illuminate\Database\Eloquent\Builder;
use Modules\TreatmentFaq\Entities\TreatmentFaq;
use Modules\Treatment\Entities\Treatment;

class TreatmentFaqRepository implements TreatmentFaqRepoInterface
{
    public function getForDatatable()
    { 
        return TreatmentFaq::with('treatments')->orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Treatment::orderBy('id', 'DESC')->get()->toArray();
    }

    public function save(array $input)
    { 
        if ($Treatment_faq =  TreatmentFaq::create($input)) {
            return $Treatment_faq;
        }
        return false;
    }

    public function update(array $input)
    { 
        $Treatment_faq = TreatmentFaq::find($input['id']); 
        unset($input['id']);
        if ($Treatment_faq->update($input)) {
            return $Treatment_faq;
        }
        return false;
    }

    public function get($id)
    { 
        return TreatmentFaq::with('treatments')->where('id',$id)->first();
    }

    public function getTreatmentFaq($slug)
    {
        $faq = TreatmentFaq::where('treatment_slug',$slug)->get();
        return $faq;
    }

    public function delete(string $id)
    {
        $Treatment_faq = TreatmentFaq::find($id);
        return $Treatment_faq->delete();
    }
}