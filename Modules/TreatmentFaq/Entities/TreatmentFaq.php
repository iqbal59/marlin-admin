<?php

namespace Modules\TreatmentFaq\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Treatment\Entities\Treatment;

class TreatmentFaq extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'treatment_slug',
        'faq_title',
        'faq_description',
        'status'
    ];

    public function treatments()
    {
        return $this->belongsTo(Treatment::class, 'treatment_slug', 'slug');
    }
    
    protected static function newFactory()
    {
        return \Modules\TreatmentFaq\Database\factories\TreatmentFaqFactory::new();
    }
}
