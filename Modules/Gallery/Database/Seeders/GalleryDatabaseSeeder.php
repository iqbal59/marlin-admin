<?php

namespace Modules\Gallery\Database\Seeders;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GalleryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '236',
                'title'      => 'gallery_create',
                'created_at' => '2021-11-02 12:14:15',
                'updated_at' => '2021-11-02 12:14:15',
            ],
            [
                'id'         => '237',
                'title'      => 'gallery_edit',
                'created_at' => '2021-11-02 12:14:15',
                'updated_at' => '2021-11-02 12:14:15',
            ],
            [
                'id'         => '238',
                'title'      =>  'gallery_show',
                'created_at' => '2021-11-02 12:14:15',
                'updated_at' => '2021-11-02 12:14:15',
            ],
            [
                'id'         => '239',
                'title'      => 'gallery_delete',
                'created_at' => '2021-11-02 12:14:15',
                'updated_at' => '2021-11-02 12:14:15',
            ],
            [
                'id'         => '240',
                'title'      => 'gallery_access',
                'created_at' => '2021-11-02 12:14:15',
                'updated_at' => '2021-11-02 12:14:15',
            ],

        ];

        Permission::insert($permissions);
    }
}
