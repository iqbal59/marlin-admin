<?php


namespace Modules\Gallery\Repositories;
use Modules\Gallery\Models\Gallery;
use Modules\Gallery\Entities\GalleryImages;
use Illuminate\Database\Eloquent\Builder;

class GalleryRepository implements GalleryRepositoryInterface
{
    public function getForDatatable()
    { 
        return Gallery::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Gallery::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($gallery =  Gallery::create($input)) {
            return $gallery;
        }
        return false;
    }

    public function get($id)
    { 
        return Gallery::where('id',$id)->first();
    }

    public function getImages($id)
    {
        return GalleryImages::where('gallery_id',$id)->get();
    }

    public function delete(string $id)
    {
        $doctor = GalleryImages::find($id);
        return $doctor->delete();
    }

    public function deleteGallery(string $id)
    {
        $gallery = Gallery::find($id);
        $galleryImages = GalleryImages::where('gallery_id',$gallery->id)->delete();
        return $gallery->delete();
    }

    public function getImage($id)
    {
        return GalleryImages::find($id);
    }

    public function addImage($filePath, $id,$size)
    { 
        $image = new GalleryImages();
        $image->images = $filePath;
        $image->size = $size;
        $image->title = "content";
        $image->gallery_id = $id;
        $image->save();
        return $image->id;
    }

}