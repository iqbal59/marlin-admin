<?php

namespace Modules\Gallery\Repositories;

interface GalleryRepositoryInterface
{
    public function getForDatatable();

    public function all();

    public function save(array $input);

    public function get($id);

    public function getImages($id);

    public function deleteGallery(string $id);

    public function delete(string $id);

    public function getImage($id);

    public function addImage($filePath, $id,$size);
    
}