<?php

namespace Modules\GalleryImages\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class GalleryImages extends Model
{
    use HasFactory;
    protected $table = 'gallery_images';
    protected $fillable = ['id','gallery_id','images','size'];
   
}