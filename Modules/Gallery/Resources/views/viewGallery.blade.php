@extends('layouts.admin')
@section('content')
@section('head')
<meta name="csrf_token" id="csrf_token" content="{{ csrf_token() }}" />
@endsection
<style>
    .dz-image img {
    -o-object-fit: cover;
    object-fit: cover;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    max-width: 100%;
    max-height: 100%;
    border-radius: 0px !important;
}
.dz-preview i.fas.fa-times {
    color: #fff;
    background: red;
    padding: 3px 5px;
    margin-top: 5px;
    border-radius: 50px;
    cursor: pointer !important;
}
.dz-filename {
    display: none !important;
}
</style>
<div class="card">
    <div class="card-header">
       View Gallery 
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $gallery->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Image
                        </th>
                        <td>
                            <div id="image-dropzone" class="dropzone" action="{{route('admin.gallery.images_save', ['gallery_id' => $gallery->id])}}" data-gallery_id="{{$gallery->id}}" data-fetch-url="{{route('admin.gallery.images_data', ['gallery_id' => $gallery->id])}}" data-delete-url="{{route('admin.gallery.images_delete')}}">
                            </div>
                        </td>
                    </tr> 
                    
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
$(function() {
    var myDropzone = $("#image-dropzone")[0].dropzone; 
    myDropzone.options.headers = {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    };
    myDropzone.options.addRemoveLinks = true;
    myDropzone.options.dictRemoveFile = "<i class='fas fa-times'></i>";
    myDropzone.on("removedfile", function(file) {
    $.get($('#image-dropzone').data('delete-url') + '?id=' + file.serverId);
    });
    myDropzone.on("success", function(file, response) {
        file.serverId = response.serverId;
        $(".dz-preview:last-child").data('server-id', file.serverId);
        $(".dz-preview:last-child").find('.dz-image img').attr('src', response.imagesUrl);
        if (response.video) {
            $(".dz-preview:last-child").find('.dz-image').addClass('video');
            $(".dz-preview:last-child").find(".dz-image.video").append("<i class='fas fa-video'></i>");
        }
    });
    $.ajax({
        "url": $('#image-dropzone').data('fetch-url'),
        "type": "GET",
        "success": function(data) {
            $.each(data, function(key, value) {
                var mockFile = { name: value.url, size: value.size, serverId: value.id, title: value.title };
                myDropzone.emit("addedfile", mockFile);
                myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
                myDropzone.files.push(mockFile);
                myDropzone.emit("complete", mockFile);
                $(".dz-preview:last-child").data('server-id', mockFile.serverId);
                $(".dz-preview:last-child").data('title', value.title);
                $(".dz-preview:last-child").data('description', value.description);
                if (value.video) {
                    $(".dz-preview:last-child").find('.dz-image').addClass('video');
                    $(".dz-preview:last-child").find(".dz-image.video").append("<i class='fas fa-video'></i>");
                }
            });
        }
    });
});
</script>
@endsection