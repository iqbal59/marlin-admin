@extends('layouts.admin')
@section('content')
@push('vendor-style')
@endpush
@push('vendor-script')
@endpush

<div class="card">
    <div class="card-header">
      Add Gallery  
    </div>
    <div class="card-body">
        <form action="{{ route("admin.gallery.save") }}"  id="productForm" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>              
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent
@endsection
