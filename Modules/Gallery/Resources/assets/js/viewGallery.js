 {
     { dd(999) } }
 $(function() {
     alert(99999);
     var myDropzone = $("#image-dropzone")[0].dropzone;
     myDropzone.options.headers = {
         'x-csrf-token': _token,
     };
     myDropzone.options.addRemoveLinks = true;
     myDropzone.options.dictRemoveFile = "<i class='fas fa-times'></i>";

     myDropzone.on("removedfile", function(file) {
         $.get($('#image-dropzone').data('delete-url') + '?id=' + file.serverId);
     });

     myDropzone.on("success", function(file, response) {
         file.serverId = response.serverId;
         $(".dz-preview:last-child").data('server-id', file.serverId);
         $(".dz-preview:last-child").find('.dz-image img').attr('src', response.imagesUrl);
         if (response.video) {
             $(".dz-preview:last-child").find('.dz-image').addClass('video');
             $(".dz-preview:last-child").find(".dz-image.video").append("<i class='fas fa-video'></i>");
         }
     });

     $.ajax({
         "url": $('#image-dropzone').data('fetch-url'),
         "type": "GET",
         "success": function(data) {
             $.each(data, function(key, value) {
                 var mockFile = { name: value.url, size: value.size, serverId: value.id, title: value.title };
                 myDropzone.emit("addedfile", mockFile);
                 myDropzone.options.thumbnail.call(myDropzone, mockFile, value.url);
                 myDropzone.files.push(mockFile);
                 myDropzone.emit("complete", mockFile);
                 $(".dz-preview:last-child").data('server-id', mockFile.serverId);
                 $(".dz-preview:last-child").data('title', value.title);
                 $(".dz-preview:last-child").data('description', value.description);
                 if (value.video) {
                     $(".dz-preview:last-child").find('.dz-image').addClass('video');
                     $(".dz-preview:last-child").find(".dz-image.video").append("<i class='fas fa-video'></i>");
                 }
             });
         }
     });

     $("body").on('click', '.dz-preview', function() {
         var image = $(this);
         var id = image.data('server-id');
         var title = image.data('title');
         var description = image.data('description');
         if (title == 'undefined' || title == 'null' || title == false || title == null) {
             title = '';
         }
         if (description == 'undefined' || description == 'null' || description == false || description == null) {
             description = '';
         }
         var html = "Title:<input type='text' id='imgTitle' name='title' class='form-control' value='" + title + "' /><br/>\
        Description:<textarea id='imgDescription' name='description' class='form-control'>" + description + "</textarea>";
         bootbox.confirm(html, function(result) {
             if (result) {
                 $.ajax({
                     "url": $('#image-dropzone').data('title-update-url'),
                     "type": "post",
                     "data": {
                         _token: _token,
                         id: id,
                         title: $("#imgTitle").val(),
                         description: $("#imgDescription").val()
                     },
                     "success": function(data) {
                         image.data('title', data.title);
                         image.data('description', data.description);
                     }
                 });
             }
         });
     });

     $("#image-dropzone").sortable({
         items: '.dz-preview',
         cursor: 'move',
         opacity: 0.5,
         containment: '#image-dropzone',
         distance: 20,
         tolerance: 'pointer',
         update: function(event, ui) {

             var order = [];
             $(".dz-preview").each(function() {
                 order.push($(this).data('server-id'));
             });

             $.ajax({
                 "url": $('#image-dropzone').data('reorder-url'),
                 "type": "post",
                 "data": {
                     _token: _token,
                     order: order
                 }
             });

         }
     });

 });