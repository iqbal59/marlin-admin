<?php
use Modules\Gallery\Http\Controllers\GalleryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'gallery', 'as' => 'gallery.'], function () {         
        Route::get('/', [GalleryController::class, 'list'])->name('list');
        Route::get('add', [GalleryController::class, 'add'])->name('add');
        Route::post('save', [GalleryController::class, 'save'])->name('save');
        Route::post('saveImage', [GalleryController::class, 'saveImage'])->name('saveImage');
        Route::get('delete', [GalleryController::class, 'deleteGallery'])->name('delete');
        Route::get('view', [GalleryController::class, 'view'])->name('view');
            Route::prefix('images')->name('images_')->group(function () {
                Route::post('save', 'GalleryController@imagesSave')->name('save');
                Route::get('data/{gallery_id}', 'GalleryController@imagesData')->name('data');
                Route::get('delete', 'GalleryController@imagesDelete')->name('delete');
        });       
    });
});

