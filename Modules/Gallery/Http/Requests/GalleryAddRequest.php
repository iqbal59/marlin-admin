<?php

namespace Modules\Gallery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('gallery_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
