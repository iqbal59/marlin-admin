<?php

namespace Modules\Gallery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GallerySaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('gallery_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:50|min:2',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
        ];
    }
}
