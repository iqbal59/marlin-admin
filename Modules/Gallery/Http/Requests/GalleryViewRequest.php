<?php

namespace Modules\Gallery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('gallery_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
