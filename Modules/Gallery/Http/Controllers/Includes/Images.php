<?php

namespace  Modules\Gallery\Http\Controllers\Includes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage; 
use Modules\Gallery\Repositories\GalleryRepositoryInterface as GalleryRepository;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface as BlogsRepository;
use Modules\Banner\Repositories\Includes\BannerContent\BannerContentRepositoryInterface as BannerContentRepository;

use Modules\Gallery\Models\GalleryImages;
trait Images
{
    public function imagesData(GalleryRepository $galleryRepo,Request $request,BannerContentRepository $bannerContentRepo)
    {  
        $images = $bannerContentRepo->getImages($request->gallery_id); 
        foreach ($images as $key => $image) { 
            $images[$key]->url = Storage::disk('public')->url($image->images);          
        }
        return $images;
    }

    public function imagesSave(Request $request,GalleryRepository $galleryRepo,BannerContentRepository $bannerContentRepo)
    {     
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filePath = 'bannerimages';
            $fileName = Storage::disk('public')->putFile('bannerimages/',$file);
            $size = $file->getSize(); 
            $id = $bannerContentRepo->addImage($fileName, $request->gallery_id,$size);
            $image = $bannerContentRepo->getImage($id);  
            $imagesUrl = Storage::disk('public')->url($fileName); 
            return response()->json(['status' => true, 'serverId' => $id,'size'=>$size, 'imagesUrl' => $imagesUrl], 200);
        }
        return response()->json('Upload Failed!', 400);
    }

    public function imagesDelete(Request $request, GalleryRepository $galleryRepo,BannerContentRepository $bannerContentRepo)
    {  
        $bannerContentRepo->imageDelete($request->id);     
    }
}
