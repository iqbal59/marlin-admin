<?php

namespace Modules\Gallery\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Gallery\Http\Requests\GalleryAddRequest;
use Modules\Gallery\Http\Requests\GalleryViewRequest;
use Modules\Gallery\Http\Requests\GallerySaveRequest;
use Modules\Gallery\Http\Requests\GalleryDeleteRequest;
use Modules\Gallery\Repositories\GalleryRepositoryInterface as GalleryRepository;
use Illuminate\Support\Facades\Storage;
use Modules\Gallery\Http\Controllers\Includes\Images;

class GalleryController extends Controller
{
    use Images;

    public function list(GalleryRepository $galleryRepo)
    {  
        $galleries = $galleryRepo->getForDatatable();  
        return view('gallery::listGallery', compact('galleries'));                         
    }

    public function add(GalleryAddRequest $request , GalleryRepository $galleryRepo)
    {   
        return view('gallery::addGallery');        
    }

    public function save(GallerySaveRequest $request, GalleryRepository $galleryRepo)
    {   
        $inputData = [
            'title'  => $request->title,
        ];
        $gallery = $galleryRepo->save($inputData);  
        connectify('success', 'Success!', 'Gallery added successfully');               
        return redirect()
        ->route('admin.gallery.list');
    } 

    public function view(GalleryViewRequest $request,GalleryRepository $galleryRepo)
    { 
        $gallery = $galleryRepo->get($request->id);       
        return view('gallery::viewGallery', compact('gallery'));
    }

    public function deleteGallery(GalleryRepository $galleryRepo, GalleryDeleteRequest $request)
    { 
        if ($galleryRepo->deleteGallery($request->id)) {
            connectify('success', 'Success!', 'Gallery deleted successfully');               
            return redirect()
            ->route('admin.gallery.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

}
