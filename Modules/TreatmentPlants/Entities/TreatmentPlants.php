<?php

namespace Modules\TreatmentPlants\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TreatmentPlants extends Model
{
    use HasFactory;

    protected $fillable = ['id','title','days','cost','no_of_travelers','inside_hospital','outside_hospital','description','success_rate','created_by','status','treatment_id','treatment_id','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\TreatmentPlants\Database\factories\TreatmentPlantsFactory::new();
    }
}
