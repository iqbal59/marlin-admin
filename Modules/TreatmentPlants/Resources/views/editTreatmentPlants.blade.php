@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    </style>

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} Treatment Plants
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentplants.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$treatment->id}}">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($treatment) ? $treatment->title : '') }}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            
            <div class="form-group {{ $errors->has('days') ? 'has-error' : '' }}">
                <label for="days"> Days </label>
                <input type="number" id="days" name="days" class="form-control" value="{{ old('days', isset($treatment) ? $treatment->days : '') }}" >
                @if($errors->has('days'))
                    <em class="invalid-feedback">
                        {{ $errors->first('days') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 
            
            <div class="form-group {{ $errors->has('cost') ? 'has-error' : '' }}">
                <label for="cost"> Cost </label>
                <input type="number" id="cost" name="cost" class="form-control" value="{{ old('cost', isset($treatment) ? $treatment->cost : '') }}" >
                @if($errors->has('cost'))
                    <em class="invalid-feedback">
                        {{ $errors->first('cost') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('no_of_travelers') ? 'has-error' : '' }}">
                <label for="no_of_travelers"> No Of Travelers </label>
                <input type="number" id="no_of_travelers" name="no_of_travelers" class="form-control" value="{{ old('no_of_travelers', isset($treatment) ? $treatment->no_of_travelers : '') }}" >
                @if($errors->has('no_of_travelers'))
                    <em class="invalid-feedback">
                        {{ $errors->first('no_of_travelers') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('inside_hospital') ? 'has-error' : '' }}">
                <label for="inside_hospital"> Inside Hospital </label>
                <input type="text" id="inside_hospital" name="inside_hospital" class="form-control" value="{{ old('no_of_travelers', isset($treatment) ? $treatment->no_of_travelers : '') }}" >
                @if($errors->has('inside_hospital'))
                    <em class="invalid-feedback">
                        {{ $errors->first('inside_hospital') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('outside_hospital') ? 'has-error' : '' }}">
                <label for="outside_hospital"> Ouside  Hospital </label>
                <input type="text" id="outside_hospital" name="outside_hospital" class="form-control" value="{{ old('outside_hospital', isset($treatment) ? $treatment->outside_hospital : '') }}" >
                @if($errors->has('outside_hospital'))
                    <em class="invalid-feedback">
                        {{ $errors->first('outside_hospital') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('success_rate') ? 'has-error' : '' }}">
                <label for="success_rate"> Success Rate </label>
                <input type="text" id="success_rate" name="success_rate" class="form-control" value="{{ old('success_rate', isset($treatment) ? $treatment->success_rate : '') }}" >
                @if($errors->has('success_rate'))
                    <em class="invalid-feedback">
                        {{ $errors->first('success_rate') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description" required>
                {!! old('description', isset($treatment) ? $treatment->description : '') !!}
                </textarea>   
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status', $treatment->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>   
            <br>         
            {{-- <div class="form-group {{ $errors->has('treatment_id') ? 'has-error' : '' }}">
            <label for="treatment_id">Treatment <i class="mdi mdi-flag-checkered text-danger "></i> </label>
                <select id="treatment_id" name="treatment_id" class="js-states form-control">
                    <option value="">Select Treatment Plans</option>
                    @foreach($allTreatment as $key => $value) 
                    

                    <option value="{{$value->id}}" {{(old('treatment_id', $treatment->treatment_id) == $value->id ? 'selected' : '')}} > {{$value->name}} </option>

                    @endforeach
                </select>
                @if($errors->has('treatment_id'))
                <em class="invalid-feedback">
                    {{ $errors->first('treatment_id') }}
                </em>
            @endif               
            <p class="helper-block">
                {{ trans('cruds.user.fields.name_helper') }}
            </p>
            </div>   --}}
               
 
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#treatment_id").select2({
                placeholder: "Select Treatment",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection