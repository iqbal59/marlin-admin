@extends('layouts.admin')
@section('content')
<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="card">
    <div class="card-header">
       View TreatmentPlans
    </div>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped video-table">
                <tbody>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            {{ $treatment->title }}
                        </td>
                    </tr>
                    @if($treatment->days !="")
                    <tr>

                        <th>
                            Days
                        </th>
                        <td> 
                            {{ $treatment->days }}
                        </td>
                    </tr>
                    @endif
                    @if($treatment->description !="")
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {!! $treatment->description !!} 
                        </td>
                    </tr>
                    @endif
                    @if($treatment->cost !="")
                    <tr>
                        <th>
                        Cost
                        </th>
                        <td>
                            {{ $treatment->cost }}
                        </td>
                    </tr>    
                    @endif
                    @if($treatment->no_of_travelers !="")
                    <tr>
                        <th>
                        No Of Travelers
                        </th>
                        <td>
                            {{ $treatment->no_of_travelers }}
                        </td>
                    </tr>  
                    @endif
                    @if($treatment->inside_hospital !="")
                    <tr>
                        <th>
                        Inside Hospital
                        </th>
                        <td>
                            {{ $treatment->inside_hospital }}
                        </td>
                    </tr>  
                    @endif
                    @if($treatment->outside_hospital !="")
                    <tr>
                        <th>
                        Outside Hospital
                        </th>
                        <td>
                            {{ $treatment->outside_hospital }}
                        </td>
                    </tr>  
                    @endif
                    @if($treatment->success_rate !="")
                    <tr>
                        <th>
                        Sucess Rate
                        </th>
                        <td>
                            {{ $treatment->success_rate }}
                        </td>
                    </tr>  
                    @endif
                    @if($addedBy!= "")
                    <tr>
                        <th>Added By</th>
                        <td>{{ $addedBy->first_name }}</td>
                    </tr> 
                    @endif
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection