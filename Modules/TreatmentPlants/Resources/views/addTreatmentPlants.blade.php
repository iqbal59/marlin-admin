@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        {{ trans('panel.add_treatment') }} 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.treatmentplants.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">Title<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}" >
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                        <div class="form-group {{ $errors->has('days') ? 'has-error' : '' }}">
                            <label for="days">Days </label>
                            <input type="number" min ="1" id="days" name="days" class="form-control" value="{{old('days')}}" >
                            @if($errors->has('days'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('days') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('no_of_travelers') ? 'has-error' : '' }}">
                        <label for="no_of_travelers">No Of Travelers</label>
                        <input type="number" min="1" id="no_of_travelers" name="no_of_travelers" class="form-control" value="{{old('no_of_travelers')}}" >
                        @if($errors->has('no_of_travelers'))
                            <em class="invalid-feedback">
                                {{ $errors->first('no_of_travelers') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                        <div class="form-group {{ $errors->has('inside_hospital') ? 'has-error' : '' }}">
                            <label for="inside_hospital">Inside Hospital </label>
                            <input type="text" id="inside_hospital" name="inside_hospital" class="form-control" value="{{old('inside_hospital')}}" >
                            @if($errors->has('inside_hospital'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('inside_hospital') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('outside_hospital') ? 'has-error' : '' }}">
                        <label for="outside_hospital">Outside Hospital</label>
                        <input type="text" id="outside_hospital" name="outside_hospital" class="form-control" value="{{old('outside_hospital')}}" >
                        @if($errors->has('outside_hospital'))
                            <em class="invalid-feedback">
                                {{ $errors->first('outside_hospital') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                        <div class="form-group {{ $errors->has('success_rate') ? 'has-error' : '' }}">
                            <label for="success_rate">Success  Rate (%)</label>
                            <input type="text" id="success_rate" name="success_rate" class="form-control" value="{{old('success_rate')}}">
                            @if($errors->has('success_rate'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('success_rate') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.user.fields.name_helper') }}
                            </p>
                        </div>  
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Description </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('cost') ? 'has-error' : '' }}">
                        <label for="cost">Cost</label>
                        <input type="number" id="cost" name="cost" class="form-control" value="{{old('cost')}}" >
                        @if($errors->has('cost'))
                            <em class="invalid-feedback">
                                {{ $errors->first('cost') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>
                
            
        
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Department",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.treatment.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection
