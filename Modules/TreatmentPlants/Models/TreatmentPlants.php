<?php

namespace Modules\TreatmentPlants\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
 

class TreatmentPlants extends Model
{
    use HasFactory;
     protected $table = 'treatment_plants';

    protected $fillable = ['id','title','days','cost','no_of_travelers','inside_hospital','outside_hospital','description','success_rate','created_by','status','treatment_id','treatment_id','added_by'];

    // public function treatments()
    // {
    //     return $this->hasMany('Modules\Treatment\Models\Treatment');
    // }

    public function getCanDeleteAttribute()
    {
        // if (
        //     $this->treatments->count()
        // ) {
        //     return false;
        // }
        return true;
    }


   
}