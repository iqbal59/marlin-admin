<?php


namespace Modules\TreatmentPlants\Repositories;
use Modules\TreatmentPlants\Models\TreatmentPlants;
use Illuminate\Database\Eloquent\Builder;

class TreatmentPlantsRepository implements TreatmentPlantsRepositoryInterface
{
    public function getForDatatable()
    { 
        return TreatmentPlants::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return TreatmentPlants::orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($treatment =  TreatmentPlants::create($input)) {
            return $treatment;
        }
        return false;
    }

    public function get($id)
    { 
        return TreatmentPlants::where('id',$id)->first();
    }

    public function update(array $input)
    { 
        $treatment = TreatmentPlants::find($input['id']); 
        unset($input['id']);
        if ($treatment->update($input)) {
            return $treatment;
        }
        return false;
    }

    public function delete(string $id)
    {
        $treatment = TreatmentPlants::find($id);
        return $treatment->delete();
    }

    public function getTreatments($treatmentId)
    {
        $treatment = TreatmentPlants::where('status',1)->where('treatment_id',$treatmentId)->get();
        return $treatment;
    }

}