<?php

namespace Modules\TreatmentPlants\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsAddRequest;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsSaveRequest;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsEditRequest;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsUpdateRequest;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsViewRequest;
use Modules\TreatmentPlants\Http\Requests\TreatmentPlantsDeleteRequest;
use Modules\TreatmentPlants\Repositories\TreatmentPlantsRepositoryInterface as TreatmentPlantsRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use App\Repositories\User\UserRepositoryInterface as UserRepository;


class TreatmentPlantsController extends Controller
{
    public function list(TreatmentPlantsRepository $treatmentplansRepo)
    {  
        $treatments = $treatmentplansRepo->getForDatatable();   
        return view('treatmentplants::listTreatmentPlants', compact('treatments'));                         
    }

    public function add(TreatmentPlantsAddRequest $request , TreatmentPlantsRepository $treatmentplansRepo)
    {  
        return view('treatmentplants::addTreatmentPlants');               
    }

    public function save(TreatmentPlantsSaveRequest $request, TreatmentPlantsRepository $treatmentplansRepo)
    {   
         $inputData = [
            'title'  => $request->title,
            'days'  => $request->days,
            'cost'  => $request->cost,
            'no_of_travelers'=>$request->no_of_travelers,
            'inside_hospital'=>$request->inside_hospital,
            'outside_hospital'=>$request->outside_hospital,
            'success_rate'=>$request->success_rate,
            'description'=>$request->description,      
            'added_by'     => auth()->user()->id,    
        ];
        $treatment = $treatmentplansRepo->save($inputData);    
        connectify('success', 'Success!', 'TreatmentPlants added successfully');         
        return redirect()
            ->route('admin.treatmentplants.list');
    } 

    public function view(TreatmentPlantsViewRequest $request,TreatmentPlantsRepository $treatmentplansRepo,UserRepository $userRepo)
    {
        $treatment = $treatmentplansRepo->get($request->id);
        $addedBy = $userRepo->get($treatment->added_by);   
        return view('treatmentplants::viewTreatmentPlants', compact('addedBy','treatment'));
    }

    public function edit(TreatmentPlantsEditRequest $request, TreatmentPlantsRepository $treatmentplansRepo, TreatmentRepository $treatmentRepo)
    {  
        $treatment =$treatmentplansRepo->get($request->id); 
        $allTreatment = $treatmentRepo->allTreatments();        
        return view('treatmentplants::editTreatmentPlants', compact('treatment','allTreatment'));
    }

    public function update(TreatmentPlantsUpdateRequest $request, TreatmentPlantsRepository $treatmentplansRepo)
    { 
        $inputData = [
            'id'  => $request->id,
            'title'  => $request->title,
            'days'  => $request->days,
            'cost'  => $request->cost,
            'no_of_travelers'=>$request->no_of_travelers,
            'inside_hospital'=>$request->inside_hospital,
            'outside_hospital'=>$request->outside_hospital,
            'success_rate'=>$request->success_rate,
            'description'=>$request->description,
            'treatment_id'=>$request->treatment_id,
        ]; 
        
        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }

        $treatment = $treatmentplansRepo->update($inputData);
        connectify('success', 'Success!', 'TreatmentPlants updated successfully');         
        return redirect()
            ->route('admin.treatmentplants.list');
    }  

    public function delete(TreatmentPlantsRepository $treatmentplansRepo, TreatmentPlantsDeleteRequest $request)
    { 
        if ($treatmentplansRepo->delete($request->id)) {
            connectify('success', 'Success!', 'TreatmentPlants deleted successfully');         
            return redirect()
            ->route('admin.treatmentplants.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

     
}
