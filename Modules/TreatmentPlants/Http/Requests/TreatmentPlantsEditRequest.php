<?php

namespace Modules\TreatmentPlants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentPlantsEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatmentplants_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
