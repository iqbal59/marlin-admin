<?php

namespace Modules\TreatmentPlants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentPlantsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatmentplants_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
