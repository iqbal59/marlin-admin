<?php

namespace Modules\TreatmentPlants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentPlantsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatmentplants_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
