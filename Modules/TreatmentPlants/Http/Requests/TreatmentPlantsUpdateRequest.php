<?php

namespace Modules\TreatmentPlants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentPlantsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatmentplants_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            // 'days' => 'required',
            // 'cost' => 'required',
            // 'no_of_travelers' => 'required',
            // 'inside_hospital' => 'required',
            // 'outside_hospital' => 'required',
            // 'description' => 'required',
            // 'success_rate' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            // 'days.required' => ':attribute is required',
            // 'cost.required' => ':attribute is required',
            // 'no_of_travelers.required' => ':attribute is required',
            // 'inside_hospital.required' => ':attribute is required',
            // 'outside_hospital.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
            // 'success_rate.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            // 'days' => 'Days',
            // 'cost' => 'Cost',
            // 'no_of_travelers' => 'No of Travelers',
            // 'inside_hospital' => 'Inside Hospital',
            // 'outside_hospital' => 'Ouside Hospital',
            // 'description' => 'Description',
            // 'success_rate' => 'Sucess Rate',
        ];
    }
}
