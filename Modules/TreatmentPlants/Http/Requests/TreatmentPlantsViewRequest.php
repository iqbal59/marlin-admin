<?php

namespace Modules\TreatmentPlants\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentPlantsViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('treatmentplants_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
