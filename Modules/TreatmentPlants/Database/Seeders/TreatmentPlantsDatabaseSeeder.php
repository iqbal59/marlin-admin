<?php

namespace Modules\TreatmentPlants\Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TreatmentPlantsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '211',
                'title'      => 'treatmentplants_create',
                'created_at' => '2021-10-19 12:14:15',
                'updated_at' => '2021-10-19 12:14:15',
            ],
            [
                'id'         => '212',
                'title'      => 'treatmentplants_edit',
                'created_at' => '2021-10-19 12:14:15',
                'updated_at' => '2021-10-19 12:14:15',
            ],
            [
                'id'         => '213',
                'title'      =>  'treatmentplants_show',
                'created_at' => '2021-10-19 12:14:15',
                'updated_at' => '2021-10-19 12:14:15',
            ],
            [
                'id'         => '214',
                'title'      => 'treatmentplants_delete',
                'created_at' => '2021-10-19 12:14:15',
                'updated_at' => '2021-10-19 12:14:15',
            ],
            [
                'id'         => '215',
                'title'      => 'treatmentplants_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],

        ];

        Permission::insert($permissions);
    }
}
