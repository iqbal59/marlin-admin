<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentPlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatment_plants', function (Blueprint $table) {
            $table->id();
            $table->text('title')->nullable();
            $table->integer('days')->nullable();
            $table->double('cost')->nullable();
            $table->integer('no_of_travelers')->nullable();
            $table->text('inside_hospital')->nullable();
            $table->text('outside_hospital')->nullable();
            $table->string('description')->nullable();
            $table->string('success_rate')->nullable();
            $table->string('created_by')->nullable();
            $table->boolean('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatment_plants');
    }
}
