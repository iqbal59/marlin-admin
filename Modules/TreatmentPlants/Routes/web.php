<?php
use Modules\TreatmentPlants\Http\Controllers\TreatmentPlantsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('treatmentplants')->group(function() {
    Route::get('/', 'TreatmentPlantsController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'treatmentplants', 'as' => 'treatmentplants.'], function () {    
        Route::get('/', [TreatmentPlantsController::class, 'list'])->name('list');
        Route::get('add', [TreatmentPlantsController::class, 'add'])->name('add');
        Route::post('save', [TreatmentPlantsController::class, 'save'])->name('save');
        Route::get('edit', [TreatmentPlantsController::class, 'edit'])->name('edit');
        Route::post('update', [TreatmentPlantsController::class, 'update'])->name('update');
        Route::get('view', [TreatmentPlantsController::class, 'view'])->name('view');
        Route::get('delete', [TreatmentPlantsController::class, 'delete'])->name('delete');       
    });
});
