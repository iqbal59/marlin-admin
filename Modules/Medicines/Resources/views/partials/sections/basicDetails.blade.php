<style>
#pills-tabContent .table th, .table td {
    white-space: normal;
    line-height: 24px;
}
</style>
<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $medicine->id }}</td>
            </tr>
            <tr>
                <th>{{ trans('cruds.user.fields.name') }}</th>
                <td>{{ $medicine->name }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ $brand->name }}</td>
            </tr>
            @if($medicine->description != null)
            <tr>
                <th>Description</th>
                <td>{!! $medicine->description !!} </td>
            </tr>
            @endif
            @if($medicine->image !='')
            <tr>
                <th>Image</th>
                <td><img src="{{asset('storage/'.$medicine->image)}}" alt="" width="500px"></td>
            </tr>
            @endif
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif   
        </tbody>
    </table>
</div>