<div class="card">
    <div class="card-header">
        Faq of Hospital
        @can('faq_create')
        <a class="btn btn-warning float-right" href="{{route('admin.faq.add', ['Id' => $medicine->id,'type'=>'Medicine','retrn_url'=> "admin.medicine.view",'retrn_prms' => $medicine->id ])}}">
            Add Faq
        </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Faq">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title</th>
                        {{-- <th>Subject</th>
                        <th>Question</th>
                        <th>Answer By</th>
                        <th>Date</th>                         --}}
                        <th>Actions</th>  
                    </tr>
                </thead>
                <tbody>
                    @foreach($faqs as $key => $faq)
                        <tr data-entry-id="{{ $faq->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $faq->title ?? '' }}</td>
                            {{-- <td>{{ $faq->subject ?? '' }}</td>
                            <td>{{ $faq->question ?? '' }}</td>
                            <td>{{ $faq->answerBy ?? '' }}</td>
                            <td>{{ $faq->entrydate ?? '' }}</td>                             --}}
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.faq.view', ['id' => $faq->id]) }}">
                                    {{ trans('global.view') }}
                                </a> 

                                <a class="btn btn-xs btn-info" href="{{route('admin.faq.edit', ['id' => $faq->id, 'hospital_id' => $faq->id,'retrn_url'=> "admin.medicine.view",'retrn_prms' => $medicine->id ]) }}">
                                    {{ trans('global.edit') }}
                                </a>   

                                <form action="{{ route('admin.faq.delete',  ['id' => $faq->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="id" value="{{$faq->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="retrn_url" value="admin.medicine.view">
                                    <input type="hidden" name="retrn_prms" value={{$medicine->id}}>
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>  
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  $('.datatable-Faq:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection



