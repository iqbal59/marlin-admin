@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
        Add Medicine 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.medicine.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : '' }}">
                        <label for="brand_id">  Brand<i class="mdi mdi-flag-checkered text-danger "></i>
                        </label>
                            <select id="brand_id" name="brand_id" class="js-states form-control">
                                <option value=""> Brand </option>
                                @foreach($brands as $brand) 
                                <option value="{{$brand->id}}" name="brand_id" id="brand_id">                                   
                                {{$brand->name}}
                                </option>
                                @endforeach
                            </select>
                            @if($errors->has('brand_id'))
                            <em class="invalid-feedback">
                                {{ $errors->first('brand_id') }}
                            </em>
                        @endif               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>    
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                        <label for="price">Price </label>
                        <input type="number" id="price" name="price" class="form-control" value="{{old('price')}}" >
                        @if($errors->has('price'))
                            <em class="invalid-feedback">
                                {{ $errors->first('price') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>  --}}
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">Description  </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>  
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="image">{{ trans('panel.image') }}  </label>
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>   
                </div>
                
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#brand_id").select2({
                placeholder: "Select Brand",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.treatment.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection
