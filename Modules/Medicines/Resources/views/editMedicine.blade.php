@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<style>
    input[type="number"] {
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    }
    
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
    }
    
    .number-input {
      border: 2px solid #ddd;
      display: inline-flex;
    }
    
    .number-input,
    .number-input * {
      box-sizing: border-box;
    }
    
    .number-input button {
      outline:none;
      -webkit-appearance: none;
      background-color: transparent;
      border: none;
      align-items: center;
      justify-content: center;
      width: 3rem;
      height: 2rem;
      cursor: pointer;
      margin: 0;
      position: relative;
    }
    
    .number-input button:before,
    .number-input button:after {
      display: inline-block;
      position: absolute;
      content: '';
      width: 1rem;
      height: 2px;
      background-color: #212121;
      transform: translate(-50%, -50%);
    }
    .number-input button.plus:after {
      transform: translate(-50%, -50%) rotate(90deg);
    }
    
    .number-input input[type=number] {
      font-family: sans-serif;
      max-width: 3rem;
      padding: .5rem;
      border: solid #ddd;
      border-width: 0 2px;
      font-size: 1rem;
      height: 2rem;
      font-weight: bold;
      text-align: center;
    }
    i.mdi.mdi-minus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    i.mdi.mdi-plus {
        width: 2rem;
        text-align: center;
        line-height: 30px;
    }
    span.on {
        width: 95px;
        float: left;
    }
    .custom-switch {
        padding-left: 0;
    }
    .custom-switch .custom-control-label::before{
        width: 2rem;
    }
    .custom-switch .custom-control-label::after {
        width: calc(1.3rem - 4px);
        }
    .badge-contr span.off {
        width: 70px;
        float: left;
    }
    .badge-contr span.on {
        float: none;
    }
    .medicine-edit-cn span.select2-selection.select2-selection--multiple:after {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 5px;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
    content: "";
}
    </style>

<div class="card medicine-edit-cn">
    <div class="card-header">
        {{ trans('global.edit') }} Medicine
    </div>

    <div class="card-body">
        <form action="{{ route("admin.medicine.update") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$medicines->id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Name<i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($medicines) ? $medicines->name : '') }}"  >
                        @if($errors->has('name'))
                            <em class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : '' }}">
                        <label for="brand_id">  Brand<i class="mdi mdi-flag-checkered text-danger "></i>
                        </label>
                        <select id="brand_id" name="brand_id" class="js-states form-control">
                        @foreach ($brands as $key => $value)
                            <option value="{{$value->id}}" {{(old('brand_id', $medicines->medicine_brands_id) == $value->id ? 'selected' : '')}} > {{$value->name}} </option>
                        @endforeach
                        </select>
                        @if($errors->has('brand_id'))
                            <em class="invalid-feedback">{{ $errors->first('brand_id') }}</em>
                        @endif
                    </div>
                </div>
            </div>

            {{-- <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                <label for="price">Price </label>
                <input type="number" id="price" name="price" class="form-control" value="{{ old('price', isset($medicines) ? $medicines->price : '') }}"  >
                @if($errors->has('price'))
                    <em class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>   --}}



            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> {{ trans('panel.description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description"  >
                {!! old('description', isset($medicines) ? $medicines->description : '') !!}
                </textarea>   
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div class="row">
                <div class="col-md-6">
                        <label for="image"> {{ trans('panel.image') }} </label>                
                        @if(Storage::disk('public')->exists($medicines->image)) 
                            <img class="preview" src="{{asset('storage/'.$medicines->image)}}" alt="" width="150px">
                                <a href="{{route('admin.medicine.remove_file',['id'=>$medicines->id])}}" class="confirm-delete text-danger">
                                    <i class="material-icons pink-text">clear</i>
                                </a>
                        @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                            <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                        </div>  
                        @endif
                
            </div>    
            
                <div class="col-md-6">
                        <label for="name">Diseases</label>
                        @if($selecteValue == null)
                            <select class="js-example-basic-multiple" name="diseases[]" multiple="multiple">
                                @foreach($diseases as $data)  
                                    <option value="{{$data->id}}">{{$data->title}}</option>                 
                                @endforeach
                            </select>
                        @else
                            <select class="js-example-basic-multiple" name="diseases[]" multiple="multiple">
                                @foreach($diseases as $data)  
                                <option value="{{$data->id}}" @if(in_array($data->id, $selecteValue)) selected @endif>{{$data->title}}</option>
                        @endforeach
                    </select>
                    @endif    
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 badge-contr">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="status">Status</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" {{ old('status',$medicines->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                            <span class="off">No</span>
                            <label class="custom-control-label" for="status"></label>
                            <span class="on">Yes</span>
                        </div>
                    </div> 
                </div>
            </div>
            
            <br>       
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#brand_id").select2({
                placeholder: "Select Brand",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
 </script>
@endsection