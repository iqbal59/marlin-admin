@extends('layouts.admin')
@section('content')
@can('medicine_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.medicine.add") }}">
                <i class="mdi mdi-plus"></i>
               Add Medicine 
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        List Medicine 
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">
                        </th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Name</th>
                        <th>
                            Status
                        </th>
                        <th>{{ trans('cruds.user.fields.roles') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($medicines as $key => $medicine)
                        <tr data-entry-id="{{ $medicine->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $medicine->name }}</td> 
                            <td>
                                @if($medicine->status == 1)
                                    <i class="mdi mdi-checkbox-marked-circle-outline btn-success"></i>
                                    Active
                                @else
                                    <i class="mdi mdi-close-circle-outline btn-danger"></i>
                                    In Active
                                @endif
                            </td>                           
                            <td>  
                            @can('medicine_show')          
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.medicine.view', ['id' => $medicine->id]) }}">
                                        {{ trans('global.view') }}
                                    </a> 
                            @endcan
                            @can('medicine_edit')  
                                    <a class="btn btn-xs btn-info" href="{{route('admin.medicine.edit', ['id' => $medicine->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                             
                            @endcan 
                            @can('medicine_delete')      
                                    <form action="{{ route('admin.medicine.delete',  ['id' => $medicine->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$medicine->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            @endcan        
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('medicinebrands_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 100,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection