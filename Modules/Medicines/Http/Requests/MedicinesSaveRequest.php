<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicine_create') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'brand_id' => 'required',
            // 'price' => 'required',
            // 'image' => 'required',
            // 'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'brand_id.required' => ':attribute is required',
            // 'price.required' => ':attribute is required',
            // 'image.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'brand_id' => 'Brand',
            // 'price' => 'Price',
            // 'image' => 'Image',
            // 'description' => 'Image',
        ];
    }
}
