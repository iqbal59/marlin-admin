<?php

namespace Modules\Medicines\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class MedicineFaqRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'medicine_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'medicine_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'medicine_id' => 'Medicine Id',
        ];
    }
}
