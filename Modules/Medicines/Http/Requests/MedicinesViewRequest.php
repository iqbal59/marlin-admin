<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('services_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
