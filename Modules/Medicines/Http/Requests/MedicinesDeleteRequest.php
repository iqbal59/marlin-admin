<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesDeleteRequest extends FormRequest
{
     
    public function authorize()
    {
        return auth()->user()->can('medicine_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
