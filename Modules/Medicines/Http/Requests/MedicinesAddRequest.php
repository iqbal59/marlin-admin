<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicine_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
