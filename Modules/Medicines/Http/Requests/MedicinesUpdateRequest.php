<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicine_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'brand_id' => 'required',
            // 'price' => 'required',
            // 'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ':attribute is required',
            'brand_id.required' => ':attribute is required',
            // 'price.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Name',
            'brand_id' => 'Brand',
            // 'price' => 'Price',
            // 'description' => 'Image',
        ];
    }
}
