<?php

namespace Modules\Medicines\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MedicinesEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('medicine_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
