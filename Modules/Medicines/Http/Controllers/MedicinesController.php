<?php

namespace Modules\Medicines\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Modules\Medicines\Http\Requests\MedicinesAddRequest;
use Modules\Medicines\Http\Requests\MedicinesSaveRequest;
use Modules\Medicines\Http\Requests\MedicinesEditRequest;
use Modules\Medicines\Http\Requests\MedicinesUpdateRequest;
use Modules\Medicines\Http\Requests\MedicinesViewRequest;
use Modules\Medicines\Http\Requests\MedicinesDeleteRequest;
use Modules\Medicines\Repositories\MedicinesRepositoryInterface as MedicinesRepository;
use Modules\MedicineBrands\Repositories\MedicineBrandsRepositoryInterface as MedicineBrandsRepository;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Intervention\Image\ImageManagerStatic as Image;
use App\Repositories\User\UserRepositoryInterface as UserRepository;
class MedicinesController extends Controller
{
    public function list(MedicinesRepository $medicinesRepo)
    {  
        $medicines = $medicinesRepo->getForDatatable();     
        return view('medicines::listMedicines', compact('medicines'));                         
    }

    public function add(MedicinesAddRequest $request , MedicinesRepository $medicinesRepo, MedicineBrandsRepository $medicinebrandRepo)
    {     
        $brands = $medicinebrandRepo->all();
        return view('medicines::addMedicines', compact('brands'));    
    }

    public function save(MedicinesSaveRequest $request, MedicinesRepository $medicinesRepo)
    {   
        $inputData = [
            'name'  => $request->name,
            'price'  => 1,
            'medicine_brands_id'  => $request->brand_id,
            'description'  => $request->description,
            'added_by'     => auth()->user()->id,
        ];     
        if($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(430, 310); 
            $image_resize->save(storage_path('app/public/medicine_image/' .$filename));  
            $inputData['image'] = 'medicine_image/' .$filename;           

        }

        $medicine = $medicinesRepo->save($inputData);  
        connectify('success', 'Success!', 'Medicine Added successfully');        
            return redirect()
            ->route('admin.medicine.list');
    } 


    public function view(MedicinesViewRequest $request,MedicinesRepository $medicinesRepo,MedicineBrandsRepository $medicinebrandRepo,FaqRepository $faqRepo,UserRepository $userRepo)
    { 
        $medicine = $medicinesRepo->get($request->id);  
        $brand = $medicinebrandRepo->get($medicine->medicine_brands_id);
        $faqs = $faqRepo->getFaq($request->id,$type="Medicine"); 
        $addedBy = $userRepo->get($medicine->added_by); 
        return view('medicines::viewMedicine', compact('addedBy','medicine','brand','faqs'));
    }

    public function edit(MedicinesEditRequest $request,DiseasesRepository $diseasesRepo, MedicinesRepository $medicinesRepo,MedicineBrandsRepository $medicinebrandRepo)
    {  
        $medicines =$medicinesRepo->get($request->id);   
        $brands = $medicinebrandRepo->all();
        $diseases = $diseasesRepo->all(); 
        $selecteValue = json_decode($medicines->diseases_id);
        return view('medicines::editMedicine', compact('diseases','selecteValue','medicines','brands'));
    }


    public function update(MedicinesUpdateRequest $request, MedicinesRepository $medicinesRepo)
    {     
        $inputData = [
            'id'  => $request->id,
            'name'  => $request->name,
            'medicine_brands_id'  => $request->brand_id,
            'description'  => $request->description,
            'price'  => $request->price,
            'diseases_id' => json_encode($request->diseases),
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
        if ($request->hasFile('image')) { 
            $this->_removeFile($medicinesRepo, $request->id);                 
            // $imageUrl = Storage::disk('public')->putFile('medicine_image/',$request->file('image'));
            // $inputData['image'] = $imageUrl;
            $image       = $request->file('image');
            $filename    = $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(430, 310); 
            $image_resize->save(storage_path('app/public/medicine_image/' .$filename));         
            $inputData['image'] = 'medicine_image/' .$filename;     
        } 

        $medicine = $medicinesRepo->update($inputData);
        connectify('success', 'Success!', 'Medicine updated successfully');        
        return redirect()
            ->route('admin.medicine.list');
    }  

    public function removeFile(MedicinesRepository $medicinesRepo,Request $request)
    {   
        if ($this->_removeFile($medicinesRepo, $request->id)) {
            $medicinesRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'File deleted successfully');        
        return redirect()
            ->route('admin.medicine.edit', ['id' => $request->id]);
    }

    private function _removeFile($medicinesRepo, $id)
    { 
        $logo = $medicinesRepo->get($id);
        if (Storage::disk('public')->delete($logo->image)) {
            return true;
        }
        return false;
    }

    public function delete(MedicinesRepository $medicinesRepo, MedicinesDeleteRequest $request)
    { 
        if ($medicinesRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Medicine deleted successfully');        
            return redirect()
            ->route('admin.medicine.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
}
