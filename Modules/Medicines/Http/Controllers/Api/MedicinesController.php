<?php

namespace Modules\Medicines\Http\Controllers\Api;

use Modules\Medicines\Repositories\MedicinesRepositoryInterface as MedicinesRepository;
use Modules\MedicineBrands\Repositories\MedicineBrandsRepositoryInterface as MedicineBrandsRepository;
use Illuminate\Routing\Controller;
use Exception;
use Modules\Medicines\Models\Medicines;
use Modules\Enquiry\Models\Enquiry;
use Modules\Enquiry\Http\Requests\Api\EnquiryAddRequest;
use Modules\Medicines\Http\Requests\Api\MedicineFaqRequest;

use Illuminate\Http\Request;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;

class MedicinesController extends Controller
{
    public function list(MedicinesRepository $medicinesRepo, MedicineBrandsRepository $medicinebrandRepo)
    {        
        try {
            $medicines = $medicinesRepo->all(); 
            $medicineBrand = $medicinebrandRepo->all();
            $topbrand=array();
            foreach ($medicineBrand as $key => $value) {
                $topbrand[$key]['name']=$value->name;      
            }                 
            $data = compact("medicines","topbrand");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }  
    } 

    public function medicineEnquiry(EnquiryAddRequest $request,Enquiry $enquiry)
    {   
        try {
            $medicine = Medicines::find($request->medicine_id);
            if($medicine != null){
                $enquiry = new Enquiry;
                $enquiry->enquiryable_id  = $request->medicine_id;
                $enquiry->enquiryable_type  = $request->name;
                $enquiry->email  = $request->email;
                $enquiry->contact  = $request->contact;
                $enquiry->name  = $request->name;
                $enquiry->message  = $request->message;
                $medicine->enquiries()->save($enquiry); 

                $response = ['status' => true,'message' => 'Medicine Enquiry Added Successfully'];
                return response()->json($response, 200);
            }        
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function allMedicineFaq(FaqRepository $faqRepo)
    {  
        try {
            $allMedicineFaq = $faqRepo->getAllFaq($askable_type="Medicine");
            $data = compact("allMedicineFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function MedicineFaq(MedicineFaqRequest $request,FaqRepository $faqRepo)
    {   
        try {
            $medicineId = $request->medicine_id;
            $medicineFaq = $faqRepo->getFaq($medicineId,$askable_type="Medicine");
            $data = compact("medicineFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}
