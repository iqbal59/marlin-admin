<?php
use Modules\Medicines\Http\Controllers\MedicinesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('medicines')->group(function() {
    Route::get('/', 'MedicinesController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'medicine', 'as' => 'medicine.'], function () {         
            Route::get('/', [MedicinesController::class, 'list'])->name('list');
            Route::get('add', [MedicinesController::class, 'add'])->name('add');
            Route::post('save', [MedicinesController::class, 'save'])->name('save');
            Route::get('edit', [MedicinesController::class, 'edit'])->name('edit');
            Route::post('update', [MedicinesController::class, 'update'])->name('update');
            Route::get('view', [MedicinesController::class, 'view'])->name('view');
            Route::get('delete', [MedicinesController::class, 'delete'])->name('delete');
            Route::get('remove_file', [MedicinesController::class, 'removeFile'])
            ->name('remove_file'); 
            
    });
});
