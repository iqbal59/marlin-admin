<?php

use Illuminate\Http\Request;
use Modules\Medicines\Http\Controllers\Api\MedicinesController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/medicines', function (Request $request) {
    return $request->user();
});

Route::prefix('medicines')->group(function () {
    Route::get('/list', [MedicinesController::class, 'list']);
    Route::post('/medicineEnquiry', [MedicinesController::class, 'medicineEnquiry']);

    Route::get('/allMedicineFaq', [MedicinesController::class, 'allMedicineFaq']);
    Route::post('/MedicineFaq', [MedicinesController::class, 'MedicineFaq']);
});