<?php


namespace Modules\Medicines\Repositories;
use Modules\Medicines\Models\Medicines;
use Illuminate\Database\Eloquent\Builder;

class MedicinesRepository implements MedicinesRepositoryInterface
{
    public function getForDatatable()
    { 
        return Medicines::orderBy('id', 'DESC')->get();
    }

    public function all()
    { 
        return Medicines::where('status',1)->orderBy('id', 'DESC')->get();
    }

    public function save(array $input)
    { 
        if ($medicine =  Medicines::create($input)) {
            return $medicine;
        }
        return false;
    }

    public function get($id)
    { 
        return Medicines::where('id',$id)->first();
    }   

    public function resetFile(string $id)
    {
        $medicine = Medicines::find($id);
        $medicine->image = '';
        return $medicine->save();
    }

    public function update(array $input)
    { 
        $medicine = Medicines::find($input['id']); 
        unset($input['id']);
        if ($medicine->update($input)) {
            return $medicine;
        }
        return false;
    }

    public function delete(string $id)
    {
        $doctor = Medicines::find($id);
        return $doctor->delete();
    }



}