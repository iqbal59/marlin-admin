<?php

namespace Modules\Medicines\Repositories;

interface MedicinesRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($id);

    public function resetFile(string $id);

    public function update(array $input);

    public function delete(string $id);
    
}