<?php

namespace Modules\Medicines\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Medicines extends Model
{
    use HasFactory;

    protected $fillable = ['name','price','medicine_brands_id','diseases_id','description','logo','image','status','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\Medicines\Database\factories\MedicinesFactory::new();
    }
}
