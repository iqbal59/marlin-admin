<?php

namespace Modules\Medicines\Models;
use Modules\Enquiry\Models\Enquiry;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicines extends Model
{
    use HasFactory;
 
    protected $table = 'medicines';

    protected $fillable = ['name','price','diseases_id','medicine_brands_id','description','logo','image','status','added_by'];

    public function medicineBrands()
    {
        return $this->belongsTo('Modules\MedicineBrands\Models\MedicineBrands',  'medicine_brands_id', 'id');
    }

    public function enquiries()
    { 
        return $this->morphMany(Enquiry::class, 'enquiryable');
    }

}