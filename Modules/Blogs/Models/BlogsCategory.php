<?php

namespace Modules\Blogs\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogsCategory extends Model
{
    use HasFactory;
    protected $table = 'blogs_categories';

    protected $fillable = [
        'id','title','parent_id','is_editable'
    ];
    public function blogs()
    {
        return $this->hasMany('Modules\Blogs\Models\Blogs');
    }

    public function getCanDeleteAttribute()
    { 
        if (
            $this->blogs->count()
        ) {
            return false;
        }
        return true;
    }
   
}