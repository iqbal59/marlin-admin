<?php

namespace Modules\Blogs\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class BlogImages extends Model
{
    use HasFactory;
    protected $table = 'blog_images';
    protected $fillable = ['id','blog_id','images','size'];
}