<?php

namespace Modules\Blogs\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Blogs extends Model
{
    use HasFactory;
    use Sluggable;
    protected $table = 'blogs';

    protected $fillable = [
        'id','slug','sub_title','success_story_id','title','meta_title','meta_tags','meta_keywords','meta_description','content','image','blogs_category_id','status','type','diseases_id'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function blogcategory()
    {
        return $this->belongsTo('Modules\Blogs\Models\BlogsCategory',  'blogs_category_id', 'id');
    }

   
}