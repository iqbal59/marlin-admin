<?php

use Illuminate\Http\Request;
use Modules\Blogs\Http\Controllers\Api\BlogsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/blogs', function (Request $request) {
    return $request->user();
});

Route::prefix('blogs')->group(function () {
    Route::get('/list', [BlogsController::class, 'list']);
    Route::post('/blog_view', [BlogsController::class, 'blogView']);
    Route::post('/categoryBasedBlogList', [BlogsController::class, 'categoryBasedBlogList']);
    
    Route::post('/bestHospitalForBlog', [BlogsController::class, 'bestHospitalForBlog']);
    Route::post('/bestDoctorForBlog', [BlogsController::class, 'bestDoctorForBlog']);

    Route::post('/blogSearch', [BlogsController::class, 'blogSearch']);
    Route::post('/blogCategoryListing', [BlogsController::class, 'blogCategoryListing']);

    Route::get('/allBlogsFaq', [BlogsController::class, 'allBlogsFaq']);
    Route::post('/BlogFaq', [BlogsController::class, 'BlogFaq']);

    Route::get('/recentPosts', [BlogsController::class, 'recentPosts']);
    Route::post('/blogSuccessStories', [BlogsController::class, 'blogSuccessStories']);


});