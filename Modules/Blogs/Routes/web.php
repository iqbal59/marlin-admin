<?php
use Modules\Blogs\Http\Controllers\BlogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('blogs')->group(function() {
    Route::get('/', 'BlogsController@index');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::middleware('auth')->prefix('blogCategory')->name('blogCategory.')->group(function () {
        Route::get('/blogCategoryList', [BlogsController::class, 'blogCategoryList'])->name('blogCategoryList');
        Route::get('/add', [BlogsController::class, 'add'])->name('add');
        Route::post('save', [BlogsController::class, 'save'])->name('save');
        Route::get('edit', [BlogsController::class, 'edit'])->name('edit');
        Route::post('update', [BlogsController::class, 'update'])->name('update');
        Route::get('view', [BlogsController::class, 'view'])->name('view');
        Route::get('delete', [BlogsController::class, 'delete'])->name('delete');
            
    });  
    Route::middleware('auth')->prefix('blog')->name('blog.')->group(function () {
    
        Route::get('/List', [BlogsController::class, 'List'])->name('List');
        // Route::get('/cmsTypeBasedList', [BlogsController::class, 'cmsTypeBasedList'])->name('cmsTypeBasedList');
        Route::get('/addBlog', [BlogsController::class, 'addBlog'])->name('addBlog');
        Route::post('saveBlogs', [BlogsController::class, 'saveBlogs'])->name('saveBlogs');
        Route::get('editBlogs', [BlogsController::class, 'editBlogs'])->name('editBlogs');
        Route::post('updateBlogs', [BlogsController::class, 'updateBlogs'])->name('updateBlogs');
        Route::get('viewBlogs', [BlogsController::class, 'viewBlogs'])->name('viewBlogs');
        Route::get('deleteBlogs', [BlogsController::class, 'deleteBlogs'])->name('deleteBlogs');
        // Route::post('ckeditor_image_upload', [BlogsController::class, 'ckeditor_image_upload'])->name('ckeditor_image_upload');
        Route::get('remove_file', [BlogsController::class, 'removeFile'])->name('remove_file');
       
    });  
});