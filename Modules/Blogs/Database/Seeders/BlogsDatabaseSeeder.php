<?php

namespace Modules\Blogs\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class BlogsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '306',
                'title'      => 'blogs_category_access',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '307',
                'title'      => 'blogs_category_create',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '308',
                'title'      => 'blogs_category_edit',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '309',
                'title'      => 'blogs_category_show',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '310',
                'title'      => 'blogs_category_delete',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],

            [
                'id'         => '311',
                'title'      => 'blogs_access',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '312',
                'title'      => 'blogs_create',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '313',
                'title'      => 'blogs_edit',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '314',
                'title'      => 'blogs_show',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
            [
                'id'         => '315',
                'title'      => 'blogs_delete',
                'created_at' => '2021-11-09 12:14:15',
                'updated_at' => '2021-11-09 12:14:15',
            ],
        ];

        Permission::insert($permissions);
    }
}
