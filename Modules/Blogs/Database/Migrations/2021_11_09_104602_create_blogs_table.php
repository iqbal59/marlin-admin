<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->index('slug')->unique();
            $table->text('title')->nullable();
            $table->text('sub_title')->nullable();
            $table->text('content')->nullable();
            $table->text('mini_content')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_tags')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('image')->nullable();
            $table->boolean('status')->default(true);
            $table->string('order')->default(0);
            $table->integer('banner_id')->nullable();
            $table->integer('type');
            $table->integer('blogs_category_id')->nullable();
            $table->integer('parent_id')->default(0);
            $table->boolean('is_editable')->default(true);
            $table->string('diseases_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
