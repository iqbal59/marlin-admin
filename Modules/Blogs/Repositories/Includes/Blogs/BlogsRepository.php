<?php

namespace Modules\Blogs\Repositories\Includes\Blogs;

use App\Repositories\BaseRepository;
use Modules\Blogs\Models\Blogs;
use Modules\Blogs\Models\BlogImages;
use Illuminate\Database\Eloquent\Builder;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface;

class BlogsRepository implements BlogsRepositoryInterface
{
    public function all()
    {
        return Blogs::where('status', 1)->get();
    }
    public function getForDatatable()
    {
        return Blogs::get();
    }

    public function getBySlug($slug)
    {
        return Blogs::with('blogcategory')->where('slug', $slug)->first();
    }

    public function get($Id)
    {
        return Blogs::with('blogcategory')->where('id', $Id)->first();
    }

    public function save(array $input)
    {
        if ($blogs =  Blogs::create($input)) {
            return $blogs;
        }
        return false;
    }

    public function update(array $input)
    {
        $blogs = Blogs::find($input['id']);
        unset($input['id']);
        if ($blogs->update($input)) {
            return $blogs;
        }
        return false;
    }

    public function delete(string $id)
    {
        $blogs = Blogs::find($id);
        return $blogs->delete();
    }

    public function resetFile(string $id)
    {
        $blogs = Blogs::find($id);
        $blogs->image = '';
        return $blogs->save();
    }

    public function categoryBasedBlog($categoryId)
    {
        return Blogs::where('status', 1)->where('blogs_category_id', $categoryId)->get();
    }

    public function getallBlogs($searchText)
    {
        return Blogs::where('title', 'like', '%'.$searchText.'%')->get();
    }

    public function getRecentBlogs()
    {
        return Blogs::where('status', 1)->orderBy('id', 'DESC')->take(4)->get();
    }

    public function addImage($filePath, $id, $size)
    {
        $image = new BlogImages();
        $image->images = $filePath;
        $image->size = $size;
        $image->blog_id = $id;
        $image->status = 1;
        $image->save();
        return $image->id;
    }

    public function deleteGallery(string $id)
    {
        $gallery = Blogs::find($id);
        return $galleryImages = BlogImages::where('blog_id', $gallery->id)->delete();
    }

    public function getImage($id)
    {
        return BlogImages::find($id);
    }

    public function getImages($id)
    {
        return BlogImages::where('blog_id', $id)->get();
    }

    public function imageDelete(string $id)
    {
        $blog = BlogImages::find($id);
        return $blog->delete();
    }
}