<?php

namespace Modules\Blogs\Repositories\Includes\Blogs;

interface BlogsRepositoryInterface
{
    public function all();

    public function getForDatatable();

    public function get($Id);

    public function getBySlug($slug);

    public function save(array $input);

    public function update(array $input);

    public function delete(string $id);

    public function resetFile(string $id);

    public function categoryBasedBlog($categoryId);

    public function getallBlogs($searchText);

    public function getRecentBlogs();

    public function addImage($filePath, $id, $size);

    public function deleteGallery(string $id);

    public function getImage($id);

    public function getImages($id);

    public function imageDelete(string $id);
}