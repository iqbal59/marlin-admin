<?php

namespace Modules\Blogs\Repositories\Includes\BlogsCategory;

use App\Repositories\BaseRepository;
use Modules\Blogs\Models\BlogsCategory;
use Illuminate\Database\Eloquent\Builder;
use Modules\Blogs\Repositories\Includes\BlogsCategory\BlogsCategoryRepositoryInterface;

class BlogsCategoryRepository implements BlogsCategoryRepositoryInterface
{
    public function all()
    {
        return BlogsCategory::get();
    }

    public function save(array $input)
    {
        if ($blogsCategory =  BlogsCategory::create($input)) {
            return $blogsCategory;
        }
        return false;
    }

    public function get($categoryId)
    {
        return BlogsCategory::find($categoryId);
    }

    public function update(array $input)
    {
        $blogCategory = BlogsCategory::find($input['id']);
        unset($input['id']);
        if ($blogCategory->update($input)) {
            return $blogCategory;
        }
        return false;
    }

    public function delete(string $id)
    {
        $blogCategory = BlogsCategory::find($id);
        return $blogCategory->delete();
    }
    
    
}
