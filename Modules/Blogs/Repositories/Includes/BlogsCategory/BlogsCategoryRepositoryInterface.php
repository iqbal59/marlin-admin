<?php

namespace Modules\Blogs\Repositories\Includes\BlogsCategory;

interface BlogsCategoryRepositoryInterface
{
    public function all();

    public function save(array $input);

    public function get($categoryId);

    public function update(array $input);

    public function delete(string $id);

}
