@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        View Blog
        <a class="btn btn-primary float-right" href="{{ url()->previous() }}">
            {{ trans('global.back_to_list') }}
        </a>
    </div>
    
    <div class="card-body">
        
        @include('blogs::blogs.partials.tabSection')
        
    </div>
</div>
@endsection