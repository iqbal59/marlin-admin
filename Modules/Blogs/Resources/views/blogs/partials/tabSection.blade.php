<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-success mx-2 active" 
                    id="pills-basic-tab" 
                    data-toggle="pill" 
                    href="#pills-basic" 
                    role="tab" 
                    aria-controls="pills-basic" 
                    aria-selected="true">Info</a>
            </li>     
            {{-- <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-multi-tab" 
                    data-toggle="pill" 
                    href="#pills-multi" 
                    role="tab" 
                    aria-controls="pills-multi" 
                    aria-selected="true">Multiple Image</a>
            </li>        --}}
            <li class="nav-item">
                <a class="nav-link btn-success mx-2" 
                    id="pills-faq-tab" 
                    data-toggle="pill" 
                    href="#pills-faq" 
                    role="tab" 
                    aria-controls="pills-faq" 
                    aria-selected="false">Faq</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-basic" role="tabpanel" aria-labelledby="pills-basic-tab">
                @include('blogs::blogs.partials.sections.basicDetails')
            </div>
            {{-- <div class="tab-pane fade" id="pills-multi" role="tabpanel" aria-labelledby="pills-multi-tab">
                @include('blogs::blogs.partials.sections.multiTable')
            </div> --}}
 
            <div class="tab-pane fade" id="pills-faq" role="tabpanel" aria-labelledby="pills-faq-tab">
                @include('blogs::blogs.partials.sections.faqTable')
            </div>
        </div>
    </div>
</div>