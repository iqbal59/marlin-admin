<div class="card">
    <div class="card-header">
        Faq of Blog
        @can('faq_create')
        <a class="btn btn-warning float-right" href="{{route('admin.faq.add', ['Id' => $blogs->id,'type'=>'Blogs','retrn_url'=> "admin.blog.viewBlogs",'retrn_prms' => $blogs->id ])}}">
            Add Faq                                                                                                                                                      
        </a>
        @endcan
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Faq">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title</th>
                        <th>Subject</th>
                        <th>Question</th>
                        <th>Answer By</th>
                        <th>Date</th>                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($faqs as $key => $faq)
                        <tr data-entry-id="{{ $faq->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $faq->title ?? '' }}</td>
                            <td>{{ $faq->subject ?? '' }}</td>
                            <td>{{ $faq->question ?? '' }}</td>
                            <td>{{ $faq->answerBy ?? '' }}</td>
                            <td>{{ $faq->entrydate ?? '' }}</td>                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 10,
  });
  $('.datatable-Faq:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection



