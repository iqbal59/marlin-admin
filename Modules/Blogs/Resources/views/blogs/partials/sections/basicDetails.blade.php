<style>
    .table.video-table th, .table.video-table td {
        line-height: 24px;
        white-space: normal;
    }
    </style>
<div class="mb-2">
    <table class="table table-bordered table-striped video-table">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $blogs->id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ $blogs->title }}</td>
            </tr>
            <tr>
                <th>Category</th>
                <td> {{ $blogs->blogcategory->title }}</td>
            </tr>
            <tr>
                <th>Content</th>
                <td>{!! $blogs->content !!}  </td>
            </tr>
            <tr>
                <th>{{ trans('panel.meta_title') }}</th>
                <td>{{ $blogs->meta_title }} </td>
            </tr>
            <tr>
                <th>
                Type
                </th>
                <td>
                    @if($blogs->type == 0)
                        Block
                    @else
                        Pages
                    @endif
                </td>
            </tr>            
            <tr>
                <th>
            {{ trans('panel.meta_description') }}
                </th>
                <td>
                    {!! $blogs->meta_description !!}
                </td>
            </tr>
            <tr>
                <th>
                {{ trans('panel.meta_tags') }}
                </th>
                <td>
                    {{ $blogs->meta_tags }}
                </td>
            </tr>
            <tr>
                <th>
                {{ trans('panel.meta_keywords') }}
                </th>
                <td>
                    {{ $blogs->meta_keywords }}
                </td>
            </tr>                     
            <tr>
                <th>
                Meta Image
                </th>                        
                <td>
                @if($blogs->image !=null)
                <img src="{{asset('storage/'.$blogs->image)}}" alt="" width="500px">
                @endif
                </td>                
            </tr>            
        </tbody>
    </table>
</div>