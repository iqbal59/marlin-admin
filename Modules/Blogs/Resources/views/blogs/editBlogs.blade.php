@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<style>
    .iframe-contr iframe{
        max-width: 100%;
        height: 275px;
    }
    .prevv img.preview {
        width: 100%;
        margin-top: 8px;
        height: 266px;
        object-fit: cover;
    }
    </style>
    
    
    <style>
        input[type="number"] {
        -webkit-appearance: textfield;
        -moz-appearance: textfield;
        appearance: textfield;
        }
        
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
          -webkit-appearance: none;
        }
        
        .number-input {
          border: 2px solid #ddd;
          display: inline-flex;
        }
        
        .number-input,
        .number-input * {
          box-sizing: border-box;
        }
        
        .number-input button {
          outline:none;
          -webkit-appearance: none;
          background-color: transparent;
          border: none;
          align-items: center;
          justify-content: center;
          width: 3rem;
          height: 2rem;
          cursor: pointer;
          margin: 0;
          position: relative;
        }
        
        .number-input button:before,
        .number-input button:after {
          display: inline-block;
          position: absolute;
          content: '';
          width: 1rem;
          height: 2px;
          background-color: #212121;
          transform: translate(-50%, -50%);
        }
        .number-input button.plus:after {
          transform: translate(-50%, -50%) rotate(90deg);
        }
        
        .number-input input[type=number] {
          font-family: sans-serif;
          max-width: 3rem;
          padding: .5rem;
          border: solid #ddd;
          border-width: 0 2px;
          font-size: 1rem;
          height: 2rem;
          font-weight: bold;
          text-align: center;
        }
        i.mdi.mdi-minus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        i.mdi.mdi-plus {
            width: 2rem;
            text-align: center;
            line-height: 30px;
        }
        span.on {
            width: 95px;
            float: left;
        }
        .custom-switch {
            padding-left: 0;
        }
        .custom-switch .custom-control-label::before{
            width: 2rem;
        }
        .custom-switch .custom-control-label::after {
            width: calc(1.3rem - 4px);
            }
        .badge-contr span.off {
            width: 70px;
            float: left;
        }
        .badge-contr span.on {
            float: none;
        }
        </style>



<div class="card">
    <div class="card-header">
      Edit Blogs
    </div>

    <div class="card-body">
        <form action="{{ route("admin.blog.updateBlogs") }}" method="POST" enctype="multipart/form-data">
            @csrf 
            <input type="hidden" name="id" value="{{$blogs->id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name"> {{ trans('panel.title') }}<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($blogs) ? $blogs->title : '') }}" required>
                        @if($errors->has('department'))
                            <em class="invalid-feedback">
                                {{ $errors->first('department') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    <label for="category_id"> {{ trans('panel.category_id') }} <i class="mdi mdi-flag-checkered text-danger "></i></label>  
                
                    <select id="category_id" name="category_id" class="js-states form-control">
                        @foreach ($blogCategory as $key => $value)
                        <option value="{{$value->id}}" {{(old('category_id', $blogs->blogs_category_id) == $value->id ? 'selected' : '')}} > {{$value->title}} </option>
                        @endforeach
                    </select>
                    <p class="helper-block">
                        {{ trans('cruds.user.fields.name_helper') }}
                    </p>
                     </div> 
                </div>
            </div> 

            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label for="type"> Type<i class="mdi mdi-flag-checkered text-danger "></i></label>  
                        <select class="js-states form-control" name="type" id="type" required="true"> 
                            <option {{ ($blogs->type) == 1 ? 'selected' : '' }}  value="1">Block</option>
                            <option {{ ($blogs->type) == 2 ? 'selected' : '' }}  value="2">Pages</option> 
                        </select>
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                        <label for="meta_title"> {{ trans('panel.meta_title') }}  </label>
                        <input type="text" id="meta_title" name="meta_title" class="form-control" value="{{ old('meta_title', isset($blogs) ? $blogs->meta_title : '') }}" >
                        @if($errors->has('meta_title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div> 
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                <label for="content"> {{ trans('panel.content') }} </label>
                <textarea class="ckeditor form-control" id="content" name="content" >
                {!! old('content', isset($blogs) ? $blogs->content : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('content'))
                    <em class="invalid-feedback">
                        {{ $errors->first('content') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                <label for="meta_description"> {{ trans('panel.meta_description') }} </label>
                <textarea class="ckeditor form-control" id="meta_description" name="meta_description" >
                {!! old('meta_description', isset($blogs) ? $blogs->meta_description : '') !!}
                </textarea>      

                @error('content')<small class="form-text text-danger">{{ $message }}</small>@enderror
                @if($errors->has('meta_description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('meta_description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            
          
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('meta_tags') ? 'has-error' : '' }}">
                        <label for="meta_tags"> {{ trans('panel.meta_tags') }} </label>
                        <input type="text" id="meta_tags" name="meta_tags" class="form-control" value="{{ old('meta_tags', isset($blogs) ? $blogs->meta_tags : '') }}" >
                        @if($errors->has('meta_tags'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_tags') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="meta_keywords"> {{ trans('panel.meta_keywords') }} </label>
                        <input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="{{ old('meta_keywords', isset($blogs) ? $blogs->meta_keywords : '') }}"  >
                        @if($errors->has('meta_keywords'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_keywords') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="name">Tags</label> 
                        <select id="diseases_id" name="diseases_id" class="js-states form-control">
                            @foreach ($tags as $key => $data)
                            <option value="{{$data['slug']}}" {{(old('diseases_id', $blogs->diseases_id) == $data['slug'] ? 'selected' : '')}} > {{$data['title']}} </option>
                            @endforeach
                        </select>     
                    </div>  
                </div>  
                <div class="col-md-6">

                        <label for="image"> {{ trans('panel.image') }} </label>
                        
                        @if(Storage::disk('public')->exists($blogs->image)) 
                        <img class="preview" src="{{asset('storage/'.$blogs->image)}}" alt="" width="150px">
                            <a href="{{route('admin.blog.remove_file',['id'=>$blogs->id])}}" class="confirm-delete text-danger">
                            <i class="material-icons pink-text">clear</i>
                            </a>
                        @else
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">                
                        <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                        @endif
                    </div> 
            </div>


            <div class="row">
                <div class="col-md-6">
                    <label for="name">Patient Stories  </label>
                    @if($selectepatientStories == null)
                        <select class="js-example-basic-multiple" name="patientstories[]" multiple="multiple">
                            @foreach($patientStories as $data)  
                                <option value="{{$data->id}}">{{$data->title}}</option>                 
                            @endforeach
                        </select>
                    @else
                        <select class="js-example-basic-multiple" name="patientstories[]" multiple="multiple">
                            @foreach($patientStories as $data)  
                            <option value="{{$data->id}}" @if(in_array($data->id, $selectepatientStories)) selected @endif>{{$data->title}}</option>
                    @endforeach
                </select>
                @endif    
                </div>
                <div class="col-md-6">
            
            <div class="col-md-6 badge-contr">
                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">Status</label>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" {{ old('status',$blogs->status == 1) ? "checked" : "" }} class="custom-control-input" id="status" name="status">
                        <span class="off">No</span>
                        <label class="custom-control-label" for="status"></label>
                        <span class="on">Yes</span>
                    </div>
                </div> 
            </div>
                </div>
             
            </div>
            <br> 
            <br>
                   
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
 
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#type").select2({
            placeholder: "Select Type",
            allowClear: true
        });
    });
</script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
 </script>
 <script>
    $(document).ready(function() {
        $("#diseases_id").select2({
            placeholder: "Select tag",
            allowClear: true
        });
    });
</script>
@endsection