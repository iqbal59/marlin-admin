@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">Add Blogs
        
    </div>

    <div class="card-body">
        <form action="{{route('admin.blog.saveBlogs')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
            <div class="col-md-6">
                    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                        <label for="category_id">Category<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <select id="category_id" name="category_id" class="js-states form-control">
                                <option value="">Select Category</option>
                                @foreach($blogCategory as $category) 
                                <option value="{{$category->id}}" name="hospital-dropdown" id="hospital-dropdown" >
                                {{$category->title}}
                                </option>
                                @endforeach
                            </select>    
                            @if($errors->has('category_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('category_id') }}
                                </em>
                            @endif           
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label for="type">Type<i class="mdi mdi-flag-checkered text-danger "></i></label>
                            <select id="type" name="type" class="js-states form-control">
                            <option value="">Select Category</option>
                            <option value="1">Block</option>   
                            <option value="2" selected>Pages</option>
                            </select>  
                            @if($errors->has('type'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('type') }}
                                </em>
                            @endif               
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title">{{ trans('panel.title')}} <i class="mdi mdi-flag-checkered text-danger "></i></label>
                        <input type="text" id="title" name="title" class="form-control" value="{{old('title')}}">
                        @if($errors->has('title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : '' }}">
                        <label for="sub_title">{{ trans('panel.sub_title')}}</label>
                        <input type="text" id="sub_title" name="sub_title" class="form-control" value="{{old('sub_title')}}" >
                        @if($errors->has('sub_title'))
                            <em class="invalid-feedback">
                                {{ $errors->first('sub_title') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>  
                </div>
            </div>

            <div class="row ">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content"> {{ trans('panel.content') }} </label>
                        <textarea class="form-control" id="content" name="content" value="{{old('content')}}" required></textarea>       
                        @if($errors->has('content'))
                            <em class="invalid-feedback">
                                {{ $errors->first('content') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('mini_content') ? 'has-error' : '' }}">
                        <label for="mini_content"> {{ trans('panel.mini_content') }} </label>
                        <textarea class="form-control" id="mini_content" name="mini_content" value="{{old('mini_content')}}"></textarea>       
                        @if($errors->has('mini_content'))
                            <em class="invalid-feedback">
                                {{ $errors->first('mini_content') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.user.fields.name_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label for="image">{{ trans('panel.image') }} </label>
                <input type="file" name="image" id="input-file-now-banner" class="dropify validate" data-default-file="" />
                @if($errors->has('image'))
                    <em class="invalid-feedback">
                        {{ $errors->first('image') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div> 

            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('admin.department.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
    CKEDITOR.replace('mini_content', {
        filebrowserUploadUrl: "{{route('admin.department.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
<script>
    $(document).ready(function() {
        $("#category_id").select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
    $(document).ready(function() {
        $("#type").select2({
            placeholder: "Select Type",
            allowClear: true
        });
    });
</script>
@endsection
