@extends('layouts.admin')
@section('content')
@can('blogs_category_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.blogCategory.add") }}">
                <i class="mdi mdi-plus"></i> 
                Add Blog Category
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
       List Blog Category
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>{{ trans('cruds.user.fields.id') }}</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($blogsCategory as $key => $category)
                        <tr data-entry-id="{{ $category->id }}">
                            <td></td>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $category->title ?? '' }}</td>                           
                            <td> 
                            @can('blogs_category_show')                                        
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.blogCategory.view', ['id' => $category->id]) }}">
                                        {{ trans('global.view') }}
                                    </a>                             
                            @endcan
                            @can('blogs_category_edit')   
                                    <a class="btn btn-xs btn-info" href="{{route('admin.blogCategory.edit', ['id' => $category->id]) }}">
                                        {{ trans('global.edit') }}
                                    </a>                        
                            @endcan
                            @can('blogs_category_delete')           
                                    <form action="{{ route('admin.blogCategory.delete',  ['id' => $category->id]) }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="id" value="{{$category->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if($category->can_delete == true)    
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        @else
                                        <button disabled class="addMore btn btn-xs btn-danger" title="Cannot delete: this blog category has child!"> Delete</button>
                                        @endif  
                                    </form>
                            @endcan
                            

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    $.extend(true, $.fn.dataTable.defaults, {
        order: [[ 1, 'asc' ]],
        pageLength: 100,
    });
    $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
})

</script>
@endsection