<?php

namespace Modules\Blogs\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BlogsCategories extends Model
{
    use HasFactory;

    protected $fillable = ['id','title','parent_id','is_editable'];
    
    protected static function newFactory()
    {
        return \Modules\Blogs\Database\factories\BlogsCategoriesFactory::new();
    }
}
