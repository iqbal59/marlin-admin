<?php

namespace Modules\Blogs\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blogs extends Model
{
    use HasFactory;

    protected $fillable = [
    'id',
    'slug',
    'title',
    'sub_title',
    'meta_title',
    'meta_tags',
    'meta_keywords',
    'meta_description',
    'content',
    'mini_content',
    'image',
    'blogs_category_id',
    'type',
    'diseases_id',
    'success_story_id'];
    
    protected static function newFactory()
    {
        return \Modules\Blogs\Database\factories\BlogsFactory::new();
    }
}
