<?php

namespace Modules\Blogs\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BlogImages extends Model
{
    use HasFactory;
    protected $table = 'blog_images';
    protected $fillable = ['id','blog_id','images','size'];
}
