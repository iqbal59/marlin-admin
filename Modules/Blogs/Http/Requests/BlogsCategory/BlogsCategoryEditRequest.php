<?php

namespace Modules\Blogs\Http\Requests\BlogsCategory;

use Illuminate\Foundation\Http\FormRequest;

class BlogsCategoryEditRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}