<?php

namespace Modules\Blogs\Http\Requests\BlogsCategory;

use Illuminate\Foundation\Http\FormRequest;

class BlogsCategoryUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_category_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title'
        ];
    }
}