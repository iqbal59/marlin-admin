<?php

namespace Modules\Blogs\Http\Requests\BlogsCategory;

use Illuminate\Foundation\Http\FormRequest;

class BlogsCategoryAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_category_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}