<?php

namespace Modules\Blogs\Http\Requests\BlogsCategory;

use Illuminate\Foundation\Http\FormRequest;

class BlogsCategoryViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_category_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}