<?php

namespace Modules\Blogs\Http\Requests\BlogsCategory;

use Illuminate\Foundation\Http\FormRequest;

class BlogsCategoryDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_category_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}