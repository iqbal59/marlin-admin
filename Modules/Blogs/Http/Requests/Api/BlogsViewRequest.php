<?php

namespace Modules\Blogs\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class BlogsViewRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'blog_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'blog_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'blog_id' => 'Blog Id',
        ];
    }
}
