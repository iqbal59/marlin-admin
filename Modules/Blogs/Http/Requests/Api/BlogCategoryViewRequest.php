<?php

namespace Modules\Blogs\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class BlogCategoryViewRequest extends FormRequest
{
    public function authorize()
    {
        return true; 
    }
    
    public function rules()
    { 
        return [
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'category_id' => 'Blog Category Id',
        ];
    }
}
