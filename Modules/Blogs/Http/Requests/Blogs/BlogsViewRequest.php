<?php

namespace Modules\Blogs\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class BlogsViewRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_show') ? true : false;
    }
    
    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}

