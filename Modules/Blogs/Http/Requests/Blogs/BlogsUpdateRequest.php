<?php

namespace Modules\Blogs\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class BlogsUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_edit') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            // 'content' => 'required',
            // 'meta_title' => 'required',
            // 'meta_description' => 'required',
            // 'meta_tags' => 'required',
            // 'meta_keywords' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            // 'content.required' => ':attribute is required',
            // 'meta_title.required' => ':attribute is required',
            // 'meta_description.required' => ':attribute is required',
            // 'meta_tags.required' => ':attribute is required',
            // 'meta_keywords.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            // 'content' => 'Content',
            // 'meta_title' => 'Meta Title',
            // 'meta_description' => 'Meta Description',
            // 'meta_tags' => 'Meta Tags',
            // 'meta_keywords' => 'Meta Keywords',
        ];
    }
}