<?php

namespace Modules\Blogs\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class BlogsAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}