<?php

namespace Modules\Blogs\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class BlogsSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_create') ? true : false;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'category_id' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => ':attribute is required',
            'category_id.required' => ':attribute is required',
            'type.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Title',
            'category_id' => 'Cms Category',
            'type' => 'Type',
        ];
    }
}