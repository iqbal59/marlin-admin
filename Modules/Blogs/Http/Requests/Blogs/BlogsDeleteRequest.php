<?php

namespace Modules\Blogs\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class BlogsDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('blogs_delete') ? true : false;
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}