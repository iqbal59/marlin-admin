<?php

namespace Modules\Blogs\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Blogs\Http\Controllers\Includes\Blogs;
use Modules\Blogs\Http\Controllers\Includes\BlogsCategory;

class BlogsController extends Controller
{
    use Blogs;
    use BlogsCategory;
}