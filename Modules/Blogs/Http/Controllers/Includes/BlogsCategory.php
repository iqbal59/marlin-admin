<?php

namespace Modules\Blogs\Http\Controllers\Includes;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryAddRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategorySaveRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryEditRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryUpdateRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryViewRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryDeleteRequest;
use Modules\Blogs\Repositories\Includes\BlogsCategory\BlogsCategoryRepositoryInterface as BlogsCategoryRepository;
use Illuminate\Http\Request;
trait BlogsCategory
{
    public function blogCategoryList(BlogsCategoryRepository $blogsCategoryRepo)
    {   
        $blogsCategory = $blogsCategoryRepo->all();                           
        return view('blogs::blogsCategory.listBlogsCategory', compact('blogsCategory'));           
    }

    public function add(BlogsCategoryAddRequest $request)
    {  
        return view('blogs::blogsCategory.addBlogsCategory');
    }

    public function save(BlogsCategorySaveRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    {       
        $inputData = [
            'title'  => $request->title,
        ];
        $blogsCategory = $blogsCategoryRepo->save($inputData);  
        connectify('success', 'Success!', 'Blogs Category added successfully');                              
        return redirect()
            ->route('admin.blogCategory.blogCategoryList');   
    } 

    public function view(BlogsCategoryViewRequest $request,BlogsCategoryRepository $blogsCategoryRepo)
    { 
        $blogsCategory = $blogsCategoryRepo->get($request->id);  
        return view('blogs::blogsCategory.viewBlogsCategory', compact('blogsCategory'));
    }

    public function edit(BlogsCategoryEditRequest $request,BlogsCategoryRepository $blogsCategoryRepo)
    {   
        $blogCategory = $blogsCategoryRepo->get($request->id);  
        return view('blogs::blogsCategory.editBlogsCategory', compact('blogCategory'));
    }

    public function update(BlogsCategoryUpdateRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    {  
        $inputData = [
            'id' => $request->id,            
            'title' => $request->title,
        ];
       
        $logsCategory = $blogsCategoryRepo->update($inputData);
        connectify('success', 'Success!', 'Blog Category updated successfully');                              
        return redirect()
            ->route('admin.blogCategory.blogCategoryList');
    }

    public function delete(BlogsCategoryDeleteRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    {  
        if ($blogsCategoryRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Blog Category deleted successfully');                              
            return redirect()
            ->route('admin.blogCategory.blogCategoryList');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}