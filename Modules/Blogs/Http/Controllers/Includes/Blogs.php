<?php

namespace Modules\Blogs\Http\Controllers\Includes;
use Modules\Blogs\Http\Requests\Blogs\BlogsAddRequest;
use Modules\Blogs\Http\Requests\Blogs\BlogsSaveRequest;
use Modules\Blogs\Http\Requests\Blogs\BlogsEditRequest;
use Modules\Blogs\Http\Requests\Blogs\BlogsUpdateRequest;
use Modules\Blogs\Http\Requests\Blogs\BlogsViewRequest;
use Modules\Blogs\Http\Requests\Blogs\BlogsDeleteRequest;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface as BlogsRepository;
use Modules\Blogs\Repositories\Includes\BlogsCategory\BlogsCategoryRepositoryInterface as BlogsCategoryRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Diseases\Entities\Diseases;

trait Blogs
{
    
    public function List(BlogsRepository $blogRepo)
    {   
        // $cmsData  = $request->type != '' ? $cmsRepo->getByCategory($request->type) : $cmsRepo->all();
        // $return_url = (!isset($request->type)) ? '': $request->type; 
        // $categoryTitle = (!isset($request->type)) ? '': $cmsCategoryRepo->getCategory($request->type);   
        $blogs = $blogRepo->getForDatatable(); 
        return view('blogs::blogs.list',compact('blogs'));           
    }  

    public function addBlog(BlogsAddRequest $request, BlogsCategoryRepository $blogCategoryRepo)
    {   
        $blogCategory = $blogCategoryRepo->all(); 
        return view('blogs::blogs.add',compact('blogCategory'));      
    }

    public function saveBlogs(BlogsSaveRequest $request, BlogsRepository $blogRepo)
    {  
        $imageUrl=$request->hasFile('image')?$imageUrl = Storage::disk('public')->putFile('blogsimage/',$request->file('image')):$imageUrl="";
        $inputData = [
            'title'  => $request->title,
            'sub_title'  => $request->sub_title,
            'blogs_category_id'  => $request->category_id,
            'type'  => $request->type,
            'content'  => $request->content,
            'mini_content'  => $request->mini_content,
            'image'  => $imageUrl, 
        ];
        
        $blogs = $blogRepo->save($inputData);    
            connectify('success', 'Success!', 'Blogs added successfully');                              
            return redirect()
            ->route('admin.blog.List');
    }   

    public function viewBlogs(BlogsViewRequest $request,BlogsRepository $blogRepo,FaqRepository $faqRepo)
    { 
        $blogs = $blogRepo->get($request->id);  
        $faqs = $faqRepo->getFaq($request->id,$type="Blogs"); 
        return view('blogs::blogs.viewBlogs', compact('blogs','faqs'));
    }

    public function editBlogs(CmsRepository $cmsRepo,DiseasesRepository $diseasesRepo,BlogsEditRequest $request,BlogsRepository $blogRepo,BlogsCategoryRepository $blogCategoryRepo,ProblemsRepository $problemsRepo)
    {   
        $blogs = $blogRepo->get($request->id);  
        $blogCategory = $blogCategoryRepo->all();
        $diseases = $diseasesRepo->getAll()->toArray();  
        $problems = $problemsRepo->all()->toArray(); 
        $selecteValue = json_decode($blogs->diseases_id);        
        $tags = array_merge($diseases,$problems);   


        $patientStories = $cmsRepo->getByCategory(2);  
        $selectepatientStories = json_decode($blogs->success_story_id); 

        return view('blogs::blogs.editBlogs', compact('blogs','patientStories','selectepatientStories','tags','problems','blogs','blogCategory','diseases','selecteValue'));
    }

    public function updateBlogs(BlogsUpdateRequest $request, BlogsRepository $blogRepo)
    {  
        $inputData = [
            'id' => $request->id,  
            'blogs_category_id'  => $request->category_id,      
            'title'  => $request->title,
            'meta_title'  => $request->meta_title,
            'meta_tags'  => $request->meta_tags,
            'meta_keywords'  => $request->meta_keywords,
            'meta_description'  => $request->meta_description,
            'content'  => $request->content,
            'type'  => $request->type,
            'diseases_id' => $request->diseases_id,
            'success_story_id' => $request->patientstories,
            'status' => $request->status  

        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }
        
        if ($request->hasFile('image')) { 
            $this->_removeFile($blogRepo, $request->id);                 
            $imageUrl = Storage::disk('public')->putFile('blogsimage/',$request->file('image'));
            $inputData['image'] = $imageUrl;
        }

        $blogs = $blogRepo->update($inputData);
        connectify('success', 'Success!', 'Blog updated successfully');       
        return redirect()
        ->route('admin.blog.List');
      
    }

    // public function ckeditor_image_upload(Request $request)
    // { 
    //     if($request->hasFile('upload')) {
    //         $originName = $request->file('upload')->getClientOriginalName();
    //         $fileName = pathinfo($originName, PATHINFO_FILENAME);
    //         $extension = $request->file('upload')->getClientOriginalExtension();
    //         $fileName = $fileName.'_'.time().'.'.$extension;
    //         $request->file('upload')->move(public_path('images'), $fileName);
    //         $CKEditorFuncNum = $request->input('CKEditorFuncNum');
    //         $url = asset('images/'.$fileName); 
    //         $msg = 'Image successfully uploaded'; 
    //         $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
    //         @header('Content-type: text/html; charset=utf-8'); 
    //         echo $response;
    //     }
    // }

    public function removeFile(BlogsRepository $blogRepo,Request $request)
    {   
        if ($this->_removeFile($blogRepo, $request->id)) {
            $blogRepo->resetFile($request->id);
        }
        connectify('success', 'Success!', 'Blog updated successfully');       
        return redirect()
            ->route('admin.blog.editBlogs', ['id' => $request->id]);
    }

    private function _removeFile($blogRepo, $id)
    { 
        $image = $blogRepo->get($id);
        if (Storage::disk('public')->delete($image->image)) {
            return true;
        }
        return false;
    }

    public function deleteBlogs(BlogsRepository $blogRepo, BlogsDeleteRequest $request)
    { 
        if ($blogRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Blog deleted successfully');       
            return redirect()
            ->route('admin.blog.List');
        }
            return response()->json(['status' => 0, 'message' => "failed"]);
    }
    
}