<?php

namespace Modules\Blogs\Http\Controllers\Api\Includes;

use Modules\Blogs\Http\Requests\Api\BlogsViewRequest;
use Modules\Blogs\Http\Requests\Api\BlogsFaqRequest;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface as BlogsRepository;
use Modules\Blogs\Http\Requests\Api\BlogCategoryViewRequest;
use Modules\Blogs\Repositories\Includes\BlogsCategory\BlogsCategoryRepositoryInterface as BlogsCategoryRepository;
use Illuminate\Http\Request;
use Exception;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Problems\Repositories\Problems\ProblemsRepositoryInterface as ProblemsRepository;
use Modules\Diseases\Repositories\Diseases\DiseasesRepositoryInterface as DiseasesRepository;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
use Modules\Faq\Repositories\FaqRepositoryInterface as FaqRepository;
use Modules\Country\Repositories\Includes\City\CityRepositoryInterface as CityRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Cms\Repositories\Includes\Cms\CmsRepositoryInterface as CmsRepository;

trait Blogs
{
    public function list(BlogsRepository $blogRepo)
    {
        try {
            $blogs = $blogRepo->all();
            $data = compact("blogs");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function blogView(BlogsViewRequest $request, BlogsRepository $blogRepo)
    {
        try {
            $blogId = $request->blog_id;
            if (is_numeric($blogId)) {
                $blog = $blogRepo->get($blogId);
            } else {
                $blog = $blogRepo->getBySlug($blogId);
            }
            // $diseasesId = json_decode($blog->diseases_id);
            if (!$blog) {
                $response = ['status' => false, 'message' => 'The selected blog id is invalid'];
                return response()->json($response, 200);
            }
            $blogImages = $blogRepo->getImages($blogId);
            $blog->blogImages = $blogImages;
            $data = compact("blog");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function categoryBasedBlogList(BlogCategoryViewRequest $request, BlogsRepository $blogRepo)
    {
        try {
            $categoryId = $request->category_id;
            $categoryBasedBlog = $blogRepo->categoryBasedBlog($categoryId);
            if (!$categoryBasedBlog) {
                $response = ['status' => false, 'message' => 'The selected category id is invalid'];
                return response()->json($response, 200);
            }
            $data = compact("categoryBasedBlog");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function bestHospitalForBlog(Request $request, HospitalRepository $hospitalRepo, BlogsRepository $blogRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo, CityRepository $cityRepo, DepartmentRepository $deptRepo, DoctorRepository $doctorRepo)
    {
        try {
            $blogId = $request->blog_id;
            $blogDetails = $blogRepo->get($blogId);
            if ($blogDetails->diseases_id != null) {
                $diseasesDetails = $diseaseRepo->getDiseases($blogDetails->diseases_id);
                if ($diseasesDetails == null) {
                    $diseasesDetails = $problemRepo->getProblems($blogDetails->diseases_id);
                    $bestHospital = $hospitalRepo->bestHospitalsForProblems($diseasesDetails->id);
                } else {
                    $bestHospital = $hospitalRepo->bestHospitals($diseasesDetails->id);
                }
            } else {
                $response = ['status' => false, 'message' => 'No hospital available for this diseases'];
                return response()->json($response, 200);
            }

            $details = $bestHospital->map(function ($hospital, $key) use ($cityRepo, $deptRepo, $doctorRepo) {
                $cityDetails = $cityRepo->get($hospital->city_id);
                $departmentCount = $deptRepo->getDepartmentsForHospitalCount($hospital->id);
                $deptId = $deptRepo->getDepartmentsHospital($hospital->id);
                $doctor = $doctorRepo->getDepartmentDoctor($deptId);
                $hospital->cityDetails = $cityDetails;
                $hospital->departmentCount = $departmentCount;
                $hospital->doctorCount = count($doctor);
                return $hospital;
            });
            $data = compact("bestHospital");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function bestDoctorForBlog(Request $request, DoctorRepository $doctorRepo, BlogsRepository $blogRepo, DiseasesRepository $diseaseRepo, ProblemsRepository $problemRepo, DepartmentRepository $deptRepo, HospitalRepository $hospitalRepo, CityRepository $cityRepo)
    {
        try {
            $blogId = $request->blog_id;
            $blogDetails = $blogRepo->get($blogId);
            if ($blogDetails->diseases_id != null) {
                $diseasesDetails = $diseaseRepo->getDiseases($blogDetails->diseases_id);
                if ($diseasesDetails == null) {
                    $diseasesDetails = $problemRepo->getProblems($blogDetails->diseases_id);
                    $bestDoctor = $doctorRepo->bestDoctorForProblems($diseasesDetails->id);
                } else {
                    $bestDoctor = $doctorRepo->bestDoctors($diseasesDetails->id);
                }
            } else {
                $response = ['status' => false, 'message' => 'No doctor available for this diseases'];
                return response()->json($response, 200);
            }
            $details = $bestDoctor->map(function ($doctor, $key) use ($deptRepo, $hospitalRepo, $cityRepo) {
                $hospitalIds = json_decode($doctor->hospital_id);
                $hospitalDetails = $hospitalRepo->get($hospitalIds['0']);
                $cityDetails = $cityRepo->get($hospitalDetails->city_id);
                $department=$doctor->department_id != 'null' ? $deptRepo->getDepartmentName(json_decode($doctor->department_id)) : [];
                $doctor->department_id = json_decode($doctor->department_id);
                $doctor->departmentDetails = $department;
                $doctor->workingHospitalDetails = $hospitalDetails;
                $doctor->HospitalCity = $cityDetails;
            });
            $data = compact("bestDoctor");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
    public function blogSearch(Request $request, BlogsRepository $blogRepo)
    {
        try {
            if ($request->tab_id == 4) {
                $searchText = $request->search_text;
                $blogs = $blogRepo->getallBlogs($searchText);
            }
            $data = compact("blogs");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function blogCategoryListing(BlogsCategoryRepository $blogCategoryRepo)
    {
        try {
            $blogCategory = $blogCategoryRepo->all();
            $data = compact("blogCategory");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function allBlogsFaq(FaqRepository $faqRepo)
    {
        try {
            $allBlogsFaq = $faqRepo->getAllFaq($askable_type="Blogs");
            $data = compact("allBlogsFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function BlogFaq(BlogsFaqRequest $request, FaqRepository $faqRepo)
    {
        try {
            $blogId = $request->blog_id;
            $blogFaq = $faqRepo->getFaq($blogId, $askable_type="Blogs");
            $data = compact("blogFaq");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function recentPosts(Request $request, BlogsRepository $blogRepo)
    {
        try {
            $recentBlogs = $blogRepo->getRecentBlogs();
            $data = compact("recentBlogs");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }

    public function blogSuccessStories(Request $request, BlogsRepository $blogRepo, CmsRepository $cmsRepo)
    {
        try {
            $blogId = $request->blog_id;
            $blogDetails = $blogRepo->get($blogId);
            if ($blogDetails != null) {
                if ($blogDetails->success_story_id != null) {
                    $successStory = json_decode($blogDetails->success_story_id);
                    $successStories = $cmsRepo->specialFeatures($successStory);
                } else {
                    $successStories =[];
                }
            } else {
                $response = ['status' => false, 'message' => 'Invalid Id'];
                return response()->json($response, 200);
            }
            $data = compact("successStories");
            $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
            return response()->json($response, 200);
        } catch (Exception $e) {
            $response = ['status' => false, 'message' => $e->getMessage()];
            return response()->json($response, 200);
        }
    }
}