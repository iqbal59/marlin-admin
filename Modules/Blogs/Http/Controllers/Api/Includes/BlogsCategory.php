<?php

namespace Modules\Blogs\Http\Controllers\Api\Includes;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryAddRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategorySaveRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryEditRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryUpdateRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryViewRequest;
use Modules\Blogs\Http\Requests\BlogsCategory\BlogsCategoryDeleteRequest;
use Modules\Blogs\Repositories\Includes\BlogsCategory\BlogsCategoryRepositoryInterface as BlogsCategoryRepository;
use Illuminate\Http\Request;
use Modules\Blogs\Http\Requests\Api\BlogCategoryViewRequest;
use Modules\Blogs\Repositories\Includes\Blogs\BlogsRepositoryInterface as BlogsRepository;


trait BlogsCategory
{
    // public function categoryBasedBlogList(BlogCategoryViewRequest $request, BlogsRepository $blogRepo)
    // {  
    //     try {
    //         $categoryId = $request->category_id;
    //         dd($categoryId);
    //         $blog = $blogRepo->get($blogId);  
    //         if (!$blog) {
    //             $response = ['status' => false, 'message' => 'The selected blog id is invalid'];
    //             return response()->json($response, 200);
    //         }
    //         $data = compact("blog");
    //         $response = ['status' => true, 'data' => $data, 'message' => 'Success'];
    //         return response()->json($response, 200);
    //     } catch (Exception $e) {
    //         $response = ['status' => false, 'message' => $e->getMessage()];
    //         return response()->json($response, 200);
    //     }
    // }

    // public function blogCategoryList(BlogsCategoryRepository $blogsCategoryRepo)
    // {   
    //     $blogsCategory = $blogsCategoryRepo->all();                           
    //     return view('blogs::blogsCategory.listBlogsCategory', compact('blogsCategory'));           
    // }

    // public function add(BlogsCategoryAddRequest $request)
    // {  
    //     return view('blogs::blogsCategory.addBlogsCategory');
    // }

    // public function save(BlogsCategorySaveRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    // {       
    //     $inputData = [
    //         'title'  => $request->title,
    //     ];
    //     $blogsCategory = $blogsCategoryRepo->save($inputData);  
    //     connectify('success', 'Success!', 'Blogs Category added successfully');                              
    //     return redirect()
    //         ->route('admin.blogCategory.blogCategoryList');   
    // } 

    // public function view(BlogsCategoryViewRequest $request,BlogsCategoryRepository $blogsCategoryRepo)
    // { 
    //     $blogsCategory = $blogsCategoryRepo->get($request->id);  
    //     return view('blogs::blogsCategory.viewBlogsCategory', compact('blogsCategory'));
    // }

    // public function edit(BlogsCategoryEditRequest $request,BlogsCategoryRepository $blogsCategoryRepo)
    // {   
    //     $blogCategory = $blogsCategoryRepo->get($request->id);  
    //     return view('blogs::blogsCategory.editBlogsCategory', compact('blogCategory'));
    // }

    // public function update(BlogsCategoryUpdateRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    // {  
    //     $inputData = [
    //         'id' => $request->id,            
    //         'title' => $request->title,
    //     ];

    //     $logsCategory = $blogsCategoryRepo->update($inputData);
    //     connectify('success', 'Success!', 'Blog Category updated successfully');                              
    //     return redirect()
    //         ->route('admin.blogCategory.blogCategoryList');
    // }

    // public function delete(BlogsCategoryDeleteRequest $request, BlogsCategoryRepository $blogsCategoryRepo)
    // {  
    //     if ($blogsCategoryRepo->delete($request->id)) {
    //         connectify('success', 'Success!', 'Blog Category deleted successfully');                              
    //         return redirect()
    //         ->route('admin.blogCategory.blogCategoryList');
    //     }
    //     return response()->json(['status' => 0, 'message' => "failed"]);
    // }
    
}