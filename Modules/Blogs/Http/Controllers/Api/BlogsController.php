<?php

namespace Modules\Blogs\Http\Controllers\Api;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Blogs\Http\Controllers\Api\Includes\Blogs;
use Modules\Blogs\Http\Controllers\Api\Includes\BlogsCategory;

class BlogsController extends Controller
{
    use Blogs;
    use BlogsCategory;
}