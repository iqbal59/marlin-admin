<?php


namespace Modules\Facilities\Repositories;
use Modules\Facilities\Models\Facilities;
use Illuminate\Database\Eloquent\Builder;

class FacilitiesRepository implements FacilitiesRepositoryInterface
{
    public function getForDatatable()
    { 
        return Facilities::orderBy('id', 'DESC')->get()->toArray();
    }

    public function all()
    { 
        return Facilities::orderBy('id', 'DESC')->get()->toArray();
    }

    public function save(array $input)
    { 
        if ($facilities =  Facilities::create($input)) {
            return $facilities;
        }
        return false;
    }

    public function get($id)
    { 
        return Facilities::where('id',$id)->first();
    }

    public function update(array $input)
    { 
        $doctor = Facilities::find($input['id']); 
        unset($input['id']);
        if ($doctor->update($input)) {
            return $doctor;
        }
        return false;
    }

    public function delete(string $id)
    {
        $doctor = Facilities::find($id);
        return $doctor->delete();
    }



}