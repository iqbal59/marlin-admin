<?php

namespace Modules\Facilities\Repositories;

interface FacilitiesRepositoryInterface
{
    public function getForDatatable();
    
    public function all();

    public function save(array $input);

    public function get($id);

    public function update(array $input);

    public function delete(string $id);
    
}