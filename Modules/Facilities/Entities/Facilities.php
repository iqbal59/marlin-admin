<?php

namespace Modules\Facilities\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;

class Facilities extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = ['id','slug','department_id','title','description','status','added_by'];
    
    protected static function newFactory()
    {
        return \Modules\Facilities\Database\factories\FacilitiesFactory::new();
    }
    
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
