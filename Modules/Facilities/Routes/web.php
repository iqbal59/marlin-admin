<?php

use Modules\Facilities\Http\Controllers\FacilitiesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('facilities')->group(function() {
    Route::get('/', 'FacilitiesController@index');
});
Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['auth']], function () {
    Route::group(['prefix' => 'facilities', 'as' => 'facilities.'], function () {         
            Route::get('/', [FacilitiesController::class, 'list'])->name('list');
            Route::get('add', [FacilitiesController::class, 'add'])->name('add');
            Route::post('save', [FacilitiesController::class, 'save'])->name('save');
            Route::get('edit', [FacilitiesController::class, 'edit'])->name('edit');
            Route::post('update', [FacilitiesController::class, 'update'])->name('update');
            Route::get('view', [FacilitiesController::class, 'view'])->name('view');
            Route::get('delete', [FacilitiesController::class, 'delete'])->name('delete');
            Route::post('ckeditor_image_upload', [FacilitiesController::class, 'ckeditor_image_upload'])->name('ckeditor_image_upload');
        
    });
});
