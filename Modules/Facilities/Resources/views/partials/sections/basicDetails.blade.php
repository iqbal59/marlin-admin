<style>
    #pills-tabContent .table th, .table td {
        white-space: normal;
        line-height: 24px;
    }
    </style>
<div class="mb-2">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>{{ trans('cruds.user.fields.id') }}</th>
                <td>{{ $facilities->id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ $facilities->title }}</td>
            </tr>             
            <tr>
                <th>Description</th>
                <td>{!! $facilities->description !!} </td>
            </tr>
            <tr>
                <th> {{ trans('panel.department') }} </th>
                <td>{{ $department->name }}</td>
            </tr> 
            @if($addedBy!= "")
            <tr>
                <th>Added By</th>
                <td>{{ $addedBy->first_name }}</td>
            </tr> 
            @endif           
        </tbody>
    </table>
</div>