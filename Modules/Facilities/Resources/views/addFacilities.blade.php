@extends('layouts.admin')
@section('content')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<div class="card">
    <div class="card-header">
       {{ trans('panel.add_facilities') }} 
    </div>

    <div class="card-body">
        <form action="{{ route("admin.facilities.save") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('department') ? 'has-error' : '' }}">
                <label for="department">{{ trans('panel.add_department') }}<i class="mdi mdi-flag-checkered text-danger "></i> </label>
                    <select id="department" name="department" class="js-states form-control">
                        <option value="">select rating</option>
                        @foreach($departments as $data) 
                        <option value="{{$data->id}}" name="country-dropdown" id="country-dropdown" onclick="myFunction()" >
                        {{$data->name}}
                        </option>
                        @endforeach
                    </select>
                    @if($errors->has('department'))
                    <em class="invalid-feedback">
                        {{ $errors->first('department') }}
                    </em>
                    @endif               
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>    

            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title	">{{ trans('panel.add_title') }} <i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="name" name="title" class="form-control" value="{{old('title')}}" >
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>    
            
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description">{{ trans('panel.add_description') }} </label>
                <textarea class="ckeditor form-control" id="description" name="description" value="{{old('description')}}" required></textarea>       
                @if($errors->has('description'))
                    <em class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>               
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent

<script>
    $(document).ready(function() {
            $("#rating").select2({
                placeholder: "Select Rating",
                allowClear: true
            });
            $("#department").select2({
                placeholder: "Select Department",
                allowClear: true
            });
        });
</script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
<script type="text/javascript">
    CKEDITOR.replace('wysiwyg-editor', {
        filebrowserUploadUrl: "{{route('admin.facilities.ckeditor_image_upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
@endsection

 