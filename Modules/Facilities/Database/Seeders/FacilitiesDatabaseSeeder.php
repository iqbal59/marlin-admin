<?php

namespace Modules\Facilities\Database\Seeders;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FacilitiesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            [
                'id'         => '85',
                'title'      => 'facilities_create',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '86',
                'title'      => 'facilities_edit',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '87',
                'title'      =>  'facilities_show',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '88',
                'title'      => 'facilities_delete',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],
            [
                'id'         => '89',
                'title'      => 'facilities_access',
                'created_at' => '2019-10-19 12:14:15',
                'updated_at' => '2019-10-19 12:14:15',
            ],

        ];

        Permission::insert($permissions);
    }
}
