<?php

namespace Modules\Facilities\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Facilities\Http\Requests\FacilitiesAddRequest;
use Modules\Facilities\Http\Requests\FacilitiesSaveRequest;
use Modules\Facilities\Http\Requests\FacilitiesEditRequest;
use Modules\Facilities\Http\Requests\FacilitiesUpdateRequest;
use Modules\Facilities\Http\Requests\FacilitiesViewRequest;
use Modules\Facilities\Http\Requests\FacilitiesDeleteRequest;
use Modules\Facilities\Repositories\FacilitiesRepositoryInterface as FacilitiesRepository;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use App\Repositories\User\UserRepositoryInterface as UserRepository;

class FacilitiesController extends Controller
{
    public function list(FacilitiesRepository $facilitiesRepo)
    {  
        $facilities = $facilitiesRepo->getForDatatable();   
        return view('facilities::listFacilities', compact('facilities'));                         
    }

    public function add(FacilitiesAddRequest $request , FacilitiesRepository $facilitiesRepo,DepartmentRepository $departmentRepo)
    { 
        $departments = $departmentRepo->all();
        return view('facilities::addFacilities', compact('departments'));    
    }

    public function save(FacilitiesSaveRequest $request, FacilitiesRepository $facilitiesRepo)
    {    
         $inputData = [
            'department_id'  => $request->department,
            'title'  => $request->title,
            'description'  => $request->description,
            'added_by'     => auth()->user()->id,    
        ];
        $facilities = $facilitiesRepo->save($inputData);     
        connectify('success', 'Success!', 'Facilities added successfully');      
        return redirect()
            ->route('admin.facilities.list');
    } 

    public function view(FacilitiesViewRequest $request,FacilitiesRepository $facilitiesRepo,DepartmentRepository $departmentRepo,UserRepository $userRepo)
    {
        $facilities = $facilitiesRepo->get($request->id); 
        $department = $departmentRepo->get($facilities->department_id);
        $addedBy = $userRepo->get($facilities->added_by);   
        return view('facilities::viewFacilities', compact('addedBy','facilities','department'));
    }

    public function edit(FacilitiesEditRequest $request, FacilitiesRepository $facilitiesRepo, DepartmentRepository $deptRepo)
    {  
        $facilities =$facilitiesRepo->get($request->id);   
        $department = $deptRepo->all();
        return view('facilities::editFacilities', compact('facilities','department'));
    }

    public function update(FacilitiesUpdateRequest $request, FacilitiesRepository $facilitiesRepo)
    {   
        $inputData = [
            'id'  => $request->id,
            'department_id'  => $request->department,
            'title'  => $request->title,
            'description'  => $request->description,
        ];

        if(isset($request->status)){ 
            $inputData['status']=($request->status == "on")?1:0;
        }else{ 
            $inputData['status']='0';
        }


        $facilities = $facilitiesRepo->update($inputData);
        connectify('success', 'Success!', 'Facilities updated successfully');      
        return redirect()
            ->route('admin.facilities.list');
    } 
    
    public function delete(FacilitiesRepository $facilitiesRepo, FacilitiesDeleteRequest $request)
    { 
        if ($facilitiesRepo->delete($request->id)) {
            connectify('success', 'Success!', 'Facilities deleted successfully');      
            return redirect()
            ->route('admin.facilities.list');
        }
        return response()->json(['status' => 0, 'message' => "failed"]);
    }

    public function ckeditor_image_upload(Request $request)
    { 
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image successfully uploaded'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }
}
