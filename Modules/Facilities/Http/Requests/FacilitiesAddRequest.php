<?php

namespace Modules\Facilities\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FacilitiesAddRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('facilities_create') ? true : false;
    }
    public function rules()
    {
        return [
            //
        ];
    }
}
