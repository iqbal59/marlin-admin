<?php

namespace Modules\Facilities\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FacilitiesSaveRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->user()->can('doctor_create') ? true : false;
    }

    public function rules()
    {
        return [
            'department' => 'required',
            'title' => 'required',
            // 'description' => 'required',
             
        ];
    }

    public function messages()
    {
        return [
            'department.required' => ':attribute is required',
            'title.required' => ':attribute is required',
            // 'description.required' => ':attribute is required',
        ];
    }

    public function attributes()
    {
        return [
            'department' => 'Department',
            'title' => 'Title',
            // 'description' => 'Description',
        ];
    }
}
