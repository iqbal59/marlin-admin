<?php

namespace Modules\Facilities\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Facilities extends Model
{
    use Sluggable;
    use HasFactory;
    protected $table = 'facilities';

    protected $fillable = [
        'id','slug','department_id','title','description','status','added_by'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function department()
    {
        return $this->belongsTo('Modules\Department\Models\Department',  'department_id', 'id');
    }

   
}