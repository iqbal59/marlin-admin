/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const mix = require('laravel-mix');
let ImageminPlugin = require('imagemin-webpack-plugin').default;
mix.webpackConfig({
    plugins: [
        new ImageminPlugin({
            // disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '95-100',
            },
            test: /\.(jpe?g|png|gif|svg)$/i,
        }),
    ],
})




mix.sass('resources/sass/app.scss', 'public/css');

// Commen js
mix.js([
    'resources/js/app.js',
    'resources/js/vendor/off-canvas.js',
    'resources/js/vendor/hoverable-collapse.js',
    'resources/js/vendor/chart.js',
    'resources/js/vendor/misc.js',
    'resources/js/vendor/settings.js',
    'resources/js/vendor/todolist.js',
    'resources/js/vendor/jquery.dataTables.js',
], 'public/js/commen.js');


mix.copy('resources/assets/images', 'public/assets/images', false);

// Copy plugin files to public folder
mix.copyDirectory("node_modules/@mdi", "public/assets/plugins/@mdi")
    // .copyDirectory('node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js', 'public/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')
    .copyDirectory(
        [
            "node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js",
            "node_modules/perfect-scrollbar/css/perfect-scrollbar.css"
        ],
        "public/assets/plugins/perfect-scrollbar"
    )
    .copyDirectory(
        "node_modules/bootstrap-datepicker/dist",
        "public/assets/plugins/bootstrap-datepicker"
    )
    .copyDirectory(
        "node_modules/tempusdominus-bootstrap-4/build",
        "public/assets/plugins/tempusdominus-bootstrap-4"
    )
    .copyDirectory(
        "node_modules/moment/moment.js",
        "public/assets/plugins/moment/moment.js"
    )
    .copyDirectory(
        "node_modules/chart.js/dist/Chart.min.js",
        "public/assets/plugins/chartjs/chart.min.js"
    )
    .copyDirectory(
        "node_modules/jquery-sparkline/jquery.sparkline.min.js",
        "public/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"
    )
    .copyDirectory(
        "node_modules/jquery.easing/jquery.easing.min.js",
        "public/assets/plugins/jquery.easing/jquery.easing.min.js"
    );