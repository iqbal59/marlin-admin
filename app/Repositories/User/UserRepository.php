<?php


namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class UserRepository implements UserRepositoryInterface
{
    public function get($id)
    { 
        return User::where('id',$id)->first();
    }

}