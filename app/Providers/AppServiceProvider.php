<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Settings\Models\Settings;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Cache\Factory;
use Modules\Settings\Repositories\SettingsRepository;
use Illuminate\Support\Facades\Artisan;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Factory  $cache, SettingsRepository $settingsRepo)
    { 
        /**
         * Load Centres Of Excellence to cache and remains for 5 min
         */
        Artisan::call('cache:clear');
        $settings = $cache->remember('cacheExcellence', 300, function() use ($settingsRepo)
        { 
            try{
                $settingsArray = $settingsRepo->getAll()->keyBy('key');
             }catch(\Exception $e){
                $settingsArray = [];
            } 
            return $settingsArray;
        });
        
        View::share('settings', $settings);
        

    }
}
