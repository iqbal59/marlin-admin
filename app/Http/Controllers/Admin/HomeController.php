<?php

namespace App\Http\Controllers\Admin;
use Modules\Hospital\Repositories\HospitalRepositoryInterface as HospitalRepository;
use Modules\Hospital\Models\Hospital;
use Modules\Department\Repositories\DepartmentRepositoryInterface as DepartmentRepository;
use Modules\Treatment\Repositories\TreatmentRepositoryInterface as TreatmentRepository;
use Modules\Doctor\Repositories\DoctorRepositoryInterface as DoctorRepository;
class HomeController
{
    public function index(HospitalRepository $hospitalRepo,DepartmentRepository $deptRepo,TreatmentRepository $treatmentRepo,DoctorRepository $doctRepo)
    {
        $hospitalCout = $hospitalRepo->getHospitalCount();
        $departmentCount = $deptRepo->getDepartmentCount();
        $treatmentCount = $treatmentRepo->getTreatmentCount();
        $doctorCount = $doctRepo->getDoctorCount(); 
        return view('home',compact('doctorCount','hospitalCout','departmentCount','treatmentCount','doctorCount'));
        
    }
}
