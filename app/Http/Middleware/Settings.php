<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Settings\Repositories\SettingsRepository;


class Settings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $settings = cache()->remember('settings', 300, function()
        {
            $settingsRepo = new SettingsRepository();
            try{ 
                $settingsArray = $settingsRepo->autoLoad()->pluck('value','key');
            }catch(\Exception $e){ 
                $settingsArray = [];
            } 
            return $settingsArray;
        }); 
        config()->set('settings', $settings);
        return $next($request);
    }
}
