<footer class="footer">
  <div class="container-fluid clearfix d-none">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><a href="https://www.ipixtechnologies.com/" target="_blank">{{ trans('panel.design_by_ipix_technologies') }} </a>

    </span>
  </div>
</footer>