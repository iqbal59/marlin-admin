<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
    <ul class="nav scroll-cn">
        <li class="nav-item nav-profile not-navigation-link">
            <div class="nav-link">
                {{-- <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{ url('assets/images/faces/face8.jpg') }}" alt="profile image">
            </div>
            <div class="text-wrapper">
                <p class="profile-name">Richard V.Welsh</p>
                <div class="dropdown" data-display="static">
                    <a href="#" class="nav-link d-flex user-switch-dropdown-toggler" id="UsersettingsDropdown" href="#"
                        data-toggle="dropdown" aria-expanded="false">
                        <small class="designation text-muted">{{ trans('panel.manager') }}</small>
                        <span class="status-indicator online"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="UsersettingsDropdown">
                        <a class="dropdown-item p-0">
                            <div class="d-flex border-bottom">
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                    <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                                </div>
                                <div
                                    class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                                    <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                                </div>
                                <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                                    <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item mt-2">{{ trans('panel.manage_account') }} </a>
                        <a class="dropdown-item"> {{ trans('panel.change_password') }}</a>
                        <a class="dropdown-item"> {{ trans('panel.check_inbox') }}</a>
                        <a class="dropdown-item"> {{ trans('panel.sign_out') }}</a>
                    </div>
                </div>
            </div>
            </div> --}}
            {{-- <button class="btn btn-success btn-block">{{ trans('panel.dashboard') }}<i
                class="ml-2 mdi mdi-television"></i>
            </button> --}}
            <a class="btn btn-success btn-block" href="">{{ trans('panel.dashboard') }}<i
                    class="ml-2 mdi mdi-television"></i></a>
            </div>
        </li>

        <!-- Cms Management -->
        @can('cms_access')
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-cms" aria-expanded=""
                aria-controls="basic-ui-telimedcine">
                <i class="menu-icon mdi mdi-arrange-send-backward"></i>
                <span class="menu-title">{{ trans('panel.cms_management') }}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui-cms">
                <ul class="nav flex-column sub-menu">
                    @can('cms_category_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.cmsCategory.categoryList')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('Page Categories') }}
                        </a>
                    </li>
                    @endcan
                    @can('cms_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.cms.cmsList')}}">
                            <i class="mdi mdi-drag"></i>
                            Cms
                        </a>
                    </li>
                    @endcan
                    <li class="nav-item">

                        <a class="nav-link " href="{{route('admin.cms.cmsTypeBasedList',['type' => 2])}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('Cms Pages') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.cms.cmsTypeBasedList',['type' => 1])}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('Cms Block') }}
                        </a>
                    </li>

                    <!-- @can('blogs_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.blog.List')}}">
              <i class="mdi mdi-drag"></i>
             Blog 
            </a>
          </li>
          @endcan -->
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.banner.bannerList') }}">
                            <i class="mdi mdi-drag"></i>
                            Banner
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        @endcan




        <!-- Master -->
        <!-- <li class="nav-item ">
      <a class="nav-link" data-toggle="collapse" href="#basic-ui-master" aria-expanded="" aria-controls="basic-ui-master">
        <i class="menu-icon mdi mdi-arrange-send-backward"></i>

        <span class="menu-title">{{ trans('panel.master') }}</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="basic-ui-master">
        <ul class="nav flex-column sub-menu">
          @can('cms_category_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.cmsCategory.categoryList')}}">
              <i class="mdi mdi-drag"></i> 
              {{ trans('panel.master') }}
            </a>
          </li>
          @endcan
        </ul>
      </div>
    </li> -->

        @can('user_management_access')
        <!-- Authentication -->
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui" aria-expanded="" aria-controls="basic-ui">
                <i class="menu-icon mdi mdi-account-multiple-outline"></i>
                <span class="menu-title">{{ trans('panel.authentication') }}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui">
                <ul class="nav flex-column sub-menu">
                    @can('user_access')
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}"
                            href="{{ route('admin.users.index') }}">
                            <i class="mdi mdi-drag"></i>


                            {{ trans('panel.user_management') }}
                        </a>
                    </li>
                    @endcan
                    @can('role_access')
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}"
                            href="{{ route('admin.roles.index') }}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.role_management') }}
                        </a>
                    </li>
                    @endcan
                    @can('permission_access')
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}"
                            href="{{ route('admin.permissions.index') }}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.permission_management') }}
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>
        @endcan

        <!-- Country -->
        @can('country_access')
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-country" aria-expanded=""
                aria-controls="basic-ui-country">
                <i class="menu-icon mdi mdi-map-marker-multiple"></i>
                <span class="menu-title">{{ trans('panel.country') }}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui-country">
                <ul class="nav flex-column sub-menu">
                    @can('country_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.country_list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.country_management') }}
                        </a>
                    </li>
                    @endcan
                    @can('state_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.state_list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.state_management') }}
                        </a>
                    </li>
                    @endcan
                    @can('city_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.city_list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.city_management') }}
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>
        @endcan
        @can('hospital_access')
        <!-- Hospital -->
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-hospital" aria-expanded=""
                aria-controls="basic-ui-hospital">
                <i class="menu-icon mdi mdi-hospital-building"></i>
                <span class="menu-title">{{ trans('panel.hospital') }}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui-hospital">
                <ul class="nav flex-column sub-menu">
                    @can('hospital_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.hospital.list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.hospitals') }}
                        </a>
                    </li>
                    @endcan

                    @can('department_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.department.list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.departments') }}
                        </a>
                    </li>
                    @endcan

                    @can('treatmentplants_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.treatmentplants.list')}}">
                            <i class="mdi mdi-drag"></i>
                            Treatment Plan{{-- --}}s
                        </a>
                    </li>
                    @endcan

                    @can('treatment_category_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.treatmentcategory.listCategory')}}">
                            <i class="mdi mdi-drag"></i>
                            Treatment Category
                        </a>
                    </li>
                    @endcan

                    @can('treatment_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.treatment.listTreatment')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.treatments') }}
                        </a>
                    </li>
                    @endcan
                    @can('treatmentfaq_access') <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.treatmentfaq.treatment-faq')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.treatmentfaq') }}
                        </a>
                    </li>
                    @endcan
                    @can('treatmentdetails_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.treatmentdetails.treatment-details')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.treatmentdetails') }}
                        </a>
                    </li>
                    @endcan

                    @can('doctor_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.doctor.list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.doctors') }}
                        </a>
                    </li>
                    @endcan
                    @can('facilities_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.facilities.list')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.facilities') }}
                        </a>
                    </li>
                    @endcan

                    @can('medicinebarnds_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.medicinebrands.list')}}">
                            <i class="mdi mdi-drag"></i>
                            Medicine Brands
                        </a>
                    </li>
                    @endcan
                    @can('medicine_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.medicine.list')}}">
                            <i class="mdi mdi-drag"></i>
                            Medicine
                        </a>
                    </li>
                    @endcan
                    @can('services_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.services.list')}}">
                            <i class="mdi mdi-drag"></i>
                            Services
                        </a>
                    </li>
                    @endcan
                    @can('diseases_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.diseases.listdiseases')}}">
                            <i class="mdi mdi-drag"></i>
                            Diseases
                        </a>
                    </li>
                    @endcan
                    @can('problems_access')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.problems.list')}}">
                            <i class="mdi mdi-drag"></i>
                            Problems
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>
        @endcan

        <!-- Tele Medicine -->
        @can('doctor_access')
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-telimedcine" aria-expanded=""
                aria-controls="basic-ui-telimedcine">
                <i class="menu-icon mdi mdi-plus-network"></i>
                <span class="menu-title">{{ trans('panel.telemedicine') }}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui-telimedcine">
                <ul class="nav flex-column sub-menu">
                    @can('doctor_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.teledoctor.teledoctorList')}}">
                            <i class="mdi mdi-drag"></i>
                            {{ trans('panel.telemedicines') }}
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>
        @endcan

        <!-- Videos -->
        @can('videos_sections_access')
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-video" aria-expanded=""
                aria-controls="basic-ui-settings">
                <i class="menu-icon mdi mdi-video"></i>
                <span class="menu-title">Videos</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="basic-ui-video">
                <ul class="nav flex-column sub-menu">
                    @can('videos_sections_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.videossections.videoSectionList') }}">
                            <i class="mdi mdi-drag"></i>
                            Video Section
                        </a>
                    </li>
                    @endcan
                    @can('videos_access')
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.videos.videoList') }}">
                            <i class="mdi mdi-drag"></i>
                            Videos
                        </a>
                    </li>
                    @endcan

                </ul>
            </div>
        </li>
        @endcan
        <!-- Gallery -->
        {{-- @can('gallery_access')
        <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#basic-ui-image" aria-expanded=""
                aria-controls="basic-ui-settings">
                <i class="menu-icon mdi mdi-file-image"></i>
                <span class="menu-title">Gallery</span>
                <i class="menu-arrow"></i>

            </a>
            <div class="collapse" id="basic-ui-image">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.gallery.list') }}">
        <i class="mdi mdi-drag"></i>
        Gallery
        </a>
        </li>
    </ul>
    </div>
    </li>
    @endcan --}}

    <!-- enquiry -->
    @can('enquiry_access')
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-enquiry" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-comment-question-outline"></i>
            <span class="menu-title">{{ trans('panel.enquiry') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-enquiry">
            <ul class="nav flex-column sub-menu">
                @can('enquiry_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.enquiry.allEnquiryList')}}">
                        <i class="mdi mdi-drag"></i>
                        All Enquiries
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.enquiry.HospitalEnquirylist')}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.hospital_enquiry') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.enquiry.DoctorEnquirylist')}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.doctor_enquiry') }}
                    </a>
                </li>
                {{-- <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.enquiry.TeleDoctorEnquirylist')}}">
                <i class="mdi mdi-drag"></i>
                Telemedicine Doctor Enquiry
                </a>
    </li> --}}
    {{-- <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.enquiry.DepartmentEnquirylist')}}">
    <i class="mdi mdi-drag"></i>
    {{ trans('panel.department_enquiry') }}
    </a>
    </li> --}}
    <!-- <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
           {{ trans('panel.facility_enquiry') }}
            </a>
          </li> -->
    <li class="nav-item">
        <a class="nav-link " href="{{route('admin.enquiry.TreatmentEnquirylist')}}">
            <i class="mdi mdi-drag"></i>
            Treatment Enquiry
        </a>
    </li>
    {{-- <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.enquiry.MedicineEnquirylist')}}">
    <i class="mdi mdi-drag"></i>
    Medicine Enquiry
    </a>
    </li> --}}
    <li class="nav-item">
        <a class="nav-link " href="{{route('admin.enquiry.hospitalAddEnquirylist')}}">
            <i class="mdi mdi-drag"></i>
            Hospital Add Enquiry
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="{{route('admin.enquiry.DiseaseEnquirylist')}}">
            <i class="mdi mdi-drag"></i>
            Disease Enquiry
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="{{route('admin.enquiry.BookingEnquirylist')}}">
            <i class="mdi mdi-drag"></i>
            Booking Enquiry
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="{{route('admin.enquiry.CommonEnquirylist')}}">
            <i class="mdi mdi-drag"></i>
            Common Enquiries
            <!-- MMA-Biz Enquiry -->
        </a>
    </li>
    @endcan
    </ul>
    </div>
    </li>
    @endcan

    <!-- faq -->
    <!-- Settings -->
    @can('faq_access')
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-faq" aria-expanded="" aria-controls="basic-ui-faq">
            <i class="menu-icon mdi mdi-file-find"></i>
            <span class="menu-title">Faq</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-faq">
            <ul class="nav flex-column sub-menu">

                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.faq.list')}}">
                        <i class="mdi mdi-drag"></i>
                        Faq
                    </a>
                </li>
            </ul>
        </div>
    </li>
    @endcan
    @can('cms_access')
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-patient" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-book-open-page-variant "></i>
            <span class="menu-title">{{ trans('panel.patient_stories') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-patient">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
            {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 2])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-newsevents" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-library-books"></i>
            <span class="menu-title">{{ trans('panel.newsevents') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-newsevents">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
            {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 3])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-blogs" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-blogger"></i>
            <span class="menu-title">{{ trans('panel.blogs') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-blogs">
            <ul class="nav flex-column sub-menu">

                <!-- @can('cms_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 4])}}">
              <i class="mdi mdi-drag"></i>
          {{ trans('panel.list') }}
            </a>
          </li>
          @endcan -->
                @can('blogs_category_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.blogCategory.blogCategoryList')}}">
                        <i class="mdi mdi-drag"></i>
                        Blog Categories
                    </a>
                </li>
                @endcan
                @can('blogs_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.blog.List')}}">
                        <i class="mdi mdi-drag"></i>
                        Blog
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-partners" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-human-male-female"></i>
            <span class="menu-title">{{ trans('panel.partners') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-partners">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
             {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 4])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-awardsrecognations" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-trophy-outline"></i>
            <span class="menu-title">{{ trans('panel.awardsrecognitions') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-awardsrecognations">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
             {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 5])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-executiveteam" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-account-multiple-outline"></i>
            <span class="menu-title">{{ trans('panel.executive_team') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-executiveteam">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
           {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 6])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>

    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-partnerstestimonial" aria-expanded=""
            aria-controls="basic-ui-enquiry">
            <i class="menu-icon mdi mdi-grease-pencil "></i>
            <span class="menu-title">{{ trans('panel.partners_testimonial') }}</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="basic-ui-partnerstestimonial">
            <ul class="nav flex-column sub-menu">
                <!-- @can('enquiry_access')
          <li class="nav-item">
            <a class="nav-link " href="{{route('admin.enquiry.list')}}">
              <i class="mdi mdi-drag"></i>
           {{ trans('panel.add') }}
            </a>
          </li>
          @endcan -->
                @can('cms_access')
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.cms.cmsList',['type' => 7])}}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.list') }}
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </li>
    @endcan
    {{-- branches --}}
    @can('branch_access')
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-settings" aria-expanded=""
            aria-controls="basic-ui-settings">
            <i class="menu-icon mdi mdi-source-branch"></i>
            <span class="menu-title">Branch</span>
            <i class="menu-arrow"></i>

        </a>
        <div class="collapse" id="basic-ui-settings">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link " href="{{route('admin.branch.list')}}">
                        <i class="mdi mdi-drag"></i>
                        Branch
                    </a>
                </li>
            </ul>
        </div>
    </li>
    @endcan
    <!-- Settings -->
    @can('settings_access')
    <li class="nav-item ">
        <a class="nav-link" data-toggle="collapse" href="#basic-ui-settings" aria-expanded=""
            aria-controls="basic-ui-settings">
            <i class="menu-icon mdi mdi-settings"></i>
            <span class="menu-title">{{ trans('panel.settings') }}</span>
            <i class="menu-arrow"></i>

        </a>
        <div class="collapse" id="basic-ui-settings">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('admin.settings.view') }}">
                        <i class="mdi mdi-drag"></i>
                        {{ trans('panel.settings') }}
                    </a>
                </li>
            </ul>
        </div>
    </li>
    @endcan


    <!-- Settings -->
    <!-- @can('settings_access')
    <li class="nav-item ">
      <a class="nav-link" data-toggle="collapse" href="#basic-ui-settings" aria-expanded="" aria-controls="basic-ui-settings">
        <i class="menu-icon mdi mdi-account-settings"></i>
        <span class="menu-title">{{ trans('panel.settings') }}</span>
        <i class="menu-arrow"></i>
      
      </a>
      <div class="collapse" id="basic-ui-settings">
        <ul class="nav flex-column sub-menu">
        
          <li class="nav-item">
            <a class="nav-link " href="{{ route("admin.settings.view") }}">
              <i class="mdi mdi-drag"></i>
              Genaral Settings
            </a>
          </li>
        </ul>
      </div>
    </li>
    @endcan -->
    </ul>

</nav>