@extends('layouts.admin')
@section('content')
<style>
    table.mt-1 tr {
    float: left;
    width: 33.333%;
    position: relative;
    min-height: 35px;
}
td.check-box {
    position: absolute;
    right: 30px;
    top: 0;
}
input#BulkSelect {
    margin-top: 25px;
    margin-bottom: 15px;
    margin-left: 209px;
}
</style>
 <div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.role.title_singular') }}
    </div>
    <div class="card-body">
        <form action="{{ route("admin.roles.update", [$role->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('cruds.role.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($role) ? $role->title : '') }}" required>
                @if($errors->has('title'))
                <em class="invalid-feedback">
                {{ $errors->first('title') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.role.fields.title_helper') }}
                </p>  <br>
                <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                <label for="permissions">{{ trans('cruds.role.fields.permissions') }}<i class="mdi mdi-flag-checkered text-danger "></i>
                <br>
                <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                    Select All<input type="checkbox" name="BulkSelect" id="BulkSelect"/><br>
                    @if($role->id >=3)
                    <table class="mt-1">
                        <tbody>  
                            @foreach($permissions as $id => $permissions)    
                            <tr>
                                <td>{{$permissions}}  </td>
                                <td class="check-box"> 
                                <label>
                                    <input class="Bulkers" type="checkbox" name="permissions[]" value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'checked' : '' }} />
                                    <span></span>
                                </label>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <table class="mt-1">
                        <tbody>  
                            @foreach($permissions as $id => $permissions)    
                            <tr>
                                <td>{{$permissions}}  </td>
                                <td class="check-box"> 
                                <label>
                                @if($id <= 16)
                                    <input type="checkbox" name="permissions[]" value="{{ $id }}" checked onclick="return false"   />
                                    <span></span>
                                @else
                                    <input class="Bulkers" type="checkbox" name="permissions[]" value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'checked' : '' }} />
                                    <span></span>
                                 @endif
                                </label>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $("#BulkSelect").click(function() {
        $('.Bulkers').prop('checked', this.checked);
    });
    $('.Bulkers').change(function() {
        $("#BulkSelect").prop("checked", $(".Bulkers").length === $(".Bulkers:checked").length);
    });
});
</script>