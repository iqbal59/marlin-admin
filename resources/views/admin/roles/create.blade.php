@extends('layouts.admin')
@section('content')
{{-- <style>
    table.mt-1 tr {
    float: left;
    width: 33.333%;
    position: relative;
    min-height: 35px;
}
td.check-box {
    position: absolute;
    right: 30px;
    top: 0;
}
input#BulkSelect {
    margin-top: 25px;
    margin-bottom: 15px;
    margin-left: 209px;
}
</style> --}}
<style>
    table, th, td {
      border: 1px solid rgba(223, 217, 217, 0.918);
    }
    #example_filter input[type="search"]
    {
        border: 1px solid #ced4da;
        height: 1.75rem;
        padding: 0.5rem 0.81rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
    }
    .table th, .table td {
        vertical-align: middle;
        font-size: 0.875rem;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
    }
    element.style {
    width: 624px;
}
    </style>
<div class="card">
    <div class="card-header">
        Add {{ trans('cruds.role.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.roles.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('cruds.role.fields.title') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($role) ? $role->title : '') }}" required>
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.role.fields.title_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                <label for="permissions" class="w-100">{{ trans('cruds.role.fields.permissions') }}<i class="mdi mdi-flag-checkered text-danger "></i>
                <br>
                <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                    <br>
                    <table id="example" class="display nowrap table" style="width:500%">
                        <thead>
                            <tr>
                                <th>Permission</th>
                                <th>Select All&nbsp;<input type="checkbox" name="BulkSelect" id="BulkSelect"/></th>
                            </tr>
                        </thead>
                        <tbody>  
                            @foreach($permissions as $id => $permissions)    
                            <tr>
                                <td>{{$permissions}}  </td>
                                <td class="check-box"> 
                                <label>
                                    <input class="Bulkers" type="checkbox" name="permissions[]" value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'checked' : '' }} />
                                    <span></span>
                                </label>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <div>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $("#BulkSelect").click(function() {
        $('.Bulkers').prop('checked', this.checked);
    });
    $('.Bulkers').change(function() {
        $("#BulkSelect").prop("checked", $(".Bulkers").length === $(".Bulkers:checked").length);
    });
});
</script>
@section('scripts')
@parent
<script>
   $(document).ready(function() {
    $('#example').DataTable( {
    });
} );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endsection