@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        
        {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <div class="col-md-6">     
            <form action="{{ route("admin.users.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <label for="first_name">{{ trans('cruds.user.fields.name') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="text" id="first_name" name="first_name" class="form-control" value="{{ old('first_name', isset($user) ? $user->first_name : '') }}" >
                @if($errors->has('first_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('first_name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <label for="last_name">Last Name </label>
                <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name', isset($user) ? $user->last_name : '') }}" >
                @if($errors->has('last_name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.user.fields.email') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" >
                @if($errors->has('email'))
                    <em class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.email_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('avatar_location') ? 'has-error' : '' }}">
                <label for="avatar_location">Avatar Location </label>
                <input type="text" id="avatar_location" name="avatar_location" class="form-control" value="{{ old('avatar_location', isset($user) ? $user->avatar_location : '') }}" >
                @if($errors->has('avatar_location'))
                    <em class="invalid-feedback">
                        {{ $errors->first('avatar_location') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('timezone') ? 'has-error' : '' }}">
                <label for="timezone">TimeZone </label>
                <input type="text" id="timezone" name="timezone" class="form-control" value="{{ old('timezone', isset($user) ? $user->timezone : '') }}" >
                @if($errors->has('timezone'))
                    <em class="invalid-feedback">
                        {{ $errors->first('timezone') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.name_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password">{{ trans('cruds.user.fields.password') }}<i class="mdi mdi-flag-checkered text-danger "></i></label>
                <input type="password" id="password" name="password" class="form-control">
                @if($errors->has('password'))
                    <em class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.password_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                <label for="roles">{{ trans('cruds.user.fields.roles') }}<i class="mdi mdi-flag-checkered text-danger "></i>
                    <br> 
                    @foreach($roles as $id => $roles)
                    {{ $roles }} <input class="subject-list" type="checkbox"  name="roles[]" id="roles" value="{{ $id }}" {{ (in_array($id, old('roles', [])) || isset($user) && $user->roles->contains($id)) ? 'selected' : '' }}>
                    @endforeach
                @if($errors->has('roles'))
                    <em class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.user.fields.roles_helper') }}
                </p>
            </div>
            Accept Terms And Conditions  <br> Yes <input type="checkbox" id="1" name="is_term_accept" value="1">
            No <input type="checkbox" id="0" name="is_term_accept" value="0">
            
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $('.subject-list').on('change', function() {
        $('.subject-list').not(this).prop('checked', false);  
    });
</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
@endsection