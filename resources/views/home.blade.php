@extends('layouts.admin')
@section('content')
  <div class="row">
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-cube text-danger icon-lg"></i>
              
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.total_revenue') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% lower growth </p>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-account-box-multiple text-info icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.users') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div> -->
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-hospital-building text-warning icon-lg"></i>
            </div>
            <a href="{{url('admin/hospital')}}" target="_blank">
            <div class="float-right">
              <p class="mb-0 text-right">{{$hospitalCout}} {{ trans('panel.hospitals') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div></a>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div>
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-hotel text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.hotels') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Weekly Sales </p>
        </div>
      </div>
    </div> -->
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
              <div class="float-left">
              <i class="mdi mdi-animation text-info icon-lg"></i>
            </div>
            <a href="{{url('admin/department')}}" target="_blank">
            <div class="float-right">
              <p class="mb-0 text-right">{{$departmentCount}} {{ trans('panel.departments') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
            </a>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
            <a href="{{url('admin/treatment')}}" target="_blank">
              <i class="mdi mdi-note-plus-outline text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{$treatmentCount}} {{ trans('panel.treatments') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3> 
              </div>
            </div>
            </a>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% lower growth </p>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-stethoscope text-success icon-lg"></i>
            </div>
            <a href="{{url('admin/doctor')}}" target="_blank">
            <div class="float-right">
              <p class="mb-0 text-right">{{$doctorCount}} {{ trans('panel.doctors') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
            </a>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div>
    
  </div>
  <div class="row">
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-animation text-info icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.departments') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">246</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
            <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-note-plus-outline text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.treatments') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">$65,650</h3> 
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% lower growth </p>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-stethoscope text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.doctors') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">3455</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales </p>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="d-flex flex-md-column flex-xl-row flex-wrap justify-content-between align-items-md-center justify-content-xl-between">
            <div class="float-left">
              <i class="mdi mdi-note-plus-outline text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">{{ trans('panel.appoinments') }}</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0 text-left text-md-center text-xl-left d-none">
            <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Weekly Sales </p>
        </div>
      </div>
    </div> -->
    
  </div>
  <div class="row ">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-5 d-flex align-items-center">
              <canvas id="UsersDoughnutChart" class="400x160 mb-4 mb-md-0" height="200"></canvas>
            </div>
            <div class="col-md-7">
              <h4 class="card-title font-weight-medium mb-0 d-none d-md-block">Active Users</h4>
              <div class="wrapper mt-4">
                <div class="d-flex justify-content-between mb-2">
                  <div class="d-flex align-items-center">
                    <p class="mb-0 font-weight-medium">67,550</p>
                    <small class="text-muted ml-2">{{ trans('panel.email_account') }}</small>
                  </div>
                  <p class="mb-0 font-weight-medium">80%</p>
                </div>
                <div class="progress">
                  <div class="progress-bar bg-primary" role="progressbar" style="width: 88%" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
              <div class="wrapper mt-4">
                <div class="d-flex justify-content-between mb-2">
                  <div class="d-flex align-items-center">
                    <p class="mb-0 font-weight-medium">21,435</p>
                    <small class="text-muted ml-2">Treatments</small>
                  </div>
                  <p class="mb-0 font-weight-medium">34%</p>
                </div>
                <div class="progress">
                  <div class="progress-bar bg-success" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="col-sm-6 col-md-6 col-lg-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-7">
              <h4 class="card-title font-weight-medium mb-3">Amount Due</h4>
              <h1 class="font-weight-medium mb-0">$5998</h1>
              <p class="text-muted">Milestone Completed</p>
              <p class="mb-0">Payment for next week</p>
            </div>
            <div class="col-md-5 d-flex align-items-end mt-4 mt-md-0">
              <canvas id="conversionBarChart" height="150"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-md-4 grid-margin stretch-card">
      <div class="card">
        <div class="card-body py-5">
          <div class="d-flex flex-row justify-content-center align-items">
            <i class="mdi mdi-facebook text-facebook icon-lg"></i>
            <div class="ml-3">
              <h6 class="text-facebook font-weight-semibold mb-0">2.62 Subscribers</h6>
              <p class="text-muted card-text">You main list growing</p>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-md-4 grid-margin stretch-card">
      <div class="card">
        <div class="card-body py-5">
          <div class="d-flex flex-row justify-content-center align-items">
            <i class="mdi mdi-google-plus text-google icon-lg"></i>
            <div class="ml-3">
              <h6 class="text-google font-weight-semibold mb-0">3.4k Followers</h6>
              <p class="text-muted card-text">You main list growing</p>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- <div class="col-md-4 grid-margin stretch-card">
      <div class="card">
        <div class="card-body py-5">
          <div class="d-flex flex-row justify-content-center align-items">
            <i class="mdi mdi-twitter text-twitter icon-lg"></i>
            <div class="ml-3">
              <h6 class="text-twitter font-weight-semibold mb-0">3k followers</h6>
              <p class="text-muted card-text">You main list growing</p>
            </div>
          </div>
        </div>
      </div>
    </div> -->
  </div>
  <div class="row d-none">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Appointment</h4>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> # </th>
                  <th> First name </th>
                  <th> Progress </th>
                  <th> Amount </th>
                  <!-- <th> Sales </th> -->
                  <th> Deadline </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="font-weight-medium"> 1 </td>
                  <td> Herman Beck </td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <!-- <td class="text-danger"> 53.64% <i class="mdi mdi-arrow-down"></i>
                  </td> -->
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td class="font-weight-medium"> 2 </td>
                  <td> Messsy Adam </td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                  <td> $245.30 </td>
                  <!-- <td class="text-success"> 24.56% <i class="mdi mdi-arrow-up"></i>
                  </td> -->
                  <td> July 1, 2015 </td>
                </tr>
                <tr>
                  <td class="font-weight-medium"> 3 </td>
                  <td> John Richards </td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                  <td> $138.00 </td>
                  <!-- <td class="text-danger"> 28.76% <i class="mdi mdi-arrow-down"></i>
                  </td> -->
                  <td> Apr 12, 2015 </td>
                </tr>
                <tr>
                  <td class="font-weight-medium"> 4 </td>
                  <td> Peter Meggik </td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                  <td> $ 77.99 </td>
                  <!-- <td class="text-danger"> 53.45% <i class="mdi mdi-arrow-down"></i>
                  </td> -->
                  <td> May 15, 2015 </td>
                </tr>
                <tr>
                  <td class="font-weight-medium"> 5 </td>
                  <td> Edward </td>
                  <td>
                    <div class="progress">
                      <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </td>
                  <td> $ 160.25 </td>
                  <!-- <td class="text-success"> 18.32% <i class="mdi mdi-arrow-up"></i>
                  </td> -->
                  <td> May 03, 2015 </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row d-none">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title mb-4">Manage Tickets</h5>
          <div class="fluid-container">
            <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
              <div class="col-md-1">
                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face1.jpg') }}" alt="profile image"> </div>
              <div class="ticket-details col-md-9">
                <div class="d-flex">
                  <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">James :</p>
                  <p class="text-primary mr-1 mb-0">[#23047]</p>
                  <p class="mb-0 ellipsis">Donec rutrum congue leo eget malesuada.</p>
                </div>
                <p class="text-gray ellipsis mb-2">Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim vivamus. </p>
                <div class="row text-gray d-md-flex d-none">
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted text-muted">Last responded :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted text-muted">3 hours ago</small>
                  </div>
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted text-muted">Due in :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted text-muted">2 Days</small>
                  </div>
                </div>
              </div>
              <div class="ticket-actions col-md-2">
                <div class="btn-group dropdown">
                  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-reply fa-fw"></i>Quick reply</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-history fa-fw"></i>Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
              <div class="col-md-1">
                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face2.jpg') }}" alt="profile image"> </div>
              <div class="ticket-details col-md-9">
                <div class="d-flex">
                  <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">Stella :</p>
                  <p class="text-primary mr-1 mb-0">[#23135]</p>
                  <p class="mb-0 ellipsis">Curabitur aliquet quam id dui posuere blandit.</p>
                </div>
                <p class="text-gray ellipsis mb-2">Pellentesque in ipsum id orci porta dapibus. Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl. </p>
                <div class="row text-gray d-md-flex d-none">
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted">Last responded :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted">3 hours ago</small>
                  </div>
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted">Due in :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted">2 Days</small>
                  </div>
                </div>
              </div>
              <div class="ticket-actions col-md-2">
                <div class="btn-group dropdown">
                  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-reply fa-fw"></i>Quick reply</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-history fa-fw"></i>Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="row ticket-card mt-3">
              <div class="col-md-1">
                <img class="img-sm rounded-circle mb-4 mb-md-0 d-block mx-md-auto" src="{{ url('assets/images/faces/face3.jpg') }}" alt="profile image"> </div>
              <div class="ticket-details col-md-9">
                <div class="d-flex">
                  <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">John Doe :</p>
                  <p class="text-primary mr-1 mb-0">[#23246]</p>
                  <p class="mb-0 ellipsis">Mauris blandit aliquet elit, eget tincidunt nibh pulvinar.</p>
                </div>
                <p class="text-gray ellipsis mb-2">Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Lorem ipsum dolor sit amet.</p>
                <div class="row text-gray d-md-flex d-none">
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted">Last responded :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted">3 hours ago</small>
                  </div>
                  <div class="col-4 d-flex">
                    <small class="mb-0 mr-2 text-muted">Due in :</small>
                    <small class="Last-responded mr-2 mb-0 text-muted">2 Days</small>
                  </div>
                </div>
              </div>
              <div class="ticket-actions col-md-2">
                <div class="btn-group dropdown">
                  <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Manage </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-reply fa-fw"></i>Quick reply</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-history fa-fw"></i>Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>
                    <a class="dropdown-item" href="#">
                      <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
@endsection
@section('scripts')
@parent

@endsection