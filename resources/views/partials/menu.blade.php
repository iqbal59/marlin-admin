<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route("admin.home") }}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        @can('user_management_access')
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-account-outline menu-icon"></i>
                    <span class="menu-title">Access</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    @can('user_access')
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}" 
                            href="{{ route("admin.users.index") }}">
                                User Management
                        </a>
                    </li>
                    @endcan
                    @can('role_access')
                    <li class="nav-item"> 
                        <a class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}" 
                            href="{{ route("admin.roles.index") }}">
                                Role Management
                        </a>
                    </li>
                    @endcan
                    @can('permission_access')
                    <li class="nav-item"> 
                        <a class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}" 
                            href="{{ route("admin.permissions.index") }}">
                                Permission Management
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>
        @endcan
    </ul>
</nav>


