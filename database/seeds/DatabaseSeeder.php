<?php

use Illuminate\Database\Seeder;
use Modules\Country\Database\Seeders\CountryDatabaseSeeder;
use Modules\Department\Database\Seeders\DepartmentDatabaseSeeder;
use Modules\Doctor\Database\Seeders\DoctorDatabaseSeeder;
use Modules\Treatment\Database\Seeders\TreatmentDatabaseSeeder;
use Modules\Facilities\Database\Seeders\FacilitiesDatabaseSeeder;
use Modules\Hospital\Database\Seeders\HospitalDatabaseSeeder;
use Modules\Cms\Database\Seeders\CmsDatabaseSeeder;
use Modules\Blogs\Database\Seeders\BlogsDatabaseSeeder;
use Modules\Banner\Database\Seeders\BannerDatabaseSeeder;
use Modules\Enquiry\Database\Seeders\EnquiryDatabaseSeeder;
use Modules\Settings\Database\Seeders\SettingsDatabaseSeeder;
use Modules\Faq\Database\Seeders\FaqDatabaseSeeder;
use Modules\Diseases\Database\Seeders\DiseasesDatabaseSeeder;
use Modules\TreatmentPlants\Database\Seeders\TreatmentPlantsDatabaseSeeder;
use Modules\MedicineBrands\Database\Seeders\MedicineBrandsDatabaseSeeder;
use Modules\Medicines\Database\Seeders\MedicinesDatabaseSeeder;
use Modules\Videos\Database\Seeders\VideosDatabaseSeeder;
use Modules\Gallery\Database\Seeders\GalleryDatabaseSeeder;
use Modules\Services\Database\Seeders\ServicesDatabaseSeeder;
use Modules\Problems\Database\Seeders\ProblemsDatabaseSeeder;
use Modules\Branch\Database\Seeders\BranchDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            CountryDatabaseSeeder::class,
            DepartmentDatabaseSeeder::class,
            DoctorDatabaseSeeder::class,
            TreatmentDatabaseSeeder::class,
            FacilitiesDatabaseSeeder::class,
            HospitalDatabaseSeeder::class,
            CmsDatabaseSeeder::class,
            BannerDatabaseSeeder::class,
            EnquiryDatabaseSeeder::class,
            SettingsDatabaseSeeder::class,
            FaqDatabaseSeeder::class,
            DiseasesDatabaseSeeder::class,
            TreatmentPlantsDatabaseSeeder::class,
            MedicineBrandsDatabaseSeeder::class,
            MedicinesDatabaseSeeder::class,
            VideosDatabaseSeeder::class,
            GalleryDatabaseSeeder::class,
            ServicesDatabaseSeeder::class,
            ProblemsDatabaseSeeder::class,
            BlogsDatabaseSeeder::class,
            BranchDatabaseSeeder::class,
        ]);
    }
}
